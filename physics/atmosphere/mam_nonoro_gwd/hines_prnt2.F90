!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_prnt2 (m_alpha,sigma_alpha,vel_u,vel_v, &
                        ubot,vbot,alt,dragil,v_alpha, &
                        nmessg,ilprt1,ilprt2,levprt1,levprt2, &
                        naz,nlons,nlevs,nazmth,work)
  !
  !  print out altitude profiles of cutoff wavenumbers, rms winds and
  !  background winds at each horizontal azimuth for the hines doppler spread
  !  gravity wave parameterization scheme.
  !
  !  feb. 2/96 - c. mclandress
  !

  implicit none
  real :: zkm

  !  input arguements:
  !  -----------------
  !
  !     * m_alpha      = cutoff wavenumber at each azimuth (1/m).
  !     * sigma_alpha  = total rms wind in each azimuth (m/s).
  !     * vel_u    = background zonal wind component (m/s).
  !     * vel_v    = background meridional wind component (m/s).
  !     * ubot     = background zonal wind component at bottom level.
  !     * vbot     = background meridional wind component at bottom level.
  !     * alt      = altitude (m).
  !     * dragil   = logical :: flag indicating longitudes and levels where
  !     *            calculations to be performed.
  !     * nmessg   = unit number for printed output.
  !     * ilprt1   = first longitudinal index to print.
  !     * ilprt2   = last longitudinal index to print.
  !     * levprt1  = first altitude level to print.
  !     * levprt2  = last altitude level to print.
  !     * naz      = actual number of horizontal azimuths used.
  !     * nlons    = number of longitudes.
  !     * nlevs    = number of vertical levels.
  !     * nazmth   = azimuthal array dimension (nazmth >= naz).
  !
  !  input work arrays:
  !  ------------------
  !
  !     * v_alpha  = wind component at each azimuth (m/s).
  !
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: ilprt1 !< Variable description\f$[units]\f$
  integer, intent(in)  :: ilprt2 !< Variable description\f$[units]\f$
  integer, intent(in)  :: levprt1   !< Variable description\f$[units]\f$
  integer, intent(in)  :: levprt2   !< Variable description\f$[units]\f$
  integer, intent(in)  :: nmessg !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlevs !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  ! ccc      logical :: dragil(nlons,nlevs)
  integer, intent(in), dimension(nlons,nlevs) :: dragil !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: m_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: sigma_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: alt !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: ubot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: vbot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: v_alpha !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! internal variables.
  ! -------------------
  !
  integer  :: ibig
  integer  :: i
  integer  :: l
  integer  :: n
  integer  :: naz1
  ! ccc      parameter  ( ibig = 50)
  ! ccc      real  :: zkm, work(ibig)
  real, intent(inout), dimension(nazmth) :: work
  !-----------------------------------------------------------------------

  naz1 = naz
  if (naz > 12)  naz1 = 12
  !
  !  print out values for range of longitudes.
  !
  do i = ilprt1,ilprt2
    !
    ! print cutoff wavenumber at all azimuths.
    !
    write (nmessg, * )
    write (nmessg,6001) i
    write (nmessg, * )
6001 format ('Cutoff wavenumber (X 1.E3) at longitude I = ',i3)
    do l = levprt1,levprt2
      zkm = alt(i,l)/1.e3
      do n = 1,naz1
        work(n) = m_alpha(i,l,n) * 1.e3
      end do ! loop 5
      write (nmessg,6100) zkm, (work(n),n = 1,naz1)
    end do ! loop 10
    if (naz > 12) then
      do l = levprt1,levprt2
        zkm = alt(i,l)/1.e3
        do n = 13,naz
          work(n) = m_alpha(i,l,n) * 1.e3
        end do ! loop 6
        write (nmessg,6100) zkm, (work(n),n = 13,naz)
      end do ! loop 11
    end if
    write (nmessg, * )
6100 format (f5.1,'km',12f6.2)
    !
    ! print rms wind at all azimuths.
    !
    write (nmessg, * )
    write (nmessg,6002) i
    write (nmessg, * )
6002 format ('RMS wind (m/s) at longitude I = ',i3)
    do l = levprt1,levprt2
      zkm = alt(i,l)/1.e3
      write (nmessg,6110) zkm, (sigma_alpha(i,l,n),n = 1,naz1)
    end do ! loop 20
    if (naz > 12) then
      do l = levprt1,levprt2
        zkm = alt(i,l)/1.e3
        write (nmessg,6110) zkm, (sigma_alpha(i,l,n),n = 13,naz)
      end do ! loop 21
    end if
    write (nmessg, * )
6110 format (f5.1,'km',12f6.1)
    !
    ! print background wind at all azimuths.
    !
    write (nmessg, * )
    write (nmessg,6003) i
    write (nmessg, * )
6003 format ('Background wind (m/s) at longitude I = ',i3)
    do l = levprt1,levprt2
      zkm = alt(i,l)/1.e3
      call hines_wind (v_alpha, &
                       vel_u(1,l), vel_v(1,l), ubot, vbot, &
                       dragil(1,l), naz, i, i, nlons, nazmth)
      write (nmessg,6110) zkm, (v_alpha(i,n),n = 1,naz1)
    end do ! loop 30
    if (naz > 12) then
      do l = levprt1,levprt2
        zkm = alt(i,l)/1.e3
        call hines_wind (v_alpha, &
                         vel_u(1,l), vel_v(1,l), ubot, vbot, &
                         dragil(1,l), naz, i, i, nlons, nazmth)
        write (nmessg,6110) zkm, (v_alpha(i,n),n = 13,naz)
      end do ! loop 31
    end if
    !
  end do ! loop 100
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
