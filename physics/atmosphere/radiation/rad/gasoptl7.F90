!> \file
!>\brief Compute the gas optical thickness for thermal wavelengths
!!
!! @author Jiangnan Li
!
subroutine gasoptl7(taug, gw, dp, ib, ig, o3, q, co2, ch4, an2o, &
                    f11, f12, f113, f114, inptr, inpt, mcont, &
                    dir, dip, dt, lev1, gh, il1, il2, ilg, lay)
  !
  !     * feb 02/2018 - j.li. corrects the water vapour continuum in band 6
  !     *                     which was not accurate for much warmer temps.
  !     * apr 21/2010 - j.li.     new version for gcm15i:
  !     *                         - revision of many bands for greater
  !     *                           accuracy.
  !     * feb 09,2009 - j.li.     previous version gasoptl6 for gcm15h:
  !     *                         - 3d ghg implemented, thus no need
  !     *                           for "trace" common block or
  !     *                           temporary work arrays to hold
  !     *                           mixing ratios of ghg depending on
  !     *                           a passed, specified option.
  !     *                         - calls tline{1,2,3}z instead of
  !     *                           tline{1,2,3}y. also calls tlinehc4,
  !     *                           tconthl4 instead of tlinehc3,tconthl3.
  !     * apr 18,2008 - m.lazare/ previous version gasoptl5 for gcm15g:
  !     *               l.solheim/- cosmetic change to add threadprivate
  !     *               j.li.       for common block "trace", in support
  !     *                         - calls tline{1,2,3}y instead of
  !     *                           tline{1,2,3}x. also calls tlinehc3,
  !     *                           tconthl3 instead of tlinehc2,tconthl2.
  !     * jun 21,2006 - j.li.     previous version gasoptl4 for gcm15f:
  !     *                         - update the minor gas absorption.
  !     *                         - calls new tconthl2.
  !     * may 05,2006 - m.lazare. previous version gasoptl3 for gcm15e:
  !     *                         - pass integer :: variables "init" and
  !     *                           "mit" instead of actual integer
  !     *                           values, to "tline_" routines.
  !     * dec 07/2004 - j.li.     previous version gasoptl2 for gcm15d.
  !----------------------------------------------------------------------c
  !     calculation of the optical depths due to nongray gaseous         c
  !     absorption for the infrared, in each layer for a given band ib   c
  !     and cumulative probability gw.                                   c
  !     from band1 to band4, the solar and infrared interaction is       c
  !     considered. the total solar energy considered in the infrared    c
  !     region is 11.9006 w / m2                                        c
  !     tline, etc., deal with line absorption and tcontl and tconthl    c
  !     deal with water vapor continuum                                  c
  !                                                                      c
  !     taug:  gaseous optical depth                                     c
  !     dp:    air mass path for a model layer (exlained in raddriv).    c
  !     o3:    o3 mass mixing ratio                                      c
  !     q:     water vapor mass mixing ratio                             c
  !     an2o:  n2o, also co2, ch4, f11, f12, f113, f114 are mass mixing  c
  !            ratios                                                    c
  !     dir:   interpretation factor for mass ratio of h2o / co2         c
  !            between two neighboring standard input ratios             c
  !     dip:   interpretation factor for pressure between two            c
  !            neighboring standard input data pressure levels           c
  !     dt:    layer temperature - 250 k                                 c
  !     inpr:  number of the ratio level for the standard 5 ratios       c
  !     inpt:  number of the level for the standard input data pressures c
  !     mcont: the highest level for water vapor continuum calculation   c
  !----------------------------------------------------------------------c
  implicit none
  real :: can2o
  real, dimension(5,18) :: cl1co2
  real, dimension(5,18) :: cl1h2o
  real, dimension(5,3) :: cl2cf
  real, dimension(5,3) :: cl2cs
  real, dimension(5,18) :: cl2h2o
  real, dimension(5,4,2) :: cl3cf
  real, dimension(2) :: cl3ch4
  real, dimension(5,4,2) :: cl3cs
  real, dimension(5,18,2) :: cl3h2o
  real, dimension(5,4,5) :: cl4cf
  real, dimension(5,18,5) :: cl4ch4
  real, dimension(5,4,5) :: cl4cs
  real, dimension(5) :: cl4f12
  real, dimension(5,18,5) :: cl4h2o
  real, dimension(5,18,5) :: cl4n2o
  real, dimension(5,4,2) :: cl5cf
  real, dimension(5,4,2) :: cl5cs
  real, dimension(2) :: cl5f11
  real, dimension(2) :: cl5f12
  real, dimension(5,18,2) :: cl5h2o
  real, dimension(5,18,2) :: cl5o3
  real, dimension(5,4,3) :: cl6cf
  real, dimension(5,4,3) :: cl6cs
  real, dimension(5,18,3) :: cl6f11
  real, dimension(5,18,3) :: cl6f12
  real, dimension(5,18,3) :: cl6h2o
  real, dimension(5,5,4,3) :: cl7cf
  real, dimension(5,5,7,3) :: cl7co2d
  real, dimension(5,11,3) :: cl7co2u
  real, dimension(5,5,4,3) :: cl7cs
  real, dimension(5,5,7,3) :: cl7h2od
  real, dimension(5,11,3) :: cl7h2ou
  real, dimension(3) :: cl7n2o
  real, dimension(3) :: cl7o3
  real, dimension(5,6,4) :: cl8cf
  real, dimension(5,6,4) :: cl8cs
  real, dimension(5,18,6) :: cl8h2o
  real, dimension(5,6,4) :: cl9cf
  real, dimension(5,6,4) :: cl9cs
  real, dimension(5,18,4) :: cl9h2o
  real :: crmco2
  real :: fact
  real, intent(inout) :: gw
  real, dimension(1) :: gw1
  real, dimension(1) :: gw2
  real, dimension(2) :: gw3
  real, dimension(5) :: gw4
  real, dimension(2) :: gw5
  real, dimension(3) :: gw6
  real, dimension(3) :: gw7
  real, dimension(6) :: gw8
  real, dimension(4) :: gw9
  integer :: i
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: init
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: lc
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer, intent(in) :: mcont!< Highest model level for water vapor continuum calculation \f$[1]\f$
  integer :: mtl
  !
  real, intent(inout), dimension(ilg,lay) :: taug !< Gas optical thickness \f$[1]\f$
  !
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path for layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg,lay) :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: q !< H2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: co2 !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ch4 !< CH4 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: an2o !< N2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: f11 !< CFC114 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: f12 !< CFC12 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: f113 !< CFC113 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: f114 !< CFC114 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: dir !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: dip !< Interpretation between two neighboring standard input pressure levels \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< Layer temperature - 250 K \f$[K]\f$
  integer, intent(in), dimension(ilg,lay) :: inptr !< Number of the selected standard input H2O/CO2 ratio \f$[1]\f$
  integer, intent(in), dimension(ilg,lay) :: inpt !< Level number of the standard input pressures \f$[1]\f$
  logical, intent(in) :: gh !< If is true, use large gaseous optical thickness group for calculations \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  common /bandl1/  gw1,  cl1h2o, cl1co2
  common /bandl2/  gw2,  cl2h2o, cl2cs, cl2cf
  common /bandl3/  gw3,  cl3h2o, cl3ch4, cl3cs, cl3cf
  common /bandl4/  gw4,  cl4h2o, cl4n2o, cl4ch4, cl4cs, cl4cf, cl4f12
  common /bandl5/  gw5,  cl5h2o, cl5o3, cl5cs, cl5cf, cl5f11, cl5f12
  common /bandl6/  gw6,  cl6h2o, cl6f11, cl6f12, cl6cs, cl6cf
  common /bandl7/  gw7,  cl7h2ou, cl7h2od, cl7co2u, cl7co2d, cl7n2o, cl7o3, cl7cs, cl7cf
  common /bandl8/  gw8,  cl8h2o, cl8cs, cl8cf
  common /bandl9/  gw9,  cl9h2o, cl9cs, cl9cf
  !
  !     * number of vertical levels in absorber pressure-based coefficient
  !     * array ("m" references non-saturated bands active below 1 mb only).
  !
  data mtl /18/
  !=======================================================================
  if (ib == 1) then
    !
    !----------------------------------------------------------------------c
    !     band (2500 - 2200 cm-1), nongray gaseous absorption of h2o and  c
    !     co2.                                                             c
    !----------------------------------------------------------------------c
    !
    call tline2z(taug, cl1h2o, cl1co2, q, co2, dp, dip, dt, inpt, &
                 lev1, gh, mtl, il1, il2, ilg, lay)
    !
    !----------------------------------------------------------------------c
    !     simply add the n2o effect                                        c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        can2o     =  2.0437 * 1.e-07 * sqrt(an2o(i,k) * 1.e+07)
        fact      =  q(i,k) / (q(i,k) + 8.e+04 * can2o)
        taug(i,k) =  taug(i,k) + (754.9786 + 10141.5049 * fact * fact) * &
                    can2o * dp(i,k)
      end do
    end do ! loop 100
    !
    gw =  gw1(ig)
    !
  else if (ib == 2) then
    !
    !----------------------------------------------------------------------c
    !     band (2200 - 1900 cm-1), nongray gaseous absorption of h2o + n2oc
    !----------------------------------------------------------------------c
    !
    init = 2
    call tline1z(taug, cl2h2o, q, dp, dip, dt, inpt, lev1, gh, &
                 mtl, init, il1, il2, ilg, lay)
    !
    lc =  3
    call tcontl1(taug, cl2cs, cl2cf, q, dp, dip, dt, lc, inpt, mcont, &
                 gh, il1, il2, ilg, lay)
    !
    !----------------------------------------------------------------------c
    !     simply add the n2o effect                                        c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        can2o     =  2.0437 * 1.e-07 * sqrt(an2o(i,k) * 1.e+07)
        fact      =  q(i,k) / (q(i,k) + 72000. * can2o)
        taug(i,k) =  taug(i,k) + (93. + 3500. * fact * fact) * &
                    can2o * dp(i,k)
      end do
    end do ! loop 200
    !
    gw =  gw2(ig)
    !
  else if (ib == 3) then
    !
    !----------------------------------------------------------------------c
    !     band (1900 - 1400 cm-1), nongray gaseous absorption of h2o.     c
    !----------------------------------------------------------------------c
    !
    init = 2
    call tline1z(taug, cl3h2o(1,1,ig), q, dp, dip, dt, inpt, lev1, &
                 gh, mtl, init, il1, il2, ilg, lay)
    !
    lc =  4
    call tcontl1(taug, cl3cs(1,1,ig), cl3cf(1,1,ig), q, dp, dip, dt, &
                 lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    do k = lev1, lay
      do i = il1, il2
        taug(i,k) =  taug(i,k) + cl3ch4(ig) * ch4(i,k) * dp(i,k)
      end do
    end do ! loop 300
    !
    gw =  gw3(ig)
    !
  else if (ib == 4) then
    !
    !----------------------------------------------------------------------c
    !     band3 (1100 - 1400 cm-1), overlapping absorption of h2o, n2o,   c
    !     ch4 and cfc12. direct mapping method for h2o and ch4 and n2o     c
    !     cfc are considered as minor gases                                c
    !----------------------------------------------------------------------c
    !
    call tline3z(taug, cl4h2o(1,1,ig), cl4ch4(1,1,ig), cl4n2o(1,1,ig), &
                 q, ch4, an2o, dp, dip, dt, inpt, lev1, gh, mtl, &
                 il1, il2, ilg, lay)
    !
    lc =  4
    call tcontl1(taug, cl4cs(1,1,ig), cl4cf(1,1,ig), q, dp, dip, dt, &
                 lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    !----------------------------------------------------------------------c
    !     simply add the cfc effect                                        c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        taug(i,k) =  taug(i,k) + (cl4f12(ig) * f12(i,k) + &
                    1037.3 * f113(i,k) + 1426.9 * f114(i,k)) * &
                    dp(i,k)
      end do
    end do ! loop 400
    !
    gw =  gw4(ig)
    !
  else if (ib == 5) then
    !
    !----------------------------------------------------------------------c
    !     band5 (980 - 1100 cm-1), overlapping absorption of h2o and o3   c
    !     direct mapping method. co2 and cfc are simply added              c
    !----------------------------------------------------------------------c
    !
    call tline2z(taug, cl5h2o(1,1,ig), cl5o3(1,1,ig), q, o3, dp, &
                 dip, dt, inpt, lev1, gh, mtl, &
                 il1, il2, ilg, lay)
    !
    lc =  4
    call tcontl1(taug, cl5cs(1,1,ig), cl5cf(1,1,ig), q, dp, dip, dt, &
                 lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    !----------------------------------------------------------------------c
    !     simply add the co2 + cfc effect                                  c
    !     since the interaction of co2 and h2o, q(i,k) appears in co2      c
    !     effect                                                           c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        crmco2    =  2.3056e-04 * sqrt(co2(i,k) * 1.e+04)
        taug(i,k) =  taug(i,k) + ( (0.009 +  0.093 * q(i,k) / (q(i,k) + &
                    2.1 * crmco2)) * co2(i,k) + cl5f11(ig) * f11(i,k) + &
                    cl5f12(ig) * f12(i,k) + 1687.4 * f113(i,k) + &
                    2924.1 * f114(i,k)) * dp(i,k)
      end do
    end do ! loop 500
    !
    gw =  gw5(ig)
    !
  else if (ib == 6) then
    !
    !----------------------------------------------------------------------c
    !     band (800 - 980 cm-1), nongray gaseous absorption of h2o.       c
    !     + cfc11 and cfc12                                                c
    !----------------------------------------------------------------------c
    !
    call tline3z(taug, cl6h2o(1,1,ig), cl6f11(1,1,ig), &
                 cl6f12(1,1,ig), q, f11, f12, dp, dip, dt, inpt, &
                 lev1, gh, mtl, il1, il2, ilg, lay)
    !
    !----------------------------------------------------------------------c
    !     simply add the co2 + cfc effect                                  c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        taug(i,k) =  taug(i,k) + ( (0.0074 + 0.0396 * q(i,k) / &
                    (q(i,k) + 2.8 * co2(i,k))) * co2(i,k) + &
                    1191. * f113(i,k) + 1098. * f114(i,k) ) * dp(i,k)
      end do
    end do ! loop 600
    !
    lc =  4
    call tcontl1 (taug, cl6cs(1,1,ig), cl6cf(1,1,ig), q, dp, dip, &
                  dt, lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    gw =  gw6(ig)
    !
  else if (ib == 7) then
    !
    !----------------------------------------------------------------------c
    !     band6 (540 - 800 cm-1), overlapping absorption of h2o and co2   c
    !     exact mapping method for h2o and co2, direct mapping for n2o     c
    !     o3 effect is simply added                                        c
    !----------------------------------------------------------------------c
    !
    call tlinehc4(taug, cl7h2ou(1,1,ig), cl7h2od(1,1,1,ig), &
                  cl7co2u(1,1,ig), cl7co2d(1,1,1,ig), q, co2, dp, dip, &
                  dir, dt, inptr, inpt, lev1, il1, il2, ilg, lay)
    !
    call tconthl4(taug, cl7cs(1,1,1,ig), cl7cf(1,1,1,ig), q, dp, dip, &
                  dir, dt, inptr, inpt, mcont, il1, il2, ilg, lay)
    !
    !----------------------------------------------------------------------c
    !     simply add the o3 and n2o effect                                 c
    !----------------------------------------------------------------------c
    !
    do k = lev1, lay
      do i = il1, il2
        can2o     =  2.0437 * 1.e-07 * sqrt(an2o(i,k) * 1.e+07)
        taug(i,k) =  taug(i,k) + (cl7o3(ig) * o3(i,k) + &
                    cl7n2o(ig) * can2o) * dp(i,k)
      end do
    end do ! loop 700
    !
    gw =  gw7(ig)
    !
  else if (ib == 8) then
    !
    !----------------------------------------------------------------------c
    !     band (340 - 540 cm-1), nongray gaseous absorption of h2o.       c
    !----------------------------------------------------------------------c
    !
    init = 2
    call tline1z(taug, cl8h2o(1,1,ig), q, dp, dip, dt, inpt, lev1, &
                 gh, mtl, init, il1, il2, ilg, lay)
    !
    if (ig <= 4) then
      lc =  6
      call tcontl1(taug, cl8cs(1,1,ig), cl8cf(1,1,ig), q, dp, dip, dt, &
                   lc, inpt, mcont, gh, il1, il2, ilg, lay)
    end if
    !
    gw =  gw8(ig)
    !
  else if (ib == 9) then
    !
    !----------------------------------------------------------------------c
    !     band (0 - 340 cm-1), nongray gaseous absorption of h2o.         c
    !----------------------------------------------------------------------c
    !
    init = 2
    call tline1z(taug, cl9h2o(1,1,ig), q, dp, dip, dt, inpt, lev1, &
                 gh, mtl, init, il1, il2, ilg, lay)
    !
    lc =  6
    call tcontl1(taug, cl9cs(1,1,ig), cl9cf(1,1,ig), q, dp, dip, dt, &
                 lc, inpt, mcont, gh, il1, il2, ilg, lay)
    !
    gw =  gw9(ig)
    !
  end if
  !
  return
end
!> \file
!> Calculation of the optical depths due to nongray gaseous 
!! absorption for the infrared, in each layer for a given band ib
!! and cumulative probability gw.                               
!! from band1 to band4, the solar and infrared interaction is  
!! considered. the total solar energy considered in the infrared 
!! region is 11.9006 w / m2                                     
!! tline, etc., deal with line absorption and tcontl and tconthl 
!! deal with water vapor continuum.