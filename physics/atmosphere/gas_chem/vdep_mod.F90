!>\file
!>\brief Purpose: Methane chemistry
!!
!! @author David Plummer
!
module vdep_mod

implicit none ; private

public :: vdep2

contains

  subroutine vdep2 (vdep, vdsf, vdra, vdrc, ta, vmod,                &
                    gc, vdlai,  vdlais, fc, fg, fcs, fgs, frainc, fsnowc,  &
                    nvdland, ilg, il1, il2, nvdep)

! ------------------------------------------------------------------------------
! * July 19 2021 - B. Winter  - adjusted for CanAM5 with F90 syntax
! * Apr 15, 2019 - D. Plummer - second version to work with CLASS3.6 as 
! *                             installed in GCM18
! * Aug 29, 2000 - D. Plummer - first version developed for methane chemistry
!
! * Outputs:
! *   vdep - the grid-square-average dry deposition velocity for NVDEP different
! *          chemical species
! *   vdsf - ozone deposition velocity for uptake through leaf stomata
!
! * Called by sfcproc2.F90
! ------------------------------------------------------------------------------

  implicit none

  real, dimension(ilg,nvdep), intent(inout) :: vdep  !< dry deposition velocity
  real, dimension(ilg), intent(inout) :: vdsf  !< ozone deposition velocity

! * inputs
  integer, intent(in) :: nvdland, il1, il2, ilg, nvdep
  real, dimension(ilg,4), intent(in) :: vdra, vdrc
  real, dimension(ilg), intent(in) :: fc, fcs, fg, fgs, frainc, fsnowc
  real, dimension(ilg), intent(in) :: gc, ta, vdlai, vdlais, vmod

  ! Local variables and parameters:
  real, dimension(nvdep) :: difv  !< Diffusivity of deposited species relative to the diffusivity of water vapour [D(H2O)/D(i)]
  real, dimension(nvdep) :: alpha !< Inverse of the enhancement of solubility of the gas in water
  real, dimension(nvdep) :: rmeso !< Mesophyll resistance (sec/m)
  real, dimension(nvdep) :: rsnow !< Resistance to deposition to snow (sec/m) 
  real, dimension(nvdep) :: react !< Reactivity of species relative to SO2 [R(i)/R(SO2)]
  real, dimension(nvdep) :: wmole !< Molecular weight of deposited species
  real, dimension(nvdep) :: wmsqr !< square root of wmole 

! * Surface resistance of SO2 to deposition:
  real, parameter :: rcut0 = 3000.0  !< on leaf cuticle surfaces
  real, parameter :: rgnd0 = 1000.0  !< on bare ground surfaces

  real, dimension(nvdep) :: hnrya      !< Constant for Henry's Law calculation
  real, dimension(nvdep) :: hnryb      !< Constant for Henry's Law calculation
  real, dimension(ilg,nvdep) :: hnryc  !< auxiliary array for Henry's Law
  real (kind=8) :: hnry_fac  !< auxiliary factor for Henry (= 1.0/22.5)

! * Other local variables
  integer :: i, j
  real :: prcnst


! The following were variables Dave used for modified stomatal resistant, for
! the version of CLASS (2.7) that was in gcm13e. They were used for tests, and
! remain here as a reminder that further tests may be needed.
! * Minimum stomatal resistance for the four vegetation types in CLASS : 
!   1) Needleleaf 2) Broadleaf  3) Crops  4) Grass
!  real, dimension(ican), parameter :: stomin = (/ 200.0, 200.0, 80.0, 125.0 /)
!  real, dimension(ilg) :: vdstom, vdstoms
! ------------------------------------------------------------------------------

! Assign some variables. This could all go into the gas tracer module.

! * Data constants for the gas-phase chemistry dry deposition calculation.
!
! * Variable RSNOW is from: Wesely Atmos. Enviro., 23, 1293-1304, 1989
! *                    and: Brook et al. Atmos. Enviro., 33, 5037-5051, 1999.
! * The parameterization for deposition to snow replaces the ADOM one 
! * which seemed to have significant problems for O3 over snow surfaces.
!
! * Species order:   O3     NO2    HNO3    H2O2    HCHO   CH3OOH
  difv  =         (/1.6,    1.6,    1.9,    1.4,    1.3,    1.6 /)
  alpha =         (/0.1,    1.0,    1.0,    1.0,    1.0,    1.0 /)
  rmeso =         (/0.0,  500.0,    0.0,    0.0,    0.0,    0.0 /)
  rsnow =      (/3000.0,30000.0,   20.0,  180.0, 3300.0,22000.0 /)
  react =         (/1.875,   1.0,  30.0,   30.0,    0.5,    1.5 /)
  wmole =        (/48.0,   46.0,   63.0,   34.0,   30.0,   48.0 /)
  hnrya =        (/-5.16,  -1.90,  -6.17,  -5.15,  -5.57,   3.95 /)
  hnryb =         (/1.04,   0.00,   3.78,   2.99,   2.81,   0.00 /)

  do i = 1, nvdep
    wmsqr(i) = sqrt(wmole(i))
  enddo
  !
  ! * Calculate Henry's law constants, including the enhancement to
  ! * solubility factor ALPHA and the square root of the molecular weight WMSQR

  hnry_fac = dble(1.0 / 22.5)
  do j = 1, nvdep
    do i = il1, il2
      hnryc(i,j) = hnry_fac * wmsqr(j) * alpha(j)  &
                 * 10**(-(hnrya(j)+hnryb(j)*1000.0/ta(i)))
    enddo
  enddo
  !
  ! * Deposition over land
  !
  if (nvdland == 1) then
    !
    ! * Surface resistance has an imposed minimum value of 10 s/m to
    ! * avoid unrealistically large deposition velocities for species
    ! * like HNO3 (as per Wesely, 1989)
    !
    ! * Canopy over snow: as with CLASS, if canopy has some snow cover,
    ! * then there is no transfer through the stomata.
    do j = 1, nvdep
      do i = il1, il2

        if (fcs(i) > 0.0) then
          if (fsnowc(i) > 0.0) then
            vdep(i,j) = vdep(i,j) + fcs(i)/(vdra(i,1)   &
                      + max(10.0, 1.0 / ( react(j)*vdlais(i)*  &
                        (1.0-frainc(i)-fsnowc(i)) / rcut0 &
                        + vdlais(i)*frainc(i) / (6250.0*hnryc(i,j))  &
                        + vdlais(i)*fsnowc(i)/rsnow(j) + 1.0/rsnow(j)) ))
          else
            vdep(i,j) = vdep(i,j) + fcs(i)/(vdra(i,1)  &
                      + max(10.0, 1.0 / ( react(j)*vdlais(i)* &
                        (1.0-frainc(i)) / rcut0    &
                        + vdlais(i)*frainc(i) / (6250.0*hnryc(i,j))  &
                        + (1.0 - frainc(i)) / (difv(j)*vdrc(i,1)  &
                        + rmeso(j)/vdlais(i)) + 1.0/rsnow(j)) ))
          endif
        endif
      enddo
    enddo
    !
    ! * Diagnostic calculation of ozone deposition through stomata
    do i = il1, il2
      if (fcs(i) > 0.0 .and. fsnowc(i) < 1.0E-05) then
        if (frainc(i) < 1.0) then
          vdsf(i) = fcs(i) / (vdra(i,1) + max(10.0, 1.0 /  &
                  ( (1.0 - frainc(i)) / (difv(1)*vdrc(i,1) &
                  + rmeso(1)/vdlais(i))) ))
        endif
      endif
    enddo
    !
    ! * Snow-covered ground: just surface resistance of deposition to snow
    do i = il1, il2
      if (fgs(i) > 0.0) then
        do j = 1, nvdep
          vdep(i,j) = vdep(i,j) + fgs(i)/ (vdra(i,2) + rsnow(j))
        enddo
      endif
    enddo
    !
    ! * Canopy over bare ground: if there is snow on the canopy, 
    ! * no deposition through the stomata occurs
    do j = 1, nvdep
      do i = il1, il2
        if (fc(i) > 0.0) then
          if (fsnowc(i) > 0.0) then
            vdep(i,j) = vdep(i,j) + fc(i)/(vdra(i,3) + max(10.0, 1.0 /   &
                      ( react(j)*vdlai(i)*(1.0-frainc(i) - fsnowc(i))/rcut0 &
                      + vdlai(i)*frainc(i) / (6250.0*hnryc(i,j))         &
                      + vdlai(i)*fsnowc(i)/rsnow(j) + react(j)/rgnd0) ))
          else
            vdep(i,j) = vdep(i,j) + fc(i)/(vdra(i,3) + max(10.0, 1.0 /   &
                      ( react(j)*vdlai(i)*(1.0-frainc(i)) / rcut0        &
                      + vdlai(i)*frainc(i) / (6250.0*hnryc(i,j))         &
                      + (1.0-frainc(i)) / (difv(j)*vdrc(i,3)             &
                      + rmeso(j)/vdlai(i)) + react(j)/rgnd0) ))
          endif
        endif
      enddo
    enddo
    !
    ! * Diagnostic calculation of ozone deposition through stomata
    do i = il1, il2
      if (fc(i) > 0.0 .and. fsnowc(i) < 1.0e-05) then
        if (frainc(i) < 1.0) then
          vdsf(i) = vdsf(i) + fc(i)/(vdra(i,3) + max(10.0, 1.0 /    &
                  ( (1.0-frainc(i))/(difv(1)*vdrc(i,3) + rmeso(1)/vdlai(i))) ))
        endif
      endif
    enddo
    !
    ! * Bare ground
    do i = il1, il2
      if (fg(i) > 0.0) then
        do j = 1, nvdep
          vdep(i,j) = vdep(i,j) + fg(i)/ (vdra(i,4) + rgnd0/react(j))
        enddo
      endif
    enddo
    !
  endif  ! end conditional on nvdland==1
  !
  ! * Deposition over ocean
  !
  if (nvdland == 0) then
    do i = il1, il2
      if (gc(i) > -0.5 .and. gc(i) < 0.5) then  ! open water
        prcnst = 12500.0 / max(1.0, 0.2*vmod(i))
        do j = 1, nvdep
          vdep(i,j) = vdep(i,j) + 1.0/(vdra(i,1) + prcnst*hnryc(i,j))
        enddo
      elseif (gc(i) > 0.5) then   ! sea-ice
        do j = 1, nvdep
          vdep(i,j) = vdep(i,j) + 1.0/(vdra(i,1) + rsnow(j))
        enddo
      endif
    enddo
  endif
  !
  end subroutine vdep2
end module vdep_mod
