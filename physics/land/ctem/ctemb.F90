subroutine ctemb(fieldsm,wiltsm, &
                       sand,clay, &
                       nl,nm,il1,il2,im,ig)
  !
  !     * nov 15/11 - m.lazare. routine based on classb, but for
  !     *                       ctem, to define invariant fields
  !     *                       wiltsm and fieldsm which ctem needs.
  !
  implicit none
  !
  !     * scalars.
  !
  real    :: psisat !<
  real    :: grksat !<
  real    :: thpor !<
  real    :: b !<
  integer, intent(in) :: nl !<
  integer, intent(in) :: nm !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: im !<
  integer, intent(in) :: ig !<
  integer :: i !<
  integer :: j !<
  integer :: m !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: fieldsm(nl,nm,ig) !<
  real, intent(inout) :: wiltsm(nl,nm,ig) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: sand  (nl,nm,ig) !<
  real, intent(in) :: clay  (nl,nm,ig) !<
  !
  !     * internal arrays.
  !
  integer, dimension(nl,nm,ig) :: isand !<
  !---------------------------------------------------------------------
  do j=1,ig
    do m=1,im
      do i=il1,il2
        isand  (i,m,j) = nint(sand(i,m,j))
        wiltsm (i,m,j) = 0.
        fieldsm(i,m,j) = 0.
      end do
    end do
  end do ! loop 100
  !
  do j=1,ig
    do m=1,im
      do i=il1,il2
        if (isand(i,m,j)/=-3 .and. isand(i,m,j)/=-4) then
          psisat = (10.0**(-0.0131*sand(i,m,j)+1.88))/100.0
          grksat = (10.0**(0.0153*sand(i,m,j)-0.884))*7.0556e-6
          thpor  = (-0.126*sand(i,m,j)+48.9)/100.0
          b      = 0.159*clay(i,m,j)+2.91
          !
          wiltsm (i,m,j) = (150./psisat)**(-1./b)
          wiltsm (i,m,j) = thpor * wiltsm(i,m,j)
          !
          fieldsm(i,m,j) = (1.157e-09/grksat)**(1./(2.*b+3.))
          fieldsm(i,m,j) = thpor *  fieldsm(i,m,j)
        end if
      end do
    end do
  end do ! loop 200
  !
  return
end
