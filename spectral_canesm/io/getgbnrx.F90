!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getgbnrx(ggpak,name,nf,nlon,nlat,iday,gg)
  !
  !     * jun 20/03 - m.lazare. - convert grid to ordered s-n pairs
  !     *                         with cyclic longitude removed, as
  !     *                         input to the model.
  !     *                       - npack removed from call.
  !     * sep 27/92 - m.lazare. previous version getgbnr.
  !
  !     * gets gaussian grid gg(nlon,nlat) called name from file nf.
  !     * and packs it, in ordered s-n pairs, into array ggpak.

  !     * the last point in each row is omitted since it is
  !     * just a copy of the first point in the row.
  !     * iday is the day of the year.
  !
  !     * note that this routine is exactly the same as routine "GETAGBX",
  !     * except that the input file is not rewound (to be used to save
  !     * i/o on operations that work with sequential data).
  !
  implicit none
  integer :: ibuf
  integer :: idat
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer :: ilat
  integer :: intsize
  integer :: machine
  integer :: maxx
  integer, intent(in) :: name
  integer, intent(in) :: nf
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer :: nc4to8
  !
  real, intent(in), dimension(1) :: ggpak !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlon,nlat) :: gg !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  logical :: ok
  !
  !     * icom is a shared scm i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  common /icom/ ibuf(8),idat(1)
  common /machtyp/ machine, intsize
  common /mpilat/ ilat
  !-----------------------------------------------------------------------
  !     * read the field.
  !     * stop if not found or wrong size is read.
  !
  maxx = ( nlon * nlat + 8) * machine
  call getfld2(nf,gg,nc4to8("GRID"),iday,name,1,ibuf,maxx,ok)
  if (.not.ok) then
    !     * error exit - stop if grid missing or wrong size.
    write(6,6020) iday,name,nlon,nlat
    call xit('GETGBNRX', - 1)
  end if
  write(6,6025) ibuf
  if (ibuf(5) /= nlon) then
    !     * error exit - stop if grid missing or wrong size.
    write(6,6020) iday,name,nlon,nlat
    call xit('GETGBNRX', - 1)
  end if
  if (ibuf(6) /= nlat) then
    !     * error exit - stop if grid missing or wrong size.
    write(6,6020) iday,name,nlon,nlat
    call xit('GETGBNRX', - 1)
  end if
  !
  !     * convert to ordered s-n pairs.
  !
  call rgtocg2(ggpak,gg,nlon - 1,nlat,ilat)

  return
  !
  !-----------------------------------------------------------------------
6020 format('0GRID',i10,1x,a4,5x,'SIZE',2i6,5x,'NOT FOUND')
6025 format(' ',a4,i10,1x,a4,5i6)
6030 format(' + ',55x,a4,' BIAS = ',1pe12.4)
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

