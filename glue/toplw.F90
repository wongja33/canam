!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine toplw(shb,ptoit,ilev)
  !
  !     * apr 26/2006 - m.lazare. defines integer :: variable "lwtop" which
  !     *                         separates full l/w assumptions (below
  !     *                         this model level) from simplified ones
  !     *                         above it.
  !
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: istep
  integer :: l
  integer :: lwtop
  real :: pfull
  real :: pfull0
  real, intent(in) :: ptoit
  real :: thresh
  !
  real, intent(in), dimension(ilev) :: shb !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  common /itoplw/ lwtop
  !
  !     * threshold in mbs.
  !
  data thresh/0.99/
  !============================================================================
  !     * pressure in mbs of upper interface of top layer.
  !
  !
  pfull0 = 0.01 * ptoit
  lwtop = 1
  !
  !     * if top layer below threshold, xit with lwtop=1. otherwise, calculate
  !     * layaer index for which the top interface pressure first exceeds
  !     * "thresh".
  !
  if (pfull0 < thresh) then
    istep = 0
    do l = 1,ilev
      pfull = shb(l) * 1000.
      if (istep == 0 .and. pfull >= thresh) then
        lwtop = l + 1
        istep = 1
      end if
    end do
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

