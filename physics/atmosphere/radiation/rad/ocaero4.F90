!> \file
!>\brief Calculation of organic carbon aerosol optical property for bulk aerosol parameterization
!!
!! @author Jiangnan Li
!
subroutine ocaero4 (exta, exoma, exomga, fa, absa, &
                    exta055, oma055, ga055, &
                    extb, odfb, ssab, absb, extb055, odf055, &
                    ssa055, abs055, rh, ocoload, ocyload, &
                    il1, il2, ilg, lay)
  !
  !     * feb 08, 2013 - j.li.  new version for gcm17 based on ocaero3.
  !     *                       - update to reading data,
  !     *                         simplify the code and took out
  !     *                         the coarse mode, since no input
  !     *                         from gcm.
  !     * apr 23, 2012 - j.li & y.peng. previous version ocaero3 for gcm16:
  !                                     - revised code for optical of oc.
  !     * feb 10, 2009 - j. li/ previous version ocaero2 for gcm15h/i:
  !     *                x. ma. - include rh effect for hydrophylic oc.
  !     * sep 26,2007 - x. ma/  previous version ocaero for gcm15f:
  !     *               j. li.  - correct the wrong oc input for hydrophobic
  !     *                         at infrared
  !     *                       - separate the code for bc and oc.
  !     *                       - modification to match more with other part
  !     *                         of aerosol and simplify the code
  !----------------------------------------------------------------------c
  !     calculation of optical properties for oc with two modes, fine    c
  !     mode: sigma = 2.24, r0 = 0.0212, coarse mode: sigma = 2., r0 =   c
  !     0.0602. note in gcm4 we only have the accum mode, we keep the    c
  !     coarse mode data, but not use it now                             c
  !     the refractive index is from reading                             c
  !     the growth is following knut's scheme used in aeromix3           c
  !                                                                      c
  !     exta:   extinction coefficient                                   c
  !     exoma:  extinction coefficient times single scattering albedo    c
  !     exomga: exoma times asymmetry factor                             c
  !     fa:     square of asymmetry factor                               c
  !     absa:   absorption coefficient                                   c
  !     rh:     relative humidity                                        c
  !     ocoload:oco aerosol loading for each layer                       c
  !     ocyload:ocy aerosol loading for each layer                       c
  !     exta055, oma055, ga055 are the info for 0.55                     c
  !     following info is no useful except 0.55  (i will take out all    c
  !     extb, ssab, absb are band info, odfb is extb for fine mode       c
  !     in se1 first 4 is for bands, 5 is for mass, 6 is for 0.55, 7 is  c
  !     0.83. in sw1 and sg1, the 5 is for 0.55                          c
  !----------------------------------------------------------------------c
  implicit none
  real :: abso
  real :: absy
  real :: exomgo
  real :: exomgy
  real :: exomo
  real :: exomy
  real :: exto
  real :: exty
  real :: go
  real :: gy
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: j
  integer :: k
  integer :: l
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: lay2
  integer :: lp1
  integer :: nbl
  integer :: nbs
  integer :: nh
  real :: omo
  real :: omy
  real :: rhs
  real :: tmass
  !
  parameter (nbs = 4, nbl = 9, nh = 7)
  !
  real, intent(inout), dimension(ilg,lay,nbs) :: exta !< Extinction coefficient at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exoma !< EXTA*single scattering albedo for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exomga !< EXOMA*asymetry factor for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: fa !< EXOMGA*asymetry factor for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: absb !< Absorption coefficient \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: odfb !< Extinction coefficient per mass for fine mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: extb !< Extinction coefficient per mass at 0.55\f$\mu\f$m for both modes \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: ssab !< Single scattering albedo \f$[1]\f$
  real, intent(inout), dimension(ilg,lay,nbl) :: absa !< Longwave absorption coefficient \f$[m^2/gram]\f$
  real, dimension(nbs) :: somg
  real, dimension(nbs) :: sg
  real, dimension(nbs) :: sexta
  real, dimension(nbs) :: somga
  real, dimension(nbs) :: sga
  real, dimension(7,nh) :: se1
  real, dimension(5,nh) :: sw1
  real, dimension(5,nh) :: sg1
  real, dimension(nbl,nh) :: sa1
  real, intent(in), dimension(ilg,lay) :: rh !< Relative humidity \f$[\%]\f$
  real, intent(in), dimension(ilg,lay) :: ocyload !< Hydrophilic bc loading \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ocoload !< Hydrophobic bc loading \f$[gram/gram]\f$
  real, dimension(ilg,lay) :: ocoldacc
  real, dimension(ilg,lay) :: ocyldacc
  real, dimension(ilg,lay) :: ocldacc
  real, dimension(ilg,lay) :: delta
  real, dimension(ilg,lay) :: rmass
  real, intent(inout), dimension(ilg,lay) :: exta055 !< Extinction coefficient at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: oma055 !< EXTA055 * single scattering albedo at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ga055 !< OMA055 * asymmetry factor at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: extb055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for both modes \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: odf055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for fine mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ssa055 !< Single scattering albedo at 0.55\f$\mu\f$m \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: abs055 !< Absorption coefficient at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer, dimension(ilg,lay) :: numrh
  real, dimension(7) :: rhstd
  !
  data ((se1(i,j), i = 1, 7), j = 1, nh)                       / &
                       5.27833, 2.24282, 0.74013, 0.10034, 1.01570, 4.74178, 2.21780, &
                       5.17586, 2.21064, 0.73347, 0.12568, 1.14027, 4.64903, 2.18660, &
                       5.00877, 2.16995, 0.72961, 0.16993, 1.41475, 4.50327, 2.14763, &
                       4.76548, 2.15794, 0.75460, 0.24868, 2.20339, 4.31103, 2.13947, &
                       4.61216, 2.21680, 0.81762, 0.31564, 3.43786, 4.21564, 2.20286, &
                       4.50408, 2.29541, 0.89280, 0.36684, 4.97494, 4.16079, 2.28606, &
                       4.39900, 2.36639, 0.96723, 0.40893, 6.76721, 4.10382, 2.36177/

  data ((sw1(i,j), i = 1, 5), j = 1, nh)     / &
                       0.82337, 0.99565, 0.99390, 0.93582, 0.89469, &
                       0.83723, 0.99608, 0.99423, 0.75423, 0.90468, &
                       0.86112, 0.99679, 0.99483, 0.57845, 0.92101, &
                       0.90214, 0.99791, 0.99594, 0.45321, 0.94700, &
                       0.93271, 0.99868, 0.99682, 0.43287, 0.96498, &
                       0.95105, 0.99910, 0.99736, 0.44739, 0.97523, &
                       0.96239, 0.99935, 0.99770, 0.47112, 0.98137/

  data ((sg1(i,j), i = 1, 5), j = 1, nh)     / &
                       0.60207, 0.53575, 0.47566, 0.30760, 0.58512, &
                       0.62043, 0.55494, 0.49143, 0.31751, 0.60415, &
                       0.64999, 0.58655, 0.51907, 0.33807, 0.63505, &
                       0.69608, 0.63859, 0.56991, 0.38434, 0.68404, &
                       0.72850, 0.67871, 0.61442, 0.43241, 0.71933, &
                       0.74817, 0.70558, 0.64714, 0.47315, 0.74124, &
                       0.76078, 0.72447, 0.67172, 0.50713, 0.75557/

  data ((sa1(i,j), i = 1, nbl), j = 1, nh)                     / &
                       0.07194, 0.06362, 0.15064, 0.33950, 0.26811, 0.08958, 0.15957, &
                       0.13965, 0.13655, &
                       0.06767, 0.06088, 0.14525, 0.32087, 0.27992, 0.09750, 0.17296, &
                       0.14665, 0.14213, &
                       0.06049, 0.05596, 0.13554, 0.28726, 0.28377, 0.10968, 0.19419, &
                       0.15527, 0.14563, &
                       0.04907, 0.04758, 0.11899, 0.22296, 0.24680, 0.12643, 0.22548, &
                       0.16216, 0.13838, &
                       0.04147, 0.04177, 0.10748, 0.16983, 0.19475, 0.13612, 0.24529, &
                       0.16288, 0.12664, &
                       0.03733, 0.03861, 0.10115, 0.13646, 0.15764, 0.14105, 0.25618, &
                       0.16219, 0.11824, &
                       0.03502, 0.03687, 0.09761, 0.11546, 0.13323, 0.14384, 0.26269, &
                       0.16155, 0.11274                                              /

  data rhstd /0.1, 0.5, 0.75, 0.9, 0.95, 0.97, 0.98/
  !
  !----------------------------------------------------------------------c
  do k = 1, lay
    do i = il1, il2
      rhs       =  max( min (rh(i,k), 0.98), 0.1)
      if (rhs >= 0.1 .and. rhs < 0.50) then
        l           =  1
      else if (rhs >= 0.50 .and. rhs < 0.75) then
        l           =  2
      else if (rhs >= 0.75 .and. rhs < 0.90) then
        l           =  3
      else if (rhs >= 0.90 .and. rhs < 0.95) then
        l           =  4
      else if (rhs >= 0.95 .and. rhs < 0.97) then
        l           =  5
      else if (rhs >= 0.97 .and. rhs <= 0.98) then
        l           =  6
      end if
      numrh(i,k)    =  l
      lp1           =  l + 1
      delta(i,k)    = (rh(i,k) - rhstd(l)) / (rhstd(lp1) - rhstd(l))
      !
      ! *   mass ratio
      rmass(i,k)    =  se1(5,l) + delta(i,k) * (se1(5,lp1) - se1(5,l))
      !
      ocoldacc(i,k) =  10000.0 * ocoload(i,k)
      ocyldacc(i,k) =  10000.0 * ocyload(i,k)
      ocldacc(i,k)  =  ocoldacc(i,k) + ocyldacc(i,k)
    end do
  end do ! loop 100
  !
  do j = 1, nbs
    do k = 1, lay2
      do i = il1, il2
        if (ocldacc(i,k) < 1.e-09) cycle
        l             =  numrh(i,k)
        lp1           =  l + 1
        exty          =  ocyldacc(i,k) * rmass(i,k) * (se1(j,l) + &
                        delta(i,k) * (se1(j,lp1) - se1(j,l)))
        exto          =  ocoldacc(i,k) * se1(j,1)
        !
        omy           =  sw1(j,l) + delta(i,k) * (sw1(j,lp1) - sw1(j,l))
        omo           =  sw1(j,1)
        !
        gy            =  sg1(j,l) + delta(i,k) * (sg1(j,lp1) - sg1(j,l))
        go            =  sg1(j,1)
        exomy         =  exty * omy
        exomo         =  exto * omo
        exomgy        =  exty * omy * gy
        exomgo        =  exto * omo * go
        !
        exta(i,k,j)   =  exta(i,k,j) + exty + exto
        exoma(i,k,j)  =  exoma(i,k,j) + exomy + exomo
        exomga(i,k,j) =  exomga(i,k,j) + exomgy + exomgo
        fa(i,k,j)     =  fa(i,k,j) + exomgy * gy + exomgo * go
        !
        absb(i,k,j)   =  exty * (1.0 - omy) + exto * (1.0 - omo)
        ssab(i,k,j)   =  exomy + exomo
        extb(i,k,j)   =  exty  + exto
        odfb(i,k,j)   =  extb(i,k,j)
      end do
    end do
  end do ! loop 200
  !
  do k = 1, lay
    do i = il1, il2
      if (ocldacc(i,k) < 1.e-09) cycle
      l             =  numrh(i,k)
      lp1           =  l + 1
      exty          =  ocyldacc(i,k) * rmass(i,k) * (se1(6,l) + &
                      delta(i,k) * (se1(6,lp1) - se1(6,l)))
      exto          =  ocoldacc(i,k) * se1(6,1)
      exta055(i,k)  =  exta055(i,k) + exty + exto
      odf055(i,k)   =  exta055(i,k)
      !
      omy           =  sw1(5,l) + delta(i,k) * (sw1(5,lp1) - sw1(5,l))
      omo           =  sw1(5,1)
      oma055(i,k)   =  oma055(i,k) + omy + omo
      !
      gy            =  sg1(5,l) + delta(i,k) * (sg1(5,lp1) - sg1(5,l))
      go            =  sg1(5,1)
      ga055(i,k)    =  ga055(i,k) + gy + go
      !
      odf055(i,k)   =  exta055(i,k)
      abs055(i,k)   = (1.0 - omy) * exty + (1.0 - omo) * exto

      tmass         =  ocyldacc(i,k) + ocoldacc(i,k)
      if (tmass > 1.e-15) then
        extb055(i,k) = (exty + exto) / tmass
        odf055(i,k) =  extb055(i,k)
      else
        extb055(i,k) =  0.0
        odf055(i,k) =  0.0
      end if
      !
      if (exty + exto > 1.e-20) then
        ssa055(i,k)  =  (exty * omy + exto * omo) / (exty + exto)
      else
        ssa055(i,k)  =  0.0
      end if
      !
    end do
  end do ! loop 250
  !
  !----------------------------------------------------------------------c
  !     infrared, sa: specific absorptance, abs: absorption              c
  !----------------------------------------------------------------------c
  !
  do j = 1, nbl
    do k = 1, lay
      do i = il1, il2
        if (ocldacc(i,k) < 1.e-09) cycle
        l             =  numrh(i,k)
        lp1           =  l + 1
        absy          =  ocyldacc(i,k) * (sa1(j,l) + &
                        delta(i,k) * (sa1(j,lp1) - sa1(j,l)))
        abso          =  ocoldacc(i,k) * sa1(j,1)

        absa(i,k,j)   =  absa(i,k,j) + absy + abso
      end do
    end do
  end do ! loop 400
  !
  return
end
!> \file
!> Calculation of optical properties for organic carbon with two modes, both
!! having log-normal distributions.  For the fine mode the distribution is assumed
!! to have a \f$\sigma\f$ = 2.24 and \f$r_0\f$ = 0.0212 while the coarse mode is
!! assumed to have a \f$sigma\f$ = 2. and \f$r_0\f$ = 0.0602.
!!\n 
!! The refractive indices are from Reading(?) while the hydroscopic growth follows
!! the approach described in aeromix3.f.