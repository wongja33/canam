!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine rdswet(snow,wsnow,reff,gcrow,reff0,delt,ilg,il1,il2)
  !
  !     * m. namazi & k. von salzen, snow grain due to wet aging.
  !     * only over land happens.
  !     * for the rest of regions keep the same value,
  !
  implicit none

  !     * integer :: and real :: constants.
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i
  real, intent(inout) :: reff0 !< Variable description\f$[units]\f$
  real, parameter :: pi = 3.1415927
  real, parameter :: c1 = 4.22e-13

  !     * input arrays.
  real, intent(in), dimension(ilg) :: snow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: wsnow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gcrow !< Variable description\f$[units]\f$
  real, intent(in)  :: delt   !< Timestep for atmospheric model \f$[seconds]\f$

  !     * input/output arrays.
  real, intent(inout), dimension(ilg) :: reff !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !     * internal work arrays.
  real, dimension(ilg) :: fliq
  !----------------------------------------------------------------------
  do i = il1,il2
    if (gcrow(i) < - 0.5 .and. snow(i) > 0.) then
      !           if (reff(i)==0)  reff(i)=reff0
      reff(i) = max(reff0,reff(i))
      reff(i) = reff(i) * 1.e+6
      fliq(i) = wsnow(i)/(snow(i) + wsnow(i))
      reff(i) = ((0.75e+18 * c1/pi) * (fliq(i) ** 3) * delt &
                + reff(i) ** 3) ** (1./3.)
      reff(i) = reff(i) * 1.e-6
    end if
  end do ! loop 100

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
