#if defined(_DYNAMIC_ALLOCATION) || defined(_POINTER_ASSIGNMENT)
#  define FOUR_DIM_ 4
#  define TWO_DIM_  2
#  define PLUS_TWO
#  define PLUS_ONE
#  define ILEV_DIM_   ilev
#  define ILG_DIM_    ilg
#  define ILEV_DIM_PLUS_TWO   ilev+2
#  define ILGZ_DIM_   ilgz
#  define IM_DIM_     im
#  define NRMFLD_DIM_ nrmfld
#  define LEVSA_DIM_  levsa
#  define NBS_DIM_    nbs
#  define NTRAC_DIM_  ntrac
#  define NTLD_DIM_   ntld
#  define ICTEM_DIM_  ictem
#  define ICTEM_DIM_PLUS_ONE  ictem+1
#  define ICAN_DIM_   ican
#  define ICAN_DIM_PLUS_ONE ican+1
#  define IGND_DIM_   ignd
#  define ISAINTT_DIM_ isaintt
#  define KEXTT_DIM_  kextt
#  define ISDUST_DIM_ isdust
#  define ISDIAG_DIM_ isdiag
#  define ISDNUM_DIM_ isdnum
#  define KINTT_DIM_  kintt
#  define LEVOZ_DIM_  levoz
#  define LEVOZC_DIM_ levozc
#  define LEVWF_DIM_  levwf
#  define LEVAIR_DIM_ levair
#  define NLKLM_DIM_  nlklm
#  define NRFP_DIM_   nrfp
#  define NBL_DIM_    nbl
#  define LEVOX_DIM_  levox
!  Define the correct attribute for arrays
#  ifdef _DYNAMIC_ALLOCATION
#    define ALLOCABLE_ allocatable
#  endif
#  ifdef _POINTER_ASSIGNMENT
#    define ALLOCABLE_ pointer
#    define POINT_
#  endif

#else
#  define ALLOCABLE_
#  define ASSIGN_
#  include "cppdef_sizes.h"
#  define FOUR_DIM_ 4
#  define TWO_DIM_ 2
#  define PLUS_TWO +2
#  define PLUS_ONE +1
!   Defined in cppdef_sizes
#  define NRFP_DIM_  _PAR_NRFP
#  define ILEV_DIM_  _PAR_ILEV
#  define ILG_DIM_   _PAR_ILG
#  define ILEV_DIM_PLUS_TWO _PAR_ILEV+2
#  define LEVOZ_DIM_ _PAR_LEVOZ
#  define NTRAC_DIM_ _PAR_NTRAC
#  define NTLD_DIM_ _PAR_NTLD
#  define IM_DIM_    _PAR_NTLD+_PAR_NTLK+_PAR_NTWT
#  define NRFP_DIM_ _PAR_NRFP
#  define LEVOX_DIM_ _PAR_LEVOX
!   This is defined in msizes.F90
#  define ILGZ_DIM_  1+1
!   Defined in compar_mod
#  define NRMFLD_DIM_ 2
!   Defined in psizes_19
#  define LEVSA_DIM_ 70
!      #if defined explvol
!      # define LEVSA_DIM_ 70
!      #else
!      # define LEVSA_DIM_ 1
!      #endif
!   Defined in rdmod_mod.F
#  define NBS_DIM_ 4
#  define NBL_DIM_ 9
!   Defined in psizes_19
#  define ICTEM_DIM_ 9
#  define ICTEM_DIM_PLUS_ONE ICTEM_DIM_+1
#  define ICAN_DIM_ 4
#  define ICAN_DIM_PLUS_ONE ICAN_DIM_+1
#  define IGND_DIM_ 3
#  define LEVOZC_DIM_ 24
#  define LEVWF_DIM_ 6
#  define LEVAIR_DIM_ 25
#  define NLKLM_DIM_ 200
!   Defined in compar_mod
#  define ISAINTT_DIM_ 3
#  define KEXTT_DIM_ 4
#  define ISDUST_DIM_ 96
#  define ISDIAG_DIM_ 22
#  define ISDNUM_DIM_ 30
#  define KINTT_DIM_ 3
#endif
! Gas-pase chemistry parameter sizes, defined in chem_params_mod
#  define LEVSSAD_DIM_ levssad
#  define NWFLX_DIM_  nwflx
#  define NVDEP_DIM_  nvdep
#  define NSFCEM_DIM_  nsfcem
#  define NLVARC_DIM_  nlvarc
#  define NCHDG_DIM_  nchdg
#  define NLSO4_DIM_  nlso4
