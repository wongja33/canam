# include "cppdef_config.h"
# define ENABLE(A) A=.true.
# ifdef carbon
  ENABLE(CARBON)
# endif
# ifdef mam
  ENABLE(MAM)
# endif
# ifdef flake
  ENABLE(FLAKE)
# endif
# ifdef cslm
  ENABLE(CSLM)
# endif
# ifdef transient_aerosol_emissions
  ENABLE(TRANSIENT_AEROSOL_EMISSIONS)
# endif
# ifdef emists
  ENABLE(EMISTS)
# endif
# ifdef agcm_ctem
  ENABLE(AGCM_CTEM)
# endif
# ifdef explvol
  ENABLE(EXPLVOL)
# endif
# ifdef radforce
  ENABLE(RADFORCE)
# endif
# ifdef tprhs
  ENABLE(TPRHS)
# endif
# ifdef qprhs
  ENABLE(QPRHS)
# endif
# ifdef uprhs
  ENABLE(UPRHS)
# endif
# ifdef vprhs
  ENABLE(VPRHS)
# endif
# ifdef qconsave
  ENABLE(QCONSAV)
# endif
# ifdef tprhsc
  ENABLE(TPRHSC)
# endif
# ifdef qprhsc
  ENABLE(QPRHSC)
# endif
# ifdef uprhsc
  ENABLE(UPRHSC)
# endif
# ifdef vprhsc
  ENABLE(VPRHSC)
# endif
# ifdef xconsav
  ENABLE(XCONSAV)
# endif
# ifdef xprhs
  ENABLE(XPRHS)
# endif
# ifdef xprhsc
  ENABLE(XPRHSC)
# endif
# ifdef xtraconv
  ENABLE(XTRACONV)
# endif
# ifdef xtrachem
  ENABLE(XTRACHEM)
# endif
# ifdef xtrapla1
  ENABLE(XTRAPLA1)
# endif
# ifdef xtrapla2
  ENABLE(XTRAPLA2)
# endif
# ifdef xtrals
  ENABLE(XTRALS)
# endif
# ifdef aodpth
  ENABLE(AODPTH)
# endif
# ifdef use_cosp
  ENABLE(USE_COSP)
# endif
# ifdef xtradust
  ENABLE(XTRADUST)
# endif
# ifdef x01
  ENABLE(X01)
# endif
# ifdef x02
  ENABLE(X02)
# endif
# ifdef x03
  ENABLE(X03)
# endif
# ifdef x04
  ENABLE(X04)
# endif
# ifdef x05
  ENABLE(X05)
# endif
# ifdef x06
  ENABLE(X06)
# endif
# ifdef x07
  ENABLE(X07)
# endif
# ifdef x08
  ENABLE(X08)
# endif
# ifdef x09
  ENABLE(X09)
# endif
# ifdef pla
  ENABLE(PLA)
# endif
# ifdef pam
  ENABLE(PAM)
# endif
# ifdef pfrc
  ENABLE(PFRC)
# endif
# ifdef rad_flux_profs
  ENABLE(RAD_FLUX_PROFS)
# endif
# ifdef use_gem
  ENABLE(USE_GEM)
# endif

#undef carbon
#undef mam
#undef cslm
#undef flake
#undef transient_aerosol_emissions
#undef emists
#undef agcm_ctem
#undef explvol
#undef radforce
#undef tprhs
#undef qprhs
#undef uprhs
#undef vprhs
#undef qconsav
#undef tprhsc
#undef qprhsc
#undef uprhsc
#undef vprhsc
#undef xconsav
#undef xprhs
#undef xprhsc
#undef xtraconv
#undef xtrachem
#undef xtrapla1
#undef xtrapla2
#undef xtrals
#undef aodpth
#undef use_cosp
#undef xtradust
#undef x01
#undef x02
#undef x03
#undef x04
#undef x05
#undef x06
#undef x07
#undef x08
#undef x09
#undef pla
#undef pam
#undef pfrc
#undef rad_flux_profs
#undef use_gem
