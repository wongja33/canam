subroutine hetresv (fcan,      fct, litrmass, soilcmas, &
                          icc,       ig,      ilg,      il1, &
                          il2,     tbar,    thliq,     sand, &
                          clay, roottemp,    zbotw,     sort, &
                          isand, &
  !    -------------- inputs above this line, outputs below -------------
                          ltresveg, scresveg)
  !
  !               canadian terrestrial ecosystem model (ctem) v1.1
  !           heterotrophic respiration subtoutine for vegetated fraction
  !
  !     16  oct. 2001 - this subroutine calculates heterotrophic respiration
  !     v. arora        for a given sub-area, from litter and soil carbon
  !                     pools.
  !
  !     change history:

  !     j. melton and v.arora - changed tanhq10 parameters, they were switched
  !               25 sep 2012
  !     j. melton 23 aug 2012 - bring in isand, converting sand to
  !                             int was missing some gridcells assigned
  !                             to bedrock in classb
  !     ------
  !     inputs
  !
  !     FCAN      - FRACTIONAL COVERAGE OF CTEM's 9 PFTs
  !     fct       - sum of all fcan
  !                 fcan & fct are not used at this time but could
  !                 be used at some later stage
  !     litrmass  - litter mass for the 9 pfts + bare in kg c/m2
  !     soilcmas  - soil carbon mass for the 9 pfts + bare in kg c/m2
  !     icc       - no. of vegetation types (currently 9)
  !     ig        - no. of soil layers (currently 3)
  !     ilg       - no. of grid cells in latitude circle
  !     il1,il2   - il1=1, il2=ilg
  !     tbar      - soil temperature, k
  !     thliq     - liquid soil moisture content in 3 soil layers
  !     sand      - percentage sand
  !     clay      - percentage clay
  !     roottemp  - root temperature as estimated in mainres subroutine
  !     zbotw     - bottom of soil layers
  !     sort      - index for correspondence between 9 pfts and 12 values
  !                 in the parameters vectors
  !
  !     outputs
  !
  !     ltresveg  - litter respiration for the given sub-area in
  !                 uMOL CO2/M2.S, FOR CTEM's 9 PFTs
  !     scresveg  - soil carbon respiration for the given sub-area in
  !                 uMOL CO2/M2.S, FOR CTEM's 9 PFTs
  !
  implicit none

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: icc !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  integer :: j !<
  integer :: k !<
  integer :: kk !<
  integer, intent(in) :: sort(icc) !<
  integer, intent(in) :: isand(ilg,ig) !<
  !
  parameter(kk=12) ! product of class pfts and l2max ( 4 x 3 = 12)

  real, intent(in)    :: fcan(ilg,icc) !<
  real, intent(in)    :: fct(ilg) !<
  real, intent(in)    :: litrmass(ilg,icc+1) !<
  real, intent(in)    :: tbar(ilg,ig) !<
  real, intent(in)    :: soilcmas(ilg,icc+1) !<
  real, intent(in)    :: thliq(ilg,ig) !<
  real, intent(in)    :: sand(ilg,ig) !<
  real, intent(in)    :: clay(ilg,ig) !<
  real, intent(in)    :: roottemp(ilg,icc) !<
  real, intent(in)    :: zbotw(ilg,ig) !<
  real, intent(inout)    :: ltresveg(ilg,icc) !<
  real, intent(inout)    :: scresveg(ilg,icc) !<

  real     :: bsratelt(kk) !<
  real     :: bsratesc(kk) !<
  real     :: zero !<
  real     :: litrq10 !<
  real     :: soilcq10 !<
  real     :: alpha !<
  real     :: litrtemp(ilg,icc) !<
  real     :: solctemp(ilg,icc) !<
  real     :: q10func !<
  real     :: psisat(ilg,ig) !<
  real     :: grksat(ilg,ig) !<
  real     :: b(ilg,ig) !<
  real     :: thpor(ilg,ig) !<
  real     :: fracarb(ilg,icc,ig) !<
  real     :: abar(kk) !<
  real     :: zcarbon !<
  real     :: tempq10l(ilg,icc) !<
  real     :: socmoscl(ilg,icc) !<
  real     :: scmotrm(ilg,ig) !<
  real     :: ltrmoscl(ilg) !<
  real     :: psi(ilg,ig) !<
  real     :: tempq10s(ilg,icc) !<
  real     :: fcoeff !<
  real     :: tanhq10(4) !<
  !
  !     ------------------------------------------------------------------
  !                     constants and parameters
  !
  !     note the structure of vectors which clearly shows the class
  !     pfts (along rows) and ctem sub-pfts (along columns)
  !
  !     needle leaf |  evg       dcd       ---
  !     broad leaf  |  evg   dcd-cld   dcd-dry
  !     crops       |   c3        c4       ---
  !     grasses     |   c3        c4       ---
  !
  !     litter respiration rates at 15 c in in kg c/kg c.year
  data bsratelt/0.4453, 0.5986, 0.0000, &
               0.6339, 0.7576, 0.6957, &
  !    &              0.4020, 0.4020, 0.0000,
               0.7000, 0.7000, 0.0000, &
               0.5260, 0.5260, 0.0000/
  !
  !     soil carbon respiration rates at 15 c in kg c/kg c.year
  !     data bsratesc/0.0260, 0.0260, 0.0000,
  !    &              0.0208, 0.0208, 0.0208,
  !    &              0.0310, 0.0310, 0.0000,
  !    &              0.0125, 0.0125, 0.0000/
  !
  !     decrease bsratesc to increase pre-industrial soil c pool
  !     for cmip6 canesm5
  data bsratesc/0.0210, 0.0210, 0.0000, &
               0.0175, 0.0170, 0.0208, &
               0.0263, 0.0263, 0.0000, &
               0.0106, 0.0106, 0.0000/
  !
  !
  !     parameter determining average carbon profile, same as in bio2str f0r
  !     average root profile
  !
  data abar/4.70, 5.86, 0.00, &
           3.87, 3.46, 3.97, &
           3.97, 3.97, 0.00, &
           5.86, 4.92, 0.00/
  !
  !     parameters of the hyperbolic tan q10 formulation
  !
  data tanhq10/1.44, 0.56, 0.075, 46.0/
  !                     a     b      c     d
  !     q10 = a + b * tanh[ c (d-temperature) ]
  !     when a = 2, b = 0, we get the constant q10 of 2. if b is non
  !     zero then q10 becomes temperature dependent
  !
  !     alpha, parameter for finding litter temperature as a weighted average
  !     of top soil layer temperature and root temperature
  data alpha/0.7/
  !
  !     ---------------------------------------------------------------
  !
  !     initialize required arrays to zero

  do j = 1, icc
    do i = il1, il2
      litrtemp(i,j)=0.0       ! litter temperature
      tempq10l(i,j)=0.0
      solctemp(i,j)=0.0       ! soil carbon pool temperature
      tempq10s(i,j)=0.0
      socmoscl(i,j)=0.0       ! soil moisture scalar for soil carbon decomposition
      ltresveg(i,j)=0.0       ! litter resp. rate for each pft
      scresveg(i,j)=0.0       ! soil c resp. rate for each pft
    end do ! loop 110
  end do ! loop 100
  !
  do j = 1, ig
    do i = il1, il2
      psisat(i,j) = 0.0       ! saturation matric potential
      grksat(i,j) = 0.0       ! saturation hyd. conductivity
      thpor(i,j) = 0.0        ! porosity
      b(i,j) = 0.0            ! parameter b of clapp and hornberger
      scmotrm(i,j)=0.0        ! soil carbon moisture term
      !         isand(i,j)=nint(sand(i,j)) ! now passed in. jm. aug 232012
    end do ! loop 130
  end do ! loop 120

  do i = il1, il2
    ltrmoscl(i)=0.0           ! soil moisture scalar for litter decomposition
  end do ! loop 140
  !
  do k = 1, ig
    do j = 1, icc
      do i = il1, il2
        fracarb(i,j,k)=0.0    ! fraction of carbon in each soil layer for each vegetation
      end do
    end do
  end do ! loop 150
  !
  !     initialization ends
  !
  !     ------------------------------------------------------------------
  !
  !     estimate temperature of the litter and soil carbon pools. litter
  !     temperature is weighted average of temperatue of top soil layer
  !     (where the stem and leaf litter sits) and root temperature, because
  !     litter pool is made of leaf, stem, and root litter.
  !
  do j = 1,icc
    do i = il1, il2
      if (fcan(i,j) > 0.) then
        litrtemp(i,j)=alpha*tbar(i,1)+roottemp(i,j)*(1.0-alpha)
      end if
    end do ! loop 210
  end do ! loop 200
  !
  !     estimation of soil carbon pool temperature is not straight forward.
  !     ideally soil c pool temperature should be set same as root temperature,
  !     since soil c profiles are similar to root profiles. but in the event
  !     when the roots die then we may run into trouble. so we find the
  !     temperature of the soil c pool assuming that soil carbon is
  !     exponentially distributed, just like roots. but rather than using
  !     the parameter of this exponential profile from our variable root
  !     distribution we use fixed vegetation-dependent parameters.
  !
  do j = 1, icc
    do i = il1, il2
      if (fcan(i,j) > 0.) then
        !
        zcarbon=3.0/abar(sort(j))                ! 95% depth
        if (zcarbon<=zbotw(i,1)) then
          fracarb(i,j,1)=1.0             ! fraction of carbon in
          fracarb(i,j,2)=0.0             ! soil layers
          fracarb(i,j,3)=0.0
        else
          fcoeff=exp(-abar(sort(j))*zcarbon)
          fracarb(i,j,1)= &
                              1.0-(exp(-abar(sort(j))*zbotw(i,1))-fcoeff)/(1.0-fcoeff)
          if (zcarbon<=zbotw(i,2)) then
            fracarb(i,j,2)=1.0-fracarb(i,j,1)
            fracarb(i,j,3)=0.0
          else
            fracarb(i,j,3)= &
                                  (exp(-abar(sort(j))*zbotw(i,2))-fcoeff)/(1.0-fcoeff)
            fracarb(i,j,2)=1.0-fracarb(i,j,1)-fracarb(i,j,3)
          end if
        end if
        !
        solctemp(i,j)=tbar(i,1)*fracarb(i,j,1) + &
                   tbar(i,2)*fracarb(i,j,2) + &
                   tbar(i,3)*fracarb(i,j,3)
        solctemp(i,j)=solctemp(i,j) / &
        (fracarb(i,j,1)+fracarb(i,j,2)+fracarb(i,j,3))
        !
        !         MAKE SURE WE DON'T USE TEMPERATURES OF 2nd AND 3rd SOIL LAYERS
        !         if they are specified bedrock via sand -3 flag
        !
        if (isand(i,3)==-3) then ! third layer bed rock
          solctemp(i,j)=tbar(i,1)*fracarb(i,j,1) + &
                     tbar(i,2)*fracarb(i,j,2)
          solctemp(i,j)=solctemp(i,j) / &
          (fracarb(i,j,1)+fracarb(i,j,2))
        end if
        if (isand(i,2)==-3) then ! second layer bed rock
          solctemp(i,j)=tbar(i,1)
        end if
      end if
    end do ! loop 240
  end do ! loop 230
  !
  !     find moisture scalar for soil c decomposition
  !
  !     this is modelled as function of logarithm of matric potential.
  !     we find values for all soil layers, and then find an average value
  !     based on fraction of carbon present in each layer. this makes
  !     moisture scalar a function of vegetation type.
  !
  do j = 1, ig
    do i = il1, il2
      !
      if (isand(i,j)==-3.or.isand(i,j)==-4) then
        scmotrm (i,j)=0.2
        psi (i,j) = 10000.0 ! set to large number so that
        !                               ! ltrmoscl becomes 0.2
      else ! i.e., sand/=-3 or -4
        psisat(i,j)= (10.0**(-0.0131*sand(i,j)+1.88))/100.0
        b(i,j)     = 0.159*clay(i,j)+2.91
        thpor(i,j) = (-0.126*sand(i,j)+48.9)/100.0
        psi(i,j)   = psisat(i,j)*(thliq(i,j)/thpor(i,j))**(-b(i,j))
        !
        if (psi(i,j)>10000.0) then
          scmotrm(i,j)=0.2
        else if (psi(i,j)<=10000.0 .and.  psi(i,j)>6.0) then
          scmotrm(i,j)=1.0 - 0.8* &
     ( (log10(psi(i,j)) - log10(6.0))/(log10(10000.0)-log10(6.0)) )
        else if (psi(i,j)<=6.0 .and. psi(i,j)>=4.0) then
          scmotrm(i,j)=1.0
        else if (psi(i,j)<4.0 .and. psi(i,j)>psisat(i,j) ) then
          scmotrm(i,j)=1.0 - &
           0.5*( (log10(4.0) - log10(psi(i,j))) / &
          (log10(4.0)-log10(psisat(i,j))) )
        else if (psi(i,j)<=psisat(i,j) ) then
          scmotrm(i,j)=0.5
        end if
        scmotrm(i,j)=max(0.2,min(1.0,scmotrm(i,j)))
      end if ! sand==-3 or -4
      !
    end do ! loop 270
  end do ! loop 260
  !
  do j = 1, icc
    do i = il1, il2
      if (fcan(i,j) > 0.) then
        socmoscl(i,j) = scmotrm(i,1)*fracarb(i,j,1) + &
                          scmotrm(i,2)*fracarb(i,j,2) + &
                          scmotrm(i,3)*fracarb(i,j,3)
        socmoscl(i,j) = socmoscl(i,j) / &
                          (fracarb(i,j,1)+fracarb(i,j,2)+fracarb(i,j,3))
        !
        !         MAKE SURE WE DON'T USE SCMOTRM OF 2nd AND 3rd SOIL LAYERS
        !         if they are specified bedrock via sand -3 flag
        !
        if (isand(i,3)==-3) then ! third layer bed rock
          socmoscl(i,j) = scmotrm(i,1)*fracarb(i,j,1) + &
                            scmotrm(i,2)*fracarb(i,j,2)
          socmoscl(i,j) = socmoscl(i,j) / &
                            (fracarb(i,j,1)+fracarb(i,j,2))
        end if
        if (isand(i,2)==-3) then ! second layer bed rock
          socmoscl(i,j) = scmotrm(i,1)
        end if
        socmoscl(i,j)=max(0.2,min(1.0,socmoscl(i,j)))
      end if
    end do ! loop 290
  end do ! loop 280
  !
  !     find moisture scalar for litter decomposition
  !
  !     the difference between moisture scalar for litter and soil c
  !     is that the litter decomposition is not constrained by high
  !     soil moisture (assuming that litter is always exposed to air).
  !     in addition, we use moisture content of the top soil layer
  !     as a surrogate for litter moisture content. so we use only
  !     psi(i,1) calculated in loops 260 and 270 above.
  !
  do i = il1, il2
    if (psi(i,1)>10000.0) then
      ltrmoscl(i)=0.2
    else if (psi(i,1)<=10000.0 .and. psi(i,1)>6.0) then
      ltrmoscl(i)=1.0 -  0.8* &
     ( (log10(psi(i,1)) - log10(6.0))/(log10(10000.0)-log10(6.0)) )
    else if (psi(i,1)<=6.0) then
      ltrmoscl(i)=1.0
    end if
    ltrmoscl(i)=max(0.2,min(1.0,ltrmoscl(i)))
  end do ! loop 300
  !
  !     use temperature of the litter and soil c pools, and their soil
  !     moisture scalars to find respiration rates from these pools
  !
  do j = 1, icc
    do i = il1, il2
      if (fcan(i,j) > 0.) then
        !
        !         first find the q10 response function to scale base respiration
        !         rate from 15 c to current temperature, we do litter first
        !
        tempq10l(i,j)=litrtemp(i,j)-273.16
        litrq10 = tanhq10(1) + tanhq10(2)* &
                    (tanh(tanhq10(3)*(tanhq10(4)-tempq10l(i,j))  ) )
        !
        q10func = litrq10**(0.1*(litrtemp(i,j)-273.16-15.0))
        ltresveg(i,j)= ltrmoscl(i) * litrmass(i,j)* &
                         bsratelt(sort(j))*2.64*q10func ! 2.64 converts bsratelt from kg c/kg c.year
        !                                          ! to u-mol co2/kg c.s
        !         respiration from soil c pool
        !
        tempq10s(i,j)=solctemp(i,j)-273.16
        soilcq10= tanhq10(1) + tanhq10(2)* &
                    (tanh(tanhq10(3)*(tanhq10(4)-tempq10s(i,j))  ) )
        !
        q10func = soilcq10**(0.1*(solctemp(i,j)-273.16-15.0))
        scresveg(i,j)= socmoscl(i,j)* soilcmas(i,j)* &
                         bsratesc(sort(j))*2.64*q10func  ! 2.64 converts bsratesc from kg c/kg c.year
        !                                           ! to u-mol co2/kg c.s
        !
      end if
    end do ! loop 330
  end do ! loop 320
  !
  return
end

