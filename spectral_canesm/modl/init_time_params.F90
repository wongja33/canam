!> \file init_time_params.F90
!> \brief Initialize various agcm time variable
!!
!! @author Clint Seinen

subroutine init_time_params(kstart, delt, iyear,  &
                            iday, lday, mday, mdayt, ndays, nsecs, imdh)
    ! notes:
    !       - iyear could be derived from kstart and delt, but as it is currently cooked into
    !           some other infrastructure we'll leave it here for now and just check for consistency
    !       - it appears the description for mday is NOT ACCURATE - within the AGCM driver, it is updated in lock step with iday.
    !           Work should be done to either remove mday if it is not used anymore, or its description updated.
    !           Inspection of many of the calls to routines that look to use mday actually just appear to use mdayt (see intranoz)
    !       - is mdayt really just for chemistry forcing?
    !       - the new routine `convert_dayofyear2monthday` is VERY similar to idatec, except that idatec returns a month 
    !           string. Work should be done to inspect the calls to idatec to see if it could be updated without 
    !           deleterious affects.
    
    use iso_fortran_env, only : STDERR => error_unit, STDOUT => output_unit
    implicit none
    !========
    ! Inputs
    !========
    real,    intent(in) :: delt     !< AGCM timestep \f$[seconds]\f$
    integer, intent(in) :: kstart   !< step counter that this run is starting from (from year 1) \f$[unitless]\f$
    integer, intent(in) :: iyear    !< Integer year counter \f$[years]\f$
    
    !=========
    ! Outputs
    !=========
    integer, intent(out) :: iday    !< day of the year \f$[days]\f$
    integer, intent(out) :: lday    !< first day of the next month \f$[days]\f$
    integer, intent(out) :: mday    !< STATED TO BE next mid-month day (gt, sic, sicn), but is generally just set to iday \f$[days]\f$
    integer, intent(out) :: mdayt   !< next mid-month day (chemistry forcing) \f$[days]\f$
    integer, intent(out) :: ndays   !< number of days since kount=0 \f$[days]\f$
    integer, intent(out) :: nsecs   !< number of seconds elapsed in current day \f$[seconds]\f$
    integer, intent(out) :: imdh    !< integer representation of the month-day date encoded as 'mmddhh' \f$[mmddhh]\f$

    !============
    ! Local Vars
    !============
    integer, parameter :: seconds_in_a_hour = 60*60
    integer, parameter :: days_in_a_year    = 365
    integer, parameter :: seconds_in_a_day  = 60*60*24
    integer, parameter :: seconds_in_a_year = 60*60*24*365
    integer :: date_year, date_month, date_day, date_hour, date_seconds
    integer :: day_of_year
    integer :: seconds_into_current_day
    integer :: number_of_years_passed
    real    :: days_since_kount0
    real    :: back_calcd_days_since_kount0
    real    :: delt_days            ! time-step in days
    real    :: days_into_current_year

    ! these are needed for dummy function arguments that aren't needed in this context
    integer :: dummy_var1, dummy_var2, dummy_var3

    ! determine date associated with kstart
    delt_days                   = real(delt)/real(seconds_in_a_day)
    days_since_kount0           = kstart*delt_days
    number_of_years_passed      = int(days_since_kount0/days_in_a_year)
    days_into_current_year      = mod( days_since_kount0, real(days_in_a_year) )    ! can include fractional days
    day_of_year                 = int(days_into_current_year) + 1                   ! days start at 1
    seconds_into_current_day    = nint(mod(days_into_current_year, 1.0)*seconds_in_a_day)

    ! back calculate days_since_kount0 to make sure no time was lost to roundoff
    back_calcd_days_since_kount0 = number_of_years_passed*days_in_a_year &
                                 + seconds_into_current_day/seconds_in_a_day &
                                 + (day_of_year-1)
    if ( back_calcd_days_since_kount0 /= days_since_kount0 ) then
        write(STDERR,*) "Problem in calculating date from kstart and delt!", kstart, delt
        write(STDERR,*) "   It appears time was lost to round off!"
        write(STDERR,*) "   Initial days_since_kount0:", days_since_kount0
        write(STDERR,*) "   back calculated days_since_kount0:", back_calcd_days_since_kount0
        call xit("init_time_params", -1)
    endif

    date_year    = number_of_years_passed + 1                   ! start from year 1
    call convert_dayofyear2monthday(day_of_year, date_month, date_day)
    date_hour    = seconds_into_current_day/seconds_in_a_hour
    date_seconds = mod( seconds_into_current_day, seconds_in_a_hour)

    ! make sure iyear is consistent with kstart
    if ( iyear /= date_year ) then
        write(STDERR,*) "iyear", iyear, "is not consistent with the year derived from kstart! ==> ", date_year
        call xit("init_time_params", -1)
    end if 
    
    ! assign output params
    iday  = day_of_year
    call getlday(lday, iday)
    mday  = iday
    call getmdayt(mdayt, dummy_var1, iday, dummy_var2, dummy_var3)
    ndays = number_of_years_passed*365 + day_of_year - 1
    nsecs = date_seconds
    imdh  = 10000*date_month + 100*date_day + date_hour
end subroutine init_time_params
