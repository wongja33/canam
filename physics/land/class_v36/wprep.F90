subroutine wprep(thlqco, thlqgo, thlqcs, thlqgs, thicco, thicgo, &
                       thiccs, thicgs, hcpco,  hcpgo,  hcpcs,  hcpgs, &
                       grksc,  grksg,  grkscs, grksgs, &
                       spcc,   spcg,   spccs,  spcgs,  tspcc,  tspcg, &
                       tspccs, tspcgs, rpcc,   rpcg,   rpccs,  rpcgs, &
                       trpcc,  trpcg,  trpccs, trpcgs, evpic,  evpig, &
                       evpics, evpigs, zpondc, zpondg, zpndcs, zpndgs, &
                       xsnowc, xsnowg, xsnocs, xsnogs, zsnowc, zsnowg, &
                       zsnocs, zsnogs, albsc,  albsg,  albscs, albsgs, &
                       rhosc,  rhosg,  hcpsc,  hcpsg,  hcpscs, hcpsgs, &
                       runfc,  runfg,  runfcs, runfgs, &
                       trunfc, trunfg, trnfcs, trnfgs, tbasc,  tbasg, &
                       tbascs, tbasgs, gflxc,  gflxg,  gflxcs, gflxgs, &
                       sublc,  sublcs, wlostc, wlostg, wlstcs, wlstgs, &
                       rac,    racs,   snc,    sncs,   tsnowc, tsnowg, &
                       ovrflw, subflw, basflw, tovrfl, tsubfl, tbasfl, &
                       pcfc,   pclc,   pcpn,   pcpg,   qfcf,   qfcl, &
                       qfn,    qfg,    qfc,    hmfg, &
                       rovg,   rofc,   rofn,   trunof, &
                       thliqx, thicex, thldum, thidum, &
                       dt,     rdummy, zero,   izero,  delzz, &
                       fc,     fg,     fcs,    fgs, &
                       thliqc, thliqg, thicec, thiceg, hcpc,   hcpg, &
                       tbarc,  tbarg,  tbarcs, tbargs, tbase,  tsurx, &
                       fsvf,   fsvfs,  raican, snocan, raicns, snocns, &
                       evapc,  evapcg, evapg,  evapcs, evpcsg, evapgs, &
                       rpcp,   trpcp,  spcp,   tspcp,  rhosni, zpond, &
                       zsnow,  albsno, wsnocs, wsnogs, rhoscs, rhosgs, &
                       thpor,  hcps,   grksat, isand,  delzw,  delz, &
                       ilg,    il1,    il2,    jl,     ig,     igp1, &
                       nlandcs,nlandgs,nlandc, nlandg, radd,   sadd)
  !
  !     * aug 25/11 - d.verseghy. refine calculation of temperature of
  !     *                         lumped precipitation.
  !     * nov 24/09 - d.verseghy. restore evapotranspiration when
  !     *                         precipitation is occurring.
  !     * mar 27/08 - d.verseghy. move modification of grksat in presence
  !     *                         of ice to grinfl and grdran.
  !     * feb 19/07 - d.verseghy. modifications to reflect shift of canopy
  !     *                         water deposition calculations to tsolvc,
  !     *                         and suppression of all evapotranspiration
  !     *                         when precipitation is occurring.
  !     * mar 23/06 - d.verseghy. modify calculations of snow thermal
  !     *                         properties to account for water content.
  !     * mar 21/06 - p.bartlett. initialize additional variables to zero.
  !     * dec 07/05 - d.verseghy. add initialization of tbase subareas.
  !     * oct 05/05 - d.verseghy. modify delzz calculation for ig>3.
  !     * apr 15/05 - d.verseghy. sublimation of intercepted snow takes
  !     *                         place before evaporation of intercepted
  !     *                         rain.
  !     * mar 30/05 - d.verseghy/r.soulis. add runoff temperature
  !     *                         initializations and modification of
  !     *                         grksat for temperature and presence
  !     *                         of ice.
  !     * sep 13/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 21/04 - y.delage, r.harvey, d.verseghy. new lower limit
  !     *                         on radd and sadd.
  !     * sep 26/02 - d.verseghy. modifications associated with bugfix
  !     *                         in subcan.
  !     * aug 06/02 - d.verseghy. shortened class3 common block.
  !     * jun 18/02 - d.verseghy. move partitioning of precipitation
  !     *                         between rainfall and snowfall into
  !     *                         "CLASSI"; tidy up subroutine call
  !     *                         change rhosni from constant to
  !     *                         variable.
  !     * oct 04/01 - m.lazare.   new diagnostic field "ROVG".
  !     * nov 09/00 - d.verseghy. move diagnostic calculations from
  !     *                         subcan into this routine.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         changes related to variable soil depth
  !     *                         (moisture holding capacity) and depth-
  !     *                         varying soil properties.
  !     * jan 02/95 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics; introduce calculation of
  !     *                         overland flow.
  !     * aug 24/95 - d.verseghy. class - version 2.4.
  !     *                         rationalize use of "WLOST":
  !     *                         completion of water budget diagnostics.
  !     * aug 18/95 - d.verseghy. revisions to allow for inhomogeneity
  !     *                         between soil layers and fractional
  !     *                         organic matter content.
  !     * dec 16/94 - d.verseghy. class - version 2.3.
  !     *                         initialize two new diagnostic fields.
  !     * aug 20/93 - d.verseghy. class - version 2.2.
  !     *                         revised calculation of canopy
  !     *                         sublimation rate.
  !     * jul 30/93 - d.verseghy/m.lazare. new diagnostic fields.
  !     * apr 15/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. preparation and initialization for
  !     *                         land surface water budget calculations.

  use times_mod, only : delt
  use hydcon, only : tfrez, hcpw, hcpice, rhow, rhoice
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: igp1 !<
  integer, intent(in) :: nlandcs !<
  integer, intent(in) :: nlandgs !<
  integer, intent(in) :: nlandc !<
  integer, intent(in) :: nlandg !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: thlqco(ilg,ig) !<
  real, intent(inout) :: thlqgo(ilg,ig) !<
  real, intent(inout) :: thlqcs(ilg,ig) !<
  real, intent(inout) :: thlqgs(ilg,ig) !<
  real, intent(inout) :: thicco(ilg,ig) !<
  real, intent(inout) :: thicgo(ilg,ig) !<
  real, intent(inout) :: thiccs(ilg,ig) !<
  real, intent(inout) :: thicgs(ilg,ig) !<
  real, intent(inout) :: hcpco (ilg,ig) !<
  real, intent(inout) :: hcpgo (ilg,ig) !<
  real, intent(inout) :: hcpcs (ilg,ig) !<
  real, intent(inout) :: hcpgs (ilg,ig) !<
  real, intent(inout) :: grksc (ilg,ig) !<
  real, intent(inout) :: grksg (ilg,ig) !<
  real, intent(inout) :: grkscs(ilg,ig) !<
  real, intent(inout) :: grksgs(ilg,ig) !<
  real, intent(inout) :: gflxc (ilg,ig) !<
  real, intent(inout) :: gflxg (ilg,ig) !<
  real, intent(inout) :: gflxcs(ilg,ig) !<
  real, intent(inout) :: gflxgs(ilg,ig) !<
  real, intent(inout) :: thldum(ilg,ig) !<
  real, intent(inout) :: thidum(ilg,ig) !<
  real, intent(inout) :: qfc   (ilg,ig) !<
  real, intent(inout) :: hmfg  (ilg,ig) !<
  !
  real, intent(inout) :: thliqx(ilg,igp1) !<
  real, intent(inout) :: thicex(ilg,igp1) !<
  !
  real, intent(inout) :: spcc  (ilg) !<
  real, intent(inout) :: spcg  (ilg) !<
  real, intent(inout) :: spccs (ilg) !<
  real, intent(inout) :: spcgs (ilg) !<
  real, intent(inout) :: tspcc (ilg) !<
  real, intent(inout) :: tspcg (ilg) !<
  real, intent(inout) :: tspccs(ilg) !<
  real, intent(inout) :: tspcgs(ilg) !<
  real, intent(inout) :: rpcc  (ilg) !<
  real, intent(inout) :: rpcg  (ilg) !<
  real, intent(inout) :: rpccs (ilg) !<
  real, intent(inout) :: rpcgs (ilg) !<
  real, intent(inout) :: trpcc (ilg) !<
  real, intent(inout) :: trpcg (ilg) !<
  real, intent(inout) :: trpccs(ilg) !<
  real, intent(inout) :: trpcgs(ilg) !<
  real, intent(inout) :: evpic (ilg) !<
  real, intent(inout) :: evpig (ilg) !<
  real, intent(inout) :: evpics(ilg) !<
  real, intent(inout) :: evpigs(ilg) !<
  real, intent(inout) :: zpondc(ilg) !<
  real, intent(inout) :: zpondg(ilg) !<
  real, intent(inout) :: zpndcs(ilg) !<
  real, intent(inout) :: zpndgs(ilg) !<
  real, intent(inout) :: xsnowc(ilg) !<
  real, intent(inout) :: xsnowg(ilg) !<
  real, intent(inout) :: xsnocs(ilg) !<
  real, intent(inout) :: xsnogs(ilg) !<
  real, intent(inout) :: zsnowc(ilg) !<
  real, intent(inout) :: zsnowg(ilg) !<
  real, intent(inout) :: zsnocs(ilg) !<
  real, intent(inout) :: zsnogs(ilg) !<
  real, intent(inout) :: albsc (ilg) !<
  real, intent(inout) :: albsg (ilg) !<
  real, intent(inout) :: albscs(ilg) !<
  real, intent(inout) :: albsgs(ilg) !<
  real, intent(inout) :: rhosc (ilg) !<
  real, intent(inout) :: rhosg (ilg) !<
  real, intent(inout) :: hcpsc (ilg) !<
  real, intent(inout) :: hcpsg (ilg) !<
  real, intent(inout) :: hcpscs(ilg) !<
  real, intent(inout) :: hcpsgs(ilg) !<
  real, intent(inout) :: runfc (ilg) !<
  real, intent(inout) :: runfg (ilg) !<
  real, intent(inout) :: runfcs(ilg) !<
  real, intent(inout) :: runfgs(ilg) !<
  real, intent(inout) :: trunfc(ilg) !<
  real, intent(inout) :: trunfg(ilg) !<
  real, intent(inout) :: trnfcs(ilg) !<
  real, intent(inout) :: trnfgs(ilg) !<
  real, intent(inout) :: tbasc (ilg) !<
  real, intent(inout) :: tbasg (ilg) !<
  real, intent(inout) :: tbascs(ilg) !<
  real, intent(inout) :: tbasgs(ilg) !<
  !
  real, intent(inout) :: sublc (ilg) !<
  real, intent(inout) :: sublcs(ilg) !<
  real, intent(inout) :: wlostc(ilg) !<
  real, intent(inout) :: wlostg(ilg) !<
  real, intent(inout) :: wlstcs(ilg) !<
  real, intent(inout) :: wlstgs(ilg) !<
  real, intent(inout) :: rac   (ilg) !<
  real, intent(inout) :: racs  (ilg) !<
  real, intent(inout) :: snc   (ilg) !<
  real, intent(inout) :: sncs  (ilg) !<
  real, intent(inout) :: tsnowc(ilg) !<
  real, intent(inout) :: tsnowg(ilg) !<
  real, intent(inout) :: ovrflw(ilg) !<
  real, intent(inout) :: subflw(ilg) !<
  real, intent(inout) :: basflw(ilg) !<
  real, intent(inout) :: tovrfl(ilg) !<
  real, intent(inout) :: tsubfl(ilg) !<
  real, intent(inout) :: tbasfl(ilg) !<
  real, intent(inout) :: pcfc  (ilg) !<
  real, intent(inout) :: pclc  (ilg) !<
  real, intent(inout) :: pcpn  (ilg) !<
  real, intent(inout) :: pcpg  (ilg) !<
  real, intent(inout) :: qfcf  (ilg) !<
  real, intent(inout) :: qfcl  (ilg) !<
  real, intent(inout) :: qfn   (ilg) !<
  real, intent(inout) :: qfg   (ilg) !<
  real, intent(inout) :: rovg  (ilg) !<
  real, intent(inout) :: rofc  (ilg) !<
  real, intent(inout) :: rofn  (ilg) !<
  real, intent(inout) :: trunof(ilg) !<
  real, intent(inout) :: dt    (ilg) !<
  real, intent(inout) :: rdummy(ilg) !<
  real, intent(inout) :: zero  (ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: izero !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fc    (ilg) !<
  real, intent(in) :: fg    (ilg) !<
  real, intent(in) :: fcs   (ilg) !<
  real, intent(in) :: fgs   (ilg) !<
  real, intent(in) :: fsvf  (ilg) !<
  real, intent(in) :: fsvfs (ilg) !<
  real, intent(in) :: raican(ilg) !<
  real, intent(in) :: snocan(ilg) !<
  real, intent(in) :: raicns(ilg) !<
  real, intent(in) :: snocns(ilg) !<
  real, intent(inout) :: evapc (ilg) !<
  real, intent(inout) :: evapcg(ilg) !<
  real, intent(inout) :: evapg (ilg) !<
  real, intent(inout) :: evapcs(ilg) !<
  real, intent(in) :: evpcsg(ilg) !<
  real, intent(inout) :: evapgs(ilg) !<
  real, intent(in) :: rpcp  (ilg) !<
  real, intent(in) :: trpcp (ilg) !<
  real, intent(in) :: spcp  (ilg) !<
  real, intent(in) :: tspcp (ilg) !<
  real, intent(in) :: rhosni(ilg) !<
  real, intent(in) :: zpond (ilg) !<
  real, intent(in) :: zsnow (ilg) !<
  real, intent(in) :: albsno(ilg) !<
  real, intent(in) :: wsnocs(ilg) !<
  real, intent(in) :: wsnogs(ilg) !<
  real, intent(inout) :: rhoscs(ilg) !<
  real, intent(inout) :: rhosgs(ilg) !<
  real, intent(in) :: tbase (ilg) !<
  real, intent(in) :: tsurx(ilg,4) !<
  !
  real, intent(in) :: thliqc(ilg,ig) !<
  real, intent(in) :: thliqg(ilg,ig) !<
  real, intent(in) :: thicec(ilg,ig) !<
  real, intent(in) :: thiceg(ilg,ig) !<
  real, intent(in) :: tbarc (ilg,ig) !<
  real, intent(in) :: tbarg (ilg,ig) !<
  real, intent(in) :: tbarcs(ilg,ig) !<
  real, intent(in) :: tbargs(ilg,ig) !<
  real, intent(in) :: hcpc  (ilg,ig) !<
  real, intent(in) :: hcpg  (ilg,ig) !<
  !
  !     * soil information arrays.
  !
  real, intent(in) :: thpor (ilg,ig) !<
  real, intent(in) :: hcps  (ilg,ig) !<
  real, intent(in) :: grksat(ilg,ig) !<
  real, intent(inout) :: delzz (ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  real, intent(in) :: delz  (ig) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * internal work arrays.
  !
  real, intent(inout) :: radd  (ilg) !<
  real, intent(inout) :: sadd  (ilg) !<
  !
  integer :: i !<
  integer :: j !<
  !-----------------------------------------------------------------------
  !     * initialize 2-d arrays.
  !
  do j=1,ig
    do i=il1,il2
      thlqco(i,j)=0.0
      thlqgo(i,j)=0.0
      thlqcs(i,j)=0.0
      thlqgs(i,j)=0.0
      thicco(i,j)=0.0
      thicgo(i,j)=0.0
      thiccs(i,j)=0.0
      thicgs(i,j)=0.0
      hcpco (i,j)=0.0
      hcpgo (i,j)=0.0
      hcpcs (i,j)=0.0
      hcpgs (i,j)=0.0
      grksc (i,j)=0.0
      grksg (i,j)=0.0
      grkscs(i,j)=0.0
      grksgs(i,j)=0.0
      gflxc (i,j)=0.0
      gflxg (i,j)=0.0
      gflxcs(i,j)=0.0
      gflxgs(i,j)=0.0
      qfc   (i,j)=0.0
      hmfg  (i,j)=0.0
      thldum(i,j)=0.0
      thidum(i,j)=0.0
      if (j==3.and.ig==3) then
        delzz (i,j)=delzw(i,j)
      else
        delzz (i,j)=delz(j)
      end if
    end do
  end do ! loop 50
  !
  do j=1,igp1
    do i=il1,il2
      thliqx(i,j)=0.0
      thicex(i,j)=0.0
    end do
  end do ! loop 75
  !
  !     * initialize other diagnostic and work arrays.
  !
  do i=il1,il2
    evpics(i)=evapcs(i)+evpcsg(i)
    evpigs(i)=evapgs(i)
    evpic (i)=evapc (i)+evapcg(i)
    evpig (i)=evapg (i)
    tsnowc(i)=0.0
    tsnowg(i)=0.0
    wlostc(i)=0.0
    wlostg(i)=0.0
    wlstcs(i)=0.0
    wlstgs(i)=0.0
    rac   (i)=raican(i)
    racs  (i)=raicns(i)
    snc   (i)=snocan(i)
    sncs  (i)=snocns(i)
    pcfc  (i)=0.0
    pclc  (i)=0.0
    pcpn  (i)=0.0
    pcpg  (i)=0.0
    qfn   (i)=0.0
    qfg   (i)=0.0
    rovg  (i)=0.0
    rofc  (i)=0.0
    rofn  (i)=0.0
    ovrflw(i)=0.0
    subflw(i)=0.0
    basflw(i)=0.0
    tovrfl(i)=0.0
    tsubfl(i)=0.0
    tbasfl(i)=0.0
    zpondc(i)=0.0
    zpondg(i)=0.0
    zpndcs(i)=0.0
    zpndgs(i)=0.0
    xsnowc(i)=0.0
    xsnowg(i)=0.0
    xsnocs(i)=0.0
    xsnogs(i)=0.0
    zsnowc(i)=0.0
    zsnowg(i)=0.0
    zsnocs(i)=0.0
    zsnogs(i)=0.0
    albsc (i)=0.0
    albsg (i)=0.0
    albscs(i)=0.0
    albsgs(i)=0.0
    rhosc (i)=0.0
    rhosg (i)=0.0
    hcpsc (i)=0.0
    hcpsg (i)=0.0
    hcpscs(i)=0.0
    hcpsgs(i)=0.0
    runfc (i)=0.0
    runfg (i)=0.0
    runfcs(i)=0.0
    runfgs(i)=0.0
    trunfc(i)=0.0
    trunfg(i)=0.0
    trnfcs(i)=0.0
    trnfgs(i)=0.0
    trunof(i)=0.0
    tbasc (i)=tbase(i)-tfrez
    tbasg (i)=tbase(i)-tfrez
    tbascs(i)=tbase(i)-tfrez
    tbasgs(i)=tbase(i)-tfrez
    dt    (i)=delt
    rdummy(i)=0.
    zero  (i)=0.
    izero (i)=0
    !
    !     * precipitation diagnostics.
    !
    if (rpcp(i)>0.) then
      pclc(i)=(fcs(i)*(1.0-fsvfs(i))+fc(i)*(1.0-fsvf(i)))* &
                 rpcp(i)*rhow
      pcpn(i)=(fcs(i)*fsvfs(i)+fgs(i))*rpcp(i)*rhow
      pcpg(i)=(fc(i)*fsvf(i)+fg(i))*rpcp(i)*rhow
    end if
    !
    if (spcp(i)>0.) then
      pcfc(i)=(fcs(i)*(1.0-fsvfs(i))+fc(i)*(1.0-fsvf(i)))* &
                 spcp(i)*rhosni(i)
      pcpn(i)=pcpn(i)+(fcs(i)*fsvfs(i)+fgs(i)+ &
                 fc(i)*fsvf(i)+fg(i))*spcp(i)*rhosni(i)
    end if
  end do ! loop 100
  !
  !     * rainfall/snowfall rates and other initialization procedures
  !     * over grid cell subareas. downward water fluxes are lumped
  !     * together with precipitation, and upward and downward water
  !     * fluxes cancel out. correction made to soil saturated hydraulic
  !     * conductivity for water viscosity effects.
  !
  !     * calculations for canopy over snow.
  !
  if (nlandcs>0) then
    !
    do j=1,ig
      do i=il1,il2
        if (fcs(i)>0.) then
          thlqcs(i,j)=thliqc(i,j)
          thiccs(i,j)=thicec(i,j)
          hcpcs (i,j)=hcpc  (i,j)
          if (thpor(i,j)>0.0001) then
            grkscs(i,j)=grksat(i,j)*(1.7915e-03/ &
                     (2.0319e-04+1.5883e-03*exp(-((max(0.0, &
                     min(100.,tbarcs(i,j)))**0.9)/22.))))
          else
            grkscs(i,j)=grksat(i,j)
          end if
        end if
      end do
    end do ! loop 200
    !
    do i=il1,il2
      if (fcs(i)>0.) then
        if (snocns(i)>0.) then
          sublcs(i)=evapcs(i)
          evapcs(i)=0.0
        else
          sublcs(i)=0.0
        end if
        if (sublcs(i)>0.0) then
          qfcf(i)=qfcf(i)+fcs(i)*sublcs(i)*rhow
        else
          qfcf(i)=qfcf(i)+fcs(i)*(1.0-fsvfs(i))*sublcs(i)* &
                         rhow
          qfn(i)=qfn(i)+fcs(i)*fsvfs(i)*sublcs(i)*rhow
        end if
        if (evapcs(i)>0.0) then
          qfcl(i)=qfcl(i)+fcs(i)*evapcs(i)*rhow
        else
          qfcl(i)=qfcl(i)+fcs(i)*(1.0-fsvfs(i))*evapcs(i)* &
                         rhow
          qfn(i)=qfn(i)+fcs(i)*fsvfs(i)*evapcs(i)*rhow
        end if
        !
        if (spcp(i)>0. .or. sublcs(i)<0.) then
          sadd(i)=spcp(i)-sublcs(i)*rhow/rhosni(i)
          if (abs(sadd(i))<1.0e-12) sadd(i)=0.0
          if (sadd(i)>0.0) then
            if (sublcs(i)>0.) then
              qfcf(i)=qfcf(i)-fcs(i)*fsvfs(i)* &
                                 sublcs(i)*rhow
              qfn(i)=qfn(i)+fcs(i)*fsvfs(i)* &
                                 sublcs(i)*rhow
            end if
            spccs (i)=sadd(i)
            if (spcp(i)>0.0) then
              tspccs(i)=tspcp(i)+tfrez
            else
              tspccs(i)=min(tsurx(i,1),tfrez)
            end if
            sublcs(i)=0.0
          else
            pcpn(i)=pcpn(i)-fcs(i)*fsvfs(i)*spcp(i)* &
                         rhosni(i)
            pcfc(i)=pcfc(i)+fcs(i)*fsvfs(i)*spcp(i)* &
                         rhosni(i)
            sublcs(i)=-sadd(i)*rhosni(i)/rhow
            spccs (i)=0.0
            tspccs(i)=0.0
          end if
        else
          spccs(i)=0.0
          tspccs(i)=0.0
        end if
        !
        if (rpcp(i)>0. .or. evapcs(i)<0.) then
          radd(i)=rpcp(i)-evapcs(i)
          if (abs(radd(i))<1.0e-12) radd(i)=0.0
          if (radd(i)>0.) then
            if (evapcs(i)>0.) then
              qfcl(i)=qfcl(i)-fcs(i)*fsvfs(i)* &
                                 evapcs(i)*rhow
              qfn(i)=qfn(i)+fcs(i)*fsvfs(i)* &
                                 evapcs(i)*rhow
            end if
            rpccs (i)=radd(i)
            if (rpcp(i)>0.0) then
              trpccs(i)=trpcp(i)+tfrez
            else
              trpccs(i)=max(tsurx(i,1),tfrez)
            end if
            evapcs(i)=0.0
          else
            pcpn(i)=pcpn(i)-fcs(i)*fsvfs(i)*rpcp(i)*rhow
            pclc(i)=pclc(i)+fcs(i)*fsvfs(i)*rpcp(i)*rhow
            evapcs(i)=-radd(i)
            rpccs (i)=0.0
            trpccs(i)=0.0
          end if
        else
          rpccs(i)=0.0
          trpccs(i)=0.0
        end if
        zpndcs(i)=zpond (i)
        zsnocs(i)=zsnow (i)
        albscs(i)=albsno(i)
        hcpscs(i)=hcpice*rhoscs(i)/rhoice+hcpw*wsnocs(i)/ &
                       (rhow*zsnocs(i))
        qfn   (i)=qfn(i)+fcs(i)*evpcsg(i)*rhow
      end if
    end do ! loop 250
  end if
  !
  !     * calculations for snow-covered ground.
  !
  if (nlandgs>0) then
    !
    do j=1,ig
      do i=il1,il2
        if (fgs(i)>0.) then
          thlqgs(i,j)=thliqg(i,j)
          thicgs(i,j)=thiceg(i,j)
          hcpgs (i,j)=hcpg  (i,j)
          if (thpor(i,j)>0.0001) then
            grksgs(i,j)=grksat(i,j)*(1.7915e-03/ &
                     (2.0319e-04+1.5883e-03*exp(-((max(0.0, &
                     min(100.,tbargs(i,j)))**0.9)/22.))))
          else
            grksgs(i,j)=grksat(i,j)
          end if
        end if
      end do
    end do ! loop 300
    !
    do i=il1,il2
      if (fgs(i)>0.) then
        qfn(i)=qfn(i)+fgs(i)*evapgs(i)*rhow
        if (spcp(i)>0. .or. evapgs(i)<0.) then
          sadd(i)=spcp(i)-evapgs(i)*rhow/rhosni(i)
          if (abs(sadd(i))<1.0e-12) sadd(i)=0.0
          if (sadd(i)>0.0) then
            spcgs (i)=sadd(i)
            if (spcp(i)>0.0) then
              tspcgs(i)=tspcp(i)
            else
              tspcgs(i)=min((tsurx(i,2)-tfrez),0.0)
            end if
            evapgs(i)=0.0
          else
            evapgs(i)=-sadd(i)*rhosni(i)/rhow
            spcgs (i)=0.0
            tspcgs(i)=0.0
          end if
        else
          spcgs (i)=0.0
          tspcgs(i)=0.0
        end if
        !
        if (rpcp(i)>0.) then
          radd(i)=rpcp(i)-evapgs(i)
          if (abs(radd(i))<1.0e-12) radd(i)=0.0
          if (radd(i)>0.) then
            rpcgs (i)=radd(i)
            trpcgs(i)=trpcp(i)
            evapgs(i)=0.0
          else
            evapgs(i)=-radd(i)
            rpcgs (i)=0.0
            trpcgs(i)=0.0
          end if
        else
          rpcgs (i)=0.0
          trpcgs(i)=0.0
        end if
        zpndgs(i)=zpond (i)
        zsnogs(i)=zsnow (i)
        albsgs(i)=albsno(i)
        hcpsgs(i)=hcpice*rhosgs(i)/rhoice+hcpw*wsnogs(i)/ &
                       (rhow*zsnogs(i))
      end if
    end do ! loop 350
  end if
  !
  !     * calculations for canopy over bare ground.
  !
  if (nlandc>0) then
    !
    do j=1,ig
      do i=il1,il2
        if (fc(i)>0.) then
          thlqco(i,j)=thliqc(i,j)
          thicco(i,j)=thicec(i,j)
          hcpco (i,j)=hcpc  (i,j)
          if (thpor(i,j)>0.0001) then
            grksc (i,j)=grksat(i,j)*(1.7915e-03/ &
                     (2.0319e-04+1.5883e-03*exp(-((max(0.0, &
                     min(100.,tbarc(i,j)))**0.9)/22.))))
          else
            grksc (i,j)=grksat(i,j)
          end if
        end if
      end do
    end do ! loop 400
    !
    do i=il1,il2
      if (fc(i)>0.) then
        if (snocan(i)>0.) then
          sublc(i)=evapc(i)
          evapc(i)=0.0
        else
          sublc(i)=0.0
        end if
        if (sublc(i)>0.0) then
          qfcf(i)=qfcf(i)+fc(i)*sublc(i)*rhow
        else
          qfcf(i)=qfcf(i)+fc(i)*(1.0-fsvf(i))*sublc(i)* &
                         rhow
          qfn(i)=qfn(i)+fc(i)*fsvf(i)*sublc(i)*rhow
        end if
        if (evapc(i)>0.0) then
          qfcl(i)=qfcl(i)+fc(i)*evapc(i)*rhow
        else
          qfcl(i)=qfcl(i)+fc(i)*(1.0-fsvf(i))*evapc(i)* &
                         rhow
          qfg(i)=qfg(i)+fc(i)*fsvf(i)*evapc(i)*rhow
        end if
        !
        if (spcp(i)>0. .or. sublc(i)<0.) then
          sadd(i)=spcp(i)-sublc(i)*rhow/rhosni(i)
          if (abs(sadd(i))<1.0e-12) sadd(i)=0.0
          if (sadd(i)>0.0) then
            if (sublc(i)>0.) then
              qfcf(i)=qfcf(i)-fc(i)*fsvf(i)*sublc(i)* &
                                 rhow
              qfn(i)=qfn(i)+fc(i)*fsvf(i)*sublc(i)* &
                                 rhow
            end if
            spcc  (i)=sadd(i)
            if (spcp(i)>0.0) then
              tspcc (i)=tspcp(i)+tfrez
            else
              tspcc(i)=min(tsurx(i,3),tfrez)
            end if
            sublc (i)=0.0
          else
            pcpn(i)=pcpn(i)-fc(i)*fsvf(i)*spcp(i)* &
                         rhosni(i)
            pcfc(i)=pcfc(i)+fc(i)*fsvf(i)*spcp(i)* &
                         rhosni(i)
            sublc (i)=-sadd(i)*rhosni(i)/rhow
            spcc  (i)=0.0
            tspcc (i)=0.0
          end if
        else
          spcc  (i)=0.0
          tspcc (i)=0.0
        end if
        !
        if (rpcp(i)>0. .or. evapc(i)<0.) then
          radd(i)=rpcp(i)-evapc(i)
          if (abs(radd(i))<1.0e-12) radd(i)=0.0
          if (radd(i)>0.) then
            if (evapc(i)>0.) then
              qfcl(i)=qfcl(i)-fc(i)*fsvf(i)*evapc(i)* &
                                 rhow
              qfg(i)=qfg(i)+fc(i)*fsvf(i)*evapc(i)* &
                                 rhow
            end if
            rpcc  (i)=radd(i)
            if (rpcp(i)>0.0) then
              trpcc (i)=trpcp(i)+tfrez
            else
              trpcc (i)=max(tsurx(i,3),tfrez)
            end if
            evapc (i)=0.0
          else
            pcpg(i)=pcpg(i)-fc(i)*fsvf(i)*rpcp(i)*rhow
            pclc(i)=pclc(i)+fc(i)*fsvf(i)*rpcp(i)*rhow
            evapc (i)=-radd(i)
            rpcc  (i)=0.0
            trpcc (i)=0.0
          end if
        else
          rpcc  (i)=0.0
          trpcc (i)=0.0
        end if
        zpondc(i)=zpond (i)
        zsnowc(i)=0.
        rhosc (i)=0.
        hcpsc (i)=0.
        qfg   (i)=qfg(i)+fc(i)*evapcg(i)*rhow
      end if
    end do ! loop 450
  end if
  !
  !     * calculations for bare ground.
  !
  if (nlandg>0) then
    !
    do j=1,ig
      do i=il1,il2
        if (fg(i)>0.) then
          thlqgo(i,j)=thliqg(i,j)
          thicgo(i,j)=thiceg(i,j)
          hcpgo (i,j)=hcpg  (i,j)
          if (thpor(i,j)>0.0001) then
            grksg (i,j)=grksat(i,j)*(1.7915e-03/ &
                     (2.0319e-04+1.5883e-03*exp(-((max(0.0, &
                     min(100.,tbarg(i,j)))**0.9)/22.))))
          else
            grksg (i,j)=grksat(i,j)
          end if
        end if
      end do
    end do ! loop 500
    !
    do i=il1,il2
      if (fg(i)>0.) then
        qfg(i)=qfg(i)+fg(i)*evapg(i)*rhow
        if (spcp(i)>0.) then
          sadd(i)=spcp(i)-evapg(i)*rhow/rhosni(i)
          if (abs(sadd(i))<1.0e-12) sadd(i)=0.0
          if (sadd(i)>0.0) then
            qfn(i)=qfn(i)+fg(i)*evapg(i)*rhow
            qfg(i)=qfg(i)-fg(i)*evapg(i)*rhow
            spcg  (i)=sadd(i)
            tspcg (i)=tspcp(i)
            evapg (i)=0.0
          else
            pcpn(i)=pcpn(i)-fg(i)*spcp(i)*rhosni(i)
            pcpg(i)=pcpg(i)+fg(i)*spcp(i)*rhosni(i)
            evapg (i)=-sadd(i)*rhosni(i)/rhow
            spcg  (i)=0.0
            tspcg (i)=0.0
          end if
        else
          spcg  (i)=0.0
          tspcg (i)=0.0
        end if
        !
        if (rpcp(i)>0. .or. evapg(i)<0.) then
          radd(i)=rpcp(i)-evapg(i)
          if (abs(radd(i))<1.0e-12) radd(i)=0.0
          if (radd(i)>0.) then
            rpcg  (i)=radd(i)
            if (rpcp(i)>0.0) then
              trpcg (i)=trpcp(i)
            else
              trpcg (i)=max((tsurx(i,4)-tfrez),0.0)
            end if
            evapg (i)=0.0
          else
            evapg (i)=-radd(i)
            rpcg  (i)=0.0
            trpcg (i)=0.0
          end if
        else
          rpcg (i)=0.0
          trpcg(i)=0.0
        end if
        zpondg(i)=zpond (i)
        zsnowg(i)=0.
        rhosg (i)=0.
        hcpsg (i)=0.
      end if
    end do ! loop 550
  end if
  !
  return
end subroutine wprep
