!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tstamp(kount,delt,iyr,imn,idy,ihr,iymdh)
  implicit none
  real, intent(in) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  integer :: itspd
  integer :: itsph
  integer :: itspy
  integer, intent(inout) :: iymdh !< Variable description\f$[units]\f$
  integer, intent(in) :: kount   !< Current model timestep \f$[unitless]\f$
  integer :: iyearst !<
  integer, intent(inout) :: iyr !< Variable description\f$[units]\f$
  integer, intent(inout) :: imn !< Variable description\f$[units]\f$
  integer, intent(inout) :: idy !< Variable description\f$[units]\f$
  integer, intent(inout) :: ihr !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer :: kyears !<
  integer :: kmonths !<
  integer :: kdays !<
  integer :: khours !<
  integer :: month(365) !<
  integer :: mthbnd(12) !<

  data month/31*1,28*2,31*3,30*4,31*5,30*6,31*7,31*8,30*9, &
      31*10,30*11,31*12/
  data mthbnd/0,31,59,90,120,151,181,212,243,273,304,334/

  integer :: daysec !<
  integer :: yearsec !<
  integer :: idelt !<
  data daysec/86400/

  idelt=nint(delt)
  itspy=365*daysec/idelt
  itspd=daysec/idelt
  itsph=3600/idelt

  kyears=kount/itspy
  kdays = (kount-kyears*itspy)/itspd
  khours= (kount-kyears*itspy-kdays*itspd)/itsph

  iyr=kyears+1
  imn=month(kdays+1)
  idy=kdays-mthbnd(imn)+1
  ihr=khours

  !     * in 32-bit this works only until year 2147
  iymdh=iyr*1000000+imn*10000+idy*100+ihr


  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
