#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * jul 30, 2018 - m.lazare.     remove non-transient cpp if blocks.
!     * jun 11, 2018 - m.lazare.     read from unit number nuch ("llchem" file)
!     *                              instead of nugg for non-transient chemistry
!     *                              forcing (several calls). also read from unit
!     *                              number nuox ("oxi" file) for non-transient
!     *                              oxidants in getchem4 (subroutine also changed).
!     * nov 19, 2012 - k.vonsalzen.  new version for gcm17+:
!     *                              clean-up and merging with pla code.
!     * may 10, 2012 - k.vonsalzen/  previous version getemip for gcm16:
!     *                y.peng/       - revised call to new getchem4.
!     *                m.lazare.     - rationalize use of mdayt.
!     *                                this is now calculated outside
!     *                                of this common deck and
!     *                                used in new versions: {getchem4,
!     *                                getcac2,getwildf2,getwfh2,getach2}
!     *                                where mdayt is no longer calculated.
!     * apr 27, 2010 - k.vonsalzen. previous version getemi for gcm15i.
!     * input of tracer emissions.
!--------------------------------------------------------
!     * 3d atmospheric chemical fields.
!
call getchem4(dmsopak,dmsopal,edmspak,edmspal,               &
             suz0pak,suz0pal,pdsfpak,pdsfpal,                &
               ohpak,  ohpal,                                &
             h2o2pak,h2o2pal,  o3pak,  o3pal,                &
              no3pak, no3pal,hno3pak,hno3pal,                &
              nh3pak, nh3pal, nh4pak, nh4pal,                &
             lon1,nlat,levox,incd,iday,mdayt,mdayt1,         &
             kount,ijpak,nuch,nuox,lx,gll)
!
!     * secondary organic aerosol from voa production.
!
call getcac2 (eostpak,eostpal, &
                    lon1,nlat,incd,iday,mdayt,mdayt1, &
                    kount,ijpak,nuch,gll)
#if defined transient_aerosol_emissions
!
!     * 3d wild fire emission factors.
!
      call getwfh2(fbbcpak,fbbcpal,lon1,nlat,levwf,incd,iday, &
              mdayt,mdayt1,mon,mon1,kount,ijpak,nuch,lf,gll)
!
!     * 3d aircraft emission factors.
!
      call getach2(fairpak,fairpal,lon1,nlat,levair,incd,iday, &
              mdayt,mdayt1,mon,mon1,kount,ijpak,nuch,lair,gll)
#if defined emists
!
!     * future, anthropogenic and anthropogenically-influenced
!     * emissions.
!
      call gettemi2(sairpak,ssfcpak,sbiopak,sshipak,sstkpak,sfirpak, &
               sairpal,ssfcpal,sbiopal,sshipal,sstkpal,sfirpal, &
               oairpak,osfcpak,obiopak,oshipak,ostkpak,ofirpak, &
               oairpal,osfcpal,obiopal,oshipal,ostkpal,ofirpal, &
               bairpak,bsfcpak,bbiopak,bshipak,bstkpak,bfirpak, &
               bairpal,bsfcpal,bbiopal,bshipal,bstkpal,bfirpal, &
               lon1,nlat,incd,irefyra,iday,kount,ijpak,nutr,gll)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
