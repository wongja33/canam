subroutine getctemf (nlu,newfrac,prefrac,c_clim_time, &
                     month,iday, &
                     lndcvryr1,lndcvryr2, &
                     lctem,icc,ntld,nlon,nlat,ijpak,gg)
  !
  !     * feb 29, 2016 - m.lazare. new routine based on first part of ctem_interpol_land_cover.dk
  !     *                          to obtain next mid-month ctem vegetation fraction fields.
  !
  implicit none

  integer, intent(in) :: nlu ! i/o unit number for file that contains land cover + may be other variables
  !
  !     * i/o fields:
  !
  real, intent(in), dimension(ijpak,ntld,icc) :: newfrac
  real, intent(inout), dimension(ijpak,ntld,icc) :: prefrac
  real, intent(inout), dimension(3,2) :: c_clim_time
  !
  integer, intent(in), dimension(icc) :: lctem

  integer*4 :: mynode
  common /mpinfo/ mynode
  !
  !     * work array.
  !
  real, intent(in) :: gg( * )
  !
  integer, intent(in) :: ijpak
  integer, intent(in) :: ntld
  integer :: k
  integer :: m
  integer, intent(in) :: nlon
  integer, intent(in) :: nlat
  integer, intent(in) :: icc

  integer, intent(in) :: iday ! compressed time day     (1, 2, 3...   364, 365)  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: month ! extended time month     (1, 2, 3...  12)
  !
  integer, intent(inout) :: lndcvryr1
  integer, intent(in) :: lndcvryr2
  !
  !     * local data.
  !
  integer :: nc4to8
  !
  integer, dimension(12) :: start_days_of_months
  integer, dimension(12) :: mid_month_days
  integer :: monthp1
  integer :: tmestmp2
  !
  data start_days_of_months /1,32,60,91,121,152,182,213,244,274, &
 305,335/
  data mid_month_days /15,46,74,105,135,166,196,227,258,288,319, &
 349/
  !-----------------------------------------------------------------------------
  !     * find the land cover for the current day using values
  !     * previously extracted.
  !
  lndcvryr1 = lndcvryr2 * 10000 + month * 100 + iday - &
              start_days_of_months(month) + 1 ! for use in writecs
  !
  !     * at this time i do not have land cover beyond 2100 so the time stamp
  !     * used in saving land cover in the cm file in writecs subroutine cannot
  !     * be greater than the last day of 2100.
  !
  if (lndcvryr1 > 21001231) then
    lndcvryr1 = 21001231
  end if
  !
  c_clim_time(1,1) = lndcvryr2
  c_clim_time(2,1) = mid_month_days(month)
  c_clim_time(3,1) = 0.0
  !
  monthp1 = month + 1
  if (monthp1 /= 13) then
    tmestmp2 = lndcvryr2 * 100 + month + 1
    c_clim_time(1,2) = lndcvryr2
    c_clim_time(2,2) = mid_month_days(month + 1)
    c_clim_time(3,2) = 0.0
  else if (monthp1 == 13) then
    tmestmp2 = (lndcvryr2 + 1) * 100 + 1 ! january of next year
    if (tmestmp2 > 210012) tmestmp2 = 210012
    c_clim_time(1,2) = lndcvryr2 + 1
    c_clim_time(2,2) = mid_month_days(1)
    c_clim_time(3,2) = 0.0
  end if
  !
  !     * 15th of this month values now become prefrac
  !
  prefrac(:,:,:) = newfrac(:,:,:)
  !
  !     * and we get values for the 15th of the next month
  !
  rewind nlu
  do k = 1, icc
    do m = 1, ntld
      call getggbx(newfrac(1,m,k),nc4to8("FRAC"),nlu,nlon,nlat, &
                   tmestmp2,lctem(k),gg)
    end do
  end do
  !
  return
end
