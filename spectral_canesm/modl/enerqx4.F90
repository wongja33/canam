!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine enerqx4(twv,twvx,qwf0,qscal,ttrac,ttracx,xwf0, &
                         xscal,tracg,itrac,ntrac,itrvar, &
                         qg,pressg,imoist, &
                         dshj,il1,il2,ilg,ilev,lonsl, &
                         wj)

  !     * sep 10/07 - m.lazare.    new version for gcm15g:
  !     *                          - wj passed in and used as real*8
  !     *                            for greater accuracy.
  !     *                          - remove lon for plug-compatability.
  !     * jun 27/06 - m.lazare.    previous version enerqx3 for gcm15f:
  !     *                          - rationalization of real :: vs real*8.
  !     *                          - work array "WX" now local.
  !     *                          note that up to now, enerqx2 called
  !     *                          from both physics and semilag
  !     *                          dynamics. due to real*8, removal of
  !     *                          physics "WRK" pointers and portable
  !     *                          physics, this policy now abandoned.
  !     * may 05/03 - k.vonsalzen. previous version enerqx2 up to gcm15e.
  !
  !     * this subroutine is purely diagnostic.
  !     * in the latitude loop it accumulates the following:
  !     *
  !     * twv    = total water vapour, i.e. precipitable water.
  !     * twvx   = total moisture sources/sinks.
  !     * ttrac  = total tracer amount.
  !     * ttracx = total tracer source/sinks.
  !     *
  !     * wj     = multiple-latitude array of gaussian weights.
  !     * ilg    = first dimension of real :: grid arrays.
  !     * lonsl  = number of distinct longitudes in a single latitude.
  !     * imoist = moisture variable switch to indicate whether code
  !     *          being activated in this routine usage (=1) or not.
  !     * itrvar = same as above except for tracers.
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: imoist
  integer, intent(inout) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(inout) :: itrvar
  integer, intent(inout) :: lonsl
  integer, intent(inout) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  !
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: qg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qwf0 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,ntrac) :: tracg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,ntrac) :: xwf0 !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real*8, intent(in) , dimension(ilg) :: wj !< Variable description\f$[units]\f$
  !
  !     * the following are passed through the physics call from the
  !     * core routines and thus always must be native real !
  !
  real, intent(inout), dimension(ilev) :: twv !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: twvx !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev) :: qscal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: ttrac !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: ttracx !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilev,ntrac) :: xscal !< Variable description\f$[units]\f$

  !
  integer :: il
  integer :: l
  integer :: n
  !
  !     * native temporary variables for max intrinsic.
  !
  real   :: qwf0tmp !<
  real   :: xwf0tmp !<
  !
  !     * internal work array:
  !
  real,  dimension(ilg)  :: wx !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !-----------------------------------------------------------------------
  !     * set up array of "EFFECTIVE" weights used in quadrature.
  !
  do il=il1,il2
    wx(il)=.5*wj(il)/real(lonsl)
  end do ! loop 50
  !
  !     * accumulate the energetics.
  !
  if (imoist==1) then
    do l=1,ilev
      do il=il1,il2
        twv  (l)=twv  (l) + dshj(il,l)* pressg(il)*(wx(il)/grav)* &
                           qg(il,l)
        twvx (l)=twvx (l) + dshj(il,l)* pressg(il)*(wx(il)/grav)* &
                         qwf0(il,l)*qg(il,l)
        qwf0tmp =qwf0(il,l)
        qscal(l)=max(qscal(l),qwf0tmp)
      end do
    end do ! loop 200
  end if
  !
  if (itrac/=0 .and. itrvar==1) then
    !
    !        * accumulate the mass associated with each tracer.
    !
    do n=1,ntrac
      do l=1,ilev
        do il=il1,il2
          ttrac (l,n)=ttrac (l,n)+dshj(il,l)*pressg(il)*(wx(il)/grav)* &
                  tracg(il,l,n)
          ttracx(l,n)=ttracx(l,n)+dshj(il,l)*pressg(il)*(wx(il)/grav)* &
                  xwf0(il,l,n)*tracg(il,l,n)
          xwf0tmp    =xwf0(il,l,n)
          xscal(l,n) =max(xscal(l,n),xwf0tmp)
        end do
      end do
    end do ! loop 210
  end if
  !
  return
  !-----------------------------------------------------------------------
end subroutine enerqx4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
