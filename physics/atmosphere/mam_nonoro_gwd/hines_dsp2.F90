!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_dsp2 (drag_u,drag_v,heat,diffco,flux_u,flux_v, &
                       vel_u,vel_v,bvfreq,density,visc_mol,alt, &
                       rms_wind,k_alpha,v_alpha,m_alpha, &
                       sigma_alpha,sigsqh_alpha,ak_alpha, &
                       specfac,do_alpha,drag,dragil, &
                       mmin_alpha,i_alpha,sigma_t,densb,bvfb, &
                       ubot,vbot,iheatcal,icutoff,iprnt1,iprnt2, &
                       nsmax,smco,alt_cutoff,kstar,m_min,slope, &
                       f1,f2,f3,f5,f6,naz,il1,il2, &
                       levbot,levtop,nlons,nlevs,nazmth,wrk1,wrk2, &
                       wrk3)
  !
  !  main routine for hines doppler spread gravity wave parameterization
  !  scheme which calculates zonal and meridional components of gravity
  !  wave drag, heating rates and diffusion coefficient on a longitude
  !  by altitude grid.
  !
  !  aug. 13/95 - c. mclandress
  !
  !  modifications:
  !  --------------
  !  aug.25/99 - c. mclandress (heating formulation corrected)
  !  aug. 8/99 - c. mclandress (nonzero m_min for all other values of slope)
  !  feb. 2/96 - c. mclandress (added: minimum cutoff wavenumber m_min
  !                             12 and 16 azimuths; inclusion of introwaves
  !                             by ubot and vbot; diffusion and heating
  !                             calculated at half level; logical :: flags
  !                             v_alpha array only at single level
  !                             new print routine)
  !
  !  output arguements:
  !  ------------------
  !
  !     * drag_u = zonal component of gravity wave drag (m/s^2).
  !     * drag_v = meridional component of gravity wave drag (m/s^2).
  !     * heat   = gravity wave heating (k/sec).
  !     * diffco = diffusion coefficient (m^2/sec)
  !     * flux_u = zonal component of vertical momentum flux (pascals)
  !     * flux_v = meridional component of vertical momentum flux (pascals)
  !
  !  input arguements:
  !  -----------------
  !
  !     * vel_u      = background zonal wind component (m/s) (positive is
  !     *              eastward).
  !     * vel_v      = background meridional wind component (m/s) (positive
  !     *              northward).
  !     * bvfreq     = background brunt vassala frequency (radians/sec).
  !     * density    = background density (kg/m^3)
  !     * visc_mol   = molecular viscosity (m^2/s)
  !     * alt        = altitude of momentum, density, buoyancy levels (m)
  !     *              (levels must be ordered so alt(i,levtop) > alt(i,levbot))
  !     * rms_wind   = root mean square gravity wave wind at bottom (reference)
  !     *              level (m/s).
  !     * k_alpha    = horizontal wavenumber of each azimuth (1/m).
  !     * iheatcal   = 1 to calculate heating rates and diffusion coefficient.
  !     * iprnt1     = 1 to print out flux, drag arrays at specified longitudes.
  !     * iprnt2     = 1 to print out azimuthal arrays at specified longitudes.
  !     * icutoff    = 1 to exponentially damp drag, heating and diffusion
  !     *              arrays above the altitude alt_cutoff.
  !     * alt_cutoff = altitude in meters above which exponential decay applied.
  !     * smco       = smoothing factor used to smooth cutoff vertical
  !     *              wavenumbers and total rms winds in vertical direction
  !     *              before calculating drag or heating
  !     *              (smco >= 1 ==>  a 1:smco:1 three-point stencil is used).
  !     * nsmax      = number of times smoother applied ( >= 1),
  !     *            = 0 means no smoothing performed.
  !     * kstar      = typical gravity wave horizontal wavenumber (1/m).
  !     * m_min      = minimum allowable cutoff wavenumber, e.g., 1/(3km).
  !     * slope      = slope of incident vertical wavenumber spectrum
  !     *              (slope must equal 1., 1.5, 2. or 3.).
  !     * F1 to F6   = Hines's fudge factors (F4 not needed since used for
  !     *              vertical flux of vertical momentum).
  !     * naz        = number of horizontal azimuths used (4, 8, 12 or 16).
  !     * il1        = first longitudinal index to use (il1 >= 1).
  !     * il2        = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * levbot     = index of bottom (reference) drag level.
  !     * levtop     = index of top drag level (if levbot > levtop then the
  !     *              vertical indexes increase from top down, otherwise
  !     *              indexes increase from bottom up).
  !     * nlons      = number of longitudes.
  !     * nlevs      = number of vertical levels.
  !     * nazmth     = azimuthal array dimension (nazmth >= naz).
  !
  !  input work arrays:
  !  ------------------
  !
  !     * m_alpha      = cutoff vertical wavenumber (1/m).
  !     * v_alpha      = wind component at each azimuth (m/s).
  !     * sigma_alpha  = total rms wind in each azimuth (m/s).
  !     * sigsqh_alpha = portion of wind variance from waves having wave
  !     *                normals in the alpha azimuth (m/s).
  !     * sigma_t      = total rms horizontal wind (m/s).
  !     * ak_alpha     = spectral amplitude factor at each azimuth
  !     *                (i.e.,{ajkj}) in m^4/s^2.
  !     * specfac      = ak_alpha * k_alpha.
  !     * do_alpha     = logical :: flag indicating azimuths and longitudes
  !     *                where calculations to be performed.
  !     * drag         = logical :: flag indicating longitudes where calculations
  !     *                to be performed.
  !     * dragil       = logical :: flag indicating longitudes and levels where
  !     *                calculations to be performed.
  !     * I_ALPHA      = Hines' integral.
  !     * mmin_alpha   = minimum value of cutoff wavenumber.
  !     * densb        = background density at reference level.
  !     * bvfb         = buoyancy frequency at bottom reference and
  !     *                work array for icutoff = 1.
  !     * ubot         = background zonal wind component at reference level.
  !     * vbot         = background meridional wind component at reference level.
  !
  implicit none
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlevs !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: levbot   !< Variable description\f$[units]\f$
  integer, intent(in)  :: levtop   !< Variable description\f$[units]\f$
  integer, intent(in)  :: icutoff !< Variable description\f$[units]\f$
  integer, intent(in)  :: nsmax !< Variable description\f$[units]\f$
  integer, intent(in)  :: iheatcal !< Variable description\f$[units]\f$
  integer, intent(in)  :: iprnt1 !< Variable description\f$[units]\f$
  integer, intent(in)  :: iprnt2 !< Variable description\f$[units]\f$
  ! ccc      logical  :: dragil(nlons,nlevs), drag(nlons), do_alpha(nlons,nazmth)
  integer, intent(inout), dimension(nlons,nlevs) :: dragil !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(nlons) :: drag !< Variable description\f$[units]\f$
  integer, intent(in), dimension(nlons,nazmth) :: do_alpha !< Variable description\f$[units]\f$
  real, intent(in) :: kstar !< Variable description\f$[units]\f$
  real, intent(in) :: m_min !< Variable description\f$[units]\f$
  real, intent(in) :: f1 !< Variable description\f$[units]\f$
  real, intent(in) :: f2 !< Variable description\f$[units]\f$
  real, intent(in) :: f3 !< Variable description\f$[units]\f$
  real, intent(in) :: f5 !< Variable description\f$[units]\f$
  real, intent(in) :: f6 !< Variable description\f$[units]\f$
  real, intent(in) :: slope !< Variable description\f$[units]\f$
  real, intent(in) :: alt_cutoff !< Variable description\f$[units]\f$
  real, intent(in) :: smco !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: drag_u !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: drag_v !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: heat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: diffco !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: flux_u !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: flux_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_u !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: vel_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: bvfreq !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: density !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: visc_mol !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: alt !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: rms_wind !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons) :: bvfb !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons) :: densb !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons) :: ubot !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons) :: vbot !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: sigma_t !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: sigma_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: sigsqh_alpha !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs,nazmth) :: m_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: v_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: ak_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: k_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: mmin_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: i_alpha !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nazmth) :: specfac !< Variable description\f$[units]\f$

  real, intent(in), dimension(nlons) :: wrk1 !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: wrk2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(nazmth) :: wrk3 !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: ierror
  integer  :: i
  integer  :: n
  integer  :: l
  integer  :: lev1p
  integer  :: lev2m
  integer  :: nunit
  integer  :: ilprt1
  integer  :: ilprt2
  integer  :: lev1
  integer  :: lev2
  integer  :: iorder
  integer  :: icount
  real    :: zero
  real    :: rms_min
  data  zero / 0. /, rms_min / 0.001 /
  !-----------------------------------------------------------------------
  !
  !  check that things set up correctly, abort if not.
  !
  ierror = 0
  if (naz > nazmth)                                  ierror = 10
  if (naz /= 4 .and. naz /= 8 &
      .and. naz /= 12 .and. naz /= 16)                    ierror = 20
  if (slope /= 1. .and. slope /= 1.5 &
      .and. slope /= 2. .and. slope /= 3.)                 ierror = 30
  if (ierror /= 0) then
    write (6, * ) 'aborting in HINES_DSP2: IERROR = ',ierror
    stop
  end if
  !
  !  ordering of levels.
  !
  if (levbot < levtop) then
    lev1 = levbot
    lev2 = levtop
    iorder = - 1
  else
    lev1 = levtop
    lev2 = levbot
    iorder = 1
  end if
  !
  lev1p = lev1 + 1
  lev2m = lev2 - 1
  !
  !  initialize output and some work arrays.
  !
  do l = 1,nlevs
    do i = il1,il2
      drag_u(i,l) = zero
      drag_v(i,l) = zero
      heat(i,l)   = zero
      diffco(i,l) = zero
      flux_u(i,l) = zero
      flux_v(i,l) = zero
      sigma_t(i,l) = zero
    end do
  end do ! loop 5
  !
  !  initialize cutoff wavenumber array to minimum value.
  !
  do n = 1,naz
    do l = 1,nlevs
      do i = il1,il2
        m_alpha(i,l,n) = m_min
      end do
    end do
  end do ! loop 10
  !
  !  longitudes where drag to be calculated.
  !
  icount = 0
  do i = il1,il2
    drag(i) = 0
    if (rms_wind(i) >= rms_min) then
      drag(i) = 1
      icount = icount + 1
    end if
  end do ! loop 15
  !
  !  return to calling program if no drag.
  !
  if (icount == 0)  return
  !
  !  buoyancy, density and winds at bottom level.
  !
  do i = il1,il2
    if (drag(i) == 1) then
      bvfb(i)  = bvfreq(i,levbot)
      densb(i) = density(i,levbot)
      ubot(i)  = vel_u(i,levbot)
      vbot(i)  = vel_v(i,levbot)
    end if
  end do ! loop 20
  !
  !  calculate cutoff vertical wavenumber and velocity variances.
  !
  call hines_wavnum2 (m_alpha, sigma_alpha, sigsqh_alpha, sigma_t, &
                      ak_alpha, vel_u, vel_v, ubot, vbot, &
                      visc_mol, density, densb, bvfreq, bvfb, &
                      rms_wind, v_alpha, i_alpha, mmin_alpha, &
                      do_alpha, dragil, drag, m_min, kstar, slope, &
                      f1, f2, f3, naz, levbot, levtop, &
                      il1, il2,  nlons, nlevs, nazmth,wrk1,wrk2)
  !
  !  multiplicative spectral factor at each azimuth.
  !
  do n = 1,naz
    do i = il1,il2
      if (drag(i) == 1) then
        specfac(i,n) = ak_alpha(i,n) * k_alpha(i,n)
      end if
    end do
  end do ! loop 50
  !
  !  smooth cutoff wavenumbers and total rms velocity in the vertical
  !  direction nsmax times, using flux_u as temporary work array.
  !
  if (nsmax > 0) then
    do n = 1,naz
      call hines_smooth (m_alpha(1,1,n), &
                         flux_u, dragil, smco, nsmax, &
                         il1, il2, lev1, lev2, nlons, nlevs)
    end do ! loop 80
    call hines_smooth (sigma_t, &
                       flux_u, dragil, smco, nsmax, &
                       il1, il2, lev1, lev2, nlons, nlevs)
  end if
  !
  !  calculate zonal and meridional components of the
  !  momentum flux and drag.
  !
  call hines_drag2 (flux_u, flux_v, drag_u, drag_v, &
                    alt, density, densb, m_alpha, &
                    specfac, dragil, m_min, slope, naz, &
                    il1, il2, lev1, lev2, nlons, nlevs, nazmth)
  !
  !  heating rate and diffusion coefficient at midpoint between momentum levels.
  !
  if (iheatcal == 1) then
    call hines_heat2 (heat, diffco, &
                      alt, m_alpha, specfac, dragil, &
                      bvfreq, density, densb, &
                      sigma_t, sigma_alpha, visc_mol, &
                      kstar, slope, f1, f2, f3, f5, f6, &
                      naz, il1, il2, &
                      levbot, levtop, nlons, nlevs, nazmth)
  end if
  !
  !  apply exponential decay to drag, heating and diffusion above
  !  alt_cutoff (use bvfb as temporary work array).
  !
  if (icutoff == 1) then
    call hines_exp (drag_u, &
                    bvfb, alt, alt_cutoff, iorder, &
                    il1, il2, lev1, lev2, nlons, nlevs)
    call hines_exp (drag_v, &
                    bvfb, alt, alt_cutoff, iorder, &
                    il1, il2, lev1, lev2, nlons, nlevs)
    if (iheatcal == 1) then
      call hines_exp (heat, &
                      bvfb, alt, alt_cutoff, iorder, &
                      il1, il2, lev1, lev2, nlons, nlevs)
      call hines_exp (diffco, &
                      bvfb, alt, alt_cutoff, iorder, &
                      il1, il2, lev1, lev2, nlons, nlevs)
    end if
  end if
  !
  !  print out flux, drag, etc arrays for diagnostic purposes.
  !
  if (iprnt1 == 1) then
    ilprt1 = 1
    ilprt2 = 1
    nunit  = 11
    call hines_prnt1 (flux_u, flux_v, drag_u, drag_v, vel_u, vel_v, &
                      alt, sigma_t, sigma_alpha, m_alpha, &
                      1, 1, nunit, ilprt1, ilprt2, lev1, lev2, &
                      naz, nlons, nlevs, nazmth)
  end if
  !
  !  print out azimuthal arrays for diagnostic purposes.
  !
  if (iprnt2 == 1) then
    ilprt1 = 1
    ilprt2 = 1
    nunit  = 11
    call hines_prnt2 (m_alpha, sigma_alpha, vel_u, vel_v, &
                      ubot, vbot, alt, dragil, v_alpha, &
                      nunit, ilprt1, ilprt2, lev1, lev2, &
                      naz, nlons, nlevs, nazmth,wrk3)
  end if
  !
  !  finished.
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

