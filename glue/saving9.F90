!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine saving9(lssp,lsgg,lsrad,lsbeg,lstr,lshf,lcsw,lclw, &
                         lscosp, &
                         issp,isgg,israd,isbeg,istr,ishf,icsw,iclw, &
                         iscosp,isst, &
                         lsday,ls3hr,lsst,delt,ndays,nsecs)
  !
  !     * mar 22/19 - j. cole    add "LSST/ISST" to save site data
  !     * nov 03/18 - m.lazare.  add "LS3HR" for cmip6 data output.
  !     * apr 25/10 - j.cole.    new version for gcm15i:
  !     *                        - add iscosp/lscosp for cosp simulator.
  !     * dec 03/07 - l.solheim/ previous version saving8 for gcm15g/h:
  !     *             m.lazare.  - calls revised savingx instead of saving.
  !     *                        - implicit none imposed with promotion
  !     *                          as necessary.
  !     * feb 15/05 - m.lazare.  previous version saving7 before gcm15g.
  !
  !     * set logical :: switches for saving or not the sp,gg,beg and rad fields
  !     * also to save certain fields every day and possibly at higher
  !     * frequency, save the tracers at different frequencies
  !     * and compute or not the longwave and shortwave radiation.
  !
  implicit none
  !
  real, intent(in)      :: delt   !< Timestep for atmospheric model \f$[seconds]\f$
  integer*8 :: numsec !< Variable description\f$[units]\f$
  integer*8 :: ndays8 !< Variable description\f$[units]\f$
  integer*8 :: nsecs8 !< Variable description\f$[units]\f$
  integer, intent(in)   :: ndays !< Variable description\f$[units]\f$
  integer, intent(in)   :: nsecs !< Variable description\f$[units]\f$
  integer   :: idt !< Variable description\f$[units]\f$
  integer, intent(in)   :: issp !< Variable description\f$[units]\f$
  integer, intent(in)   :: isgg   !< Timestep interval to save general physics fields \f$[unitless]\f$
  integer, intent(in)   :: israd   !< Timestep interval to save radiative transfer output \f$[unitless]\f$
  integer, intent(in)   :: isbeg   !< Timestep interval for coupling to ocean/sea-ice and to save fields related to that coupling \f$[unitless]\f$
  integer   :: isday !< Variable description\f$[units]\f$
  integer, intent(in)   :: istr !< Variable description\f$[units]\f$
  integer, intent(in)   :: ishf   !< Timestep interval to save high frequency output \f$[unitless]\f$
  integer, intent(in)   :: icsw !< Variable description\f$[units]\f$
  integer, intent(in)   :: iclw !< Variable description\f$[units]\f$
  integer   :: is3hr !< Variable description\f$[units]\f$
  integer, intent(in)   :: iscosp   !< Timestep interval to save COSP output \f$[unitless]\f$
  integer, intent(in)   :: isst !< Variable description\f$[units]\f$
  logical, intent(in)   :: lssp !< Variable description\f$[units]\f$
  logical, intent(in)   :: lsgg !< Variable description\f$[units]\f$
  logical, intent(in)   :: lsrad !< Variable description\f$[units]\f$
  logical, intent(in)   :: lsbeg !< Variable description\f$[units]\f$
  logical, intent(in)   :: lsday !< Variable description\f$[units]\f$
  logical, intent(in)   :: lstr !< Variable description\f$[units]\f$
  logical, intent(in)   :: lshf !< Variable description\f$[units]\f$
  logical, intent(in)   :: lcsw   !< Switch to do solar radiative transfer (.true.= use, .false. = don't use) \f$[unitless]\f$
  logical, intent(in)   :: lclw   !< Switch to do thermal radiative transfer (.true.= use, .false. = don't use) \f$[unitless]\f$
  logical, intent(in)   :: ls3hr !< Variable description\f$[units]\f$
  logical, intent(in)   :: lscosp   !< Switch to use COSP (.true.= use, .false. = don't use) \f$[unitless]\f$
  logical, intent(in)   :: lsst !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  ndays8=ndays
  nsecs8=nsecs
  numsec=ndays8*86400_8+nsecs8
  if (numsec<0)            call xit('SAVING9',-1)  ! trap overflow
  idt   =int(delt)
  isday =24*int(3600./delt)
  is3hr = 3*int(3600./delt)
  !
  call savingx(lssp ,issp ,idt,numsec)
  call savingx(lsgg ,isgg ,idt,numsec)
  call savingx(lsrad,israd,idt,numsec)
  call savingx(lsday,isday,idt,numsec)
  call savingx(lcsw, icsw, idt,numsec)
  call savingx(lclw, iclw, idt,numsec)
  call savingx(lsbeg,isbeg,idt,numsec)
  call savingx(lstr, istr, idt,numsec)
  call savingx(lshf, ishf, idt,numsec)
  call savingx(ls3hr,is3hr,idt,numsec)
  call savingx(lscosp, iscosp, idt,numsec)
  call savingx(lsst, isst, idt,numsec)
  !
  !     * tracer saving frequency must be a multiple of general
  !     * spectral saving so that diagnostic operations such as
  !     * interpolation to pressure (through the use of lnsp) will
  !     * function.
  !
  if (mod(istr,issp)/=0)            call xit('SAVING9',-2)
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
