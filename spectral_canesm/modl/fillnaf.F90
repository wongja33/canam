!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine fillnaf(wtr,tranche,tracna,ioff, &
                         ntrac,ntracn,ntrspec,itradv, &
                         ilev,ijpak,ilg,il1,il2)

  !     * dec 10/03 - m.lazare.   consistent with new methodology for
  !     *                         ibm port.
  !     * oct 29/02 - j.scinocca. previous version filnaf.

  !     * fills "TRANCHE" with "TRACNA" for non-advected
  !     * tracers. otherwise, "TRANCHE" is filled with "WTR".
  !     * "IOFF" is offset into tracna for the particular task.

  implicit none
  integer :: i
  integer, intent(inout) :: ijpak
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: ioff
  integer :: l
  integer :: n
  integer :: na
  integer :: nna
  integer, intent(inout) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(inout) :: ntracn !< Variable description\f$[units]\f$
  integer, intent(inout) :: ntrspec
  !
  real, intent(in), dimension(ijpak,ilev,ntracn) :: tracna !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,ntrac) :: tranche !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,ntrspec) :: wtr !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntrac) :: itradv !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--------------------------------------------------------------------
  na=0
  nna=0
  do n=1,ntrac
    if (itradv(n)==1) then
      na=na+1
      do l=1,ilev
        do i=il1,il2
          tranche(i,l,n)=wtr(i,l,na)
        end do
      end do
    else
      nna=nna+1
      do l=1,ilev
        do i=il1,il2
          tranche(i,l,n) = tracna(ioff+i,l,nna)
        end do
      end do
    end if
  end do ! loop 300

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
