!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oequip2 (ilg,ilev,neqp,ilevi,il1,il2,achpa,atemp)
  !-------------------------------------------------------------------------
  !     * oct 24/2006 - m.lazare.        new version for gcm15f:
  !     *                                - implicit none with real(8)
  !     *                                  scalar for temperature
  !     *                                  to work in 32-bit environment.
  !     * jun 19/01 - k. von salzen      previous version oequip up to gcm15e.
  !     * feb 2/98  - k. von salzen      new subroutine.
  !
  !     * chemical parameters required for in-cloud oxidation of s(iv).
  !     * this subroutine provides henry's law constant and reaction rates.
  !-----------------------------------------------------------------------
  implicit none
  !
  real, intent(in), dimension(ilg,ilev) :: atemp !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,neqp) :: achpa !< Variable description\f$[units]\f$
  !
  real*8 :: temp
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: neqp !< Variable description\f$[units]\f$
  integer, intent(in) :: ilevi   !< Variable description\f$[units]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: l !< Variable description\f$[units]\f$
  integer :: il !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  !
  do l = ilevi,ilev
    do il = il1,il2
      temp = atemp(il,l)
      achpa(il,l,1) = temp * &
                      1.0093e-01 * exp (3120. * (1./temp - 1./298.))
      achpa(il,l,2) = temp * &
                      1.7158e-03 * exp (5210. * (1./temp - 1./298.))
      achpa(il,l,3) = temp * &
                      1.0295e-10 * exp (6330. * (1./temp - 1./298.))
      achpa(il,l,4) = temp * &
                      2.5520e-03 * exp (2423. * (1./temp - 1./298.))
      achpa(il,l,5) = temp * &
                      1.0973e-09 * exp (1510. * (1./temp - 1./298.))
      achpa(il,l,6) = temp * &
                      7.9595e+03 * exp (6600. * (1./temp - 1./298.))
      achpa(il,l,7) = temp * &
                      9.4366e-04 * exp (2560. * (1./temp - 1./298.))
      achpa(il,l,8) = temp * &
                      206.032e+03 * exp (29.1657 * (298./temp - 1.) &
                      + 16.8322 * (1. + log (298./temp) &
                      - 298./temp) )
      achpa(il,l,9) = temp * &
                      8.44851e+09 * exp (34.8536 * (298./temp - 1.) &
                      - 5.3930 * (1. + log (298./temp) &
                      - 298./temp) )
      achpa(il,l,10) = temp * &
                       161.675e+03 * exp (30.2355 * (298./temp - 1.) &
                       + 19.9083 * (1. + log (298./temp) &
                       - 298./temp) )
      achpa(il,l,11) = 4.4e+11 * exp ( - 4131. / temp)
      achpa(il,l,12) = 2.6e+03 * exp ( - 966.  / temp)
      achpa(il,l,13)  = 8.0e+04 &
                        * exp ( - 3650. * (1. / temp - 1. / 298.) )
    end do
  end do ! loop 100
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
