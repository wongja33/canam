!>\file
!>\brief
!!
!! @author Barbara Winter, David Plummer
!
module chem_params_mod

  ! This module contains all parameters required by the gas-phase chemistry
  ! routines. They used to be spread out among the subroutines or in the sizes
  ! file psizes_19.

  implicit none

  ! Array dimensions for gas-phase chemistry
  integer, parameter :: jpntjv=43  !< total size of J-value look-up table
  integer, parameter :: jpnjt1=19  !< no. of J-values in EUV look-up table
  integer, parameter :: jpnjt2=22  !< no. of J-values in vis look-up table
  integer, parameter :: jpntpl=201 !< vertical dimension of look-up table
  integer, parameter :: jpntsza=33 !< number of solar zenith angle entries
  integer, parameter :: jpntoz=8   !< number of overhead ozone amounts
  integer, parameter :: jpnalb=4   !< number of sfc albedo entries for vis table
  integer, parameter :: levssad=61 !< no. levels of specified stratospheric aerosol surface area density
  integer, parameter :: nlvarc=25  !< no. levels in aircraft emission file
  integer, parameter :: nlso4=17   !< no. levels on external sulphate file
  integer, parameter :: nsfcem=6   !< number of separate categories for surface-based emissions
  integer, parameter :: nvdep=6    !< number of dry deposition velocities
  integer, parameter :: nchdg=14   !< number of 3-D chemical diagnostic fields
  integer, parameter :: nwflx=6    !< no. diagnostic chem fluxes accumulated
  integer, parameter :: jpnrks=114 !< total gas-phase chemical reactions
  integer, parameter :: jpwetr=7   !< no. species undergoing wet deposition

  ! Constants related to the ideal gas law
  real, parameter :: vpavno=6.0221409E+26 !< Avogadros Number [KMOL-1]
  real, parameter :: vpugc=8.31446E+03    !< Universal gas constant [JK-1KMOL-1]
  real, parameter :: vpmma=28.97          !< Molecular mass of dry air
  real, parameter :: vpbltz=vpugc/vpavno  !< Boltzmann's constant [JK-1 MOL-1]
  real, parameter :: vppi=3.1415926535898 !< pi [radians]
  real, parameter :: vplosch=2.69E+19     !< Loschmidt number [molec cm-3]
  real, parameter :: vpcv=18.0/28.964     !< ratio of molecular weight of water to dry air

  ! Julian mid-month days, required by subroutines getsfsa, getemis, getacem
  integer, dimension(12) :: mmd
  data mmd / 16, 46, 75, 106, 136, 167, 197, 228, 259, 289, 320, 350/

  ! Index numbers for wet-deposition calculations of trace gas species,
  ! required by subroutine physici.F90
  integer, dimension(jpwetr) :: irnpos   !< tracers undergoing wet deposition

  ! Constants for Henry's Law used for moderately soluble species.
  ! The last three are assumed to be almost completely in liquid phase.
  ! These are from JPL-2015 and give c_aq/c_gas (mol/L/atm) but have been
  ! expressed as H = A exp(B*(1/T - 1/T_o)) where T_o=298K
  !          H2O2,     HNO4,     HCHO      CH3OOH,  HNO3  HBr,  HCl
  real, parameter :: chnrya(jpwetr) = &
        (/ 8.70E+04, 1.20E+04, 3.23E+03, 3.00E+02, 0.0, 0.0, 0.0 /)
  real, parameter :: chnryb(jpwetr) = &
        (/  7290.0,   6900.0,  7100.0,   5280.0,   0.0, 0.0, 0.0 /)

  ! Former common block JVALUE, for photolysis rate look-up table values.
  ! The file (Fortran binary) from which these are read was originally written
  ! in double precision. The gcm13e model and its gcm18 successor was run at
  ! 64-bit precision and there was no need to specify the kind or precision of
  ! the variables below. CanAM5, on the other hand, is run at 32-bit precision,
  ! so double precision must be specified.
  ! The internet says: "double precision" is 77 like shag carpets and corduroy;
  ! specify "kind" instead. Another internet tentacle says: "kind=8" is gauche,
  ! and there are better ways that will retain the same precision relative to
  ! the compiler, regardless of compiler/platform changes. Trying this below.
  ! These variables are needed in gcm18.F90, for reading the external files,
  ! and in chem3.F90, for photolysis calculations.
  integer, parameter :: dp = kind(1.0d0)
  integer (kind=dp), dimension(jpnjt1) :: jpidx1
  integer (kind=dp), dimension(jpnjt2) :: jpidx2

  real (kind=dp), dimension(jpnalb) :: qralb
  real (kind=dp), dimension(jpntoz) :: qo3srat
  real (kind=dp), dimension(jpntpl) :: qo3c, qpref
  real (kind=dp), dimension(jpntsza) :: qzenang
  real (kind=dp), dimension(jpnjt1,jpntpl,jpntsza,jpntoz) :: rjvtab1
  real (kind=dp), dimension(jpnjt2,jpntpl,jpntsza,jpntoz,jpnalb) :: rjvtab2

  ! Parameters specific to the size of the gas-phase chemistry solver
  integer, parameter :: jpspcs=44  !< number of gas-phase species treated in the solver
  integer, parameter :: jpnewt=26  !< size of gas-phase chemistry newtonian
  integer, parameter :: jphnewt=10  !< size of PSC heterogeneous chemistry newtonian
  integer, parameter :: jpsrxn=6  !< number of reactions in heterogeneous chemistry
  integer, parameter :: jppsc2=6
  integer, parameter :: jphcsta = 32  !< domain of heterogenous chemistry
  integer, parameter :: jphcsto = 49  !< domain of heterogenous chemistry
  integer, parameter :: jpiter=99 !< maximum number of iterations in the chemistry solver

  ! Heights at which aircraft emissions are given (metres)
  ! Required by subroutines acemlab and acemlev.
  real, dimension(nlvarc) :: zed
  data zed /   305.0,   915.0,  1525.0,  2135.0,  2745.0,  &
              3355.0,  3965.0,  4575.0,  5185.0,  5795.0,  &
              6405.0,  7015.0,  7625.0,  8235.0,  8845.0,  &
              9455.0, 10065.0, 10675.0, 11285.0, 11895.0,  &
             12505.0, 13115.0, 13725.0, 14335.0, 14945.0 /

  ! Pressure levels on the SO4 (SFSA) file
  ! Required by subroutines getsfsa and sfsalev.
  real, dimension(nlso4) :: pso4
  data pso4 / 10.0,  20.0,  30.0,  50.0,  70.0, 100.0, 150.0, 200.0, &
             250.0, 300.0, 400.0, 500.0, 600.0, 700.0, 850.0, 925.0, 1000.0 /

  ! Pressure levels on the SSAD file. Required by subroutine getssad.
  real, dimension(levssad) :: pref
  data pref /  268.7, 292.1, 317.5, 345.1, 375.1,          &
               407.8, 443.2, 481.7, 523.6, 569.2,          &
               618.7, 672.5, 730.9, 794.5, 863.6,          &
               938.7, 1020.3, 1109.0, 1205.5, 1310.3,      &
              1424.2, 1548.1, 1682.7, 1829.0, 1988.0,      &
              2160.9, 2348.8, 2553.0, 2775.1, 3016.4,      &
              3278.7, 3563.8, 3873.6, 4210.5, 4576.6,      &
              4974.6, 5407.1, 5877.3, 6388.4, 6943.9,      &
              7547.7, 8204.1, 8917.5, 9692.9, 10535.7,     &
             11451.9, 12447.7, 13530.1, 14706.7, 15985.5,  &
             17375.5, 18886.5, 20528.8, 22313.9, 24254.2,  &
             26363.3, 28655.7, 31147.5, 33856.0, 36800.0, 40000.0 /

  logical :: linto3 = .true. !< Flag for interactive ozone (Default = .true.)
  logical :: lpphc  = .true. !< Flag for heteregeneous chemistry (Default = .true.)

end module chem_params_mod
