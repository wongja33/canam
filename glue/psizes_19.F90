#include "cppdef_config.h"
#include "cppdef_sizes.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module psizes_19
  !=======================================================================
  !     * feb 18/2018 - m.lazare.    new support for ioztyp=6 for cmip6.
  !     * aug 11,2017 - m.lazare.    add size definitions for cslm.
  !     * aug 07,2017 - m.lazare.    initial git version.
  !     * mar 04,2016 - m.lazare.    new working version for gcm19:
  !     *                            - add sizes related to ictem+1.
  !     *                            - lon replaced by lonp.
  !     * feb 12,2015 - m.lazare.    previous version for gcm18:
  !     *                            - remove definition of im and ilgm.
  !     *                              these are now calculated in new
  !     *                              msizes, consistantly with new
  !     *                              generalized tiling.
  !     * jul 13,2013 - m.lazare/    new version for gcm17:
  !     * jul 13,2013 - e. chan/     - added parameters for class mosaic tiles.
  !     *               k.vonsalzen. - moved soil layer definitions from
  !     *                              class driver.
  !     *                            - added new sizes for pam (msgt,msgp1,msgp2).
  !     * may 08/2012 - j.cole.      previous version psizes_16 for gcm16:
  !     *                            - define value for max_sam, the
  !     *                              maximum number of samples that will
  !     *                              be taken in the mcica computations.
  !     * may 05/2010 - j.cole/      previous version psizes_15i for gcm15i:
  !     *               k.vonsalzen/ - "use_mcica" and/or "use_cosp" cpp
  !     *               v.arora.       directives replace isccpsim" update
  !     *                              directive.
  !     *                            - define ictem and nol2pfts parameters
  !     *                              for ctem.
  !     *                            - define parameter levair, the number
  !     *                              of aircraft emissions levels.
  !     *                            - define parameter levozc, the number
  !     *                              of levels in the randel historical
  !     *                              ozone dataset.
  !     *                            - radforce update directive changed
  !     *                              to cpp.
  !     *                            - use_mcica and use_cosp cpp directives
  !     *                              changed to lower case.
  !     * feb 22/2009 - j.cole.      previous version psizes_15h for gcm15h:
  !     *                            - now using decorrelation lengths
  !     *                              for cloud overlap (ioverlap=2).
  !     * apr 21/2008 - l.solheim.   previous version psizes_15g for gcm15g:
  !     * apr 10/2008 - m.lazare/    - add "radforce" section.
  !     *               j.cole.      - modify expression for ilenphs.
  !     *                            - add levwf parameter statement for
  !     *                              aerocom emissions.
  !     *                            - revised mcica/isccpsim paramater
  !     *                              statements.
  !     *                            - cleaned-up workspace in new inparms_15g.
  !     * feb 07/2007 - m.lazare/    previous version psizes_15f for gcm15f:
  !     *               j.cole.      - modify abort condition to allow ioztyp=3.
  !     *                            - add sizes for isccp simulator.
  !     * may 31/2006 - m.lazare.    - remove calculation of iwrkph and iva,
  !     *                              since are no longer needed with all
  !     *                              associated workspace removed for this
  !     *                              new version.
  !     * dec 15/2005 - k.vonsalzen. previous version psizes_15d for gcm15d/e:
  !     *                            - ntracnc augmented by two (for two
  !     *                              momentum components).
  !     *                            - iwrkc augmented by one grid slice
  !     *                              for added convection internal
  !     *                              work array.
  !     * may 15/2004 - l.solheim. previous version psizes_15b for gcm15b/c.
  !=======================================================================
  !
  use msizes
  implicit none
  integer :: ilendyn
  integer :: ilenrst
  integer :: ipid
  integer :: ipidtmp
  integer :: ivaltr
  integer :: maxlen
  integer :: nlev1e
  integer :: nlev2e
  integer :: nlev3e
  integer :: nleve
  !.....work space requirements
  !     ipid is calculated in inparms_16

  !.....land surface (class)
  !
  !     * number of distinct canopy types in class
  integer, parameter :: ican = 4
  integer, parameter :: icanp1 = ican + 1
  !
  !     * number of soil layers in class
  !
  integer, parameter :: ignd = 3
  real, parameter , dimension(ignd) :: delz = (/0.10,0.25,3.75/)
  real, parameter , dimension(ignd) :: zbot = (/0.10,0.35,4.10/)
  !
  !     * number of vegetation types
  !
  integer, parameter :: ictem = 9
  integer, parameter :: ictemp1 = ictem + 1

  integer, dimension(4), parameter :: nol2pfts = (/ 2, 3, 2, 2 /)
  integer, parameter :: l2max = 3
  !
  !     * maximum number of lake levels in cslm.
  !
  integer, parameter :: nlklm = 200
  integer, parameter :: igl = 1
  !
  !     * gathered array sizes.
  !
#ifdef use_spectral
  integer, parameter :: ilglc=ilgl*ican !<
  integer, parameter :: ilgim=ilg*im !<
#else if defined use_gem
  integer :: ilglc
  integer :: ilgim
#endif

  !.....ozone

  !     * switch for input ozone distribution
  integer, parameter :: ioztyp = _PAR_IOZTYP

  !     * number of ozone levels
  integer, parameter :: levoz = _PAR_LEVOZ

  !     * number of levels in randel ozone dataset
  integer, parameter :: levozc = 24

  !...Oxidants

  !   * switch for input oxidants distribution
  integer, parameter :: ioxtyp = _PAR_IOXTYP

  !   * number of oxidants levels
  integer, parameter :: levox = _PAR_LEVOX

  !.....stratospheric aerosols
  !     * number of levels in ethz stratospheric aerosols dataset
#if defined explvol
      integer, parameter :: levsa = 70
#else
      integer, parameter :: levsa = 1
#endif

  !.....radiation

  !     * number of spectral intervals in shortwave radiation code
  integer, parameter :: nbs = 4

  !     * number of spectral intervals in longwave radiation code
  integer, parameter :: nbl = 9


  !     * number of windfires and open burning levels
  integer, parameter :: levwf = 6
  !     * number of aircraft emission levels
  integer, parameter :: levair = 25


#if defined use_mcica
      integer, parameter :: mcica = 1
#else
      integer, parameter :: mcica = 0
#endif

  ! nxloc => number of subcolumns to use for mcica
#if (defined(use_mcica) || defined(use_cosp))
      integer, parameter :: nxloc = 150
#else
      integer, parameter :: nxloc = 1
#endif

#if defined(use_mcica)
! potential maximum number of samples that will be taken
! in the mcica computations
#if defined(radforce)
      integer, parameter :: max_sam = 600
#else
      integer, parameter :: max_sam = 1
#endif
#else
      integer, parameter :: max_sam = 1
#endif

#if (defined(use_mcica) || defined(use_cosp))
! define the pdf to use for the horizontal variability of cloud condensate
! idist = 1 use beta distribution
! idist = 2 use gamma distribution
      integer, parameter :: idist = 2

! ioverlap = 0 => maximum-random overlap of clouds (cccma)
! ioverlap = 1 => maximum-random overlap of clouds (geleyn and hollingsworth)
! ioverlap = 2 => set overlap using decorrelation lengths
      integer, parameter :: ioverlap = 2

! horizontal variability flag for cloud generator
! ipph = 0 => homogeneous clouds
! ipph = 1 => inhomogeneous clouds
      integer, parameter :: ipph = 1

! ipph_re = 0 => homogeneous effective radius in clouds
! ipph_re = 1 => inhomogeneous effective radius in clouds
! ivers = 1, 2 or 3 => sets the level of mcica noise (1 clds (most noise), 2 spec, 3 ref (least noise))
      integer, parameter :: ipph_re = 0
      integer, parameter :: ivers = 1
#endif
  !
#if defined radforce
!
!     * number of radiative forcing perturbations employed
      integer, parameter :: nrfp = _PAR_NRFP
#else
      integer, parameter :: nrfp = 1
#endif
#ifdef use_spectral
#if (defined(radforce) || defined(rad_flux_profs))
!
!     * number of levels in diagnostic output fields for
!     * radiative forcing.
      integer, parameter :: levrf = ilev + 2
#else
      integer, parameter :: levrf = 1
#endif
#else if defined use_gem
      integer :: levrf
#endif
#if (defined(pla) && defined(pam))
!     * array dimensions for pla aerosol calculations

      integer, parameter :: msgt = ilev - levs
      integer, parameter :: msgp1 = msgt + 1
      integer, parameter :: msgp2 = msgt + 2
#endif

contains

  subroutine inparms_16(ipid)
    !=======================================================================
    !     * feb 18/2018 - m.lazare.  new support for ioztyp=6 for cmip6.
    !     * may 08/2012 - m.lazare.  new version for gcm16:
    !     *                          - support ioztyp=5.
    !     * dec 21/2007 - m.lazare.  new version for gcm15g/h/i:
    !     *                          - code cleaned up in conjunction with
    !     *                            new scadr15; only used pointers kept.
    !     * jun 01/2006 - m.lazare.  previous version for gcm15f:
    !     *                          - iwrkph and iva no longer calculated since
    !     *                            since physics workspace using pointers
    !     *                            replaced by internal work arrays
    !     *                            for this version.
    !     * nov 27/2003 - l.solheim. previous version inparms_15d for
    !     *                          gcm15b/c/d/e.
    !     *                          - perform consistency checks on parmsub
    !     *                            parameters and calculate iwrkph, iva
    !     *                            and ipid work space dimensions
    !=======================================================================
    implicit none
    integer, intent(inout) :: ipid
    !.......perform consistency checks on parmsub parameters
#ifdef use_spectral
    if (nnodex < 1)                 call xit("INPARMS", - 1)
#if defined transient_ozone_concentrations
        !   * only randel and cmip6 ozone have transient data !
        if (ioztyp /= 4 .and. ioztyp /= 6) call xit("INPARMS", - 2)
#endif
    if (ioztyp < 1 .or. ioztyp > 6)  call xit("INPARMS", - 3)

    ! * permitted choices for oxidants.
    if (ioxtyp < 0 .or. ioxtyp > 1)  call xit("INPARMS", - 4)

    if (lonp >= lonsl) then
      if (mod(lonp,lonsl) /= 0)      call xit("INPARMS", - 5)
    else
      if (mod(lonsl,lonp) /= 0)      call xit("INPARMS", - 6)
    end if

    if (lond >= lonsld) then
      if (mod(lond,lonsld) /= 0)     call xit("INPARMS", - 7)
    else
      if (mod(lonsld,lond) /= 0)     call xit("INPARMS", - 8)
    end if
    if (mod(nlat, nnodex) /= 0)     call xit("INPARMS", - 9)
    if (mod(nlatd,nnodex) /= 0)     call xit("INPARMS", - 10)
    if (mod(ilat, nlatj) /= 0)      call xit("INPARMS", - 11)
    if (mod(ilatd,nlatjd) /= 0)     call xit("INPARMS", - 12)
    if (mod(lmtotal,nnodex) /= 0)   call xit("INPARMS", - 13)
    if (mod(lmtotal,2) /= 0)        call xit("INPARMS", - 14)
    if (mod(latotal,nnodex) /= 0)   call xit("INPARMS", - 15)
    if (mod(latotal,2) /= 0)        call xit("INPARMS", - 16)

    !-----------------------------------------------------------------------
    !       * ipid work space calculation
    !-----------------------------------------------------------------------
    !       * the work space of size "ipid" is used in several areas of the
    !       * model: dynamics (ilendyn), mhexp_ (ilenmhe),
    !       * mhanl_ (ilenmha), restart (ilenrst) and energetics.

    ilendyn = ilgd * (6 * ilev + 3) + ilev * (4 * la + 2 * ilev + 5) + 2 * la

    !       * for multiple-latitudes, work space is required for swapping
    !       * data for maximum efficiency.
    !       * the factor of four in the expression for "ivaltr" stems from
    !       * the fact that the work space internal to these routines
    !       * reflects operations on complex :: variables and there are 2 arrays.

    nlev1e = 3 * ilev + levs + ntraca * ilev + 1
    nlev2e = 2 * ilev + 1
    nlev3e = 2 * ilev
    nleve = nlev1e+nlev2e+1
    ivaltr = 4 * max( ilhd * nlatjd * nleve, ilh * nlatj * nleve)

    !       * space for rstart

    ilenrst = ilev * 2

    !       * resulting total space

    ipidtmp = max(ilendyn,ilenrst,ivaltr)
    ipid = ipidtmp + 1

    !       write(6,*)'ILENDYN: ',ILENDYN
    !       write(6,*)'NLEV1E,NLEV2E,NLEV3E,NLEVE,IVALTR: ',
    !    &             nlev1e,nlev2e,nlev3e,nleve,ivaltr
    !       write(6,*)'ILHD,NLATJD,NLEVE,ILH,NLATJ,NLEVE: ',
    !    &             ilhd,nlatjd,nleve,ilh,nlatj,nleve
    !       write(6,*)'ILENRST: ',ILENRST

    !       * maxlen is an arbitrary limit on work space components
    !       * it should be increased if and when required

    maxlen = 999999999

    !       * error exits

    if (ipid  > maxlen)            call xit("INPARMS", - 18)
    !
#endif
    return
  end subroutine inparms_16

end module psizes_19
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
