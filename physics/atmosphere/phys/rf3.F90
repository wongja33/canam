!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
function rf3(x,y,z)
  !
  !     * jun 15/2013 - m.lazare.   new version for gcm17+:
  !     *                           - define "TINY" and "BIG" in
  !     *                             double-precision format, to
  !     *                             avoid inaccuracy who showed up
  !     *                             (only) in rcm.
  !     * sep 11/2006 - f.majaess.  previous version rf2, hard-coding
  !     *                           real(8).
  !     * mar 24/1999 - j scinocca. original version rf in "COMM".
  !
  !     * numerical recipes routine used to evaluate incomplete
  !     * elliptic integrals. called by gwlooku2 through functions
  !     * ef2,kf2
  !     * "PAUSE" statement is replaced with with a call to the
  !     * "XIT" subroutine.
  !     (c) copr. 1986-92 numerical recipes software #=! e=#,)]ubcj
  !
  implicit none
  real*8 :: rf3
  real*8 :: errtol
  real*8 :: tiny
  real*8 :: big
  real*8 :: third
  real*8 :: c1
  real*8 :: c2
  real*8 :: c3
  real*8 :: c4
  real*8, intent(in) :: x !< Variable description\f$[units]\f$
  real*8, intent(in) :: y !< Variable description\f$[units]\f$
  real*8, intent(in) :: z !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  parameter (errtol = 0.08e0,tiny = 1.5d-76,big = 3.d74,third = 1.e0/3.e0, &
                      c1 = 1.e0/24.e0,c2 = 0.1e0,c3 = 3.e0/44.e0,c4 = 1.e0/14.e0)

  real*8 :: alamb
  real*8 :: ave
  real*8 :: delx
  real*8 :: dely
  real*8 :: delz
  real*8 :: e2
  real*8 :: e3
  real*8 :: sqrtx
  real*8 :: sqrty
  real*8 :: sqrtz
  real*8 :: xt
  real*8 :: yt
  real*8 :: zt
  !======================================================================
  if (min(x,y,z) < 0.e0 .or. min(x + y,x + z,y + z) < tiny &
      .or. max(x,y,z) > big) then
    write(6,'(A)') ' INVALID ARGUMENTS IN RF3'
    call                                       xit ('RF3', - 1)
  end if

  xt = x
  yt = y
  zt = z

1 continue
  sqrtx = sqrt(xt)
  sqrty = sqrt(yt)
  sqrtz = sqrt(zt)
  alamb = sqrtx * (sqrty + sqrtz) + sqrty * sqrtz
  xt = 0.25e0 * (xt + alamb)
  yt = 0.25e0 * (yt + alamb)
  zt = 0.25e0 * (zt + alamb)
  ave = third * (xt + yt + zt)
  delx = (ave - xt)/ave
  dely = (ave - yt)/ave
  delz = (ave - zt)/ave
  if (max(abs(delx),abs(dely),abs(delz)) > errtol)goto 1

  e2 = delx * dely - delz ** 2
  e3 = delx * dely * delz
  rf3 = (1.e0 + (c1 * e2 - c2 - c3 * e3) * e2 + c4 * e3)/sqrt(ave)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
