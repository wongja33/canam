!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine fourb(fourj1,grid,nlev, &
                       ilh,nlat,ilat,joff,lm, &
                       lon,ifax,trigs, &
                       four1,wrks)
  !
  !     * nov 16/03 - m. lazare.   revised to support revised swapf4x.
  !     * oct 29/03 - m. lazare.   new generic routine based on mhanlp_
  !     *                          and/or mhanlq_. with the number of
  !     *                          total levels ("NLEV") passed in, this
  !     *                          can now be used in either case.
  !     *                          legendre moved to new legbp routine.
  !     * aug 03/03 - m. lazare. - now using column-wise transform, so
  !     *                          new calls to swapf3x and swapf4x.
  !     *                        - other unnecessary input arrays removed.
  !     * aug 01/03 - m. lazzre. - calls new ffwfg4.
  !     * jun 25/03 - m. lazare. - final output array is "SPEC", not
  !     *                          actual spectral fields, since "SWAPS1"
  !     *                          is moved out of parallel region into
  !     *                          model driver. thus wrkt removed.
  !     *                        - dimension->real.
  !     * nov 22/98 - m. lazare. previous routine mhanlp3.
  !     *
  !     * transform grid values to fourier coefficients.
  !     *
  !     * input fields
  !     * ------------
  !     *
  !     * as in fourf, this routine depends on data in /grd/ being aligned
  !     * appropriately (to increase the effective vector length). the
  !     * total number of levels (for all variables) is "NLEV".

  !     * the data in the above grid fields is not in the optimal organization
  !     * for the present routine.  on entry into this routine, the data in
  !     * the above common block is transposed into working array "FOUR1".
  !     *
  !
  !     * output fields:
  !     * -------------
  !
  !     * fourier array:  the fourj1 array is big enough to contain all
  !     * the data for the ffts.
  !
  !     * work array:
  !     * ----------
  !
  !     * the array "WRKS" must be dimensioned at least as large as the sum
  !     * of the sizes of all the input grid data.
  !     *
  !     * additional parameters
  !     * ---------------------
  !     *
  !     * ilh  = first dimension of complex :: arrays.
  !     * lon  = number of distinct longitudes.
  !     * nlat = number of latitudes processed in single pass. [ajs 29/sep/92]
  !     * ilat = number of latitudes on a single node.
  !     * joff = offset into total latitude set for given task.
  !     * nlev = number of total vertical levels (across all variables)
  !     *        being transformed.

  !     *
  !     * routines called
  !     * ---------------
  !     *
  !     * ffwfg4 - grid to fourier transforms.
  !     * swapf3x- internal data reorganization routines.
  !     * swapf4x-
  !     *
  implicit none
  integer, intent(in) :: ilat
  integer :: ilg
  integer, intent(in) :: ilh
  integer :: ir
  integer, intent(in) :: joff
  integer, intent(in) :: lm
  integer, intent(in) :: lon
  integer, intent(in) :: nlat
  integer, intent(in) :: nlev
  integer :: nlevn
  !
  !     * fourier output arrays.
  !
  real, intent(in), dimension(2,nlev,ilat,ilh) :: fourj1 !< Variable description\f$[units]\f$
  !
  !     * input array.
  !
  real, intent(in) :: grid(*) !< Variable description\f$[units]\f$
  !
  !     * work arrays.
  !
  real, intent(in), dimension(2, ilh, nlat, nlev) :: four1 !< Variable description\f$[units]\f$
  real, intent(in), dimension(2, ilh, nlat, nlev) :: wrks !< Variable description\f$[units]\f$
  !
  !     * other arrays.
  !
  real, intent(in), dimension(lon) :: trigs !< Variable description\f$[units]\f$
  integer, intent(in) :: ifax(*) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !-----------------------------------------------------------------------
  ir    = lm - 1
  ilg   = 2*ilh
  !
  !     * transpose the grid input field into the respective
  !     * "FOURx" working array.
  !     * i.e.  grid(2*ilh*nlat+1,nlev) to four(2,ilh,nlat,nlev).
  !     * this puts the grid data into column-wise format for the ffts.
  !
  call swapf3x(grid,ilh,nlat,nlev,lon,four1)
  !
  !     * all the fft are done at once.
  !
  nlevn = nlev*nlat
  call ffwfg4(four1,ilh,four1,ilg,ir,lon,wrks,nlevn,ifax,trigs)
  !
  !     * after the fft, the data must be again transformed into the
  !     * format expected by the legendre transform routine.
  !
  call swapf4x(fourj1,four1,nlev,nlat,ilat,joff,ilh)
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
