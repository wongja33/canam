!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

 module gem_wrapper_mod
  implicit none

  ! Indices of the first local (Physics) grid point in the CPU tile, within the global grid
  integer :: lcl_i0
  integer :: lcl_j0
!
!  Model step monthly saving frequency
  integer :: ismon
!
  real :: gmt
!
!  Model top pressure
  real :: ptoit

!     * indxna,indxa - index values of nonadvected/advected model tracers
  integer, dimension(:), allocatable :: indxna !<
  integer, dimension(:), allocatable :: indxa  !<

  save

 end module gem_wrapper_mod
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
