!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine intfld(row,rol,il1,il2,ilg,delt,gmt,iday,mday)
  !
  !     * may 03/14 - m.lazare. new routine for gcm18+:
  !     *                       - based on intgtio2 but no
  !     *                         dependance on gcrow
  !     *                         (hence "IMASK" removed also).
  !     *                         made general for any field by
  !     *                         removing references to "GT".
  !     * apr 29/03 - m.lazare. previous version intgtio2:
  !     *                       1,lon -> il1,il2.
  !     * oct 16/97 - m.lazare. previous version intgtio.
  !
  !     * input/output:
  !     * row  = current value array.
  !     * input:
  !     * rol  = target value array (valid at mday).
  !
  !     * il1    = start longitude index.
  !     * il2    = end   longitude index.
  !     * ilg    = dimension of longitude arrays.
  !     * delt   = model timestep in seconds.
  !     * gmt    = number of seconds in current day.
  !     * iday   = current julian day.
  !     * mday   = date of target field.
  !
  implicit none

  real, intent(inout), dimension(ilg) :: row !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rol !< Variable description\f$[units]\f$

  real, intent(in) :: delt
  real, intent(in) :: gmt

  integer, intent(in) :: il1
  integer, intent(in) :: il2
  integer, intent(in) :: ilg
  integer, intent(in) :: iday
  integer, intent(in) :: mday  
  
  real :: day, fmday, daysm, stepsm
  integer :: i
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  day=real(iday)+gmt/86400.
  fmday=real(mday)
  if (fmday<day) fmday=fmday+365.
  daysm=fmday-day
  stepsm=daysm*86400./delt
  !
  !     * general interplation.
  !
  do i=il1,il2
    row  (i)  = ((stepsm-1.) * row  (i) + rol  (i)) / stepsm
  end do ! loop 210
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
