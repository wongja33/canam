!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * nov 19, 2012 - k.vonsalzen.  new version for gcm17+:
!     *                              clean-up and merging with pla code.
!     * apr 27, 2010 - k.vonsalzen.  previous version intemi up to gcm16.
!     * interpolate tracer emission.
!---------------------------------------------------------------------
!     * interpolate basic tracer emission arrays.
!
call intcac(phys_arrays%eostrow,phys_arrays%eostrol, &
                  il1,il2,ilg,ilev,delt,gmt,iday,mdayt)
!
#if defined transient_aerosol_emissions
      call intprf(phys_arrays%fbbcrow,phys_arrays%fbbcrol,phys_arrays%fairrow,phys_arrays%fairrol, &
             ilg,il1,il2,levwf,levair,delt,gmt,iday,mdayt)
#endif
!---------------------------------------------------------------------
#if defined emists
#ifndef pla
!     * initialize 2d tracer emission arrays.
!
      x2demisa(il1:il2,:)=0.
      x2demiss(il1:il2,:)=0.
      x2demisk(il1:il2,:)=0.
      x2demisf(il1:il2,:)=0.
!
#endif
#endif
#if defined emists
!     * interpolate emissions.
!
      call inttemi2(                                                                                             &
            phys_arrays%sairrow,phys_arrays%sairrol,phys_arrays%ssfcrow,phys_arrays%ssfcrol,phys_arrays%sbiorow, &
            phys_arrays%sbiorol,phys_arrays%sshirow,phys_arrays%sshirol,phys_arrays%sstkrow,phys_arrays%sstkrol, &
            phys_arrays%sfirrow,phys_arrays%sfirrol,phys_arrays%oairrow,phys_arrays%oairrol,phys_arrays%osfcrow, &
            phys_arrays%osfcrol,phys_arrays%obiorow,phys_arrays%obiorol,phys_arrays%oshirow,phys_arrays%oshirol, &
            phys_arrays%ostkrow,phys_arrays%ostkrol,phys_arrays%ofirrow,phys_arrays%ofirrol,phys_arrays%bairrow, &
            phys_arrays%bairrol,phys_arrays%bsfcrow,phys_arrays%bsfcrol,phys_arrays%bbiorow,phys_arrays%bbiorol, &
            phys_arrays%bshirow,phys_arrays%bshirol,phys_arrays%bstkrow,phys_arrays%bstkrol,phys_arrays%bfirrow, &
            phys_arrays%bfirrol,ilg,il1,il2,delt,gmt,iday,mdayt)
#endif
#if defined emists
#ifndef pla
!
!---------------------------------------------------------------------
!     * fill 2d tracer emisssion arrays with interpolated results.
!
      x2demisa(il1:il2,iso2)=phys_arrays%sairrow(il1:il2)
      x2demiss(il1:il2,iso2)=phys_arrays%ssfcrow(il1:il2)
      x2demisk(il1:il2,iso2)=phys_arrays%sstkrow(il1:il2)
      x2demisf(il1:il2,iso2)=phys_arrays%sfirrow(il1:il2)
!
      x2demisa(il1:il2,ioco)=.5*phys_arrays%oairrow(il1:il2)
      x2demiss(il1:il2,ioco)=.5*phys_arrays%osfcrow(il1:il2)
      x2demisk(il1:il2,ioco)=.5*phys_arrays%ostkrow(il1:il2)
      x2demisf(il1:il2,ioco)=.5*phys_arrays%ofirrow(il1:il2)
      x2demisa(il1:il2,iocy)=.5*phys_arrays%oairrow(il1:il2)
      x2demiss(il1:il2,iocy)=.5*phys_arrays%osfcrow(il1:il2)
      x2demisk(il1:il2,iocy)=.5*phys_arrays%ostkrow(il1:il2)
      x2demisf(il1:il2,iocy)=.5*phys_arrays%ofirrow(il1:il2)
!
      x2demisa(il1:il2,ibco)=.8*phys_arrays%bairrow(il1:il2)
      x2demiss(il1:il2,ibco)=.8*phys_arrays%bsfcrow(il1:il2)
      x2demisk(il1:il2,ibco)=.8*phys_arrays%bstkrow(il1:il2)
      x2demisf(il1:il2,ibco)=.8*phys_arrays%bfirrow(il1:il2)
      x2demisa(il1:il2,ibcy)=.2*phys_arrays%bairrow(il1:il2)
      x2demiss(il1:il2,ibcy)=.2*phys_arrays%bsfcrow(il1:il2)
      x2demisk(il1:il2,ibcy)=.2*phys_arrays%bstkrow(il1:il2)
      x2demisf(il1:il2,ibcy)=.2*phys_arrays%bfirrow(il1:il2)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
