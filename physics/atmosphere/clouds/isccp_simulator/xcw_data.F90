!>\file
!>\brief Stochastic Cloud Generator Data module.
!!
!! @author Routine author name(s)
!
 module xcw_data

!      * Dec 12/2007 - Jason Cole.   New version for GCM15g:
!      *                             option "idist" passed in to
!      *                             permit use of gamma or beta
!      *                             distribution for horizontal
!      *                             distribution of cloud condensate.
!      * Jan 09/2007 - Jason Cole.   Previous version xcw_data For GCM15f.
!

  implicit none

  integer, parameter :: n1 = 1000
  integer, parameter :: n2 = 140
  real, dimension(n1, n2) :: xcw

  save
!
  contains
!
! *Subroutine to generate xcw data needed by stochastic cloud generator
!
   subroutine calc_xcw_data()
      use psizes_19, only: idist !< Distribution to use for horizontal distribution of cloud condensate\f$[units]\f$
!==================================================================
! PHYSICAL (ADJUSTABLE) PARAMETERS
!
! Define and document here any adjustable parameters.
! This should be variable described using the Doxygen format above as
! well as a description of its minimum/default/maximum.
!
! Here is an example,
!
! REAL BETA !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
!           !! It is compute differently when using bulk or PAM aerosols.
!           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
!==================================================================
! Local variables
   integer :: i
   integer :: j
   real :: avg
   real :: std
   real :: a
   real :: b
   real :: bg
   real :: c
   real :: d
   real :: r
   real :: q
   real :: beta
   real :: alpha
   real :: prob_min
   real :: prob
   real :: dist
   real :: a_beta
   real :: a_gamma

! loop over standard deviations
   do j = 1, n2
! mean and std dev
      avg = 1.0
      std = 0.025 * (j+3)
!
! upper and lower limits for beta dist (a and b).
! upper limit for gamma dist (bg).
! bg superficially optimized by p. raisanen (july 2002?)
!
      a  = 0.0
      b  = (5. + 5.* std**2) * avg
      bg = (5. + 5.* std**2) * avg
!
! using mean and std dev, determine parameters of
! beta dist and gamma dist
!
      c = (avg - a) / (b - avg)
      d = ((b - a) / std)**2
      r = c * (d - 2.0 - c) / (c * (c**2 + 3.0 * c + 3.0) + 1.0)
      q = c * r
      beta  = avg / std**2
      alpha = avg * beta
      prob_min = 0.0
! - prob = cumulative frequency
! - a_beta and a_gamma = returned value given prob
!
      if (idist == 2) then ! gamma distribution
         dist = 2.0
         do i = 1, n1
            prob = real(i - 1.0) / real(n1 - 1.)
            call root_limit(dist, q, r, a, bg, alpha, beta, prob_min, prob, a_gamma)
            xcw(i, j) = a_gamma
         end do ! i
      else if (idist == 1) then ! beta distribution
         dist = 1.0
         do i = 1, n1
            prob = real(i - 1.0) / real(n1 - 1.0)
            call root_limit(dist, q, r, a, bg, alpha, beta, prob_min, prob, a_beta)
            xcw(i, j) = a_beta
         end do ! i
      else
          write(*, *) "inproper choice for idist in xcw_data!"
          write(*, *) "valid values for idist is 1 or 2."
          write(*, *) "current value to idist is: ",idist
          write(*, *) "stopping in xcw_data."
          stop
      end if
   end do

   return
   end subroutine calc_xcw_data

 end module xcw_data
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

