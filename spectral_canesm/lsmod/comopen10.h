#include "cppdef_config.h"
!! --- NEW: #define with_COUPLED_
!! --- NEW: #define with_MPI_
!! --- NEW: #define gaschem

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
! * aug 17/2021 - B. Winter   Add new units 28:33 for gas-phase chemistry input
! * mar 22/2019 - j. cole      add new unit numer 18 for site output (nusite).
! * nov 03/2018 - m.lazare.    add new unit number 17 for 3-hr saving (nu3hr).
! * jul 27/2018 - m.lazare.    remove {com,csp,csn} parmsub variables.
! * jun 12/2018 - m.lazare.    - add new unit number 25 (nuch) for chem
! *                              and new unit number 26 (nuox) for oxi.
! * jul 13/2013 - k.vonsalzen. new version for gcm17:
! *                            - additional unit numbers for pam.
! * may 08/2012 - j.scinocca.  previous version comopen9 for gcm16:
! *                            - add irelax unit number and open for
! *                              relaxation cpp option.
! * may 05/2010 - k.vonsalzen/ previous version comopen8 for gcm15i:
! *               m.lazare.    - "HISTEMI" update directive
! *                              replaced by
! *                              "transient_aerosol_emissions" cpp
! *                              directive.
! *                            - add new "transient_ozone_emissions"
! *                              cpp directive option.
! *                            - other update directives replaced
! *                              by cpp directives.
! * feb 23/2007 - k.vonsalzen. previous version comopen7 for gcm15h:
! *                            - define and open new unit for
! *                              transient aerosol emissions
! *                              under control of "%DF HISTEMI".
! * jan 10/2004 - m.lazare. previous version comopen6 for
! *                         gcm13c/gcm15b to gcm15g:
! *                         - opening of outgcm,outinv,outend logical
! *                           units moved to gcm driver and done
! *                           differently depending on whether doing
! *                           parallel i/o or not.
! *                         - "FIZ" removed.
! *                         - define "IPARIO" common block and
! *                           initialize its variable "IPIO" based
! *                           on update directive "PARALLEL_IO".
! *                         - value for npgg now hardcoated to zero.
! * may 30/2003 - m.lazare. new version for ibm (comopen5):
! *                         - "FIZTRAC" removed.
! *                         - npps,npsf,npvg removed.
! * dec 12/2001 - m.lazare. previous version comopen4:
! *                         add nuinv/luinv/outinv unit for
! *                         invariant fields. based on comopen3.
!
! * machine constants for packing routines.
!     * "MACHINE" has a value of 1 for 64-bit machines (integer :: size)
!     * and a value of 2 for 32-bit machines (integer :: size).
!     * "INTSIZE" is the ratio of the floating-point word size to
!     * integer :: size (i.e. is unity unless running real*8 :: on a 32-bit
!     * machine).
!
common /machtyp/ machine,intsize
!
! * define "IPARIO" common block and initialize its variable "IPIO" based
! * on update directive "PARALLEL_IO".
!
common /ipario/ ipio
!
! * string arrays for defining outgcm,outend,outinv,outtm unit names.
!
character*128 :: oname !<
character*128 :: tname !<
character*128 :: iname !<
character*128 :: cname !<
character*128 :: hname !<
character*128 :: sname !<
!
#if defined parallel_io
  data ipio   /1 /
#else
  data ipio   /0 /
#endif
!
! * Logical units.
! * Note that LUIA must not change because its value is used in
! * putggb3 to use kount in ibuf(2) even when running with multi-year
! * date-time stamp (ie when running with interactive ocean).
!
data npgg   /0 /
data nusp   /11/
data nugg   /12/
data nupr   /13/
data nurs   /14/
data nuia   /15/
data nu3hr  /17/
data nusite /18/
data luia   /19/
data nutd   /20/
data nuinv  /21/
#if defined transient_aerosol_emissions
  data nutr   /22/
#endif
#if defined transient_ozone_concentrations
  data nuoz_chem /23/
  data nuoz_rad  /24/
#endif
data nuch      /25/
data nuox      /26/
#if defined (agcm_ctem)
  data nutm      /27/
#endif
#if defined relax
  data iorelax_spc/35/      ! spectral p/c/t/es/ps reference
  data iorelax_tspc/36/     ! spectral bias-corrections
  data iorelax_tg1/37/      ! land tg1 reference
  data iorelax_wg1/38/      ! land wg1 reference
  data iorelax_tgrd/39/     ! gridded bias-corrections
  data iorelax_sst/40/      ! sst reference
  data iorelax_sicn/41/     ! sicn reference
  data iorelax_sic/42/      ! sic reference
  data iorelax_sss/43/      ! sss reference
#endif
#if defined (pla)
#if defined (pfrc)
integer :: nupf = 81 !<
#endif
#endif
#if defined with_MPI_ || defined with_COUPLED_
      call mpi_init(ierr)
#endif
!
open(nusp,file = 'OLDRS',form = 'UNFORMATTED')
open(nugg,file = 'AN',form = 'UNFORMATTED')
open(nuch,file = 'CHEM',form = 'UNFORMATTED')
open(nuox,file = 'OXI',form = 'UNFORMATTED')

#if defined transient_aerosol_emissions
  open(nutr,file = 'TRANS',form = 'UNFORMATTED')
#endif
#if defined transient_ozone_concentrations
  open(nuoz_chem,file = 'HISTOZ_CHEM',form = 'UNFORMATTED')
  open(nuoz_rad,file = 'HISTOZ_RAD',form = 'UNFORMATTED')
#endif
#if defined gaschem
  data nujtab1   /28/
  data nujtab2   /29/
  data nusfcem   /30/
  data nuacftem  /31/
  data nussad    /32/
  data nuso4     /33/

  open(nujtab1, file='JTAB1',   form='unformatted')
  open(nujtab2, file='JTAB2',   form='unformatted')
  open(nusfcem, file='SFCEMIS', form='unformatted')
  open(nuacftem,file='ACFTEMIS',form='unformatted')
  open(nussad,  file='SSADF',   form='unformatted')
  open(nuso4,   file='SULPHATE',form='unformatted')
#endif
#if defined relax
  open(iorelax_spc,file = 'SPCFILE',form = 'UNFORMATTED')
  open(iorelax_tspc,file = 'TSPCCLIM',form = 'UNFORMATTED')
  open(iorelax_tgrd,file = 'TGRDCLIM',form = 'UNFORMATTED')

  open(iorelax_sst,file = 'SSTFILE',form = 'UNFORMATTED')
  open(iorelax_sicn,file = 'SICNFILE',form = 'UNFORMATTED')
  open(iorelax_sic,file = 'SICFILE',form = 'UNFORMATTED')
  open(iorelax_sss,file = 'SSSFILE',form = 'UNFORMATTED')

  open(iorelax_tg1,file = 'TG1FILE',form = 'UNFORMATTED')
  open(iorelax_wg1,file = 'WG1FILE',form = 'UNFORMATTED')
#endif
!
!     * define machine constants (passed to i/o routines via common
!     * block machtyp.
!
machine = me32o64(idummy)
intsize = inteflt(idummy)
!
!     * define units for multi-year sst/ice input files and set
!     * myrssti switch and "AN" access units accordingly.
!
#if defined myrssti
      myrssti = 1
!
      data lugc   /50/
      data lugt   /51/
      data lusic  /52/
!
      open(lugc,file = 'NUGC',form = 'UNFORMATTED')
      open(lugt,file = 'NUGT',form = 'UNFORMATTED')
      open(lusic,file = 'NUSIC',form = 'UNFORMATTED')
!
      nugc = lugc
      nugt = lugt
      nusic = lusic
#else
      myrssti = 0
      nugc = nugg
      nugt = nugg
      nusic = nugg
#endif
!
data luxsfx  /53/
open(luxsfx,file = 'NUXSFX',form = 'UNFORMATTED')
nuxsfx = luxsfx
!
#if (defined(pla) && defined(pam))
      data nucoa /77/
      data nuact /78/
      data nurad1/79/
      data nurad2/80/
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

