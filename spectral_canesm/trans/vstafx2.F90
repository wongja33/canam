!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine vstafx2(f,s,alp, &
                         nlev,nlat,ntot)

  !     * sep 11/2006 - f.majaess. new version for gcm15f:
  !     *                          revised to call "sgemm" for r4i4 mode.
  !     * oct 29/2003 - r.mclay/  previous version vstafx from ibm conversion:
  !     *               m.lazare. new legendre transform, like vstaf except
  !     *                         uses vendor-supplied dgemm.
  !     *                         ** note ** : alp order must be reversed
  !     *                         to use this !
  !     * oct 30/92. -  a.j.stacey. previous version vstaf.
  !
  !     * note: the usual input to routines such as blas is 32-bit reals
  !     *       and integers. since, however, we are using "dgemm", the
  !     *       input is 64-bit reals and 32-bit integers. hence, the
  !     *       required interface changes are to use 4-byte integers for
  !     *       any integers passed to "dgemm".
  !     *
  !     * also note that since "S" and "F" are complex :: in the calling
  !     * routine and since the level dimension is the innermost, all
  !     * sizes referring to level are passed as "2*...".
  !
  implicit none
  real, intent(in) :: s(2*nlev,ntot) !< Variable description\f$[units]\f$
  real, intent(in) :: f(2*nlev,nlat) !< Variable description\f$[units]\f$
  real*8, intent(in)  :: alp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real :: alpha !<
  real :: beta !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work array for 32-bit case.
  !     * this is required since sgemm requires 32-bit arrays and
  !     * "alp" is 64-bit from agcm driver !
  !
  real*4  :: alp4(nlat,ntot+1) !<
  !
  integer*4 :: ntot4 !<
  integer*4 :: nlat4 !<
  integer*4 :: nlev4 !<
  integer, intent(in) :: nlev !<
  integer, intent(in) :: nlat !<
  integer, intent(in) :: ntot !<
  integer :: machine !<
  integer :: intsize !<
  !
  !     * common block to hold word size.
  !
  common /machtyp/ machine,intsize
  !-----------------------------------------------------------------------
  alpha  = 1.
  beta   = 0.
  ntot4  = ntot
  nlat4  = nlat
  nlev4  = 2*nlev
  !
  if (machine==2) then
    alp4(1:nlat,1:ntot+1)=alp(1:nlat,1:ntot+1)
    call sgemm('N', 'T', nlev4 , nlat4, ntot4, alpha, s, &
                  nlev4, alp4, nlat4, beta, f, nlev4)
  else
    !$$$       call dgemm('N', 'T', nlev4 , nlat4, ntot4, alpha, s,
    !$$$     1           nlev4, alp, nlat4, beta, f, nlev4)
    call dgemm('N', 'T', nlev*2 , nlat, ntot, alpha, s, &
                  nlev*2, alp, nlat, beta, f, nlev*2)
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
