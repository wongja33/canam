!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getlakes(ocean_target_i,ocean_target_j,ocean_target_n, &
                    ot_lakes_i,       ot_lakes_j,       ot_lakes_n, &
                    glocean_target_i, glocean_target_j, glocean_target_n, &
                    ot_glakes_i,      ot_glakes_j,      ot_glakes_n, &
                    land_target_i,  land_target_j,  land_target_n, &
                    lt_lakes_i,     lt_lakes_j,     lt_lakes_n, &
                    great_lakes_i,  great_lakes_j,  great_lakes_n, &
                    gl_land_i,      gl_land_j,      gl_land_n, &
                    gl_delay)


  !     *  get the key-directed targetting information from the standard input
  !     *  (standard input opened by jclpnt in fluxadj main routine).

  !     * nov 07/2015 - m.lazare. coupler code modified for use in agcm:
  !     *                         - no cpp diretives.
  !     *
  !     * additional great lakes modifications
  !     *       jul19/2000     daniel robitaille
  !     *
  !     * original code
  !     *                      scott tinis
  !     *
  !     *

  implicit none

  integer :: i
  integer :: j
  integer :: k
  integer :: ipoint

  character*32 :: tok(32)
  integer  :: ntok
  character*256 :: record

  integer :: iot ! accumulates number of targetted ocean points
  integer :: igt ! accumulates number of targetted ocean points (great lakes)
  integer :: ilt ! accumulates number of targetted land points
  integer :: igl ! accumulates number of great lakes

  integer :: omax
  integer :: olmax
  integer :: lmax
  integer :: llmax
  integer :: gmax
  integer :: glmax
  parameter    (omax = 30,olmax = 10,lmax = 10,llmax = 10,gmax = 20,glmax = 7)

  integer, intent(inout), dimension(omax) :: ocean_target_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax) :: ocean_target_j !< Variable description\f$[units]\f$
  integer, intent(inout) :: ocean_target_n !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax,olmax) :: ot_lakes_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax,olmax) :: ot_lakes_j !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax) :: ot_lakes_n !< Variable description\f$[units]\f$

  integer, intent(inout), dimension(omax) :: glocean_target_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax) :: glocean_target_j !< Variable description\f$[units]\f$
  integer, intent(inout) :: glocean_target_n !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax,olmax) :: ot_glakes_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax,olmax) :: ot_glakes_j !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(omax) :: ot_glakes_n !< Variable description\f$[units]\f$

  integer, intent(inout), dimension(lmax) :: land_target_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lmax) :: land_target_j !< Variable description\f$[units]\f$
  integer, intent(inout) :: land_target_n !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lmax,llmax) :: lt_lakes_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lmax,llmax) :: lt_lakes_j !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lmax) :: lt_lakes_n !< Variable description\f$[units]\f$

  integer, intent(inout), dimension(gmax) :: great_lakes_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(gmax) :: great_lakes_j !< Variable description\f$[units]\f$
  integer, intent(inout) :: great_lakes_n !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(gmax,glmax) :: gl_land_i !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(gmax,glmax) :: gl_land_j !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(gmax) :: gl_land_n !< Variable description\f$[units]\f$

  real, intent(inout), dimension(gmax) :: gl_delay !< Variable description\f$[units]\f$

  integer :: lulk
  integer :: newunit
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !---------------------------------------------
  lulk = newunit(80)
  open(lulk,file = 'LAKES')

  !     can we do this?

  iot = 0
  igt = 0
  ilt = 0
  igl = 0

  do i = 1, 99999

    read (lulk, '(A256)', end = 999) record

    tok(1) = '  '

    call getstrtok(record,ntok,tok)

    if (tok(1) == 'LTO') then

      iot = iot + 1

      read (tok(2), * ) ocean_target_i(iot)
      read (tok(3), * ) ocean_target_j(iot)
      read (tok(4), * ) ot_lakes_n(iot)

      do j = 1, ot_lakes_n(iot)
        ipoint = 4 + j * 2 - 1
        read (tok(ipoint), * ) ot_lakes_i(iot,j)
        read (tok(ipoint + 1), * ) ot_lakes_j(iot,j)
      end do

    end if

    if (tok(1) == 'GTO') then

      igt = igt + 1

      read (tok(2), * ) glocean_target_i(igt)
      read (tok(3), * ) glocean_target_j(igt)
      read (tok(4), * ) ot_glakes_n(igt)

      do j = 1, ot_glakes_n(igt)
        ipoint = 4 + j * 2 - 1
        read (tok(ipoint), * ) ot_glakes_i(igt,j)
        read (tok(ipoint + 1), * ) ot_glakes_j(igt,j)
      end do

    end if

    if (tok(1) == 'LTL') then

      ilt = ilt + 1

      read (tok(2), * ) land_target_i(ilt)
      read (tok(3), * ) land_target_j(ilt)
      read (tok(4), * ) lt_lakes_n(ilt)

      do j = 1, lt_lakes_n(ilt)
        ipoint = 4 + j * 2 - 1
        read (tok(ipoint), * ) lt_lakes_i(ilt,j)
        read (tok(ipoint + 1), * ) lt_lakes_j(ilt,j)
      end do

    end if

    if (tok(1) == 'GL') then

      igl = igl + 1

      read (tok(2), * ) great_lakes_i(igl)
      read (tok(3), * ) great_lakes_j(igl)
      read (tok(4), * ) gl_delay(igl)
      read (tok(5), * ) gl_land_n(igl)

      do j = 1, gl_land_n(igl)
        ipoint = 5 + j * 2 - 1
        read (tok(ipoint), * )   gl_land_i(igl,j)
        read (tok(ipoint + 1), * ) gl_land_j(igl,j)
      end do

    end if

  end do

999 continue

  ocean_target_n = iot
  glocean_target_n = igt
  land_target_n  = ilt
  great_lakes_n  = igl

  !     *
  !     * write ocean cell targetting information to standard output
  !     *

  write(6,'(A)')
  write(6,'(A)') ' OCEAN CELLS TARGETTED BY LAKES (P - E):'
  write(6,'(A)')

  do i = 1, ocean_target_n
    write(6,1000) ocean_target_i(i),ocean_target_j(i), &
         (ot_lakes_i(i,k),ot_lakes_j(i,k),k = 1,ot_lakes_n(i))
  end do


  write(6,'(A)')
  write(6,'(A)') ' OCEAN CELLS TARGETTED BY GrLAKES (P - E):'
  write(6,'(A)')

  do i = 1, glocean_target_n
    write(6,1000) glocean_target_i(i),glocean_target_j(i), &
         (ot_glakes_i(i,k),ot_glakes_j(i,k),k = 1,ot_glakes_n(i))
  end do


  !     *
  !     * write land cell targetting information to standard output
  !     *

  write(6,'(A)')
  write(6,'(A)') ' LAND CELLS TARGETTED BY LAKES (SURFACE FLOW):'
  write(6,'(A)')

  do i = 1, land_target_n
    write(6,1000) land_target_i(i),land_target_j(i), &
         (lt_lakes_i(i,k),lt_lakes_j(i,k),k = 1,lt_lakes_n(i))
  end do

  !     *
  !     * write great lakes routing information
  !     *

  write(6,'(A)')
  write(6,'(A,A)') ' GREAT LAKES CELLS TARGETTED BY LAND ', &
                '(SURFACE FLOW):'
  write(6,'(A)')

  do i = 1, great_lakes_n
    write(6,1001) great_lakes_i(i),great_lakes_j(i),gl_delay(i), &
         (gl_land_i(i,k),gl_land_j(i,k),k = 1,gl_land_n(i))
  end do
  close(lulk)
  !
  return
  !---------------------------------------------------------------------
1000 format(i8,',',i2,3x,' < - ',3x,10(i2,',',i2,3x))
1001 format(i8,',',i2,' (AT ',f5.1,' DAYS)',3x,' < - ',3x, &
        10(i2,',',i2,3x))
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
