!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oceans (snorot,  gtrot,   anrot,   tnrot, &
                         rhonrot, bcsnrot, refrot, emisrot, &
                         cdmrot,  qgrot,   hfsrot,  qfsrot, fnrot, &
                         salbrot, csalrot, &
                         iwmos,   jwmos, &
                         nmw,nl,nm,ilg,nbs, &
                         snogat,  gtgat,   angat,   tngat, &
                         rhongat, bcsngat, refgat, emisgat, &
                         cdmgat,  qggat,   hfsgat,  qfsgat, fngat, &
                         salbgat, csalgat)
  !
  !     * jun 20, 2014 - m.lazare. new version called by "sfcproc3"
  !     *                          in new model version gcm18. this
  !     *                          is modelled on "classs" since
  !     *                          ocean is now tiled as well.
  !     *                          - adds salbgat/salbrot, csalgat/csalrot
  !     *                            and  emisgat/emisrot.
  !     *                          - {sno,gt,an,tn,rhon,bcsn,ref} now tiled.
  !     *                          - most output diagnostic fields removed
  !     *                            since now accumulated over ocean tile(s)
  !     *                            after call to this routine.
  !     *                          - gc removed.
  !     * nov 15, 2013 - m.lazare. previous version "oceans" called by
  !     *                          "sfcproc" in previous model version gcm17.
  !     * nov 15, 2013 - m.lazare. rofn now accumulated while
  !                                scattering, not instantaneous.
  !     * jun 11, 2013 - m.lazare. ocean scatter routine called by
  !     *                          "sfcproc" in new version gcm17.
  !     * note: this contains the following changes compared to the
  !     *       working temporary version used in conjunction with
  !     *       updates to gcm16 (ie not official):
  !     *         1) {TN,RHON,FN,TISL,ROFN} added for Maryam's
  !     *            contribution to the new snow albedo formulation.
  !     *         2) no more snorot,anrot (snorow,anrow still there)
  !     *            since these are for land only.
  !     *         3) instantaneous fields znrol,smltrol scattered
  !     *            instead of accumulated znrow,smltrow, since
  !     *            these are now done in the physics driver.
  !--------------------------------------------------------------------
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: nmw !< Variable description\f$[units]\f$
  integer, intent(in) :: nl !< Variable description\f$[units]\f$
  integer, intent(in) :: nm !< Variable description\f$[units]\f$
  integer, intent(in) :: ilg !< Variable description\f$[units]\f$
  integer :: k !< Variable description\f$[units]\f$
  integer :: l !< Variable description\f$[units]\f$
  integer :: m !< Variable description\f$[units]\f$
  integer, intent(in) :: nbs !<
  !
  !     * ocean prognostic variables.
  !
  real, intent(out), dimension(nl,nm,nbs) :: salbrot !<
  real, intent(out), dimension(nl,nm,nbs) :: csalrot !< Variable description\f$[units]\f$

  real, intent(in), dimension(ilg,nbs) :: salbgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,nbs) :: csalgat !< Variable description\f$[units]\f$

  real, intent(out), dimension(nl,nm) :: snorot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: gtrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: anrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: rhonrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: tnrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: bcsnrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: refrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: emisrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: cdmrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: qgrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: hfsrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: qfsrot !< Variable description\f$[units]\f$
  real, intent(out), dimension(nl,nm) :: fnrot !< Variable description\f$[units]\f$

  real, intent(in), dimension(ilg)   :: snogat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: gtgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: angat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: rhongat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: tngat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: bcsngat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: refgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: emisgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: cdmgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: qggat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: hfsgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: qfsgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg)   :: fngat !< Variable description\f$[units]\f$
  !
  !     * gather-scatter index arrays.
  !
  integer, intent(in), dimension(ilg) :: iwmos !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: jwmos !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !----------------------------------------------------------------------
  do k=1,nmw
    snorot (iwmos(k),jwmos(k))=snogat (k)
    gtrot  (iwmos(k),jwmos(k))=gtgat  (k)
    anrot  (iwmos(k),jwmos(k))=angat  (k)
    tnrot  (iwmos(k),jwmos(k))=tngat  (k)
    rhonrot(iwmos(k),jwmos(k))=rhongat(k)
    bcsnrot(iwmos(k),jwmos(k))=bcsngat(k)
    refrot (iwmos(k),jwmos(k))=refgat (k)
    emisrot(iwmos(k),jwmos(k))=emisgat(k)
    cdmrot (iwmos(k),jwmos(k))=cdmgat (k)
    qgrot  (iwmos(k),jwmos(k))=qggat  (k)
    hfsrot (iwmos(k),jwmos(k))=hfsgat (k)
    qfsrot (iwmos(k),jwmos(k))=qfsgat (k)
    fnrot  (iwmos(k),jwmos(k))=fngat  (k)
  end do ! loop 100
  !
  do l=1,nbs
    do k=1,nmw
      salbrot(iwmos(k),jwmos(k),l)=salbgat(k,l)
      csalrot(iwmos(k),jwmos(k),l)=csalgat(k,l)
    end do
  end do ! loop 200
  !--------------------------------------------------------------------
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
