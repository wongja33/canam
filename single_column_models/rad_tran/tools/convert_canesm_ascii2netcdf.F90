MODULE ARRAYS_MOD

    implicit none
      
    real, allocatable, dimension(:,:,:) :: & ! size (ilg, lev)
    shtj, &
    tfrow
    
    real, allocatable, dimension(:,:,:) :: & ! size (ilg, ilev)
    shj, &
    dshj, &
    dz, &
    unotnd, & ! actually temperature
    ozphs, &
    qc, & 
    co2rox, &
    ch4rox, &
    n2orox, &
    f11rox, &
    f12rox, &
    f113rox, &
    f114rox, &
    ccld, &
    rhc, &
    anu, &
    eta
    
    integer, allocatable, dimension(:,:,:) :: & ! size (ilg, 4)
    iseed
    
    real, allocatable, dimension(:,:,:,:) :: & ! size (ilg,ilev,9)
    aerin
    
    real, allocatable, dimension(:,:,:,:) :: & ! size (ilg,ilev,9)
    sw_ext_sa, & ! size (ilg,ilev,nbs)
    sw_ssa_sa, &
    sw_g_sa, &
    lw_abs_sa ! size (ilg,ilev,nbl)
    
    real, allocatable, dimension(:,:) :: &! size (ilg)
    pressg, &
    gtrow, &
    oztop, &
    cszrow, &
    vtaurow, &
    troprow, &
    emisrow, &
    cldtrol
    
    integer, allocatable, dimension(:,:) :: & ! size (ilg)
    ncldy
    
    real, allocatable, dimension(:,:,:) :: &! size (ilg,nbs)
    salbrol,&
    csalrol
    
    real, allocatable, dimension(:,:,:) :: &! size (ilg,im)
    emisrot, &
    gtrot, &
    farerot
    
    real, allocatable, dimension(:,:,:,:) :: &! size (ilg,im,nbs)
    salbrot, &
    csalrot
    
    real, allocatable, dimension(:,:,:,:) :: &! size (ilg,ilev,nxloc)
    rel_sub, &
    rei_sub, &
    clw_sub, &
    cic_sub
end module arrays_mod

module allocate_deallocate_arrays_mod
  implicit none
  public allocate_deallocate_arrays
  
contains
  
  subroutine allocate_deallocate_arrays(nlat, &
                                        ilg,   &
                                        ilev,  &
                                        lev,   &
                                        nxloc, &
                                        im,    &
                                        nbs,   &
                                        nbl)
    
    ! allocate the arrays to read in slices from canam
    
    
    use arrays_mod
    implicit none
    
    ! input
    integer, intent(in) :: nlat
    integer, intent(in) :: ilg
    integer, intent(in) :: ilev
    integer, intent(in) :: lev
    integer, intent(in) :: nxloc
    integer, intent(in) :: im
    integer, intent(in) :: nbs
    integer, intent(in) :: nbl
    
    ! local
    integer :: &
    d1, &
    d2, &
    d3, &
    d4
    
    d1 = nlat
    d2 = ilg
    d3 = lev
    allocate(shtj(d1,d2,d3))
    allocate(tfrow(d1,d2,d3))
    
    d1 = nlat
    d2 = ilg
    d3 = ilev
    allocate(shj(d1,d2,d3), &
             dshj(d1,d2,d3), &
             dz(d1,d2,d3), &
             unotnd(d1,d2,d3), & ! actually temperature
             ozphs(d1,d2,d3), &
             qc(d1,d2,d3), & 
             co2rox(d1,d2,d3), &
             ch4rox(d1,d2,d3), &
             f11rox(d1,d2,d3), &
             n2orox(d1,d2,d3), &
             f113rox(d1,d2,d3), &
             f12rox(d1,d2,d3), &
             f114rox(d1,d2,d3), &
             ccld(d1,d2,d3), & 
             rhc(d1,d2,d3), &
             anu(d1,d2,d3), &
             eta(d1,d2,d3))
        
      d1 = nlat     
      d2 = ilg
      d3 = 4
      allocate(iseed(d1,d2,d3))
      
      d1 = nlat
      d2 = ilg
      d3 = ilev
      d4 = 9
      allocate(aerin(d1,d2,d3,d4))
      
      d1 = nlat
      d2 = ilg
      d3 = ilev
      d4 = nbs
      allocate(sw_ext_sa(d1,d2,d3,d4), & 
               sw_ssa_sa(d1,d2,d3,d4), &
               sw_g_sa(d1,d2,d3,d4))
        
      d1 = nlat       
      d2 = ilg
      d3 = ilev
      d4 = nbl
      allocate(lw_abs_sa(d1,d2,d3,d4))
               
      d1 = nlat
      d2 = ilg
      allocate(pressg(d1,d2), &
               gtrow(d1,d2), &
               oztop(d1,d2), &
               cszrow(d1,d2), &
               vtaurow(d1,d2), &
               troprow(d1,d2), &
               emisrow(d1,d2), &
               cldtrol(d1,d2), &
               ncldy(d1,d2))
               
       d1 = nlat
       d2 = ilg
       d3 = nbs
       allocate(salbrol(d1,d2,d3), &
                csalrol(d1,d2,d3))
                
       d1 = nlat
       d2 = ilg
       d3 = im
       allocate(emisrot(d1,d2,d3), &
                gtrot(d1,d2,d3), &
                farerot(d1,d2,d3))
              
       d1 = nlat
       d2 = ilg
       d3 = im
       d4 = nbs
       allocate(salbrot(d1,d2,d3,d4), &
                csalrot(d1,d2,d3,d4))
        
       d1 = nlat        
       d2 = ilg
       d3 = ilev
       d4 = nxloc
       allocate(rel_sub(d1,d2,d3,d4), &
                rei_sub(d1,d2,d3,d4), &
                clw_sub(d1,d2,d3,d4), &
                cic_sub(d1,d2,d3,d4))
                
    return
    
  end subroutine allocate_deallocate_arrays
end module allocate_deallocate_arrays_mod

module netcdf_util_mod
  implicit none

  private :: check
contains
  
  subroutine create_netcdf(nlat, &
                           ilg, &
                           ilev,&
                           lev, &
                           nxloc, &
                           im, &
                           nbs, &
                           nbl, &
                           ofilename)
   
   ! create the netcdf file with the appropriate variables, etc..
   
   use netcdf
   
   implicit none
   
   ! parameter
   
   ! input
   integer, intent(in) :: nlat
   integer, intent(in) :: ilg
   integer, intent(in) :: ilev
   integer, intent(in) :: lev
   integer, intent(in) :: nxloc
   integer, intent(in) :: im
   integer, intent(in) :: nbs
   integer, intent(in) :: nbl

   character(len=*), intent(in) :: ofilename
   
   ! local
   integer :: ncid
   integer :: lat_dimid
   integer :: ilev_dimid
   integer :: lev_dimid
   integer :: ilg_dimid
   integer :: ntile_dimid
   integer :: nbl_dimid
   integer :: nbs_dimid
   integer :: subcolumn_dimid
   integer :: iseed_dimid
   integer :: aerosol_dimid
   
   integer :: varid
   
   integer, dimension(1) :: dimids_1d
   integer, dimension(2) :: dimids_2d
   integer, dimension(3) :: dimids_3d
   integer, dimension(4) :: dimids_4d
   
   character(len=10), allocatable, dimension(:) :: varnames
   
   integer ::  ivar
   
   ! Open the file
   call check(nf90_create(trim(ofilename), nf90_clobber, ncid))
   
   ! Create the dimensions
   call check(nf90_def_dim(ncid,'latitude', nlat, lat_dimid))
   call check(nf90_def_dim(ncid,'levels', lev, lev_dimid))
   call check(nf90_def_dim(ncid,'layers', ilev, ilev_dimid))
   call check(nf90_def_dim(ncid,'longitude', ilg, ilg_dimid))
   call check(nf90_def_dim(ncid,'sfc_tiles', im, ntile_dimid))
   call check(nf90_def_dim(ncid,'lw_bands', nbl, nbl_dimid))
   call check(nf90_def_dim(ncid,'sw_bands', nbs, nbs_dimid))
   call check(nf90_def_dim(ncid,'subcolumns', nxloc, subcolumn_dimid))
   call check(nf90_def_dim(ncid,'iseed', 4, iseed_dimid))
   call check(nf90_def_dim(ncid,'aerosol', 9, aerosol_dimid))
   
   ! Create the variables
   dimids_3d = (/ lat_dimid, ilg_dimid,lev_dimid /)
   call check(nf90_def_var(ncid, "shtj", nf90_float, dimids_3d, varid))
   call check(nf90_def_var(ncid, "tfrow", nf90_float, dimids_3d, varid))

   dimids_3d = (/ lat_dimid,ilg_dimid,ilev_dimid /)
   allocate(varnames(17))
   varnames(1:17)=(/ "shj    ","dshj   ","dz     ","tlayer ","ozphs  ","qc     ",&
                     "co2rox ","ch4rox ","n2orox ","f11rox ","f12rox ","f113rox",&
                     "f114rox","ccld   ","rhc    ","anu    ","eta    "/)
   do ivar = 1, 17
     call check(nf90_def_var(ncid,trim(varnames(ivar)), nf90_float, dimids_3d, varid))
   end do
   
   dimids_3d = (/ lat_dimid, ilg_dimid, iseed_dimid /)
   call check(nf90_def_var(ncid, "iseed", nf90_int, dimids_3d, varid))
   
   dimids_4d = (/ lat_dimid, ilg_dimid, ilev_dimid, aerosol_dimid /)
   call check(nf90_def_var(ncid, "aerin", nf90_float, dimids_4d, varid))
   
   dimids_4d = (/ lat_dimid, ilg_dimid, ilev_dimid, nbs_dimid /)
   call check(nf90_def_var(ncid, "sw_ext_sa", nf90_float, dimids_4d, varid))
   call check(nf90_def_var(ncid, "sw_ssa_sa", nf90_float, dimids_4d, varid))
   call check(nf90_def_var(ncid, "sw_g_sa", nf90_float, dimids_4d, varid))
   
   dimids_4d = (/ lat_dimid, ilg_dimid, ilev_dimid, nbl_dimid /)
   call check(nf90_def_var(ncid, "lw_abs_sa", nf90_float, dimids_4d, varid))
   
   dimids_2d = (/ lat_dimid, ilg_dimid /)
   call check(nf90_def_var(ncid, "pressg", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "gtrow", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "oztop", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "cszrow", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "vtaurow", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "troprow", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "emisrow", nf90_float, dimids_2d, varid))
   call check(nf90_def_var(ncid, "cldtrol", nf90_float, dimids_2d, varid))
   
   call check(nf90_def_var(ncid, "ncldy", nf90_int, dimids_2d, varid))
   
   dimids_3d = (/ lat_dimid, ilg_dimid, nbs_dimid /)
   call check(nf90_def_var(ncid, "salbrol", nf90_float, dimids_3d, varid))
   call check(nf90_def_var(ncid, "csalrol", nf90_float, dimids_3d, varid))

   dimids_3d = (/ lat_dimid, ilg_dimid, ntile_dimid /)
   call check(nf90_def_var(ncid, "emisrot", nf90_float, dimids_3d, varid))
   call check(nf90_def_var(ncid, "gtrot", nf90_float, dimids_3d, varid))
   call check(nf90_def_var(ncid, "farerot", nf90_float, dimids_3d, varid))
   
   dimids_4d = (/ lat_dimid, ilg_dimid, ntile_dimid, nbs_dimid /)
   call check(nf90_def_var(ncid, "salbrot", nf90_float, dimids_4d, varid))
   call check(nf90_def_var(ncid, "csalrot", nf90_float, dimids_4d, varid))
   
   dimids_4d = (/ lat_dimid, ilg_dimid, ilev_dimid, subcolumn_dimid /)
   call check(nf90_def_var(ncid, "rel_sub", nf90_float, dimids_4d, varid))
   call check(nf90_def_var(ncid, "rei_sub", nf90_float, dimids_4d, varid))
   call check(nf90_def_var(ncid, "clw_sub", nf90_float, dimids_4d, varid))
   call check(nf90_def_var(ncid, "cic_sub", nf90_float, dimids_4d, varid))

   call check(nf90_enddef(ncid))
   
   call check(nf90_close(ncid))
   
 end subroutine create_netcdf
 
  subroutine write_netcdf(ofilename)
    
    ! write data to the netcdf
    
    use netcdf
    use arrays_mod
    
    implicit none
    
    ! input
    character(len=*), intent(in) :: ofilename
    
    ! local
    integer :: ncid
    integer :: varid
    
    character(len=10) :: varname
    
    ! open the netcdf file

    call check(nf90_open(trim(ofilename), nf90_write, ncid))

    ! write the 2d data
    varname="pressg"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,pressg))

    varname="gtrow"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,gtrow))

    varname="oztop"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,oztop))

    varname="cszrow"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,cszrow))

    varname="vtaurow"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,vtaurow))
    
    varname="troprow"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,troprow))

    varname="emisrow"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,emisrow))

    varname="cldtrol"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,cldtrol))

    varname="ncldy"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,ncldy))
        
    ! write the 3d data
    varname="shtj"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,shtj))

    varname="tfrow"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,tfrow))
    
    varname="shj"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,shj))
    
    varname="dshj"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,dshj))

    varname="dz"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,dz))

    varname="tlayer"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,unotnd))

    varname="ozphs"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,ozphs))

    varname="qc"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,qc))

    varname="co2rox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,co2rox))

    varname="ch4rox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,ch4rox))

    varname="n2orox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,n2orox))

    varname="f11rox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,f11rox))
    
    varname="f113rox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,f113rox))

    varname="f12rox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,f12rox))

    varname="f114rox"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,f114rox))

    varname="ccld"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,ccld))

    varname="rhc"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,rhc))

    varname="anu"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,anu))

    varname="eta"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,eta))
    
    varname="iseed"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,iseed))

    varname="salbrol"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,salbrol))

    varname="csalrol"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,csalrol))
    
    varname="emisrot"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,emisrot))

    varname="gtrot"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,gtrot))

    varname="farerot"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,farerot))
    
    ! 4d data
    varname="aerin"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,aerin))
    
    varname="sw_ext_sa"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,sw_ext_sa))
    
    varname="sw_ssa_sa"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,sw_ssa_sa))
    
    varname="sw_g_sa"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,sw_g_sa))
    
    varname="lw_abs_sa"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,lw_abs_sa))
    
    varname="salbrot"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,salbrot))
    
    varname="csalrot"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,csalrot))
    
    varname="rel_sub"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,rel_sub))
    
    varname="rei_sub"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,rei_sub))
    
    varname="clw_sub"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,clw_sub))
    
    varname="cic_sub"
    call check(nf90_inq_varid(ncid,trim(varname),varid))
    call check(nf90_put_var(ncid,varid,cic_sub))
        
    ! close the netcdf file
    
    call check(nf90_close(ncid))
    
    return
    
  end subroutine write_netcdf
  
  subroutine check(status)

    use netcdf

    implicit none

    integer, intent(in) :: status

    if(status /= nf90_noerr) then 
       write(6,'(a,i5)') 'netcdf library error code ',status
       write(6,*) trim(adjustl(nf90_strerror(status)))
       stop 
    end if
    
    return
    
  end subroutine check
end module netcdf_util_mod

module read_ascii_mod
  
  implicit none

contains
  
  subroutine read_ascii(ilat,&
                        unit_num, &
                        ilg, &
                        ilev,&
                        lev, &
                        nxloc, &
                        im, &
                        nbs, &
                        nbl)
                        
    ! reads in the "slices" from the file and puts it into a global array
    use arrays_mod
    implicit none
    
    ! input
    integer, intent(in) :: ilat
    integer, intent(in) :: unit_num
    integer, intent(in) :: ilg
    integer, intent(in) :: ilev
    integer, intent(in) :: lev
    integer, intent(in) :: nxloc
    integer, intent(in) :: im
    integer, intent(in) :: nbs
    integer, intent(in) :: nbl
    
    ! local
    integer :: ijunk
    integer :: ix, &
               iz, &
               icol, &
               iband, &
               itile
               
    character(len=256) :: cjunk
    
    ! read the data on levels
    read(unit_num,*)
    do iz = 1, lev
      do ix = 1, ilg
          read(unit_num,*) ijunk, ijunk, shtj(ilat,ix,iz),tfrow(ilat,ix,iz)
      end do ! ix
    end do ! iz
    
    ! read the data on layers
    read(unit_num,*)
    read(unit_num,*)    
    do iz = 1, ilev
      do ix = 1, ilg
          read(unit_num,'(2i4,2e14.7,2f10.3,11e14.7,2f10.3)') ijunk, ijunk, &
                  shj(ilat,ix,iz), dshj(ilat,ix,iz), dz(ilat,ix,iz), unotnd(ilat,ix,iz),  &
                  ozphs(ilat,ix,iz), qc(ilat,ix,iz),                            &
                  co2rox(ilat,ix,iz), ch4rox(ilat,ix,iz), n2orox(ilat,ix,iz),        &
                  f11rox(ilat,ix,iz), f12rox(ilat,ix,iz), f113rox(ilat,ix,iz), f114rox(ilat,ix,iz), &
                  ccld(ilat,ix,iz), rhc(ilat,ix,iz), anu(ilat,ix,iz), eta(ilat,ix,iz)
      end do 
    end do
    
    ! read in the random number seeds
    read(unit_num,*)
    do ix = 1, ilg
        read(unit_num,'(i4,4i14)') ijunk, (iseed(ilat,ix,iz),iz=1,4)
    end do
        
    ! read in the aerosol information
    read(unit_num,*)
    do iz = 1, ilev
      do ix = 1, ilg 
          read(unit_num,'(2i4,9e14.7)') ijunk,ijunk,(aerin(ilat,ix,iz,icol),icol=1,9)
      end do
    end do
    
    ! read in the stratospheric aerosols
    read(unit_num,*)
    do iz = 1, ilev
      do ix = 1, ilg 
          read(unit_num,'(2i4,12e14.7)') ijunk,ijunk,                          &
                                   (sw_ext_sa(ilat,ix,iz,iband),iband=1,nbs), &
                                   (sw_ssa_sa(ilat,ix,iz,iband),iband=1,nbs), &
                                   (sw_g_sa(ilat,ix,iz,iband),iband=1,nbs)
       end do
     end do

    read(unit_num,*)     
    do iz = 1, ilev
      do ix = 1, ilg !cfg%il1, cfg%il2
          read(unit_num,'(2i4,9e14.7)') ijunk,ijunk,&
                                  (lw_abs_sa(ilat,ix,iz,iband),iband=1, nbl)
      end do
    end do
    
    ! read surface data that is not on tiles
    read(unit_num,*) 
    do ix = 1, ilg !cfg%il1, cfg%il2
         read(unit_num,'(i4,2f10.2,e14.7,5e14.7,i4)') ijunk,           &
             pressg(ilat,ix), gtrow(ilat,ix), oztop(ilat,ix), cszrow(ilat,ix),       &
             vtaurow(ilat,ix), troprow(ilat,ix), emisrow(ilat,ix), cldtrol(ilat,ix), &
             ncldy(ilat,ix)
    end do
    
    read(unit_num,*)    
    do ix = 1, ilg !cfg%il1, cfg%il2
       read(unit_num,'(i4,8e14.7)') ijunk,                           &
                             (salbrol(ilat,ix,iband),iband=1, nbs), &
                             (csalrol(ilat,ix,iband),iband=1, nbs)
    end do
    
    ! read surface data that is on tiles
    read(unit_num,*)
    do ix = 1, ilg !cfg%il1, cfg%il2
        read(unit_num,'(i4,18e14.7)') ijunk,                             &
                               (emisrot(ilat,ix,itile),itile=1,im), &
                               (gtrot(ilat,ix,itile),itile=1,im),   &
                               (farerot(ilat,ix,itile),itile=1,im) 
    end do
    
    read(unit_num,*)
    do ix = 1, ilg !cfg%il1, cfg%il2
        do itile = 1, im
          read(unit_num,'(2i4,8e14.7)') ijunk, ijunk,                          &
                                 (salbrot(ilat,ix,itile,iband),iband=1, nbs), &
                                 (csalrot(ilat,ix,itile,iband),iband=1, nbs)
        end do
    end do

    ! read in the subcolumn data
    read(unit_num,*)
    do iz = 1, ilev
      do ix = 1, ilg !cfg%il1, cfg%il2
          read(unit_num,'(2i4,300f10.3,300e14.7)') ijunk, ijunk,                  &
                                    (rel_sub(ilat,ix,iz,icol),icol=1,nxloc), &
                                    (rei_sub(ilat,ix,iz,icol),icol=1,nxloc), &
                                    (clw_sub(ilat,ix,iz,icol),icol=1,nxloc), &
                                    (cic_sub(ilat,ix,iz,icol),icol=1,nxloc)
      end do
    end do
    
    
    return
  end subroutine read_ascii
end module read_ascii_mod

program convert_canesm_ascii2netcdf
  
  ! program to read in data from ascii files that print right before call to 
  ! raddriv (radiative transfer driver) the inputs to that routine.
  ! there will be separate ascii files for each longitude "slice" in canam plus
  ! one file called "fort.98" that will contain scalar information, including
  ! array sizes.
  ! compile on laptop
  ! gfortran convert_canesm_ascii2netcdf.f90 -i/home/jcl/opt_local/include \
  ! -i/home/jcl/opt_local/include -wl,-rpath=/home/jcl/opt_local/lib \
  ! -l/home/jcl/opt_local/lib -lnetcdff  -o convert_canesm_ascii2netcdf.exe
  !
  ! compile on laptop with debug
  ! gfortran -g -o0 -cpp -fbacktrace -ffpe-trap=invalid,zero,overflow -fcheck=all \
  ! -fno-automatic -wall -wextra convert_canesm_ascii2netcdf.f90 \
  ! -i/home/jcl/opt_local/include -i/home/jcl/opt_local/include -wl,-rpath=/home/jcl/opt_local/lib \
  ! -l/home/jcl/opt_local/lib -lnetcdff  -o convert_canesm_ascii2netcdf.exe
  
  use allocate_deallocate_arrays_mod, only: allocate_deallocate_arrays
  use netcdf_util_mod, only: create_netcdf, write_netcdf
  use read_ascii_mod, only: read_ascii
  
  implicit none
  
  ! parameters
    integer, parameter :: nbs = 4
    integer, parameter :: nbl = 9
    character(len=256), parameter :: ofilename = "canam_raddriv_input.nc"
    integer, parameter :: nlat = 64
    real, parameter :: pi = 3.141
    real, parameter :: rad2deg = 180.0/pi
    
  ! local scalars
    integer :: &
    idimr, &
    ipam, &
    isvaod, &
    imam, &
    iexplvol, &
    ivtau, &
    max_sam, &
    il1, &
    il2, &
    ilg, &
    ilev, &
    lev, &
    nxloc, &
    im, &
    ivers, &
    jlat, &
    mcica, &
    iradforce, &
    irad_flux_profs, &
    iactive_rt, &
    itilerad
    
    real :: &
    solar_c, &
    cldwatmin
    
    integer :: &
    ijunk,     &
    ilat
    
  ! local arrays
    integer, dimension(nlat) :: unit_num
    real, dimension(nlat) :: lat
    
  ! read the file with all of the scalars.  will allow allocation of the arrays
    read(98,*)
    read(98,*)
    read(98,*)
    read(98,*)
    read(98,'(7i4,7i4,f10.3,e14.7,7i4)') &
    idimr, ipam, isvaod,imam, iexplvol, ivtau, max_sam, &
    il1, il2, ilg, ilev, lev, nxloc, im, &
    solar_c, cldwatmin, ivers, jlat, &
    mcica, iradforce, irad_flux_profs, iactive_rt, &
    itilerad

  ! read in the file with the unit numbers for the files and the associated 
  ! latitudes
    open(unit=15,file="map_lat_file.txt")
    do ilat = 1, nlat
      read(15,*) unit_num(ilat),lat(ilat)
    end do ! ilat
    lat = lat*rad2deg
    
  ! allocate arrays
    call allocate_deallocate_arrays(nlat, &
                                    il2, &
                                    ilev,&
                                    lev, &
                                    nxloc, &
                                    im, &
                                    nbs, &
                                    nbl)
    
  ! create the netcdf file
    call create_netcdf(nlat, &                       
                       il2, &
                       ilev,&
                       lev, &
                       nxloc, &
                       im, &
                       nbs, &
                       nbl, &
                       ofilename)
    
  
  ! loop over each slice, reading in data from the appropriate file
  do ilat = 1, nlat
    call read_ascii(ilat,&
                    unit_num(ilat), &
                    il2, &
                    ilev,&
                    lev, &
                    nxloc, &
                    im, &
                    nbs, &
                    nbl)
  end do ! ilat
  
  ! write the data to the netcdf file
  
  call write_netcdf(ofilename)
  
end program convert_canesm_ascii2netcdf
