!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module mamrad_sizes
  !=======================================================================
  !     * apr 29,2011 - m. lazare. - new module for use in gcm16.
  !=======================================================================

  implicit none
  integer, dimension(67)    :: idr           !<Variable description\f$[units]\f$
  real  ,  dimension(3, 67) :: pdr           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:, :) :: pcd           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: xvf           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: pvf           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: o2vf          !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: cn2vf         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: ovf           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: amuvf         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: cpvf          !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: gvf           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: co2vf         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: effh          !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: effsrc        !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: to2           !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: effo2nir      !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wwvs2         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wwvs3         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wco2s         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wco2st        !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wwvl3         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wch4l         !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wo3l          !<Variable description\f$[units]\f$
  real  ,  allocatable, dimension(:)    :: wchem         !<Variable description\f$[units]\f$
  integer, allocatable, dimension(:)    :: icd           !<Variable description\f$[units]\f$
  real :: x1chem       !<Variable description\f$[units]\f$
  real :: x2chem       !<Variable description\f$[units]\f$
  real :: dxchem       !<Variable description\f$[units]\f$
  integer :: k1chem    !<Variable description\f$[units]\f$
  integer :: k1co2s    !<Variable description\f$[units]\f$
  integer :: k2co2s   !<Variable description\f$[units]\f$

end module mamrad_sizes
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
