module physics_checksums

use iso_fortran_env, only : STDOUT => output_unit
use checksums, only : checksum
use mpi

implicit none; private

integer, parameter :: CHKSUM_MOD = 10000000
integer, parameter :: STRING_LENGTH = 255

!> Defines the procedures and fields associated with a single field
type field_type; private
  character(len=STRING_LENGTH) :: name = '' !< The name of the current field
  integer :: chksum     !< The bitcount of the field
  real :: min_val       !< Minimum value of the field
  real :: max_val       !< Maximum value of the field

  logical :: do_checksum = .false. !< If true, capture and print checksums

  type(field_type), pointer :: next => NULL() !< Initialize this for linked lists

  contains

  generic :: capture => capture_field_1d_real, capture_field_2d_real, capture_field_3d_real
  procedure :: initialize => initialize_field
  procedure :: print_global => print_global_field
  procedure :: print        => print_field
  procedure :: reset => reset_field
  procedure :: capture_field_1d_real
  procedure :: capture_field_2d_real
  procedure :: capture_field_3d_real

end type field_type

!> Defines procedures associated with a group of fields
type multiple_fields_type; private
  type(field_type), pointer :: first => NULL()
  type(field_type), pointer :: last  => NULL()
  character(len=STRING_LENGTH) :: name
  logical :: do_checksum = .false.!< Do the checksum for this group
  contains

  generic :: capture_field => capture_fields_1d_real, capture_fields_2d_real, capture_fields_3d_real
  procedure :: initialize => initialize_fields
  procedure :: add_field
  procedure :: reset => reset_fields
  procedure :: print_global => print_global_fields
  procedure :: print        => print_fields

  procedure :: capture_fields_1d_real
  procedure :: capture_fields_2d_real
  procedure :: capture_fields_3d_real

end type multiple_fields_type

!> Contains routines specific for capturing the state of the physici signature
type, extends(multiple_fields_type) :: physici_signature_type

  contains

  procedure :: initialize => initialize_physici_signature
  procedure :: capture => capture_physici_signature

end type physici_signature_type

public :: physici_signature_type
public :: multiple_fields_type
public :: field_type

contains

!! Begin definition of field_type procedures
!> Initialize the field type
subroutine initialize_field( self, name, do_checksum )
  class(field_type), intent(inout) :: self
  character(len=*),  intent(in   ) :: name !< Name of the field
  logical,           intent(in   ) :: do_checksum !< If true, enable the checksums

  self%name = name
  self%do_checksum = do_checksum

end subroutine initialize_field

!> Reset the field metrics
subroutine reset_field( self )
  class(field_type), intent(inout) :: self

  if (self%do_checksum) then
    self%chksum  = 0
    self%min_val = HUGE(self%min_val)
    self%max_val = -HUGE(self%max_val)
  endif

end subroutine reset_field

!> Update stats and checksum for the given field with the given name
subroutine capture_field_1d_real( self, array )
  class(field_type) :: self
  real, dimension(:), intent(in) :: array

  integer :: chksum
  real :: min_val
  real :: max_val

  if (self%do_checksum) then
    self%chksum =  self%chksum + checksum(array)
    self%min_val = min(minval(array),self%min_val)
    self%max_val = max(maxval(array),self%max_val)
  endif

end subroutine capture_field_1d_real

subroutine capture_field_2d_real( self, array )
  class(field_type) :: self
  real, dimension(:,:), intent(in) :: array

  real :: min_val
  real :: max_val
  if (self%do_checksum) then
    self%chksum = self%chksum + checksum(array)
    self%min_val = min(minval(array),self%min_val)
    self%max_val = max(maxval(array),self%max_val)
  endif

end subroutine capture_field_2d_real

subroutine capture_field_3d_real( self, array )
  class(field_type) :: self
  real, dimension(:,:,:), intent(in) :: array

  real :: min_val
  real :: max_val

  self%chksum = self%chksum + checksum(array)
  self%min_val = min(minval(array),self%min_val)
  self%max_val = max(maxval(array),self%max_val)

end subroutine capture_field_3d_real

!> Perform a global reduction across MPI tasks on the field and its stats
subroutine print_global_field(self, mpi_root, mpi_comm, write_unit)
  class(field_type), intent(inout) :: self
  integer,           intent(in   ) :: mpi_root !< The root PE within the MPI communicator
  integer,           intent(in   ) :: mpi_comm !< The MPI communicator over which to reduce over
  integer, optional, intent(in   ) :: write_unit !< If present, override the default stdout unit

  integer :: ierror, rank
  integer :: default_real, default_integer
  real :: mold_real
  integer :: mold_integer
  real :: global_min_val, global_max_val
  integer :: global_chksum

  if (self%do_checksum) then
    ! Determine the correct MPI_TYPE for the default integer and real
    select case (kind(mold_integer))
      case (4)
        default_integer = MPI_INTEGER4
      case (8)
        default_integer = MPI_INTEGER8
      case default
        stop 'Unsupported MPI_TYPE used for integer'
    end select
    select case (kind(mold_real))
      case (4)
        default_real = MPI_REAL4
      case (8)
        default_real = MPI_REAL8
      case default
        stop 'Unsupported MPI_TYPE used for real'
    end select

    call MPI_Reduce(self%chksum,  global_chksum,  1, default_integer, MPI_SUM, mpi_root, mpi_comm, ierror)
    call MPI_Reduce(self%min_val, global_min_val, 1, default_real, MPI_MIN, mpi_root, mpi_comm, ierror)
    call MPI_Reduce(self%max_val, global_max_val, 1, default_real, MPI_MAX, mpi_root, mpi_comm, ierror)

    ! Only print the stats on the root PE
    call MPI_Comm_rank(mpi_comm, rank, ierror)
    if (rank == mpi_root) then
      self%chksum = global_chksum
      self%min_val = global_min_val
      self%max_val = global_max_val
      call self%print(write_unit)
    endif
  endif

end subroutine print_global_field

!> Print the stored checksum and metrics of the field
subroutine print_field( self, write_unit )
  class(field_type) :: self
  integer, optional, intent(in   ) :: write_unit !< If present, override the default stdout unit
  integer :: dest_unit

  dest_unit = STDOUT
  if (present(write_unit)) dest_unit = write_unit
  write(dest_unit,'(A12,X,A,I8.8,X,A,ES24.16 ,X,A,ES24.16)') trim(self%name), 'chksum=', self%chksum, "min=", &
                                                             self%min_val, "max=", self%max_val

end subroutine print_field
!! End definition of field_type procedures

!! Start definition of multiple_fields_type procedures
!> Initialize the multiple_fields type
subroutine initialize_fields( self, name, do_checksum )
  class(multiple_fields_type), intent(inout) :: self
  character(len=*),            intent(in   ) :: name !< The name for this group of fields
  logical,                     intent(in   ) :: do_checksum !< Do this checksum or not

  self%name = name
  self%do_checksum = do_checksum
end subroutine initialize_fields

!> Add a field to the physics signature type
subroutine add_field( self, name, do_checksum )
  class(multiple_fields_type),   intent(inout) :: self
  character(len=*),              intent(in   ) :: name
  logical,                       intent(in   ) :: do_checksum

  if (.not. associated(self%first)) then
    allocate(self%first)
    call self%first%initialize(name, do_checksum)
    self%last => self%first
  else
    allocate(self%last%next)
    call self%last%next%initialize(name, do_checksum)
    self%last => self%last%next
  endif

end subroutine add_field

!> Reset all fields within this group
subroutine reset_fields( self )
  class(multiple_fields_type) :: self

  type(field_type), pointer :: current

  if (self%do_checksum) then
    current => self%first
    do while (associated(current))
      call current%reset()
      current => current%next
    enddo
  endif

end subroutine reset_fields

!> Update the field in the physics signature type based on the name.
!! NOTE: This is not the most efficient way to do it, but is extensible
subroutine capture_fields_1d_real( self, name, array )
  class(multiple_fields_type), intent(inout) :: self
  character(len=*),   intent(in) :: name
  real, dimension(:), intent(in) :: array

  type(field_type), pointer :: current

  if (self%do_checksum) then
    current => self%first
    do while( associated(current) )
      if (current%name == name) then
        call current%capture( array )
        exit
      else
        current => current%next
      endif
    enddo
  endif
end subroutine capture_fields_1d_real

!> Update the field in the physics signature type based on the name.
subroutine capture_fields_2d_real( self, name, array )
  class(multiple_fields_type), intent(inout) :: self
  character(len=*),     intent(in) :: name
  real, dimension(:,:), intent(in) :: array

  type(field_type), pointer :: current

  if (self%do_checksum) then
    current => self%first
    do while( associated(current) )
      if (current%name == name) then
        call current%capture( array )
        exit
      else
        current => current%next
      endif
    enddo
  endif
end subroutine capture_fields_2d_real

!> Update the field in the physics signature type based on the name.
subroutine capture_fields_3d_real( self, name, array )
  class(multiple_fields_type), intent(inout) :: self
  character(len=*),       intent(in) :: name
  real, dimension(:,:,:), intent(in) :: array

  type(field_type), pointer :: current

  if (self%do_checksum) then
    current => self%first
    do while( associated(current) )
      if (current%name == name) then
        call current%capture( array )
        exit
      else
        current => current%next
      endif
    enddo
  endif
end subroutine capture_fields_3d_real

!> Print all the checksums within this group of fields
subroutine print_global_fields(self, mpi_root, mpi_comm, write_unit)
  class(multiple_fields_type) :: self
  integer,           intent(in   ) :: mpi_root !< The root PE within the MPI communicator
  integer,           intent(in   ) :: mpi_comm !< The MPI communicator over which to reduce over
  integer, optional, intent(in   ) :: write_unit !< If present, override the default stdout unit

  type(field_type), pointer :: current
  integer :: dest_unit, rank, ierror
  dest_unit = STDOUT
  if (present(write_unit)) dest_unit = write_unit

  if (self%do_checksum) then
    call MPI_Comm_rank(mpi_comm, rank, ierror)
    if (rank == mpi_root) write(dest_unit,'(A,A,A)') '---',trim(self%name),'---'
    current => self%first
    do while( associated(current) )
      call current%print_global(mpi_root, mpi_comm, write_unit)
      current => current%next
    enddo
    if (rank == mpi_root) write(dest_unit,'(A,A,A)') '---',trim(self%name),'---'
  endif

end subroutine print_global_fields

!> Print all the checksums within this group of fields
subroutine print_fields( self, write_unit )
  class(multiple_fields_type), intent(in) :: self
  integer, optional,           intent(in) :: write_unit !< If present, then output on this particular unit

  type(field_type), pointer :: current
  integer :: dest_unit
  dest_unit = STDOUT
  if (present(write_unit)) dest_unit = write_unit

  if (self%do_checksum) then
    write(dest_unit,'(A,A,A)') '---',trim(self%name),'---'
    current => self%first
    do while( associated(current) )
      call current%print(write_unit)
      current => current%next
    enddo
    write(dest_unit,'(A,A,A)') '---',trim(self%name),'---'
  endif

end subroutine print_fields
!! End definition of multiple_fields_type procedures

!> Initialize the type that keeps track of the variables in the physici signature
subroutine initialize_physici_signature( self, name, do_checksum )
  class(physici_signature_type), intent(inout) :: self
  character(len=*),              intent(in) :: name !< The name for this group of fields
  logical,                       intent(in) :: do_checksum !< Do this checksum or not

  self%name = name
  self%do_checksum = do_checksum

  if (do_checksum) then
    call self%add_field('xg',do_checksum)
    call self%add_field('xtg',do_checksum)
    call self%add_field('xorow',do_checksum)
    call self%add_field('ug',do_checksum)
    call self%add_field('utg',do_checksum)
    call self%add_field('vg',do_checksum)
    call self%add_field('vtg',do_checksum)
    call self%add_field('tg',do_checksum)
    call self%add_field('ttg',do_checksum)
    call self%add_field('wg',do_checksum)
    call self%add_field('qg',do_checksum)
    call self%add_field('qtg',do_checksum)
    call self%add_field('qorow',do_checksum)
    call self%add_field('pressg',do_checksum)
    call self%add_field('shtj',do_checksum)
    call self%add_field('sgj',do_checksum)
    call self%add_field('shj',do_checksum)
    call self%add_field('sgbj',do_checksum)
    call self%add_field('radj',do_checksum)
    call self%add_field('arearow',do_checksum)
    call self%add_field('dlonrow',do_checksum)
    call self%add_field('tmin',do_checksum)
  endif

end subroutine initialize_physici_signature

!> Calculate the checksum and print the stats of the physici subroutine signature
subroutine capture_physici_signature( self, xg, xtg, xorow, ug, utg, vg, vtg, tg, ttg, wg, qg, qtg, qorow, pressg, &
  shtj, sgj, shj, sgbj, radj, arearow, dlonrow, tmin, il1, il2 )
  class(physici_signature_type), intent(inout) :: self
  real,    dimension(:,:,:), intent(in) :: xg
  real,    dimension(:,:,:), intent(in) :: xtg
  real,    dimension(:,:,:), intent(in) :: xorow
  real,    dimension(:,:),   intent(in) :: ug
  real,    dimension(:,:),   intent(in) :: utg
  real,    dimension(:,:),   intent(in) :: vg
  real,    dimension(:,:),   intent(in) :: vtg
  real,    dimension(:,:),   intent(in) :: tg
  real,    dimension(:,:),   intent(in) :: ttg
  real,    dimension(:,:),   intent(in) :: wg
  real,    dimension(:,:),   intent(in) :: qg
  real,    dimension(:,:),   intent(in) :: qtg
  real,    dimension(:,:),   intent(in) :: qorow
  real,    dimension(:),     intent(in) :: pressg
  real,    dimension(:,:),   intent(in) :: shtj
  real,    dimension(:,:),   intent(in) :: sgj
  real,    dimension(:,:),   intent(in) :: shj
  real,    dimension(:,:),   intent(in) :: sgbj
  real,    dimension(:),     intent(in) :: radj
  real,    dimension(:),     intent(in) :: arearow
  real,    dimension(:),     intent(in) :: dlonrow
  real,    dimension(:),     intent(in) :: tmin
  integer,                   intent(in) :: il1
  integer,                   intent(in) :: il2

  if (self%do_checksum) then
    call self%capture_field('xg',      xg(il1:il2,:,:))
    call self%capture_field('xtg',     xtg(il1:il2,:,:))
    call self%capture_field('xorow',   xorow(il1:il2,:,:))
    call self%capture_field('ug',      ug(il1:il2,:))
    call self%capture_field('utg',     utg(il1:il2,:))
    call self%capture_field('vg',      vg(il1:il2,:))
    call self%capture_field('vtg',     vtg(il1:il2,:))
    call self%capture_field('tg',      tg(il1:il2,:))
    call self%capture_field('ttg',     ttg(il1:il2,:))
    call self%capture_field('wg',      wg(il1:il2,:))
    call self%capture_field('qg',      qg(il1:il2,:))
    call self%capture_field('qtg',     qtg(il1:il2,:))
    call self%capture_field('qorow',   qorow(il1:il2,:))
    call self%capture_field('pressg',  pressg(il1:il2))
    call self%capture_field('shtj',    shtj(il1:il2,:))
    call self%capture_field('sgj',     sgj(il1:il2,:))
    call self%capture_field('shj',     shj(il1:il2,:))
    call self%capture_field('sgbj',    sgbj(il1:il2,:))
    call self%capture_field('radj',    radj(il1:il2))
    call self%capture_field('arearow', arearow(il1:il2))
    call self%capture_field('dlonrow', dlonrow(il1:il2))
    call self%capture_field('tmin',    tmin)
  endif

end subroutine capture_physici_signature

end module physics_checksums
