!> !> !! !! !
#include "cppdef_config.h"

#ifdef use_spectral
  subroutine init_surface_albedo2(ksalb_in)
    !***********************************************************************
    !     * apr 30/12 - l.solheim. new version for gcm16:
    !     *                        remove checksum since more than one choice.
    !     *                        original version init_surface_albedo:
    !     * feb 05/07 - f.majaess  adjusted in support of 32-bits mode.
    !     * mar 17/04 - l.solheim. modified dec 08/04 to remove threadprivate.
    !
    !***********************************************************************
    !     * read a cccma format file containing a lookup table for surface
    !     * albedo that has dependence on optical depth, zenith angle,
    !     * windspeed and ocean chlorophyl for each of 4 solar bands.
    !
    !     * input:
    !     *   ksalb - flags the type of surface albedo calculation to be done
    !     *           if ksalb /= 1 then simply return without filling
    !     *           the table in salbtab common
    !***********************************************************************
    implicit none
    real :: albedo_table
    integer :: i
    integer :: ibuf
    integer :: ichksum
    integer :: idat
    integer :: init_table
    integer :: ksalb
    integer :: lrec
    integer :: nb
    integer :: nc
    integer :: ns
    integer :: nt
    integer :: nw
    integer :: newunit
    integer :: nc4to8
  
    integer :: iu
    integer, intent(in) :: ksalb_in !< !==================================================================
    ! physical (adjustable) parameters
    !
    ! define and document here any adjustable parameters.
    ! this should be variable described using the doxygen format above as
    ! well as a description of its minimum/default/maximum.
    !
    ! here is an example,
    !
    ! real :: beta !< !           !! !           !! !==================================================================
    logical :: ok
    common /icom/ ibuf(8), idat(1)
    integer :: machine
    integer :: intsize
    integer*4 :: mynode
    common /mpinfo/ mynode
    common /machtyp/ machine,intsize
    !--- table dimensions
    ! nb ...number of bands
    !       4 currently 0.20-0.69, 0.69-1.19, 1.19-2.38, 2.38-4.00 um
    ! nt ...number of aerosol/cloud optical depths
    ! ns ...number of solar zenith angles
    ! nw ...number of wind speeds
    ! nc ...number of chlorophyll concentration values
    parameter (nb = 4, nt = 16, ns = 15, nw = 7, nc = 5)
  
    !       * salbcom communicates info about the type of surface albedo
    !       * calculation to be used. currently
    !       * ksalb = 0...surface albedo is calculated in class_
    !       * ksalb = 1...surface albedo is calculated from a lookup table
    !       * ksalb = 2...surface albedo is calculated from a formula
    common /salbcom/ ksalb
    !
  
    !       * surface albedo lookup table used when ksalb=1
    common /salbtab/ albedo_table(nb,nc,nw,ns,nt), init_table
    data init_table /0/
    !-----------------------------------------------------------------
    !.......assign this flag to common
    ksalb = ksalb_in
  
    if (ksalb == 1) then
      if (mynode == 0) then
        write(6, * )'INIT_SURFACE_ALBEDO2: Initializing albedo' &
                  ,' lookup table with data from a file.'
      end if
  
      !.........get an available unit number
      iu = newunit(60)
  
      !.........open the file containing the albedo lookup table
      inquire(file = "ALBTABLE",exist = ok)
      if (.not.ok) then
        write(6, * )'INIT_SURFACE_ALBEDO2: ', &
                     'Error accessing nasa_albedo_lookup_table2'
        call xit('INIT_SURFACE_ALBEDO2', - 1)
      end if
      open(iu,file = 'ALBTABLE',form = 'UNFORMATTED')
  
      !.........read the albedo lookup table
      lrec = nb * nc * nw * ns * machine
      do i = 1,nt
        call getfld2(iu,albedo_table(1,1,1,1,i), &
                     nc4to8("GRID"),i,nc4to8(" ALB"),1,ibuf,lrec,ok)
        if (.not.ok) call xit('INIT_SURFACE_ALBEDO2', - 2)
      end do
      close(iu)
      ichksum = nint(sum(albedo_table))
      !          if (ichksum/=4853) then ! checksum for old table
      init_table = 1
    end if
  
    if (ksalb == 2) then
      if (mynode == 0) then
        write(6, * )'INIT_SURFACE_ALBEDO2: Surface albedo' &
                  ,' calculation from formula'
      end if
    end if
  
    return
  end

#elif defined use_gem
  subroutine init_surface_albedo2 (ksalb_in,F_path_S)
    !***********************************************************************
    !     * feb 05/07 - f.majaess  adjusted in support of 32-bits mode.
    !     * mar 17/04 - l.solheim. modified dec 08/04 to remove threadprivate.
    !
    !***********************************************************************
    !     * read a cccma format file containing a lookup table for surface
    !     * albedo that has dependence on optical depth, zenith angle,
    !     * windspeed and ocean chlorophyl for each of 4 solar bands.
    !
    !     * input:
    !     *   ksalb - flags the type of surface albedo calculation to be done
    !     *           if ksalb .ne. 1 then simply return without filling
    !     *           the table in salbtab common
    !***********************************************************************
    implicit none
    real :: albedo_table
    integer :: i
    integer :: ichksum
    integer :: nb
    integer :: nt 
    integer :: ns 
    integer :: nw
    integer :: nc
    integer :: init_table
    integer :: ksalb
    integer :: lrec
  
    character(len=*), intent(in) :: F_path_S
    character(len=1024) :: fichier
  
    integer :: iu 
    integer, intent(in) :: ksalb_in
    integer*4 :: mynode
    common /mpinfo/ mynode
    !--- table dimensions
    ! nb ...number of bands
    !       4 currently 0.20-0.69, 0.69-1.19, 1.19-2.38, 2.38-4.00 um
    ! nt ...number of aerosol/cloud optical depths
    ! ns ...number of solar zenith angles
    ! nw ...number of wind speeds
    ! nc ...number of chlorophyll concentration values
    parameter (nb=4, nt=16, ns=15, nw=7, nc=5)
  
    !* SALBCOM communicates info about the type of surface albedo
    !* calculation to be used. Currently
    !* KSALB = 0 ...surface albedo is calculated in CLASS_
    !* KSALB = 1 ...surface albedo is calculated from a lookup table
    !* KSALB = 2 ...surface albedo is calculated from a formula
    common /salbcom/ ksalb
  
    integer, external :: wkoffit
    integer :: ftyp
#if !defined (CCCMA_IO)
    integer :: ier,cle,ni,nj,nk
    integer, external :: fnom,fstouv,fstlirx,fstfrm,fclos
#else
    logical :: ok
    common /icom/ ibuf(8), idat(1)
    integer :: machine,intsize
    common /machtyp/ machine,intsize
#endif
   !* Surface albedo lookup table used when KSALB=1
   common /salbtab/ albedo_table(nb,nc,nw,ns,nt), init_table
   data init_table /0/
   
  
   ! Assign this flag to common
   ksalb=ksalb_in
   ! Here we use the name of "rad_table.fst", but actually, it is albedo table
   fichier= trim(F_path_S)//'rad_table.fst'
  
   if (ksalb==1) then
  
     if (mynode==0) then
       WRITE(6,*)'INIT_SURFACE_ALBEDO: Initializing albedo' &
                ,' lookup table with data from a file.'
     endif
  
     ftyp = wkoffit( fichier )
#if !defined (CCCMA_IO)
     if (ftyp/=1 .and. ftyp/=33) then
         write(6,*)'INIT_SURFACE_ALBEDO: ', &
                   'Either the nasa_albedo_lookup_table2 is ', &
                   'empty or not the right type.'
         call xit('INIT_SURFACE_ALBEDO',-1)
     endif
  
     ! Open the file containing the albedo lookup table
     iu = 0
     ier = fnom( IU,fichier,'STD+RND+R/O', 0 )
     if (ier/=0) then
         write(6,*)'INIT_SURFACE_ALBEDO: fnom,', &
                   'Error accessing nasa_albedo_lookup_table2'
       call xit('INIT_SURFACE_ALBEDO',-1)
     endif
     ier = fstouv( IU, 'RND' )
     if (ier<0) then
         write(6,*)'INIT_SURFACE_ALBEDO: fstouv ,',ier, &
                   'Error accessing nasa_albedo_lookup_table2'
       call xit('INIT_SURFACE_ALBEDO',-1)
     endif
  
     ! Read the albedo lookup table
     lrec=nb*nc*nw*ns
     cle = 0
     do i=1,nt
        cle = fstlirx( albedo_table(1,1,1,1,i), &
                       cle,IU,ni,nj,nk,-1,'NASA_ALBEDO', &
                       -1,-1,I,' ','ALB' )
        if (cle<0 .or. ni*nj*nk/=lrec) &
           call xit('INIT_SURFACE_ALBEDO',-2)
     enddo
     ier = fstfrm( IU )
     ier = fclos( IU )
#else
     if (ftyp/=4) then
         write(6,*)'INIT_SURFACE_ALBEDO: ', &
                   'Either the nasa_albedo_lookup_table2 is ', &
                   'empty or not the right type.'
         call xit('INIT_SURFACE_ALBEDO',-1)
     endif
  
     ! Get an available unit number
     iu=newunit(60)
  
     ! Open the file containing the albedo lookup table
     inquire(file="../albtable",exist=ok)
     if (.not.ok) then
         write(6,*)'INIT_SURFACE_ALBEDO: ', &
                   'Error accessing nasa_albedo_lookup_table2'
       call xit('INIT_SURFACE_ALBEDO',-1)
     endif
  
     open(iu,file='../albtable',form='UNFORMATTED')
  
     ! Read the albedo lookup table
     lrec=nb*nc*nw*ns*machine
     do i=1,nt
       call getfld2(iu,albedo_table(1,1,1,1,i), &
            nc4to8("GRID"),I,nc4to8(" ALB"),1,ibuf,lrec,ok)
       if (.not.ok) call xit('INIT_SURFACE_ALBEDO',-2)
     enddo
     close(iu)
#endif
      ichksum=nint(sum(albedo_table))
      ! if (ichksum.ne.4853) then ! checksum for old table
      init_table=1
    endif
  
    if (ksalb==2) then
      if (mynode==0) then
        write(6,*)'INIT_SURFACE_ALBEDO: Surface albedo' &
                 ,' calculation from formula'
      endif
    endif
  
    return
  end
#endif
!> !> !! !! ! !! ! !! ! !! ! !! ! !! !
