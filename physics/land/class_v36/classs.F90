subroutine classs (tbarrot,thlqrot,thicrot,tsfsrot,tpndrot, &
                         zpndrot,tbasrot,albsrot,tsnorot,rhosrot, &
                         snorot, gtrot,  tcanrot,rcanrot,scanrot, &
                         grorot, cmairot,tacrot, qacrot, wsnorot, &
                         refrot, bcsnrot,emisrot,salbrot,csalrot, &
                         cdmrot,  qgrot,  hfsrot, qfsrot,fnrot, &
                         ilmos,jlmos, &
                         nml,nl,nt,nm,ilg,ig,ic,icp1,nbs, &
                         tbargat,thlqgat,thicgat,tsfsgat,tpndgat, &
                         zpndgat,tbasgat,albsgat,tsnogat,rhosgat, &
                         snogat, gtgat,  tcangat,rcangat,scangat, &
                         grogat, cmaigat,tacgat, qacgat, wsnogat, &
                         refgat, bcsngat,emisgat,salbgat,csalgat, &
                         cdmgat,  qggat,  hfsgat, qfsgat,fngat)
  !
  !     * jun 20, 2014 - m.lazare. new version for gcm18, called
  !     *                          by new "sfcproc3":
  !     *                          - adds salbgat/salbrot and
  !     *                            csalgat,csalrot (need to pass
  !     *                            nbs as well).
  !     *                          - adds emisgat/emisrot.
  !     *                          - adds gtgat/gtrot.
  !     *                          - adds nt (ntld in sfcproc2
  !     *                            call) to dimension land-only
  !     *                            rot fields, consistent with
  !     *                            new comrow12.
  !     *                          - unused iwmos,jwmos removed.
  !     * jun 12, 2013 - m.lazare. previous version for gcm17,
  !     *                          called by "sfcproc".
  !     *                          class scatter routine called by
  !     *                          "sfcproc" in new version gcm17.
  !     * note: this contains the following changes compared to the
  !     *       working temporary version used in conjunction with
  !     *       updates to gcm16 (ie not official):
  !     *         1) {REF,BCSN} added for Maryam's new code.
  !     *         2) gflx removed.
  !
  !     * oct 25/11 - m.lazare.   remove operations on internal
  !     *                         rot arrays (now done directly
  !     *                         gat->row in sfcproc).
  !     * oct 07/11 - m.lazare.   remove tsf.
  !     * oct 05/11 - m.lazare.   add sfch.
  !     * oct 04/11 - m.lazare.   remove itct.
  !     * mar 23/06 - d.verseghy. add wsno,fsno.
  !     * mar 18/05 - d.verseghy. additional variables.
  !     * feb 18/05 - d.verseghy. add "TSFS" variables.
  !     * aug 05/04 - d.verseghy. add new diagnostic variables
  !     *                         ilmo, ue and hbl.
  !     * aug 15/02 - d.verseghy. scatter operation on class
  !     *                         variables.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: nml !<
  integer, intent(in) :: nl !<
  integer, intent(in) :: nt !<
  integer, intent(in) :: nm !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ig !<
  integer, intent(in) :: ic !<
  integer, intent(in) :: icp1 !<
  integer, intent(in) :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  integer :: k !<
  integer :: l !<
  integer :: m !<
  !
  !     * land surface prognostic variables.
  !
  real, intent(inout)    :: salbrot(nl,nm,nbs) !<
  real, intent(inout)    :: csalrot(nl,nm,nbs) !<

  real, intent(inout)    :: tbarrot(nl,nt,ig) !<
  real, intent(inout)    :: thlqrot(nl,nt,ig) !<
  real, intent(inout)    :: thicrot(nl,nt,ig) !<

  real, intent(inout), dimension(nl,nt,4) :: tsfsrot !<

  real, intent(inout)    :: tpndrot(nl,nt) !<
  real, intent(inout)    :: zpndrot(nl,nt) !<
  real, intent(inout)    :: tbasrot(nl,nt) !<
  real, intent(inout)    :: albsrot(nl,nm) !<
  real, intent(inout)    :: tsnorot(nl,nm) !<
  real, intent(inout)    :: rhosrot(nl,nm) !<
  real, intent(inout)    :: snorot (nl,nm) !<
  real, intent(inout)    :: gtrot  (nl,nm) !<
  real, intent(inout)    :: tcanrot(nl,nt) !<
  real, intent(inout)    :: rcanrot(nl,nt) !<
  real, intent(inout)    :: scanrot(nl,nt) !<
  real, intent(inout)    :: grorot (nl,nt) !<
  real, intent(inout)    :: tacrot (nl,nt) !<
  real, intent(inout)    :: qacrot (nl,nt) !<
  real, intent(inout)    :: wsnorot(nl,nm) !<
  real, intent(inout)    :: cmairot(nl,nt) !<
  real, intent(inout)    :: refrot (nl,nm) !<
  real, intent(inout)    :: bcsnrot(nl,nm) !<
  real, intent(inout)    :: emisrot(nl,nm) !<
  real, intent(inout)    :: cdmrot (nl,nm) !<
  real, intent(inout)    :: qgrot  (nl,nm) !<
  real, intent(inout)    :: hfsrot (nl,nm) !<
  real, intent(inout)    :: qfsrot (nl,nm) !<
  real, intent(inout)    :: fnrot  (nl,nm) !<
  !
  real, intent(in)    :: salbgat(ilg,nbs) !<
  real, intent(in)    :: csalgat(ilg,nbs) !<

  real, intent(in)    :: tbargat(ilg,ig) !<
  real, intent(in)    :: thlqgat(ilg,ig) !<
  real, intent(in)    :: thicgat(ilg,ig) !<

  real, intent(in), dimension(ilg,4) :: tsfsgat !<

  real, intent(in)    :: tpndgat(ilg) !<
  real, intent(in)    :: zpndgat(ilg) !<
  real, intent(in)    :: tbasgat(ilg) !<
  real, intent(in)    :: albsgat(ilg) !<
  real, intent(in)    :: tsnogat(ilg) !<
  real, intent(in)    :: rhosgat(ilg) !<
  real, intent(in)    :: snogat (ilg) !<
  real, intent(in)    :: gtgat  (ilg) !<
  real, intent(in)    :: tcangat(ilg) !<
  real, intent(in)    :: rcangat(ilg) !<
  real, intent(in)    :: scangat(ilg) !<
  real, intent(in)    :: grogat (ilg) !<
  real, intent(in)    :: tacgat (ilg) !<
  real, intent(in)    :: qacgat (ilg) !<
  real, intent(in)    :: wsnogat(ilg) !<
  real, intent(in)    :: cmaigat(ilg) !<
  real, intent(in)    :: refgat (ilg) !<
  real, intent(in)    :: bcsngat(ilg) !<
  real, intent(in)    :: emisgat(ilg) !<
  real, intent(in)    :: cdmgat (ilg) !<
  real, intent(in)    :: qggat  (ilg) !<
  real, intent(in)    :: hfsgat (ilg) !<
  real, intent(in)    :: qfsgat (ilg) !<
  real, intent(in)    :: fngat  (ilg) !<
  !
  !     * gather-scatter index arrays.
  !
  integer, intent(in)  :: ilmos (ilg) !<
  integer, intent(in)  :: jlmos  (ilg) !<
  !----------------------------------------------------------------------
  do k=1,nml
    tpndrot(ilmos(k),jlmos(k))=tpndgat(k)
    zpndrot(ilmos(k),jlmos(k))=zpndgat(k)
    tbasrot(ilmos(k),jlmos(k))=tbasgat(k)
    albsrot(ilmos(k),jlmos(k))=albsgat(k)
    tsnorot(ilmos(k),jlmos(k))=tsnogat(k)
    rhosrot(ilmos(k),jlmos(k))=rhosgat(k)
    snorot (ilmos(k),jlmos(k))=snogat (k)
    gtrot  (ilmos(k),jlmos(k))=gtgat  (k)
    wsnorot(ilmos(k),jlmos(k))=wsnogat(k)
    tcanrot(ilmos(k),jlmos(k))=tcangat(k)
    rcanrot(ilmos(k),jlmos(k))=rcangat(k)
    scanrot(ilmos(k),jlmos(k))=scangat(k)
    grorot (ilmos(k),jlmos(k))=grogat (k)
    tacrot (ilmos(k),jlmos(k))=tacgat (k)
    qacrot (ilmos(k),jlmos(k))=qacgat (k)
    cmairot(ilmos(k),jlmos(k))=cmaigat(k)
    refrot (ilmos(k),jlmos(k))=refgat (k)
    bcsnrot(ilmos(k),jlmos(k))=bcsngat(k)
    emisrot(ilmos(k),jlmos(k))=emisgat(k)
    cdmrot (ilmos(k),jlmos(k))=cdmgat (k)
    qgrot  (ilmos(k),jlmos(k))=qggat  (k)
    hfsrot (ilmos(k),jlmos(k))=hfsgat (k)
    qfsrot (ilmos(k),jlmos(k))=qfsgat (k)
    fnrot  (ilmos(k),jlmos(k))=fngat  (k)
  end do ! loop 100
  !
  do l=1,nbs
    do k=1,nml
      salbrot(ilmos(k),jlmos(k),l)=salbgat(k,l)
      csalrot(ilmos(k),jlmos(k),l)=csalgat(k,l)
    end do
  end do ! loop 200
  !
  do l=1,ig
    do k=1,nml
      tbarrot(ilmos(k),jlmos(k),l)=tbargat(k,l)
      thlqrot(ilmos(k),jlmos(k),l)=thlqgat(k,l)
      thicrot(ilmos(k),jlmos(k),l)=thicgat(k,l)
    end do
  end do ! loop 300
  !
  do l=1,4
    do k=1,nml
      tsfsrot(ilmos(k),jlmos(k),l)=tsfsgat(k,l)
    end do
  end do ! loop 400
  !
  return
end
