!> \file
!>\brief Optical properties of the background aerosols
!!
!! @author Jiangnan Li
!
subroutine aeros4
  !
  !     * j.li,m.lazare. apr 05, 2002.
  !
  !----------------------------------------------------------------------c
  !    this subroutine deals with the background aerosol, 5 species      c
  !    of different aerosols can be handled. so far we consider 3        c
  !    species. 1: dust;  3: soot                                        c
  !----------------------------------------------------------------------c
  !
  implicit none
  integer :: i
  integer :: j
  integer :: nbs
  integer :: nbl
  parameter (nbs = 4, nbl = 9)
  real, dimension(nbl,5) :: absa
  real, dimension(nbs,5) :: exta
  real, dimension(nbs,5) :: ga
  real, dimension(nbs,5) :: oma
  common /aeros/ exta, oma, ga, absa
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !----------------------------------------------------------------------c
  !     shortwave                                                        c
  !----------------------------------------------------------------------c
  !
  data ((exta(i,j),j = 1, 5), i = 1, nbs) / &
                       .99442, 0.0, 1.23885, 0.0, 0.0, &
                       1.02985, 0.0,  .55462, 0.0, 0.0, &
                       1.06979, 0.0,  .27074, 0.0, 0.0, &
                       .98607, 0.0,  .14180, 0.0, 0.0    /
  !
  data ((oma(i,j),j = 1, 5), i = 1, nbs) / &
                      .64284, 0.0,  .24312, 0.0, 0.0, &
                      .69910, 0.0,  .13072, 0.0, 0.0, &
                      .76804, 0.0,  .04549, 0.0, 0.0, &
                      .79485, 0.0,  .01020, 0.0, 0.0    /
  !
  data ((ga(i,j),j = 1, 5), i = 1, nbs) / &
                     .88366, 0.0,  .38168, 0.0, 0.0, &
                     .84431, 0.0,  .26786, 0.0, 0.0, &
                     .84666, 0.0,  .16508, 0.0, 0.0, &
                     .91277, 0.0,  .08242, 0.0, 0.0   /
  !
  !----------------------------------------------------------------------c
  !     infrared                                                         c
  !----------------------------------------------------------------------c
  !
  data ((absa(i,j),j = 1, 5), i = 1, nbl) / &
                       .18987, 0.0, .09438, 0.0, 0.0, &
                       .19282, 0.0, .08084, 0.0, 0.0, &
                       .24822, 0.0, .06513, 0.0, 0.0, &
                       .35147, 0.0, .04725, 0.0, 0.0, &
                       .47792, 0.0, .04010, 0.0, 0.0, &
                       .38462, 0.0, .03378, 0.0, 0.0, &
                       .33117, 0.0, .02444, 0.0, 0.0, &
                       .42489, 0.0, .01615, 0.0, 0.0, &
                       .43651, 0.0, .01050, 0.0, 0.0        /
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
