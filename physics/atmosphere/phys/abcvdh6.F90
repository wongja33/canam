!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine abcvdh6 (a,b,c,cl, cdvlh,grav,il1,il2,ilg,ilev,lev, &
                    rgas,rkh,shtj,shtxkj,shj,shxkj,dshj, &
                    thl,tf,todt)

  !     * jul 15/88 - m.lazare : reverse order of local sigma arrays.
  !     * mar 14/88 - r.laprise: previous hybrid version for gcm3h.

  !     * calculates the three vectors forming the tri-diagonal matrix for
  !     * the implicit vertical diffusion of heat in hybrid version of
  !     * model. a is the lower diagonal, b is the main diagonal,
  !     * and c is the upper diagonal.

  implicit none
  real :: d
  real, intent(inout) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer :: i
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  real :: ovds
  real, intent(inout) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(inout) :: todt
  real, intent(inout), dimension(ilg,ilev) :: a !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: b !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: c !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: rkh !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: thl !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: cdvlh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: cl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,lev) :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: shxkj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shtxkj !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  do i = il1,il2
    ovds  = shtxkj(i,2) * (grav * shtj(i,2)/rgas) ** 2 &
            /(dshj(i,1) * (shj(i,2) - shj(i,1)) )
    c(i,1) = ovds * rkh(i,2) * (1./tf(i,2)) ** 2
  end do ! loop 50

  do i = il1,il2
    b(i,1) = - c(i,1)/shxkj(i,1)
    c(i,1) =  c(i,1)/shxkj(i,2)
  end do ! loop 75

  do l = 2,ilev
    do i = il1,il2
      ovds   = (grav * shtj(i,l)/rgas) ** 2 &
               /( (shj(i,l) - shj(i,l - 1)) * dshj(i,l) )
      a(i,l) = ovds * (1./tf(i,l)) ** 2 * rkh(i,l)
    end do
  end do ! loop 100

  do l = 2,ilev - 1
    do i = il1,il2
      d     = dshj(i,l + 1) / dshj(i,l)
      c(i,l) = a(i,l + 1) * d
    end do
  end do ! loop 150

  do l = 2,ilev - 1
    do i = il1,il2
      b(i,l) = - ( c(i,l) * shtxkj(i,l + 1) &
               + a(i,l) * shtxkj(i,  l)) / shxkj(i,  l)
      a(i,l) =   a(i,l) * shtxkj(i,  l)  / shxkj(i,l - 1)
      c(i,l) =   c(i,l) * shtxkj(i,l + 1)  / shxkj(i,l + 1)
    end do
  end do ! loop 200

  l = ilev
  do i = il1,il2
    a(i,l) = a(i,l) * shtxkj(i,l)
    cl(i)  = grav * shj(i,l) * cdvlh(i)/(rgas * thl(i) * dshj(i,l))
  end do ! loop 250

  do i = il1,il2
    b(i,l) = - (a(i,l) + cl(i)) / shxkj(i,  l)
    a(i,l) =   a(i,l)        / shxkj(i,l - 1)
  end do ! loop 300

  !     * define matrix to invert = i-2*dt*mat(a,b,c).

  do l = 1,ilev - 1
    do i = il1,il2
      a(i,l + 1) = - todt * a(i,l + 1)
      c(i,l) = - todt * c(i,l)
    end do
  end do ! loop 500

  do l = 1,ilev
    do i = il1,il2
      b(i,l) = 1. - todt * b(i,l)
    end do
  end do ! loop 550

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
