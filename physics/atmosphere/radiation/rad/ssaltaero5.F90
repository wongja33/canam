!> \file
!>\brief Calculation of sea salt aerosol optical property for bulk aerosol parameterization
!!
!! @author Jiangnan Li
!
subroutine ssaltaero5(exta,  exoma, exomga, fa, absa, &
                      exta055, oma055, ga055, &
                      extb, odfb, ssab, absb, extb055, odf055, &
                      ssa055, abs055, rh, aload1,  aload2, &
                      il1, il2, ilg, lay)
  !
  !     * apr 23, 2012 - j.li & y.peng. new version for gcm16:
  !     *                             - add the optical depth calculation
  !     *                             for 0.55 um, two modes are 0.732 and
  !     *                             6.13 um as the wet size, please note
  !     *                             the band calcultion are based on dry
  !     *                             recalcute the 4 band shortwave part
  !     *                             to make it in the same size as 0.55
  !     *                             keep the longwave part as before (glen).
  !     * feb 13,2009 - m.lazare. previous version ssaltaero4 for gcm15h/i:
  !     *                         - rhmax lowered from 0.98 to 0.95.
  !     *                         - use bounded rh (srh) instead of rh to
  !     *                           calculate growth factor x.
  !     * dec 05,2007 - m.lazare. previous version ssaltaero3 for gcm15g:
  !     *                         - srh,frh,sload1,sload2 now internal
  !     *                           work arrays.
  !     * jun 19/06 -  m.lazare. previous version ssaltaero2 for gcm15f:
  !     *                        - cosmetic: use variable instead of
  !     *                          constant in intrinsics such as "max",
  !     *                          so that can compile in 32-bit mode
  !     *                          with real(8).
  !     * apr 25,2003 - j.li.    previous version ssaltaero for up to
  !     *                        and including gcm15e.
  !----------------------------------------------------------------------c
  !     calculation of optical properties for sea salt with two modes    c
  !                                                                      c
  !     exta:   extinction coefficient                                   c
  !     exoma:  extinction coefficient times single scattering albedo    c
  !     exomga: exoma times asymmetry factor                             c
  !     fa:     square of asymmetry factor                               c
  !     absa:   absorption coefficient                                   c
  !     rh:     relative humidity                                        c
  !     sload1: sea salt loading for fine mode                           c
  !     sload2: sea salt loading for coarse mode                         c
  !----------------------------------------------------------------------c
  implicit none
  real :: abs1
  real :: abs2
  real :: exom1
  real :: exom2
  real :: exomg1
  real :: exomg2
  real :: ext0551
  real :: ext0552
  real :: ext1
  real :: ext2
  real :: g1
  real :: g2
  real :: ga0551
  real :: ga0552
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: j
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: nbl
  integer :: nbs
  real :: om0551
  real :: om0552
  real :: om1
  real :: om2
  real :: omasum
  real :: rhmax
  real :: tmass
  real :: x
  !
  parameter (nbs = 4, nbl = 9)
  !
  !     * output arrays.
  !
  real, intent(inout), dimension(ilg,lay,nbs) :: exta !< Extinction coefficient at 0.55 \f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exoma !< EXTA*single scattering albedo\ f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exomga !< EXOMA*asymetry factor \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: fa !< EXOMGA*asymetry factor \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: absb !< Absorption coefficient \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: odfb !< Extinction coefficient per mass for accumulation mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: extb !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation+coarse mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: ssab !< Single scattering albedo \f$[0]\f$
  real, intent(inout), dimension(ilg,lay,nbl) :: absa!< Longwave absorption coefficient \f$[m^2/gram]\f$
  !
  real, intent(inout), dimension(ilg,lay) :: exta055 !< Extinction coefficient at 0.55 \f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: oma055 !< EXTA055 * single scattering albedo at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ga055 !< OMA055 * asymmetry factor at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: extb055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation+coarse mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: odf055 !< Extinction coefficient per mass at 0.55\f$\mu\f$m for accumulation mode \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ssa055 !< Single scattering albedo at 0.55\f$\mu\f$m \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: abs055 !< Absorption coefficient at 0.55\f$\mu\f$m \f$[m^2/gram]\f$
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg,lay) :: rh !< Relative humidity \f$[\%]\f$
  real, intent(in), dimension(ilg,lay) :: aload1 !< Aerosol loading for mode1 \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: aload2 !< Aerosol loading for mode2 \f$[gram/gram]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work arrays.
  !
  real, dimension(ilg,lay) :: srh
  real, dimension(ilg,lay) :: frh
  real, dimension(ilg,lay) :: sload1
  real, dimension(ilg,lay) :: sload2
  real, dimension(4,nbs,2) :: se
  real, dimension(4,nbs,2) :: sw
  real, dimension(4,nbs,2) :: sg
  real, dimension(4,nbl,2) :: sa
  !
  real, dimension(4,2) :: se055
  real, dimension(2,2) :: cw055
  real, dimension(4,2) :: g055
  real, dimension(ilg,lay) :: sload551
  real, dimension(ilg,lay) :: sload552
  real   :: facss
  !
  ! ccc jiangnan corrected parameters for band calculation
  !
  data (((se(i,j,k), i = 1, 4), j = 1, nbs), k = 1, 2) / &
                         1.391e+00, - 1.545e-01, 4.648e-02, 1.281e+00, &
                         1.309e+00, 5.348e-02, 5.152e-02, 1.226e+00, &
                         8.957e-01, 3.558e-01, 4.128e-02, 9.062e-01, &
                         4.099e-01, 3.625e-01, 1.496e-02, 4.504e-01, &
                         1.317e-01, - 7.488e-03, 4.379e-03, 1.218e-01, &
                         1.362e-01, - 9.357e-03, 4.498e-03, 1.262e-01, &
                         1.434e-01, - 1.308e-02, 4.664e-03, 1.330e-01, &
                         1.554e-01, - 1.798e-02, 5.028e-03, 1.330e-01           /
  !
  data (((sw(i,j,k), i = 1, 4), j = 1, nbs), k = 1, 2) / &
                         3.938e-06, - 2.809e-06, 5.888e-09, 8.927e-06, &
                         3.236e-04, - 2.417e-04, - 6.789e-07, 7.616e-04, &
                         3.045e-03, - 1.918e-03, - 1.590e-04, 4.597e-03, &
                         2.221e-01, - 6.472e-03, 1.382e-03, 3.493e-02, &
                         3.277e-05, - 2.335e-05, 9.002e-08, 7.038e-05, &
                         2.869e-03, - 2.044e-03, 4.838e-06, 6.325e-03, &
                         1.950e-02, - 8.772e-03, - 1.112e-03, 2.904e-02, &
                         2.218e-01, 3.505e-02, - 4.841e-03, 9.729e-02          /
  !
  data (((sg(i,j,k), i = 1, 4), j = 1, nbs), k = 1, 2) / &
                         7.667e-01, 1.750e-02, - 2.244e-03, 6.964e-01, &
                         7.764e-01, 2.025e-02, - 3.875e-04, 7.049e-01, &
                         7.682e-01, 4.617e-02, 2.603e-04, 7.028e-01, &
                         7.245e-01, 7.342e-02, - 3.145e-03, 6.550e-01, &
                         8.102e-01, 7.596e-02, 1.021e-02, 7.908e-01, &
                         8.030e-01, 7.067e-02, 3.140e-03, 7.784e-01, &
                         7.973e-01, 4.625e-02, - 8.634e-04, 7.613e-01, &
                         8.404e-01, 3.119e-02, - 2.783e-03, 7.470e-01          /
  !
  data (((sa(i,j,k), i = 1, 4), j = 1, nbl), k = 1, 2) / &
                         1.300e-02, 2.315e-02, - 3.508e-04, 2.517e-03, &
                         1.322e-02, 2.226e-02, - 4.119e-04, 3.647e-03, &
                         4.858e-02, 7.799e-02, 7.454e-04, 1.185e-02, &
                         2.469e-02, 2.915e-02, - 4.593e-04, 1.497e-02, &
                         2.434e-02, 3.196e-02, - 3.737e-04, 1.115e-02, &
                         4.888e-02, 7.948e-02, 1.013e-03, 6.968e-03, &
                         9.355e-02, 1.607e-01, 3.343e-03, 1.769e-02, &
                         6.312e-02, 9.555e-02, 8.945e-05, 3.203e-02, &
                         2.530e-02, 2.432e-02, - 4.512e-04, 1.908e-02, &
                         1.621e-02, 1.378e-02, 6.919e-04, 3.440e-03, &
                         1.691e-02, 1.455e-02, 7.138e-04, 5.132e-03, &
                         4.295e-02, 1.331e-02, 1.832e-03, 1.481e-02, &
                         2.903e-02, 1.505e-02, 1.161e-03, 2.070e-02, &
                         2.939e-02, 1.525e-02, 1.167e-03, 1.781e-02, &
                         4.336e-02, 1.393e-02, 1.806e-03, 1.142e-02, &
                         6.891e-02, 7.288e-03, 2.792e-03, 2.550e-02, &
                         6.488e-02, 1.362e-02, 2.775e-03, 4.389e-02, &
                         2.800e-02, 9.300e-03, 1.212e-03, 2.329e-02           /

  !
  data ((se055(i,k), i = 1, 4), k = 1, 2) / &
                         1.415e+00, - 1.692e-01, 4.704e-02, 1.30086, &
                         1.320e-01, - 7.585e-03, 4.385e-03, 0.12199             /
  !
  data ((cw055(i,k), i = 1, 2), k = 1, 2) / &
                         0.99999,  0.99999, &
                         0.99999,  0.99999                                    /
  !
  data ((g055(i,k), i = 1, 4), k = 1, 2)  / &
                        7.646e-01, 4.447e-02, - 1.197e-03, 0.69225, &
                        7.731e-01, - 6.938e-02, 1.272e-02, 0.79106             /
  !
  data rhmax /0.95/
  !
  !----------------------------------------------------------------------c
  !     sea salt with two modes: accumulation mode with model radius     c
  !     0.209 um, sigma 2.03; coarse mode with model radius 1.75 um,     c
  !     sigma 2.03. density 2.24 kg/m^3                                  c
  !                                                                      c
  !     solar, se: specific extinction, oma: single scattering albedo,   c
  !     ga: asymmetry factor, exta: extinction coefficient               c
  !     factor 10000 because the unit of specific extinction for aerosol c
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv we use same  c
  !     dp (air density * layer thickness) is used for both ga and       c
  !     aerosol. aload s salt aerosol loading in unit g (aerosol) / g    c
  !     (air). tau = aload * specific extinction * air density *         c
  !     layer thickness                                                  c
  !     data by g. lensins, code by j. li (2002, 2)                      c
  !----------------------------------------------------------------------c
  !
  facss              =  0.90
  do k = 1, lay
    do i = il1, il2
      srh(i,k)         =  min (rh(i,k), rhmax)
      frh(i,k)         =  1.0 / (srh(i,k) - 1.05)
      x                =  10000.0 * exp(2.9115e-03 + 1.5673 * &
                         srh(i,k) + 1.1818e-02 / ((srh(i,k) - 1.06) * &
                         (srh(i,k) - 1.06)))
      sload1(i,k)      =  x * aload1(i,k)
      sload2(i,k)      =  x * aload2(i,k)
      sload551(i,k)    =  x * aload1(i,k) * facss
      sload552(i,k)    =  x * (aload2(i,k) + aload1(i,k) * (1.0 - facss))
    end do
  end do ! loop 100
  !
  do j = 1, nbs
    do k = 1, lay
      do i = il1, il2
        if (rh(i,k) >= 0.45) then
          ext1           =  sload551(i,k) * (se(1,j,1) + se(2,j,1) * &
                           srh(i,k) + se(3,j,1) * frh(i,k))
          ext2           =  sload552(i,k) * (se(1,j,2) + se(2,j,2) * &
                           srh(i,k) + se(3,j,2) * frh(i,k))
          !
          om1            =  1.0 - (sw(1,j,1) + sw(2,j,1) * srh(i,k) + &
                           sw(3,j,1) * frh(i,k))
          om2            =  1.0 - (sw(1,j,2) + sw(2,j,2) * srh(i,k) + &
                           sw(3,j,2) * frh(i,k))
          exom1          =  ext1 * om1
          exom2          =  ext2 * om2
          !
          g1             =  sg(1,j,1) + sg(2,j,1) * srh(i,k) + &
                           sg(3,j,1) * frh(i,k)
          g2             =  sg(1,j,2) + sg(2,j,2) * srh(i,k) + &
                           sg(3,j,2) * frh(i,k)
          exomg1         =  exom1 * g1
          exomg2         =  exom2 * g2
          !
          absb(i,k,j)    =  ext1 * (1.0 - om1) + ext2 * (1.0 - om2)
          !
        else
          ext1           =  sload551(i,k) * se(4,j,1)
          ext2           =  sload552(i,k) * se(4,j,2)
          exom1          =  ext1 * (1.0 - sw(4,j,1))
          exom2          =  ext2 * (1.0 - sw(4,j,2))
          g1             =  sg(4,j,1)
          g2             =  sg(4,j,2)
          exomg1         =  exom1 * g1
          exomg2         =  exom2 * g2

          absb(i,k,j)    =  ext1 * sw(4,j,1) + ext2 * sw(4,j,2)
        end if
        !
        exta(i,k,j)      =  exta(i,k,j) + ext1 + ext2
        exoma(i,k,j)     =  exoma(i,k,j) + exom1 + exom2
        exomga(i,k,j)    =  exomga(i,k,j) + exomg1 + exomg2
        fa(i,k,j)        =  fa(i,k,j) + exomg1 * g1 + exomg2 * g2
        !
        tmass            =  10000.0 * (aload1(i,k) + aload2(i,k))
        if (tmass > 1.e-15) then
          extb(i,k,j)     =  (ext1 + ext2) / tmass
        else
          extb(i,k,j)     =  0.0
        end if
        if (aload1(i,k) > 1.e-19) then
          odfb(i,k,j)     =  ext1 / (10000.0 * aload1(i,k))
        else
          odfb(i,k,j)     =  0.0
        end if
        !
        if ((ext1 + ext2) > 1.e-20) then
          ssab(i,k,j)     =  (exom1 + exom2) / (ext1 + ext2)
        else
          ssab(i,k,j)     =  0.0
        end if
        !
      end do
    end do
  end do ! loop 200
  !
  do k = 1, lay
    do i = il1, il2
      if (rh(i,k) >= 0.45) then
        ext0551        =  sload551(i,k) * (se055(1,1) + se055(2,1) * &
                         srh(i,k) + se055(3,1) * frh(i,k))
        ext0552        =  sload552(i,k) * (se055(1,2) + se055(2,2) * &
                         srh(i,k) + se055(3,2) * frh(i,k))
        !
        om0551         =  cw055(1,1)
        om0552         =  cw055(1,2)
        !
        ga0551         =  g055(1,1) + g055(2,1) * srh(i,k) + &
                         g055(3,1) * frh(i,k)
        ga0552         =  g055(1,2) + g055(2,2) * srh(i,k) + &
                         g055(3,2) * frh(i,k)
      else
        ext0551        =  sload551(i,k) * se055(4,1)
        ext0552        =  sload552(i,k) * se055(4,2)
        om0551         =  cw055(2,1)
        om0552         =  cw055(2,2)
        ga0551         =  g055(4,1)
        ga0552         =  g055(4,2)
      end if
      !
      exta055(i,k)     =  exta055(i,k) + ext0551 + ext0552
      omasum           =  ext0551 * om0551 + ext0552 * om0552
      oma055(i,k)      =  oma055(i,k) + omasum
      ga055(i,k)       =  ga055(i,k) + (ext0551 * om0551 * ga0551 * &
                         ga0551 + ext0552 * om0552 * ga0552 * ga0552)
      !
      abs055(i,k)      =  ext0551 * (1.0 - om0551) + &
                         ext0552 * (1.0 - om0552)
      tmass            =  10000.0 * (aload1(i,k) + aload2(i,k))
      if (tmass > 1.e-15) then
        extb055(i,k)    =  (ext0551 + ext0552) / tmass
      else
        extb055(i,k)    =  0.0
      end if
      if (aload1(i,k) > 1.e-19) then
        odf055(i,k)     =  ext0551 / (10000.0 * aload1(i,k))
      else
        odf055(i,k)     =  0.0
      end if
      !
      if ((ext0551 + ext0552) > 1.e-20) then
        ssa055(i,k)     =  omasum / (ext0551 + ext0552)
      else
        ssa055(i,k)     =  0.0
      end if
      !
    end do
  end do ! loop 250
  !
  !----------------------------------------------------------------------c
  !     infrared, sa: specific absorptance, abs: absorption              c
  !----------------------------------------------------------------------c
  !
  do j = 1, nbl
    do k = 1, lay
      do i = il1, il2
        !
        if (rh(i,k) >= 0.45) then
          abs1           =  sload551(i,k) * (sa(1,j,1) + sa(2,j,1) * &
                           srh(i,k) + sa(3,j,1) * frh(i,k))
          abs2           =  sload552(i,k) * (sa(1,j,2) + sa(2,j,2) * &
                           srh(i,k) + sa(3,j,2) * frh(i,k))
        else
          abs1           =  sload551(i,k) * sa(4,j,1)
          abs2           =  sload552(i,k) * sa(4,j,2)
        end if
        !
        absa(i,k,j)      =  absa(i,k,j) + abs1 + abs2
      end do
    end do
  end do ! loop 400
  !
  return
end
!> \file
!> Calculation of optical properties of sea salt in two bulk modes.
!! sea salt with two modes: accumulation mode with model radius 
!! 0.209 um, sigma 2.03; coarse mode with model radius 1.75 um, 
!! sigma 2.03. density 2.24 kg/m^3