subroutine canadd(iwater,r,tr,s,ts,raican,snocan,tcan,chcap, &
                        htcc,rofc,rovg,pcpn,pcpg,fi,fsvf, &
                        cwlcap,cwfcap,cmass,rhosni,tsurx,rdrip,sdrip, &
                        ilg,il1,il2,jl)
  !
  !     * nov 22/06 - e.chan/d.verseghy. unconditionally set tr and ts.
  !     * jan 05/05 - p.bartlett. correct/refine snow interception
  !     *                         calculations.
  !     * sep 13/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 29/02 - d.verseghy/s.fassnacht. new snow interception
  !     *                                     algorithm,
  !     * jul 24/02 - d.verseghy. move diagnostic calculations from
  !     *                         classw into this routine; change
  !     *                         rhosni from constant to variable.
  !     * jun 20/02 - d.verseghy. additional diagnostic calculations.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         pass in new "CLASS4" common block.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. calculate canopy interception; add
  !     *                         throughfall and canopy drip to
  !     *                         precipitation reaching ground.
  !     *                         adjust canopy temperature and heat
  !     *                         capacity.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, rhow, sphw, sphveg, sphice
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: iwater !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: r     (ilg) !<
  real, intent(inout) :: tr    (ilg) !<
  real, intent(inout) :: s     (ilg) !<
  real, intent(inout) :: ts    (ilg) !<
  real, intent(inout) :: raican(ilg) !<
  real, intent(inout) :: snocan(ilg) !<
  real, intent(inout) :: tcan  (ilg) !<
  real, intent(inout) :: chcap (ilg) !<
  real, intent(inout) :: htcc  (ilg) !<
  real, intent(inout) :: rofc  (ilg) !<
  real, intent(inout) :: rovg  (ilg) !<
  real, intent(inout) :: pcpn  (ilg) !<
  real, intent(inout) :: pcpg  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: fsvf  (ilg) !<
  real, intent(in) :: cwlcap(ilg) !<
  real, intent(in) :: cwfcap(ilg) !<
  real, intent(in) :: cmass (ilg) !<
  real, intent(inout) :: rhosni(ilg) !<
  real, intent(inout) :: tsurx (ilg) !<
  !
  !     * internal work arrays.
  !
  real, intent(inout) :: rdrip (ilg) !<
  real, intent(inout) :: sdrip (ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  real :: rthru !<
  real :: rint !<
  real :: sthru !<
  real :: sint !<
  real :: trcan !<
  real :: tscan !<
  real :: rwxces !<
  real :: sload !<
  real :: swxces !<
  real :: snunld !<
  real :: chcapi !<
  real :: tcani !<
  !
  !-----------------------------------------------------------------------
  do i=il1,il2
    rdrip(i)=0.0
    sdrip(i)=0.0
    if (fi(i)>0. .and. (r(i)>0. .or. s(i)>0. .or. &
        raican(i)>0. .or. snocan(i)>0.)) then
      rthru=r(i)*fsvf(i)
      rint=(r(i)-rthru)*delt*rhow
      sthru=s(i)*fsvf(i)
      sint=(s(i)-sthru)*delt*rhosni(i)
      if ((raican(i)+rint)>0.) then
        trcan=(raican(i)*tcan(i)+rint*tr(i))/(raican(i)+rint)
      else
        trcan=0.0
      end if
      if ((snocan(i)+sint)>0.) then
        tscan=(snocan(i)*tcan(i)+sint*ts(i))/(snocan(i)+sint)
      else
        tscan=0.0
      end if
      !
      rwxces=rint+raican(i)-cwlcap(i)
      if (rwxces>0.) then
        rdrip(i)=rwxces/(delt*rhow)
        if ((rdrip(i)+rthru)>0.) then
          tr(i)=(rdrip(i)*trcan+rthru*tr(i))/ &
                       (rdrip(i)+rthru)
        else
          tr(i)=0.0
        end if
        r(i)=rdrip(i)+rthru
        raican(i)=cwlcap(i)
      else
        r(i)=rthru
        raican(i)=raican(i)+rint
      end if
      !
      sload=(cwfcap(i)-snocan(i))*(1.0-exp(-sint/cwfcap(i)))
      swxces=sint-sload
      snunld=(sload+snocan(i))*(1.0-exp(-1.157e-6*delt))
      if (swxces>0. .or. snunld>0.) then
        sdrip(i)=(max(swxces,0.0)+snunld)/(delt*rhosni(i))
        if ((sdrip(i)+sthru)>0.) then
          ts(i)=(sdrip(i)*tscan+sthru*ts(i))/ &
                       (sdrip(i)+sthru)
        else
          ts(i)=0.0
        end if
        s(i)=sdrip(i)+sthru
        snocan(i)=snocan(i)+sint-max(swxces,0.0)-snunld
      else
        s(i)=sthru
        snocan(i)=snocan(i)+sint
      end if
      !
      chcapi  =chcap(i)
      tcani   =tcan(i)
      chcap(i)=raican(i)*sphw+snocan(i)*sphice+cmass(i)*sphveg
      tcan (i)=(raican(i)*sphw*trcan+snocan(i)*sphice*tscan+ &
                  cmass(i)*sphveg*tcan(i))/chcap(i)
      htcc (i)=htcc(i)+fi(i)*(chcap(i)*tcan(i)-chcapi*tcani)/ &
                  delt
      if (r(i)>0.0) then
        tr(i)=tr(i)-tfrez
      else
        tr(i)=max(tsurx(i)-tfrez,0.0)
      end if
      if (s(i)>0.0) then
        ts(i)=ts(i)-tfrez
      else
        ts(i)=min(tsurx(i)-tfrez,0.0)
      end if
      if (iwater==2) then
        rofc(i)=rofc(i)+fi(i)*(rdrip(i)*rhow+sdrip(i)* &
                 rhosni(i))
        pcpn(i)=pcpn(i)+fi(i)*(rdrip(i)*rhow+sdrip(i)* &
                 rhosni(i))
      end if
      if (iwater==1) then
        rofc(i)=rofc(i)+fi(i)*(rdrip(i)*rhow+sdrip(i)* &
                 rhosni(i))
        rovg(i)=rovg(i)+fi(i)*(rdrip(i)*rhow+sdrip(i)* &
                 rhosni(i))
        pcpn(i)=pcpn(i)+fi(i)*sdrip(i)*rhosni(i)
        pcpg(i)=pcpg(i)+fi(i)*rdrip(i)*rhow
      end if
    end if
  end do ! loop 100
  !
  return
end subroutine canadd
