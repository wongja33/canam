subroutine tfreez(zpond,tpond,zsnow,tsnow,albsno,rhosno,hcpsno, &
                        gzero,hmfg,htcs,htc,wtrs,wtrg,fi,qfrez, &
                        wsnow,ta,tbar,isand,ig,ilg,il1,il2,jl)
  !
  !     * jan 06/09 - d.verseghy. set qfrez to zero after calculation
  !     *                         of hadd.
  !     * mar 24/06 - d.verseghy. allow for presence of water in snow.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jun 20/02 - d.verseghy. cosmetic changes to subroutine call
  !     *                         shortened class4 common block.
  !     * may 24/02 - d.verseghy. pass in entire soil temperature
  !     *                         array.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * aug 18/95 - d.verseghy. class - version 2.4.
  !     *                         revisions to allow for inhomogeneity
  !     *                         between soil layers.
  !     * aug 16/95 - d.verseghy. two new arrays to complete water
  !     *                         balance diagnostics.
  !     * dec 22/94 - d.verseghy. class - version 2.3.
  !     *                         revise calculation of htc.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. freezing of ponded water.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, hcpw, hcpice, rhow, rhoice, clhmlt
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ig !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: zpond (ilg) !<
  real, intent(inout) :: tpond (ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: albsno(ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(inout) :: gzero (ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  real, intent(inout) :: wtrs  (ilg) !<
  real, intent(inout) :: wtrg  (ilg) !<
  !
  real, intent(inout) :: hmfg  (ilg,ig) !<
  real, intent(inout) :: htc   (ilg,ig) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(inout) :: qfrez (ilg) !<
  real, intent(in) :: wsnow (ilg) !<
  real, intent(in) :: ta    (ilg) !<
  real, intent(in) :: tbar  (ilg,ig) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  real :: zfrez !<
  real :: hadd !<
  real :: hcool !<
  real :: hconv !<
  real :: ttest !<
  real :: tlim !<
  real :: hexces !<
  !
  !-----------------------------------------------------------------------
  do i=il1,il2
    if (fi(i)>0. .and. zpond(i)>0. .and. (tpond(i)<0. &
        .or. qfrez(i)<0.)) then
      htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)*zsnow(i)/ &
                delt
      zfrez=0.0
      hadd=-qfrez(i)*delt
      qfrez(i)=0.0
      if (tpond(i)<0.) then
        hadd=hadd-tpond(i)*hcpw*zpond(i)
        tpond(i)=0.0
      end if
      hcool=tpond(i)*hcpw*zpond(i)
      hconv=hcool+clhmlt*rhow*zpond(i)
      htc (i,1)=htc (i,1)-fi(i)*hcpw*(tpond(i)+tfrez)* &
                  zpond(i)/delt
      if (hadd<=hcool) then
        tpond(i)=tpond(i)-hadd/(hcpw*zpond(i))
        htc(i,1)=htc(i,1)+fi(i)*hadd/delt
      else if (hadd<=hconv) then
        hadd=hadd-hcool
        zfrez=hadd/(clhmlt*rhow)
        zpond(i)=zpond(i)-zfrez
        htc(i,1)=htc(i,1)+fi(i)*hcool/delt
        zfrez=zfrez*rhow/rhoice
        if (.not.(zsnow(i)>0.0)) albsno(i)=0.50
        tsnow(i)=tsnow(i)*hcpsno(i)*zsnow(i)/(hcpsno(i)*zsnow(i) &
                    +hcpice*zfrez)
        rhosno(i)=(rhosno(i)*zsnow(i)+rhoice*zfrez)/(zsnow(i) &
                    +zfrez)
        zsnow(i)=zsnow(i)+zfrez
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                    (rhow*zsnow(i))
        tpond(i)=0.0
      else
        hadd=hadd-hconv
        zfrez=zpond(i)*rhow/rhoice
        htc(i,1)=htc(i,1)+fi(i)*hcool/delt
        ttest=-hadd/(hcpice*zfrez)
        if (zsnow(i)>0.0) then
          tlim=min(tsnow(i),tbar(i,1))
        else
          tlim=min(ta(i)-tfrez,tbar(i,1))
        end if
        tlim=min(tlim,0.0)
        if (ttest<tlim) then
          hexces=hadd+tlim*hcpice*zfrez
          gzero(i)=gzero(i)-hexces/delt
          htc(i,1)=htc(i,1)+fi(i)*(hadd-hexces)/delt
          tsnow(i)=(tsnow(i)*hcpsno(i)*zsnow(i)+ &
                       tlim*hcpice*zfrez) &
                       /(hcpsno(i)*zsnow(i)+hcpice*zfrez)
        else
          tsnow(i)=(tsnow(i)*hcpsno(i)*zsnow(i)+ttest*hcpice* &
                        zfrez)/(hcpsno(i)*zsnow(i)+hcpice*zfrez)
          htc(i,1)=htc(i,1)+fi(i)*hadd/delt
        end if
        if (.not.(zsnow(i)>0.0)) albsno(i)=0.50
        rhosno(i)=(rhosno(i)*zsnow(i)+rhoice*zfrez)/(zsnow(i)+ &
                     zfrez)
        zsnow(i)=zsnow(i)+zfrez
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                     (rhow*zsnow(i))
        zpond(i)=0.0
        tpond(i)=0.0
      end if
      htc (i,1)=htc (i,1)+fi(i)*hcpw*(tpond(i)+tfrez)* &
                  zpond(i)/delt
      hmfg(i,1)=hmfg(i,1)-fi(i)*clhmlt*rhoice*zfrez/delt
      wtrs(i)=wtrs(i)+fi(i)*zfrez*rhoice/delt
      wtrg(i)=wtrg(i)-fi(i)*zfrez*rhoice/delt
      htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)*zsnow(i)/ &
                delt
    end if
    if (fi(i)>0. .and.isand(i,1)>-4) then
      htc (i,1)=htc (i,1)-fi(i)*hcpw*(tpond(i)+tfrez)* &
                  zpond(i)/delt
    end if
  end do ! loop 100
  !
  return
end subroutine tfreez
