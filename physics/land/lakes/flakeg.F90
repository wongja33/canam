!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine flakeg (snogat,  gtgat,   angat,   rhongat, tngat, &
                   refgat,  bcsngat, gcgat, &
                   fsvgat,  fsigat,  fdlgat,  fsggat,  flggat, &
                   hlakgat, llakgat, blakgat, &
                   radjgat, lticgat, ltsngat, ldmxgat, ltavgat, &
                   ltmxgat, ltwbgat, lshpgat, lzicgat, lzsngat, &
                   snowgat, tagat,   ulgat,   vlgat,   presgat, &
                   qagat,   thlgat, &
                   vmodgat, gustgat, rhoagat, gtagat,  cszgat, &
                   zrfmgat, zrfhgat, zdmgat,  zdhgat, &
                   depbgat, rpregat, spcpgat, rhsigat, pregat, &
                   fsdbgat, fsfbgat, csdbgat, csfbgat, &
                   ikmos,   jkmos, &
                   nmk,nl,nm,ilg,nbs, &
                   snorot,  gtrot,   anrot,   rhonrot, tnrot, &
                   refrot,  bcsnrot, gcrot, &
                   fsvrot,  fsirot,  fdlrot,  fsgrot,  flgrot, &
                   hlakrow, llakrow, blakrow, &
                   radjrow, lticrow, ltsnrow, ldmxrow, ltavrow, &
                   ltmxrow, ltwbrow, lshprow, lzicrow, lzsnrow, &
                   snowrow, tarow,   ulrow,   vlrow,   presrow, &
                   qarow,   thlrow, &
                   vmodl,   gustrol, rhoarow, gtarow,  cszrow, &
                   zrfmrow, zrfhrow, zdmrow,  zdhrow, &
                   depbrow, rprerow, spcprow, rhsirow, prerow, &
                   fsdbrot, fsfbrot, csdbrot, csfbrot)
  !
  !     * dec 10, 2016 - m.lazare. new version called by sfcproc2
  !     *                          in new model version gcm19. this
  !     *                          is modelled on "oceang", with:
  !     *                          - nbs and tiled radiation removed.
  !     *                          - wrka,wrkb,cldt,csz removed.
  !     *                          - fsv,fsi,fdl,hlak,llak,blak added.
  !     *                          - radj,ltic,ltsn,ldmx,ltav,
  !     *                            ltmx,ltwb,lshp,lzic,lzsn added.
  !     *                          - rhoa,rpre added.
  !--------------------------------------------------------------------
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in)  :: nmk !< Variable description\f$[units]\f$
  integer, intent(in)  :: nl !< Variable description\f$[units]\f$
  integer, intent(in)  :: nm !< Variable description\f$[units]\f$
  integer, intent(in)  :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)  :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  integer  :: k !< Variable description\f$[units]\f$
  integer  :: ib !< Variable description\f$[units]\f$
  !
  !     * ocean prognostic variables.
  !
  real, intent(in), dimension(nl,nm) :: snorot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: gtrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: anrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: rhonrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: tnrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: refrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: bcsnrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: gcrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsvrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsirot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fdlrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsgrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: flgrot !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg)   :: snogat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: gtgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: angat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: rhongat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: tngat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: refgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: bcsngat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: gcgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: fsvgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: fsigat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: fdlgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: fsggat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg)   :: flggat !< Variable description\f$[units]\f$

  real, intent(in), dimension(nl) :: hlakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: llakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: blakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: radjrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: lticrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ltsnrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ldmxrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ltavrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ltmxrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ltwbrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: lshprow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: lzicrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: lzsnrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: snowrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: tarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ulrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: vlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: presrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: qarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: thlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: vmodl !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: gustrol !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rhoarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: gtarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: cszrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zrfmrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zrfhrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zdmrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zdhrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: depbrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rprerow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: spcprow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rhsirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: prerow !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg) :: hlakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: llakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: blakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: radjgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: lticgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ltsngat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ldmxgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ltavgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ltmxgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ltwbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: lshpgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: lzicgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: lzsngat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: snowgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ulgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: vlgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: presgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: thlgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: vmodgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: gustgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhoagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: gtagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: cszgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zrfmgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zrfhgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zdmgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zdhgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: depbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rpregat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: spcpgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhsigat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: pregat !< Variable description\f$[units]\f$

  real, intent(in), dimension(nl,nm,nbs) :: fsdbrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: fsfbrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: csdbrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: csfbrot !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg,nbs)   :: fsdbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nbs)   :: fsfbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nbs)   :: csdbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nbs)   :: csfbgat !< Variable description\f$[units]\f$
  !
  !     * gather-scatter index arrays.
  !
  integer, intent(in), dimension(ilg) :: ikmos !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: jkmos !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--------------------------------------------------------------------
  do k = 1,nmk
    snogat (k) = snorot (ikmos(k),jkmos(k))
    gtgat  (k) = gtrot  (ikmos(k),jkmos(k))
    angat  (k) = anrot  (ikmos(k),jkmos(k))
    rhongat(k) = rhonrot(ikmos(k),jkmos(k))
    tngat  (k) = tnrot  (ikmos(k),jkmos(k))
    refgat (k) = refrot (ikmos(k),jkmos(k))
    bcsngat(k) = bcsnrot(ikmos(k),jkmos(k))
    gcgat  (k) = gcrot  (ikmos(k),jkmos(k))
    fsvgat (k) = fsvrot (ikmos(k),jkmos(k))
    fsigat (k) = fsirot (ikmos(k),jkmos(k))
    fdlgat (k) = fdlrot (ikmos(k),jkmos(k))
    fsggat (k) = fsgrot (ikmos(k),jkmos(k))
    flggat (k) = flgrot (ikmos(k),jkmos(k))
  end do ! loop 100
  !
  do k = 1,nmk
    hlakgat(k) = hlakrow(ikmos(k))
    llakgat(k) = llakrow(ikmos(k))
    blakgat(k) = blakrow(ikmos(k))
    radjgat(k) = radjrow(ikmos(k))
    lticgat(k) = lticrow(ikmos(k))
    ltsngat(k) = ltsnrow(ikmos(k))
    ldmxgat(k) = ldmxrow(ikmos(k))
    ltavgat(k) = ltavrow(ikmos(k))
    ltmxgat(k) = ltmxrow(ikmos(k))
    ltwbgat(k) = ltwbrow(ikmos(k))
    lshpgat(k) = lshprow(ikmos(k))
    lzicgat(k) = lzicrow(ikmos(k))
    lzsngat(k) = lzsnrow(ikmos(k))
    snowgat(k) = snowrow(ikmos(k))
    tagat  (k) = tarow  (ikmos(k))
    ulgat  (k) = ulrow  (ikmos(k))
    vlgat  (k) = vlrow  (ikmos(k))
    presgat(k) = presrow(ikmos(k))
    qagat  (k) = qarow  (ikmos(k))
    thlgat (k) = thlrow (ikmos(k))
    vmodgat(k) = vmodl  (ikmos(k))
    gustgat(k) = gustrol(ikmos(k))
    rhoagat(k) = rhoarow(ikmos(k))
    gtagat (k) = gtarow (ikmos(k))
    cszgat (k) = cszrow (ikmos(k))
    zrfmgat(k) = zrfmrow(ikmos(k))
    zrfhgat(k) = zrfhrow(ikmos(k))
    zdmgat (k) = zdmrow (ikmos(k))
    zdhgat (k) = zdhrow (ikmos(k))
    depbgat(k) = depbrow(ikmos(k))
    rpregat(k) = rprerow(ikmos(k))
    spcpgat(k) = spcprow(ikmos(k))
    rhsigat(k) = rhsirow(ikmos(k))
    pregat (k) = prerow (ikmos(k))
  end do ! loop 200
  !
  do ib = 1,nbs
    do k = 1,nmk
      fsdbgat (k,ib) = fsdbrot (ikmos(k),jkmos(k),ib)
      fsfbgat (k,ib) = fsfbrot (ikmos(k),jkmos(k),ib)
      csdbgat (k,ib) = csdbrot (ikmos(k),jkmos(k),ib)
      csfbgat (k,ib) = csfbrot (ikmos(k),jkmos(k),ib)
    end do
  end do ! loop 300
  !--------------------------------------------------------------------
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
