!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
  subroutine read_ghg_timeseries_gem(F_iu, mynode)

    ! Purpose:
    !
    !-----------------------------------------------------------
    ! Read in the greenhouse gas time-series file selected.
    !-----------------------------------------------------------

    ! Method:
    !
    !-----------------------------------------------------------
    ! Read in all data from the specified file.  The expectation
    ! is that the following data will be in the file in the
    ! following order with the following variable names.
    !
    ! It is also assumed that the time-series are "padded" with an
    ! extra points at the beginning and end so the values can be
    ! interpolated.
    !
    ! YRFR - Years associated with each time-series as year fractions
    !        i.e., 1990.5
    !  CO2 - Carbon dioxide
    !  CH4 - Methane
    !  N2O - Nitrous Oxide
    ! CE11 - Effective CFC-11
    ! CF11 - CFC-11
    ! CF12 - CFC-12
    ! F113 - CFC-113
    ! F114 - CFC-114
    !-----------------------------------------------------------

    use ghgts_defs

    implicit none

    integer, intent(in) :: F_iu     !> Input FST file unit
    integer, intent(in) :: mynode

    integer :: ier,ni,nj,nk
    integer, external :: fstlir, fstopc

    !.... Read data from albedo lookup tables
    if (mynode==0) then
      write(6,*)'GEM:GHGTS: Initializing ghg from a file '
    endif

    ier = fstopc('MSGLVL', 'WARNIN', 0)

    !.... Read in the time
    ghgts_time(:) = 0.0
    ier = fstlir(ghgts_time,F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','YRFR')
    if (ier<0 ) call xit('GEM:GHGTS',-2)
    if (ni > max_ghg_scn) then
       write(6,*) &
         'INCREASE THE VALUE OF MAX_GHG_SCN IN MODULE GHGTS2_DEFS', &
         ' TO AT LEAST ', ni
       call xit('GEM:GHGTS',-3)
    endif

    !.... Set the number of timesteps to IBUF(5) for all gases
    !.... Note that "f11" will cover CFC-11 and effective CFC-11
    n_co2_scn  = nj
    n_ch4_scn  = nj
    n_n2o_scn  = nj
    n_f11_scn  = nj
    n_f12_scn  = nj
    n_f113_scn = nj
    n_f114_scn = nj

    !.... Now read in the gases from the file

    ! CO2
    ghgts_co2(:) = 0.0
    ier = fstlir(ghgts_co2, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','CO2')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! CH4
    ghgts_ch4(:) = 0.0
    ier = fstlir(ghgts_ch4, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','CH4')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! N2O
    ghgts_n2o(:) = 0.0
    ier = fstlir(ghgts_n2o, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','N2O')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! CE11
    ghgts_e11(:) = 0.0
    ier = fstlir(ghgts_e11, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','CE11')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! CF11
    ghgts_f11(:) = 0.0
    ier = fstlir(ghgts_f11, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','CF11')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! CF12
    ghgts_f12(:) = 0.0
    ier = fstlir(ghgts_f12, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','CF12')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! F113
    ghgts_f113(:) = 0.0
    ier = fstlir(ghgts_f113, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','F113')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    ! F114
    ghgts_f114(:) = 0.0
    ier = fstlir(ghgts_f114, F_iu,ni,nj,nk,-1,'GHG',-1,-1,-1,' ','F114')
    if (ier < 0) call xit('GEM:GHGTS',-5)

    return

  end subroutine read_ghg_timeseries_gem
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
