subroutine ctem_init_gem(c_clim_time,lndcvryr2, &
                         month,iyear,iday, lndcvrmod,lndcvr_offset)
  !
  implicit none
  !
  !     * output fields:
  !
  real, intent(out), dimension(3,2) :: c_clim_time !<
  integer, intent(out) :: lndcvryr2 !<
  !
  !     * input variables.
  !
  integer, intent(in) :: month ! extended time month     (1, 2, 3...  12)
  integer, intent(in) :: iyear ! compressed time year    (... -2, -1, 0, 1, 2....)
  integer, intent(in) :: iday ! compressed time day     (1, 2, 3...   364, 365)
  !
  integer, intent(in) :: lndcvrmod !<
  integer, intent(in) :: lndcvr_offset !<
  !
  !     * internal.
  !
  integer :: day1 !<
  integer :: day2 !<
  integer :: i !<
  integer :: monthl1 !<
  integer :: monthl2 !<
  integer, dimension(12), parameter :: mid_month_days  = &!<
                     [16,46,75,106,136,167,197,228,259,289,320,350]
  !      data mid_month_days /15,46,74,105,135,166,196,227,258,288,319,
  !     +349/
  !
  !-----------------------------------------------------------------------------
  if (lndcvrmod==12345) then
    lndcvryr2 = iyear + lndcvr_offset ! year for which we want to get the land cover

    monthl1=0
    monthl2=0
    do i=1,12
      if (mid_month_days(i)>iday) then
        monthl2=i
        monthl1=i-1
        exit
      end if
    end do
    if (monthl2==0) then
      monthl1=12
      monthl2=1
      c_clim_time(1,1)=real(lndcvryr2)
      c_clim_time(2,1)=real(mid_month_days(monthl1))
      c_clim_time(3,1)=0.0
      c_clim_time(1,2)=real(lndcvryr2+1)
      c_clim_time(2,2)=real(mid_month_days(monthl2))
      c_clim_time(3,2)=0.0
    else if (monthl2==1) then
      monthl1=12
      c_clim_time(1,1)=real(lndcvryr2-1)
      c_clim_time(2,1)=real(mid_month_days(monthl1))
      c_clim_time(3,1)=0.0
      c_clim_time(1,2)=real(lndcvryr2)
      c_clim_time(2,2)=real(mid_month_days(monthl2))
      c_clim_time(3,2)=0.0
    else
      c_clim_time(1,1)=real(lndcvryr2)
      c_clim_time(2,1)=real(mid_month_days(monthl1))
      c_clim_time(3,1)=0.0
      c_clim_time(1,2)=real(lndcvryr2)
      c_clim_time(2,2)=real(mid_month_days(monthl2))
      c_clim_time(3,2)=0.
    end if

    !
  else
    !
    !        *  get land cover for the particular year, lndcvrmod. since land
    !        *  cover data are at monthly resolution we get the values for the
    !        *  1st of july for this year
    !
    lndcvryr2 = lndcvrmod
    !
    !        * however, for interpolation using warren's subroutine i do need
    !        * c_clim_time so lets set them as the start and end of the given
    !        * year
    !
    c_clim_time(1,1)=real(lndcvryr2)
    c_clim_time(2,1)=1.0
    c_clim_time(3,1)=0.0
    !
    c_clim_time(1,2)=real(lndcvryr2+1)
    c_clim_time(2,2)=1.0
    c_clim_time(3,2)=0.0

  end if

  return
end subroutine ctem_init_gem
