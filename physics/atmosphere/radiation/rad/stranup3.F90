!> \file
!>\brief Compute the upward solar flux from model top to 1 hPa
!!
!! @author Jiangnan Li
!
subroutine stranup3(refl, itile, dp, dt, o3, o2, ib, ig, lev1, &
                    il1, il2, ilg, lay, lev, ntile)
  !
  !     * jun 02,2015 - m.lazare/ new version for gcm19:
  !     *               j.cole:   - add tiled radiation calculations
  !     *                           (ie "reflt"), under control of
  !     *                           "itilrad".
  !     * feb 09,2009 - j.li.     previous version stranup3 for gcm15h
  !     *                         through gcm18:
  !     *                         - o2 passed directly, thus no need
  !     *                           for "trace" common block.
  !     * apr 21,2008 - l.solheim/ previous version stranup2 for gcm15g:
  !     *               j.li.      - cosmetic change to add threadprivate
  !     *                            for common block "trace", in support
  !     *                            of "radforce" model option.
  !     *                          - more accuate treatment of o3 .
  !     * apr 25,2003 - j.li. previous version stranup for gcm15e/f.
  !
  !----------------------------------------------------------------------c
  !     calculation of the upward solar flux above 1 mb, no scattering   c
  !     effect is considered                                             c
  !                                                                      c
  !     refl:  reflectivity                                              c
  !     tau:   optical depth                                             c
  !     dtr:   direct transmission function                              c
  !     1.6487213 diffusivity factor (li, 2000 jas p753)                 c
  !----------------------------------------------------------------------c
  !
  implicit none
  real :: cs1o21
  real, dimension(3,6) :: cs1o3
  real :: dto3
  real :: dtr
  real, dimension(6) :: gws1
  integer :: i
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: kp1
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer :: lev1m1
  integer :: m
  integer, intent(in) :: ntile  !< Number of surface tiles in an atmospheric column \f$[unitless]\f$
  real :: tau
  !
  real, intent(inout), dimension(ilg,ntile,2,lev) :: refl !< Reflectivity \f$[W/m^2]\f$
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< Layer temperature - 250 K \f$[K]\f$
  real, intent(in), dimension(ilg,lay) :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: o2 !< O2 mixing ratio \f$[gram/gram]\f$
  integer, intent(in), dimension(ilg,ntile) :: itile !< Surface tile number \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  common /bands1/ gws1, cs1o3, cs1o21
  !=======================================================================
  !
  lev1m1 =  lev1 - 1
  !
  if (ib == 1) then
    !
    if (ig == 1) then
      do k = lev1m1, 1, - 1
        kp1 = k + 1
        do m = 1, ntile
          do i = il1, il2
            if (itile(i,m) > 0) then
              dto3        =  dt(i,k) + 23.13
              tau         = ((cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                            dto3 * cs1o3(3,ig))) * o3(i,k) + &
                            cs1o21 * o2(i,k)) * dp(i,k)
              dtr         =  exp( - 1.6487213 * tau)
              refl(i,m,1,k) =  refl(i,m,1,kp1) * dtr
              refl(i,m,2,k) =  refl(i,m,2,kp1) * dtr
            end if
          end do ! loop 100
        end do ! loop 101
      end do ! loop 105
      !
    else
      do k = lev1m1, 1, - 1
        kp1 = k + 1
        do m = 1, ntile
          do i = il1, il2
            if (itile(i,m) > 0) then
              dto3        =  dt(i,k) + 23.13
              tau         = (cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                            dto3 * cs1o3(3,ig))) * o3(i,k) * dp(i,k)
              dtr         =  exp( - 1.6487213 * tau)
              refl(i,m,1,k) =  refl(i,m,1,kp1) * dtr
              refl(i,m,2,k) =  refl(i,m,2,kp1) * dtr
            end if
          end do ! loop 110
        end do ! loop 111
      end do ! loop 115
      !
    end if
    !
  else
    !
    do k = lev1m1, 1, - 1
      kp1 = k + 1
      do m = 1, ntile
        do i = il1, il2
          if (itile(i,m) > 0) then
            refl(i,m,1,k) =  refl(i,m,1,kp1)
            refl(i,m,2,k) =  refl(i,m,2,kp1)
          end if
        end do ! loop 200
      end do ! loop 201
    end do ! loop 250
    !
  end if
  !
  return
end
!> \file
!> Calculation of the upward solar flux above 1 hPa. At such high region the 
!! air is very thin and the scattering effect is very weak.
