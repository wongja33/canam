#include "cppdef_config.h"

!>\file
!>\brief brief description of the routine purpose.
!!
!! @author routine author name(s)
!

  ! * Section 1. Input for initial start of the model, i.e. kount=0.
  ! *            Get grid physics from nugg and spectral fields from nusp.

  call getgg14(nugg,tgpat,thlqpat,thicpat, &
               sicnpak,sicpak,gtpak,snopak, &
               luinpak,luimpak,lrinpak,lrimpak, &
               envpak,gampak,psipak,alphpak,deltpak,sigxpak, &
               phispak, &
               sandpat,claypat,orgmpat,dpthpat,drnpat,socipat, &
               fcanpat,alvcpat,alicpat,lnz0pat, &
               lamxpat,lamnpat,cmaspat,rootpat, &
               hlakpak,llakpak,blakpak, &
               flkupak,flkrpak,gicnpak,flndpak, &
               lc,lg,lct,lgt,ican,icanp1,ignd,ntld,ijpak, &
               lon1,nlat,iday,gll)
  !
  call getnae (nuch,escvpak,ehcvpak,esevpak,ehevpak, &
               ijpak,lon1,nlat,gll)
  call getdue (nuch,spotpak,st01pak,st02pak, &
               st03pak,st04pak,st06pak,st13pak, &
               st14pak,st15pak,st16pak,st17pak, &
               ijpak,lon1,nlat,gll)

  if (switch_gas_chem) then
    allocate (ssadi(levssad*nlat), ssadm(levssad*nlat))
    call getssad (ssadpak, ssadpal, ssadi, ssadm, lsad, kount,  &
                  nlat, lonsl, ilat, ijpak, levssad, nussad)
    deallocate (ssadi, ssadm)

    call getsfsa (sfsapak, sfsapal, lon1, nlat, ijpak, &
                  iday, kount, nuso4, gll, ngll)
  endif
  !
  ! * Get lday value.
  !
  call getlday(lday,iday)

  call getspex (nusp,phis,ps, t ,p,c,es,la,lm,ilev,levspec, &
                ls,lh,idiv,latotal,lmtotal,lsrtotal,gll)
  !
  ! * Initialize tracer fields about to be read.
  ! * Then read from start file if not using surface input.
  !
  call putzero(trac,la*2*ilev*ntraca)
  call putzero(tracna,ip0j*ilev*ntracn*2)
  if (itrinit /= 0) then
    if (ntraca > 0)  then
      call getrac4 (nusp,trac,la,lm,ilev,lh,itrac,ntrac,ntraca, &
                    indxa,latotal,lmtotal,lsrtotal,gll)
    endif
    !
    if (ntracn > 0) then
      call getracna3 (nusp,tracna,ilev,lh,itrac,ntrac,ntracn, &
                      indxna,ip0j,lon1,nlat,gll)
    endif
  endif
  !
  if (ntracn > 0) then
    !
    ! * Copy inp time  level into inm time level
    !
    do ii = 1, ip0j*ilev*ntracn
      tracna(ii,inm) = tracna(ii,inp)
    enddo
  endif
  !
  ! * Initialize grid fields.
  !
#include "init12.h"
  !
  ! * Read specified chemistry forcing from a file at start of run.
  !
  call getmdayt(mdayt,mdayt1,iday,mon,mon1)
#include "getemip2.h"
#if defined (pla)
#if defined (pfrc)
  call getfrc (amldfpak,reamfpak,veamfpak,fr1fpak,fr2fpak, &
               ssldfpak,ressfpak,vessfpak,dsldfpak,redsfpak, &
               vedsfpak,bcldfpak,rebcfpak,vebcfpak,ocldfpak, &
               reocfpak,veocfpak,zcdnfpak,bcicfpak,bcdpfpak, &
               amldfpal,reamfpal,veamfpal,fr1fpal,fr2fpal, &
               ssldfpal,ressfpal,vessfpal,dsldfpal,redsfpal, &
               vedsfpal,bcldfpal,rebcfpal,vebcfpal,ocldfpal, &
               reocfpal,veocfpal,zcdnfpal,bcicfpal,bcdpfpal, &
               lon1,nlat,ilev,incd,iday,mdayt,mdayt1, &
               kount,ijpak,nupf,lh,gll)
#endif
#endif
!
#if defined transient_ozone_concentrations
  call getranoz (ozpak,ozpal,nc4to8("  OZ"),lo,levoz, &
                 lon1,nlat,incd,irefyro,iday,kount,ijpak, nuoz_rad,gll)
  !
  !call getranoz(o3cpak,o3cpal,nc4to8("  O3"),lochem,levozc, &
  !              lon1,nlat,incd,irefyro,iday,kount,ijpak, nuoz_chem,gll)
  !
  ! * These are zeroed out because using oxidant ozone for "ozchm".
  !
  o3cpak = 0.0
  o3cpal = 0.0
#endif
  !
  ! * Initialize t-1 spectral fields.
  !
  call ccopy (la*ilev,  p ,1,  pm  ,1)
  call ccopy (la*ilev,  c ,1,  cm  ,1)
  call ccopy (la*ilev,  t ,1,  tm  ,1)
  call ccopy (la*levs, es ,1, esm  ,1)
  call ccopy (la     , ps ,1, psm  ,1)
  call ccopy (la*ilev*ntrspec,trac,1,tracm,1)

  if (switch_gas_chem) then
    ! Read the surface emissions for the gas-phase chemistry
    call getemis (sfempak(1,1), sfempal(1,1), nusfcem, lon1, nlat, &
                  ijpak, iday, kount, -1, gll, ngll, nc4to8("COBB"))
    call getemis (sfempak(1,2), sfempal(1,2), nusfcem, lon1, nlat, &
                  ijpak, iday, kount, -1, gll, ngll, nc4to8("COSF"))
    call getemis (sfempak(1,3), sfempal(1,3), nusfcem, lon1, nlat, &
                  ijpak, iday, kount, -1, gll, ngll, nc4to8("COSH"))
    call getemis (sfempak(1,4), sfempal(1,4), nusfcem, lon1, nlat, &
                  ijpak, iday, kount, -1, gll, ngll, nc4to8("NXBB"))
    call getemis (sfempak(1,5), sfempal(1,5), nusfcem, lon1, nlat, &
                  ijpak, iday, kount, -1, gll, ngll, nc4to8("NXSF"))
    call getemis (sfempak(1,6), sfempal(1,6), nusfcem, lon1, nlat, &
                  ijpak, iday, kount, -1, gll, ngll, nc4to8("NXSH"))

    ! Read the 3-D aircraft emissions
    call getacem(acempak, acempal, lace, nuacftem, lon1, nlat, ijpak, &
                 iday, kount, gll, ngll, nc4to8("NXAC"))
  endif

!> \file
!> this is an example of adding text at the end of the routine.
!! your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! doxygen should be able to translate latex and it is possible to include
!! references using "\cite", for example, \cite vonsalzen2013.
!! equations can be included as well, as inline equations \f$ f=ma \f$,
!! or in the equation environment \n
!! (note that html will not number the equation but it will in latex),
!! \f{equation}{
!!  f_1=ma_1
!! \f}
