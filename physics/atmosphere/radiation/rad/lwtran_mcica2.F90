!> \file
!>\brief Longwave radiative transfer calculation with no subgrid-scale horizontal variability.
!!
!! @author Jiangnan Li
!
subroutine lwtran_mcica2(fut, fdt, &
                         slwf, tauc, omc, gc, fl, taual, &
                         taug, bf, bst, urbf, dbf, em0t, &
                         cld, nct, lev1, cut, maxc, &
                         itile, &
                         il1, il2, ilg, lay, lev, &
                         ntile)
  !
  !     * jun 02,2015 - m.lazare/ new version for gcm19:
  !     *               j.cole:   - add tiled radiation calculations
  !     *                           (ie "fut","fdt"), under control of
  !     *                           "itilrad".
  !     * feb 11,2009 - j.cole.  previous version lwtran_mcica2 for gcm15h
  !     *                        through gcm18:
  !     * feb 20/2007 - j. cole. previous version lwtran_mcica for gcm15g:
  !     *                        - update to match lwtran2, automatic arrays.
  !     *                        - used petri's hard work and modified to
  !     *                          suit my version. no longer has subcolumns,
  !     *                          works on only one subcolumn per gcm
  !     *                          column.
  !     * may 31/2004 - p.raisanen:
  !
  !     this code version has been updated for mcica radiation calculations
  !
  !       - clear-sky fluxes are calculated using mean profiles for the gcm
  !         column, but cloudy-sky fluxes are calculated separately
  !         for nsub subcolumns and then averaged. it is assumed that
  !         there are equally many subcolumns for each gcm column il1...il2.
  !
  !       - treatment of cloud overlap and horizontal variability eliminated.
  !         cloud fraction for subcolumns (cldsub) is still included as
  !         input, but the code assumes it is either 0 or 1.
  !
  !
  !     * feb 04,2004 - j.li, m.lazare.
  !----------------------------------------------------------------------c
  !     calculation of longwave radiative transfer using absorption      c
  !     approximation, but including corrections for scattering          c
  !     (unperturbed + backward scattering effect +  forward scattering  c
  !      effect + internal scattering effect  (li and fu, jas 2000)      c
  !----------------------------------------------------------------------c
  !     fu:      upward infrared flux                                    c
  !     fd:      downward infrared flux                                  c
  !    fut:      tiled upward   infrared flux                            c
  !    fdt:      tiled downward infrared flux                            c
  !     slwf:    input solar flux at model top level for each band       c
  !     taucsub: cloud subcolumn optical depth for the infrared          c
  !     omcsub:  cloud subcolumn single scattering albedo times optical  c
  !              depth                                                   c
  !     gcsub:   cloud subcolumn asymmetry factor times omci             c
  !     flsub:   square of cloud subcolumn asymmetry factor              c
  !     taual:   aerosol optical depth for the infrared                  c
  !     taug:    gaseous optical depth for the infrared                  c
  !     bf:      blackbody intensity integrated over each band at each   c
  !              level in units w / m^2 / sr. therefor a pi factor neededc
  !              for flux                                                c
  !     bst:     the blackbody intensity for each tiled surface.         c
  !     urbf:    u times the difference of log(bf) for two neighbor      c
  !              levels used for exponential source function             c
  !     dbf:     difference of bf for two neighbor levels used for       c
  !              linear source function                                  c
  !     em0t:    surface emission for each tiled surface                 c
  !     cldsub:  subcolumn cloud fraction (assumed to be 0 or 1)         c
  !     scatbk:  backward scattering                                     c
  !     scatfw:  forward scattering                                      c
  !     scatsm:  internal scattering                                     c
  !     taum:    taum(1) a factor related to tau in zeta factor for      c
  !              linear source term; taum(2) the cumulated taum(1) for   c
  !              subgrid variability calculation                         c
  !     xu:      the emission part in the upward flux transmission       c
  !              (li, 2002 jas p3302)                                    c
  !     xd:      the emission part in the downward flux transmission     c
  !     dtr:     direct transmission                                     c
  !     nctsub:  the highest cloud top level for subcolumns              c
  !     maxc:    minimum value of nct for all columns and subcolumns     c
  !     nsub:    number of subcolumns per gcm column                     c
  !     nsubmx:  maximum number of subcolumns per gcm column             c
  !----------------------------------------------------------------------c
  implicit none
  real :: cow
  real, intent(in) :: cut
  real :: dtr2
  real :: embk
  real :: epsd
  real :: epsu
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: km1
  integer :: kp1
  integer :: l1
  integer :: l2
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer :: m
  integer, intent(in) :: maxc
  integer, intent(in) :: ntile  !< Number of surface tiles in an atmospheric column \f$[unitless]\f$
  real :: rtaul1
  real :: ru
  real :: sf
  real :: ssalb
  real :: tau2
  real :: taudtr
  real :: taul1
  real :: taul2
  real :: ubeta
  real :: w
  real :: wgrcow
  real :: x
  real :: zeta
  !
  real, intent(inout), dimension(ilg,ntile,2,lev) :: fut !< Tiled upward infrared flux \f$[W/m^2]\f$
  real, intent(inout), dimension(ilg,ntile,2,lev) :: fdt !< Tiled downward infrared flux \f$[W/m^2]\f$
  real, intent(in), dimension(ilg) :: slwf !< Input solar flux at model top level \f$[W/m^2]\f$
  real, intent(in), dimension(ilg,lay) :: tauc !< Cloud optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: omc !< Cloud single sacttering albedo \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: gc !< Cloud symmetry factor \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: fl !< Cloud optical depth scaling factor \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: taual !< Aerosol optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: taug !< Gaseous optical depth \f$[1]\f$
  real, intent(in), dimension(ilg,lev) :: bf !< Blackbody intensity integrated over each
  real, intent(in), dimension(ilg,lay) :: urbf !< Diffuse factor times the difference of
  real, intent(in), dimension(ilg,lay) :: dbf !< Difference of bf for two neighbor levels
  real, intent(in), dimension(ilg,lay) :: cld !< Cloud fraction \f$[1]\f$
  real, intent(in), dimension(ilg,ntile) :: bst !< Tiled blackbody intensity integrated over
  real, intent(in), dimension(ilg,ntile) :: em0t !< Tiled surface emisivity \f$[1]\f$
  integer, intent(in), dimension(ilg,ntile) :: itile !< Surface tile number \f$[1]\f$
  real, dimension(ilg,2,lay) :: scatsm
  real, dimension(ilg,2,lay) :: taum
  real, dimension(ilg,2,lay) :: xd
  real, dimension(ilg,2,lay) :: xu
  real, dimension(ilg,2,lay) :: dtr
  integer, intent(in), dimension(ilg) :: nct !< Highest cloud level \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work arrays.
  !
  real, dimension(ilg,lay) :: emisw
  real, dimension(ilg,lay) :: scatbk
  real, dimension(ilg,lay) :: scatfw
  real, dimension(ilg,ntile) :: embst
  real, dimension(ilg,ntile) :: abse0t
  real, dimension(ilg,2,lev) :: fd
  real, dimension(ilg,2,lev) :: fu
  !
  data  ru / 1.6487213 /
  !=======================================================================
  !
  !-------------------
  ! initializations
  !-------------------
  !
  l1 =  lev1
  l2 =  lev1 + 1

  do k = l1, lev
    do i = il1,il2
      fu(i,2,k) = 0.
      fd(i,2,k) = 0.
    end do ! loop 20
  end do ! loop 10
  do k = lev1,lev
    do m = 1, ntile
      do i = il1, il2
        fdt(i,m,1,k) = 0.0
        fut(i,m,1,k) = 0.0
        fdt(i,m,2,k) = 0.0
        fut(i,m,2,k) = 0.0
      end do ! i
    end do ! m
  end do ! k

  do m = 1, ntile
    do i = il1, il2
      if (itile(i,m) > 0) then
        embst(i,m)  = em0t(i,m) * bst(i,m)
        abse0t(i,m) = 1.0 - em0t(i,m)
      end if
    end do ! i
  end do ! m
  !
  !----------------------------------------------------------------------c
  !     calculate the downward fluxes first without scattering           c
  !     combine the optical properties for the infrared,                 c
  !     1, aerosol + gas; 2, cloud + aerosol + gas.                      c
  !     fd (fu) is down (upward) flux.                                   c
  !     gaussian integration and diffusivity factor, ru (li jas 2000)    c
  !     above maxc, exponential source function is used                  c
  !     below maxc, linear source function is used                       c
  !----------------------------------------------------------------------c
  !
  !----------------------------------------------------------------------c
  ! computation of clear-sky downward fluxes using exponential source    c
  ! function                                                             c
  !----------------------------------------------------------------------c

  l1 =  lev1
  l2 =  lev1 + 1

  do i = il1, il2
    fd(i,1,lev1) =  slwf(i)
  end do ! loop 100
  !
  do k = l2, lev
    km1 = k - 1
    do i = il1, il2
      taul1                   =  taual(i,km1) + taug(i,km1)
      rtaul1                  =  taul1 * ru
      dtr(i,1,km1)            =  exp ( - rtaul1)
      ubeta                   =  urbf(i,km1) / (taul1 + 1.e-20)
      epsd                    =  ubeta + 1.0
      epsu                    =  ubeta - 1.0
      !
      if (abs(epsd) > 0.001) then
        xd(i,1,km1)          = (bf(i,k) - bf(i,km1) * &
                               dtr(i,1,km1)) / epsd
      else
        xd(i,1,km1)          =  rtaul1 * bf(i,km1) * dtr(i,1,km1)
      end if
      !
      if (abs(epsu) > 0.001) then
        xu(i,1,km1)           = (bf(i,k) * dtr(i,1,km1) - &
                                bf(i,km1)) / epsu
      else
        xu(i,1,km1)           =  rtaul1 * bf(i,k) * dtr(i,1,km1)
      end if
      !
      fd(i,1,k)               =  fd(i,1,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
    end do ! loop 120
  end do ! loop 110
  do k = lev1,lev
    do m = 1, ntile
      do i = il1, il2
        if (itile(i,m) > 0) then
          fdt(i,m,1,k) = fd(i,1,k)
        end if
      end do ! i
    end do ! m
  end do ! k

  !----------------------------------------------------------------------c
  ! computation of clear-sky upward fluxes                               c
  !----------------------------------------------------------------------c

  do m = 1, ntile
    do i = il1,il2
      if (itile(i,m) > 0) then
        fut(i,m,1,lev) = embst(i,m) + abse0t(i,m) * fdt(i,m,1,lev)
      end if
    end do ! i
  end do ! m

  do k = lev - 1, l1, - 1
    kp1 = k + 1
    do m = 1, ntile
      do i = il1,il2
        if (itile(i,m) > 0) then
          fut(i,m,1,k)     = fut(i,m,1,kp1) * dtr(i,1,k) + &
                             xu(i,1,k)
        end if
      end do ! i
    end do ! m
  end do ! k

  !----------------------------------------------------------------------c
  ! all-sky downward fluxes above the highest cloud top                  c
  !----------------------------------------------------------------------c

  do i = il1,il2
    fd(i,2,l1)               = slwf(i)
  end do ! loop 400

  do k = l2, lev
    do i = il1,il2
      if (k <= nct(i)) then
        fd(i,2,k)            = fd(i,1,k)
      end if
    end do ! loop 420
  end do ! loop 410

  !----------------------------------------------------------------------c
  !     add the layers downward from the highest cloud layer to the      c
  !     surface. determine the xu for the upward path.                   c
  !     using exponential source function also for all sky flux in cloud c
  !     free layers.                                                     c
  !----------------------------------------------------------------------c
  !
  do k = maxc + 1, lev
    km1 = k - 1
    do i = il1, il2
      if (k > nct(i)) then
        if (cld(i,km1) < cut) then
          fd(i,2,k)           =  fd(i,2,km1) * dtr(i,1,km1) + &
                                xd(i,1,km1)
        else
          taul2               =  tauc(i,km1) + taug(i,km1)
          ssalb               =  omc(i,km1) / taul2
          sf                  =  ssalb * fl(i,km1)
          w                   = (ssalb - sf) / (1.0 - sf)
          cow                 =  1.0 - w
          taum(i,1,km1)       = (cow * taul2 * (1.0 - sf) + &
                                taual(i,km1)) * ru
          zeta                =  dbf(i,km1) / taum(i,1,km1)
          tau2                =  taum(i,1,km1) + taum(i,1,km1)
          !
          dtr(i,2,km1)        =  exp( - taum(i,1,km1))
          dtr2                =  exp( - tau2)
          emisw(i,km1)        =  zeta * (1.0 - dtr(i,2,km1))
          embk                =  dtr2 - 1.0
          !
          xd(i,2,km1)         =  bf(i,k) - bf(i,km1) * &
                                dtr(i,2,km1) - emisw(i,km1)
          xu(i,2,km1)         =  bf(i,km1) - bf(i,k) * &
                                dtr(i,2,km1) + emisw(i,km1)
          !
          wgrcow              =  w * gc(i,km1) / cow
          taudtr              =  taum(i,1,km1) * dtr(i,2,km1)
          scatfw(i,km1)       =  wgrcow * taudtr
          scatbk(i,km1)       =  0.5 * wgrcow * (dtr2 - 1.0)
          !
          x                   =  wgrcow * (2.0 * emisw(i,km1) + &
                                (0.5 * embk - taudtr) * zeta)
          scatsm(i,1,km1)     =  - scatbk(i,km1) * bf(i,k) - &
                                scatfw(i,km1) * bf(i,km1) - x
          scatsm(i,2,km1)     =  - scatbk(i,km1) * bf(i,km1) - &
                                scatfw(i,km1) * bf(i,k) + x
          fd(i,2,k)           =  fd(i,2,km1) * dtr(i,2,km1) + &
                                xd(i,2,km1)
        end if
      end if
    end do ! loop 440
  end do ! loop 430
  do k = l1,lev
    do m = 1, ntile
      do i = il1, il2
        if (itile(i,m) > 0) then
          fdt(i,m,2,k) = fd(i,2,k)
        end if
      end do ! i
    end do ! m
  end do ! k

  !----------------------------------------------------------------------c
  ! computation of all-sky upward fluxes                                 c
  !----------------------------------------------------------------------c
  ! initialization for surface                                           c
  !----------------------------------------------------------------------c
  k = lev - 1
  do m = 1, ntile
    do i = il1, il2
      if (itile(i,m) > 0) then
        fut(i,m,2,lev) = embst(i,m) + abse0t(i,m) * fdt(i,m,2,lev)
      end if
    end do ! i
  end do ! m

  !----------------------------------------------------------------------c
  !     add the layers upward from the first layer to maxc               c
  !     scattering effect for upward path is included                    c
  !----------------------------------------------------------------------c
  !
  do k = lev - 1, maxc, - 1
    kp1 = k + 1
    do m = 1, ntile
      do i = il1, il2
        if (itile(i,m) > 0) then
          if (k >= nct(i)) then
            if (cld(i,k) < cut) then
              fut(i,m,2,k) =  fut(i,m,2,kp1) * dtr(i,1,k) + &
                             xu(i,1,k)
            else
              fut(i,m,2,k) =  fut(i,m,2,kp1) * &
                             (dtr(i,2,k) + scatfw(i,k)) + &
                             fdt(i,m,2,k) * scatbk(i,k) + &
                             scatsm(i,2,k) + xu(i,2,k)
            end if
          end if
        end if
      end do ! i
    end do ! m
  end do ! k
  !
  !----------------------------------------------------------------------c
  !     add the layers upward above the highest cloud to the toa, no     c
  !     scattering                                                       c
  !----------------------------------------------------------------------c
  !
  do k = lev - 1, l1, - 1
    kp1 = k + 1
    do m = 1, ntile
      do i = il1, il2
        if (itile(i,m) > 0) then
          if (kp1 <= nct(i)) then
            fut(i,m,2,k) = fut(i,m,2,kp1) * dtr(i,1,k) + &
                           xu(i,1,k)
          end if
        end if
      end do ! i
    end do ! m
  end do ! k
  !
  !----------------------------------------------------------------------c
  !     scattering effect for downward path in from maxc to the surface  c
  !----------------------------------------------------------------------c
  !
  do k = maxc + 1, lev
    km1 = k - 1
    do m = 1, ntile
      do i = il1, il2
        if (itile(i,m) > 0) then
          if (km1 >= nct(i)) then
            if (cld(i,km1) < cut) then
              fdt(i,m,2,k) = fdt(i,m,2,km1) * dtr(i,1,km1) + &
                             xd(i,1,km1)
            else
              fdt(i,m,2,k) = fdt(i,m,2,km1) * &
                             (dtr(i,2,km1) + scatfw(i,km1)) + &
                             fut(i,m,2,k) * scatbk(i,km1) + &
                             scatsm(i,1,km1) + xd(i,2,km1)
            end if
          end if
        end if
      end do ! i
    end do ! m
  end do ! k
  !

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
