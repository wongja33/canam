#include "cppdef_config.h"
!! --- NEW: #define with_COUPLED_
!! --- NEW: #define with_MPI_

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine xit(name,n)
  !
  !     * jul 03/12 - f.majaess.- added closure of i/o units.
  !     * jul 02/09 - f.majaess.- in the "MPI" case, allow some
  !     *                         time delay before terminating.
  !     * dec 23/08 - f.majaess.- call "MPI_ABORT" in the "MPI"
  !     *                         mode anormal termination case.
  !     * apr 24/06 - f.majaess.- limit "MPI_FINALIZE" call
  !                               under "MPI" mode to just the
  !                               normal termination case.
  !     * jan 07/05 - m.lazare. - add "COUPLED" to conditional
  !     *                         directives along with "MPI".
  !     * jan 24/04 - f.majaess. previous version xit.
  !
  !     * terminates a program by printing the program name and
  !     * a line across the page followed by a number n.
  !
  !     * n>=0 is for a normal end. the line is dashed.
  !     * normal ends terminate with   stop.
  !
  !     * n<0 is for an abnormal end. the line is dotted.
  !     * if n is less than -100 the program simply terminates.
  !     * otherwise if n is less than zero the program aborts.
  !
#if defined with_MPI_ || defined with_COUPLED_
      use mpi
#endif
  implicit none
  integer :: lename
  integer, intent(in) :: n

  character*( * ), intent(in) :: name !< Variable description\f$[units]\f$
  character*80  :: dash !<
  character*80  :: star !<
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * common block to hold the number of nodes and communicator.
  !
  integer*4 :: ierr !<
  integer*4 :: iou !<
  integer*4 :: io5 !<
  integer*4 :: io6 !<
  integer*4 :: mynode !<
  common /mpinfo/ mynode
#if defined with_MPI_ || defined with_COUPLED_
integer(4) :: nnode !<
integer(4) :: agcm_commwrld !<
integer(4) :: my_cmplx_type !<
integer(4) :: my_real_type !<
integer(4) :: my_int_type !<
integer(4) :: n4 !<
      common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type
#endif
  !
  data dash(1:40)/'----------------------------------------'/
  data dash(41:80)/'----------------------------------------'/
  data star(1:40)/'****************************************'/
  data star(41:80)/'****************************************'/
  !---------------------------------------------------------------------
  !     * in the normal termination case, avoid duplicate output
  !     * and limit it only to node=0.
  !
  if (mynode == 0 .or. (n < 0 .and. n >= - 100) ) then

    lename = len_trim(name)
    if (lename < 1) lename = 1

    if (n >= 0) write(6,6010) dash(1:8),name(1:lename), &
        dash(1:81 - lename),n
    !
    if (n < 0) write(6,6010) star(1:8),name(1:lename), &
        star(1:81 - lename),n

  end if
  if (mynode == 0) then
    io5 = 5
    io6 = 6
    print * , '0Closing data I/O units'
    do iou = 1,100
      if (iou /= io5 .and. iou /= io6) close (iou)
    end do
  end if
  call flush(6)
  !
  if (n >= 0 .or. n < - 100) then

#if defined with_MPI_ || defined with_COUPLED_
        call system('sleep 3')
        print * , '0got to before mpi_finalize'
        call flush(6)
        call mpi_finalize(ierr)
        print * , '0got to after mpi_finalize'
        call flush(6)
#endif
    stop
  else
#if defined with_MPI_ || defined with_COUPLED_
        n4 = n
        call system('sleep 3')
        call mpi_abort(agcm_commwrld,n4,ierr)
#endif
    call abort
  end if
  !
  !---------------------------------------------------------------------
  6008 format('0*** GCM RUN HAS COMPLETED SUCCESSFULLY ***')
  6010 format('0',a,'  END  ',a,1x,a,i8)
  9000 format(a4)
end subroutine xit
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
