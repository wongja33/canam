!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine vraimap (vtg,utg,ilg,lon,ilev,cosj,a)
  !
  !     * aug 20/2003 - m.lazare. convert wind tendencies from real :: to
  !     *                         "IMAGINARY" prior to conversion to
  !     *                         spectral space. simplified version of
  !     *                         vraima3 applicable for physics.
  !
  !     * convert real :: tendencies to images one (in the old tradition).
  !     * NOTE THAT IN LOOP 100, THE L.H.S.'S OF (VTG,UTG) ARE MEANT TO
  !     * correspond to old model version (putg,pvtg) to allow old model
  !     * version mhanlp4 to be used. (see appendix iii-1 of "ADIABATIC
  !     * formulation of the aes/ccc multi-level spectral general
  !     * circulation model" FOR REFERENCE).
  !
  implicit none
  real, intent(inout) :: a
  integer :: i
  integer, intent(inout) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer, intent(inout) :: lon

  real, intent(inout), dimension(ilg,ilev) :: vtg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: utg !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(ilg) :: cosj !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-------------------------------------------------------------------
  do l=1,ilev
    do i=1,lon
      vtg (i,l) =-vtg (i,l) * (cosj(i)/a)
      utg (i,l) = utg (i,l) * (cosj(i)/a)
    end do
  end do ! loop 100
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
