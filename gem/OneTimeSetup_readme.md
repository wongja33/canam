In order to compile and run CanAM_GEM, you '''must''' be using the [[HPCS/ordenv|ordenv environment]] as well as the latest version of Maestro:

1) Your ~/.profile should contain the following dual-RPN/CCCma environments (make sure ordenv is set first, before any CCCma-related stuff):
````
## environment setup - env

. /fs/ssm/eccc/mrd/ordenv/profile/1.13.0

## CCCma Bourne shell environment setup
export PATH=/home/scrd101/canesm_bin_latest:$PATH
. /home/scrd101/generic/sc_cccma_setup_profile

#export PATH=/home/scrd101/canesm_bin_latest:$PATH

````

2) Your ~/.profile.d/ directory should have "default" and "interactive" subdirectories. And within each of those, files called "post". See ~rcw001/.profile.d/* for what should appear in those. After changing any .profile* files, log out and log back in again for them to take effect.
See also: https://wiki.cmc.ec.gc.ca/wiki/User:Chamberlands/Projects/Migration_HPC_2019/AccountSetUp for further guidance.

3.1) Make sure your ~/.ssh/config contains the following text:

````
Host localhost
     UserKnownHostsFile /dev/null
     StrictHostKeyChecking no

Host gitlab.science.gc.ca
     ForwardX11 no
     User git

Host hpnls.science.gc.ca ss?nfsx.science.gc.ca
     ForwardX11 no

Host usr-svc-sci-ctn04?
     ForwardX11 yes
     ForwardX11Trusted yes

Host *.science.gc.ca
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    ForwardX11 yes

Host *.cmc.ec.gc.ca
     User armnsch
````

3.2) Subscribe to gitlab/get your gitlab credentials: Do all steps listed here (the rsa key step needs to be redone on XC50):  https://wiki.cmc.ec.gc.ca/wiki/Subscribing_To_Gitlab#Get_your_science.gc.ca_credentials


4) (Q: is this neccessary? Can the maestro stuff stay located with the code?) Make a .suites directory in your /home/ords/crd/ccrn/$USER directory. Also set up a .default_links subdirectory within your ~/.suites/

In .suites/.default_links, set up the following symbolic links:

````
 ln -s /space/hall3/work/eccc/crd/ccrn/users/${USER}/maestro banting

 ln -s /space/hall4/work/eccc/crd/ccrn/users/${USER}/maestro daley

 ln -s /space/hall3/sitestore/eccc/crd/ccrn/users/${USER}/maestro eccc-ppp3

 ln -s /space/hall4/sitestore/eccc/crd/ccrn/users/${USER}/maestro eccc-ppp4

 ln -s eccc-ppp3 ppp3

 ln -s eccc-ppp4 ppp4

````


5) Set up maestro by making sure the following is in ~/.profile.d/interactive/post file (should have been done in step 2):

````
 ## Load Maestro

 export SEQ_MAESTRO_SHORTCUT=". ssmuse-sh -d eccc/cmo/isst/maestro/1.7.1"

 ${SEQ_MAESTRO_SHORTCUT}
````

After logging out and back in again, run mserver_initSCI on the command line, with the following answers at each prompt:
- maestro2
- eccc-ppp3
- default
- default
- your email address
- default
- default
- -1

In .maestroc make sure that your console_default is set to xterm (and not konsole).

6) Set up your home directory by making a ~/raid directory, and within there, the following symbolic links:

````
 ln -s /space/hall3/sitestore/eccc/crd/ccrn/users/$USER eccc-ppp3
 ln -s /space/hall4/sitestore/eccc/crd/ccrn/users//$USER eccc-ppp4
 ln -s /space/hall3/work/eccc/crd/ccrn/users/$USER banting
 ln -s /space/hall4/work/eccc/crd/ccrn/users/$USER daley

````
Set up a symbolic link ~/data which points to ~/raid then log off and log back on.

7) Create a subdirectory called storage_model in the following directories:
````
~/data/eccc-ppp3/
~/data/eccc-ppp4
~/data/banting
~/data/daley
````
