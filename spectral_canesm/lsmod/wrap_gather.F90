#include "cppdef_config.h"
!! --- NEW: #define with_MPI_

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine wrap_gather(a_in,   sz_in,  type_in, &
                             a_out,  sz_out, type_out, &
                             master, comm,   ierr)
  !
  !     * oct 05/2003 - r.mclay.
  !
  !     * common fortran code for mpi calls in crb output routines.
  !
  implicit none
  integer*4, intent(in) :: sz_in !< Variable description\f$[units]\f$
  integer*4, intent(in) :: type_in !< Variable description\f$[units]\f$
  integer*4, intent(in) :: sz_out !< Variable description\f$[units]\f$
  integer*4, intent(in) :: type_out !< Variable description\f$[units]\f$
  integer*4, intent(in) :: comm !< Variable description\f$[units]\f$
  integer*4, intent(in) :: ierr !< Variable description\f$[units]\f$
  integer*4, intent(in) :: master !< Variable description\f$[units]\f$
  real, intent(in), dimension(sz_in) :: a_in !< Variable description\f$[units]\f$
  real, intent(in), dimension(sz_out) :: a_out !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

#ifdef with_MPI_
      call mpi_gather(a_in,   sz_in,  type_in, &
                  a_out,  sz_out, type_out, &
                  master, comm,   ierr)
#endif
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
