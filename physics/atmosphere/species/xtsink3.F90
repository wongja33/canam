!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xtsink3(xrow,pressg,dshj,shj,th,rh,shtj, &
                   ilg,il1,il2,ilev,lev,ntrac,ztmst)
  !
  !     * may 24/2017 - m.lazare. for conversion to xc40 supercomputer:
  !                               - replace ibm intrinsics by generic
  !                                 for portability.
  !     * dec 18/2007 - m.lazare. new version for gcm15g:
  !     *                         pass in "adelt" from physics as
  !     *                         ztmst and use directly, rather than
  !     *                         passing in delt and forming
  !     *                         ztmst=2.*delt inside.
  !     * nov 23/2006 - m.lazare. previous version xtsink2 for gcm15f:
  !     *                         add 32-bit calls to ibm vector libraries
  !     *                         for  the exp intrinsic (controlled by
  !     *                         the value of "machine" ("machtyp" common
  !     *                         block had to be added).
  !     * jun 19/2006 - m.lazare. new version for gcm15f:
  !     *                         - use variable instead of constant
  !     *                           in intrinsics such as "max",
  !     *                           so that can compile in 32-bit mode
  !     *                           with real(8).
  !     *                         - work arrays now local.
  !     * jan 25/2004 - m.lazare. previous version xtsink (up to gcm15e).
  !
  !     johann feichter          uni/hamburg         08/91
  !     modified  u. schlese    dkrz-hamburg        jan-95
  !
  !     * calculates change due to sedimentation for black/organic carbon,
  !     * coarse-mode dust and coarse-mode seasalt.
  !
  use phys_consts, only : grav, rgas
  use tracers_info_mod, only: ibco, ibcy, ioco, iocy, issc, iduc

  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$

  real, intent(in) :: ztmst
  !
  real, intent(inout), dimension(ilg,lev,ntrac) :: xrow !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: rh !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: th !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter, dimension(0:6) :: acoef = &
             [ .8077e+01, -.2060e+01, -.3918e+00, -.2110e+01, &
              -.2180e+01, -.7830e+00, -.9358e-01 ]

  real, parameter :: ymasna = 22.98977e-03  ! molecular weight na
  real, parameter :: ymascl = 35.453e-03    ! molecular weight cl
  real, parameter :: yrhod  = 2.165         ! density dry nacl
  real, parameter :: yrhow  = 1.            ! density h2o2
  real, parameter :: yrdry  = 1.750         ! mode radius (dry) coarse sea salt
  !
  real, parameter :: rhmax = 0.99
  !
  !==================================================================
  !
  !     * the following are internal work fields:
  !
  real :: adens
  real :: afac
  real :: afakt
  real :: aratr
  real :: atmp
  real :: cfactd
  real :: cfacts
  real :: dfac
  real :: fact
  integer :: il
  integer :: inc
  integer :: ind
  integer :: isd
  integer :: iv
  integer :: jk
  real :: p
  real :: rsddensd
  real :: rsddenss
  real :: rsdmdn
  real :: rsdms
  real :: rsdmsd
  real :: rsdmsn
  real :: sfac
  real :: sqrtzpi
  real :: vsd
  real :: vsrd
  real :: vsrs
  real :: vss
  real :: xxd
  real :: xxs
  real :: z1
  real :: z2
  real :: za
  real :: zb
  real :: zc
  real :: zdecay
  real :: zdelta
  real :: zdp
  real :: zdp1
  real :: zdp2
  real :: zdxtdtb
  real :: zdxtdto
  real :: zdz
  real :: zeta
  real :: zfac
  real :: zlambda0
  real :: zmass
  real :: zmin
  real :: zpi
  real :: zpref
  real :: zrd
  real :: zrdini
  real :: zrhoair
  real :: zrs
  real :: zrsini
  real :: zsed
  real :: zszd1
  real :: zszd2
  real :: zszd3
  real :: zszs1
  real :: zszs2
  real :: zszs3
  real :: ztc
  real :: ztref
  real :: zxnewb
  real :: zxnewo
  !
  real  ,  dimension(10)  :: rsdmd
  !
  !     * work fields for itteration.
  !
  real  ,  dimension(ilg)  :: zlambda
  real  ,  dimension(ilg)  :: xd
  real  ,  dimension(ilg)  :: xs
  real  ,  dimension(ilg)  :: vsedd
  real  ,  dimension(ilg)  :: vseds
  real  ,  dimension(ilg)  :: zkd
  real  ,  dimension(ilg)  :: zks
  !
  !     * added work arrays (new) for mass-vector intrinsics.
  !
  real  ,  dimension(ilg)  :: zknd
  real  ,  dimension(ilg)  :: zkns
  real  ,  dimension(ilg)  :: zcknd
  real  ,  dimension(ilg)  :: zckns
  real  ,  dimension(ilg)  :: efactd
  real  ,  dimension(ilg)  :: efacts
  !
  !     * fields for hygroscopic particle properties.
  !
  real, dimension(ilg,ilev) :: awrk
  real, dimension(ilg) :: rsdmss
  !
  integer, dimension(8) :: date_time
  !
  !     * for vector mass intrinsics usage, need integer(4) length passed !
  !
  integer*4 :: len
  !
  integer :: machine
  integer :: intsize
  !
  !     * common block to hold word size.
  !
  common /machtyp/ machine,intsize
  !=================================================================
  zfac = log(0.5) * ztmst
  !
  zlambda0 = 0.0000066        ! used to calc. the mean free way path
  ztref = 293.15              ! reference temperature
  zpref = 101325              ! reference pressure
  za = 1.257                  ! a,b and c are constants
  zb = 0.4                    ! needed for the calculation
  zc = - 1.1                   ! of the fall speed
  zmin = 1.e-20               ! lower limit of tracer
  zpi = 3.141592654           ! pi
  sqrtzpi = sqrt(zpi)
  inc = 10                    ! number of classes in the size distr.
  zdelta = 1.2                ! integration interval in micrometers
  !
  rsddensd = 2.6              ! density of the aerosols (micm)
  zszd1 = 2.15                ! sigma
  zszd2 = 1.9                 ! radius
  zszd3 = sqrt(2.) * log(zszd1)!
  zrdini = 0.02             ! starting point in mu for dust
  !
  rsddenss = 1.15             ! density of the aerosols (sscm)
  zszs1 = 2.03                ! sigma
  zszs2 = 3.49                ! radius
  zszs3 = sqrt(2.) * log(zszs1)!
  zrsini = 0.04             ! starting point in mu for sea salt
  !
  !     * calculate the mass in each size bin and the total mass
  !     * integrated over the size distirbution
  !
  zrd = zrdini
  !
  !     * calculate conversion of hydrophobic aerosol to hydrophylic aerosol
  !     * for black and organic carbon.
  !
  zdecay = exp(zfac * 1.16e-5)
  afac = 1./grav/ztmst
  do jk = 1,ilev
    do il = il1,il2
      fact = dshj(il,jk) * pressg(il) * afac
      zxnewb = xrow(il,jk + 1,ibco) * zdecay
      zdxtdtb = (zxnewb - xrow(il,jk + 1,ibco))
      zxnewo = xrow(il,jk + 1,ioco) * zdecay
      zdxtdto = (zxnewo - xrow(il,jk + 1,ioco))
      xrow(il,jk + 1,ibco) = xrow(il,jk + 1,ibco) + zdxtdtb
      xrow(il,jk + 1,ibcy) = xrow(il,jk + 1,ibcy) - zdxtdtb
      xrow(il,jk + 1,ioco) = xrow(il,jk + 1,ioco) + zdxtdto
      xrow(il,jk + 1,iocy) = xrow(il,jk + 1,iocy) - zdxtdto
    end do
  end do ! loop 200
  !
  !     * sedimentation of particles (stokes-cunningham). cgs units
  !     * are used.
  !
  rsdmsd = 0.
  dfac = zdelta/sqrtzpi/zszd3
  do isd = 1,inc
    !
    !       *** parameters for mineral dust.
    !
    z1 = exp( - (log(zrd/zszd2)/zszd3) ** 2)
    rsdmdn = z1 * dfac/zrd
    rsdmd(isd) = 4. * zpi/3. * rsddensd * (zrd * 0.0001) ** 3 * rsdmdn * 0.001
    rsdmsd = rsdmsd + rsdmd(isd)
    zrd = zrd + zdelta
  end do ! loop 300
  !
  !     * parameters sea salt.
  !     * first, calculate molality of nacl as function of rh and
  !     * get mass fraction of nacl in aerosol droplet from that.
  !     * assume linear mixture of densities in aerosol. this leads
  !     * to the ratio of wet to dry aerosol radius.
  !
  do jk = 1,ilev
    do il = il1,il2
      awrk(il,jk) = acoef(0)
    end do
  end do ! loop 301
  !
  do ind = 1,6
    do jk = 1,ilev
      do il = il1,il2
        awrk(il,jk) = awrk(il,jk) + acoef(ind) * min(rh(il,jk),rhmax) ** ind
      end do
    end do
  end do ! loop 302
  !
  do jk = 1,ilev
    do il = il1,il2
      atmp = awrk(il,jk) * (ymasna + ymascl)
      afakt = atmp/(1. + atmp)
      adens = afakt * yrhod + (1. - afakt) * yrhow
      aratr = ((yrhod/adens)/afakt) ** (1./3.)
      awrk(il,jk) = yrdry * aratr
    end do
  end do ! loop 303
  !
  !     *** calculate viscosity and mean free path.
  !
  afac = 2. * grav * 100./9.
  len = il2 - il1 + 1
  do jk = 1,ilev - 1
    !
    do il = il1,il2
      p = shj(il,jk) * pressg(il)
      zdp = dshj(il,jk) * pressg(il)/grav * ztmst
      ztc = th(il,jk) - 273.15
      zeta = (1.718 + 0.0049 * ztc - 0.000012 * ztc * ztc) * 0.0001
      zlambda(il) = zlambda0 * zpref/p * th(il,jk)/ztref
      zrhoair = p/rgas/th(il,jk)/1000.
      zkd(il) = afac * (rsddensd - zrhoair)/zeta
      zks(il) = afac * (rsddenss - zrhoair)/zeta
      !
      xd(il) = zrdini
      xs(il) = zrsini
      vsedd(il) = 0.
      vseds(il) = 0.
      rsdmss(il) = 0.
    end do ! loop 310
    !
    zrs = zrsini
    sfac = zdelta/sqrtzpi/zszs3
    do iv = 1,inc
      cfactd = rsdmd(iv) * 0.01
      !
      efactd(:) = 0.0
      efacts(:) = 0.0
      do il = il1,il2
        fact       = 1.e-4/zlambda(il)
        zknd (il)  = xd(il) * fact
        zkns (il)  = xs(il) * fact
        zcknd(il)  = zc * zknd(il)
        zckns(il)  = zc * zkns(il)
        efactd(il) = exp(zcknd(il))
        efacts(il) = exp(zckns(il))
      end do ! loop 312
      !
      do il = il1,il2
        z2 = exp( - (log(zrs/awrk(il,jk + 1))/zszs3) ** 2)
        rsdmsn = z2 * sfac/zrs
        rsdms = 4. * zpi/3. * rsddenss * (zrs * 0.0001) ** 3 &
                * rsdmsn * 0.001
        rsdmss(il) = rsdmss(il) + rsdms
        cfacts = rsdms * 0.01
        !
        xxd = xd(il) * xd(il) * 1.e-8 * (1. + 1./zknd(il) * (za + zb * efactd(il)))
        vsd = zkd(il) * xxd * cfactd
        vsedd(il) = vsedd(il) + vsd
        xd(il) = xd(il) + zdelta
        !
        xxs = xs(il) * xs(il) * 1.e-8 * (1. + 1./zkns(il) * (za + zb * efacts(il)))
        vss = zks(il) * xxs * cfacts
        vseds(il) = vseds(il) + vss
        xs(il) = xs(il) + zdelta
      end do ! loop 315
      zrs = zrs + zdelta
    end do ! loop 320
    !
    !      * calculate representative sedimentation velocities
    !      * for coarse mode mineral dust and sea salt (in si-units).
    !      * note that fact1,fact2 and their use in determining xaddrow
    !      * changes are commented out, since the operation is a shift
    !      * in mass and thus the column-integral remains unaffected.
    !      * the lines are left in for reference.
    !
    do il = il1,il2
      zdp1 = dshj(il,jk)
      zdp2 = dshj(il,jk + 1)
      !       fact1=dshj(il,jk)*pressg(il)/grav
      !       fact2=dshj(il,jk+1)*pressg(il)/grav
      !
      if (jk == 1) then
        zdz = rgas * th(il,jk)/grav * (log(shj(il,1)/shtj(il)))
      else
        zdz = rgas * th(il,jk)/grav * (log(shj(il,jk + 1)/shj(il,jk)))
      end if
      !
      vsrd = vsedd(il)/rsdmsd
      zmass = xrow(il,jk + 1,iduc)
      if (zmass <= zmin) then
        zsed = 0.
      else
        zsed = zmass * vsrd/zdz
        zsed = min(zsed,zmass/ztmst)
        xrow(il,jk + 1,iduc) = xrow(il,jk + 1,iduc) - zsed * ztmst
        xrow(il,jk + 2,iduc) = xrow(il,jk + 2,iduc) + zsed * ztmst * zdp1/zdp2
      end if
      !
      vsrs = vseds(il)/rsdmss(il)
      zmass = xrow(il,jk + 1,issc)
      if (zmass <= zmin) then
        zsed = 0.
      else
        zsed = zmass * vsrs/zdz
        zsed = min(zsed,zmass/ztmst)
        xrow(il,jk + 1,issc) = xrow(il,jk + 1,issc) - zsed * ztmst
        xrow(il,jk + 2,issc) = xrow(il,jk + 2,issc) + zsed * ztmst * zdp1/zdp2
      end if
    end do ! loop 330
  end do ! loop 350
  !
  return
end subroutine xtsink3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
