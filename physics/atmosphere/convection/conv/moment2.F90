!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine moment2(dudt,dvdt,du,alpha,dp,ed,eu,mc,md,mu, &
                   p,qd,qu,qhat,sd,su,shat,t,u,v,z,zf, &
                   dsr,dsubcld,jb,jd,jt, &
                   msg,dt,ilev,il1g,il2g,ilg,jlat)
  !
  !     * may 27/2006 - m.lazare.    new version for gcm15f:
  !     *                            - now uses internal work arrays
  !     *                              instead of passed workspace.
  !     * may 05/03 - k.vonsalzen    previous version moment up to gcm15e.
  !     * july 17/92. - guang jun zhang, m.lazare. original port.
  !
  !     * performs momentum mixing due to cumulus parametrization.
  !
  use phys_consts, only : grav, rgas, cpres
  implicit none
  real, intent(in) :: dt
  integer, intent(in) :: il1g  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2g  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: jlat
  integer, intent(in) :: msg

  real, intent(inout), dimension(ilg,ilev) :: dudt !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: dvdt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dsr !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: du !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dp !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: ed !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: eu !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: mc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: md !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: mu !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: p !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qu !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: qhat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: su !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: t !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: u !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: v !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: z !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zf !< Variable description\f$[units]\f$

  !
  real, intent(in), dimension(ilg) :: dsubcld !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(ilg) :: jb !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: jd !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: jt !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * internal work fields.
  !
  real :: bsj0chi
  real :: chi
  real :: dpeff
  real :: factor
  integer :: il
  integer :: j
  integer :: n
  real :: rc
  real :: rmax
  real :: rnu
  real :: cshape
  real :: windmag

  integer, parameter :: nbf = 10
  real, dimension(nbf) :: alfa1
  real, dimension(nbf) :: alfa2
  real, dimension(nbf) :: bsj0
  real, dimension(nbf) :: bsj1
  real, dimension(nbf) :: tau

  real, dimension(ilg,ilev) :: ac
  real, dimension(ilg,ilev) :: ad
  real, dimension(ilg,ilev) :: au
  real, dimension(ilg,ilev) :: acfl
  real, dimension(ilg,ilev) :: adfl
  real, dimension(ilg,ilev) :: aufl
  real, dimension(ilg,ilev) :: b1
  real, dimension(ilg,ilev) :: b1fl
  real, dimension(ilg,ilev) :: bd
  real, dimension(ilg,ilev) :: bu
  real, dimension(ilg,ilev) :: d0
  real, dimension(ilg,ilev) :: d0hat
  real, dimension(ilg,ilev) :: delpx
  real, dimension(ilg,ilev) :: delpy
  real, dimension(ilg,ilev) :: dz
  real, dimension(ilg,ilev) :: dzf
  real, dimension(ilg,ilev) :: ec
  real, dimension(ilg,ilev) :: e
  real, dimension(ilg,ilev) :: ehat
  real, dimension(ilg,ilev) :: rho
  real, dimension(ilg,ilev) :: rhohat
  real, dimension(ilg,ilev) :: uhat
  real, dimension(ilg,ilev) :: vhat
  real, dimension(ilg,ilev) :: uc
  real, dimension(ilg,ilev) :: vc
  real, dimension(ilg,ilev) :: w0
  real, dimension(ilg,ilev) :: w1
  real, dimension(ilg,ilev) :: w0fl
  real, dimension(ilg,ilev) :: w1fl
  !
  real, dimension(ilg) :: cosa
  real, dimension(ilg) :: sina
  real, dimension(ilg) :: ccx
  real, dimension(ilg) :: ccy
  real, dimension(ilg) :: umn
  real, dimension(ilg) :: vmn
  real, dimension(ilg) :: dep
  !
  real, dimension(ilg,ilev,nbf) :: fx
  real, dimension(ilg,ilev,nbf) :: hx
  real, dimension(ilg,ilev,nbf) :: dx
  real, dimension(ilg,ilev,nbf) :: aa
  real, dimension(ilg,ilev,nbf) :: bb
  real, dimension(ilg,ilev,nbf) :: cc
  real, dimension(ilg,ilev,nbf) :: fy
  real, dimension(ilg,ilev,nbf) :: hy
  real, dimension(ilg,ilev,nbf) :: dy
  !
  !----------------------------------------------------------------------
  !***********************************************************************
  ! cccc initialize relevant interfacial and midlayer variables     ccccc
  !***********************************************************************
  rc = 3000.
  chi = 3.8317/rc
  cshape = 0.8
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      bu(il,j) = 0.
      alpha(il,j) = 0.5
      bd(il,j) = 0.
      delpx(il,j) = 0.
      delpy(il,j) = 0.
      ec(il,j) = eu(il,j) + ed(il,j)
      if (t(il,j) > 0.) then
        rho(il,j) = 100. * p(il,j)/(rgas * t(il,j))
      end if
    end do
  end do ! loop 5
  !
  do n = 1,nbf
    do j = msg + 1,ilev
      do il = il1g,il2g
        fx(il,j,n) = 0.
        fy(il,j,n) = 0.
        hx(il,j,n) = 0.
        hy(il,j,n) = 0.
      end do
    end do
  end do ! loop 10
  !
  do j = msg + 2,ilev
    do il = il1g,il2g
      bu(il,j) = (   su(il,j) - shat(il,j) + 0.608 * ( qu(il,j) &
                 * (su(il,j) - grav/cpres * zf(il,j)) - qhat(il,j) &
                 * (shat(il,j) - grav/cpres * zf(il,j)) )  ) &
                 /( (shat(il,j) - grav/cpres * zf(il,j)) &
                 * (1. + .608 * qhat(il,j)) ) * grav
      bd(il,j) = (   sd(il,j) - shat(il,j) + 0.608 * ( qd(il,j) &
                 * (sd(il,j) - grav/cpres * zf(il,j)) - qhat(il,j) &
                 * (shat(il,j) - grav/cpres * zf(il,j)) )  ) &
                 /( (shat(il,j) - grav/cpres * zf(il,j)) &
                 * (1. + .608 * qhat(il,j)) ) * grav
    end do
  end do ! loop 15
  !
  do il = il1g,il2g
    umn(il) = 0.
    vmn(il) = 0.
    !       dep(il)=0.
    cosa(il) = 0.
    sina(il) = 0.
    ccx(il) = 0.
    ccy(il) = 0.
  end do ! loop 20
  !
  do j = 1,ilev
    do il = il1g,il2g
      if (j >= jt(il) .and. j < jb(il)) then
        umn(il) = umn(il) + u(il,j) * p(il,j)/t(il,j) * &
                  (zf(il,j) - zf(il,j + 1))
        vmn(il) = vmn(il) + v(il,j) * p(il,j)/t(il,j) * &
                  (zf(il,j) - zf(il,j + 1))
        !         dep(il)=dep(il)+p(il,j)/t(il,j)*(zf(il,j)-zf(il,j+1))
      end if
    end do
  end do ! loop 25
  !
  do il = il1g,il2g
    windmag = sqrt(umn(il) ** 2 + vmn(il) ** 2)
    !       if (dep(il)/=0. .and. windmag/=0.)                       then
    if (windmag /= 0.) then
      cosa(il) = umn(il)/windmag
      sina(il) = vmn(il)/windmag
      !         ccx(il)=0.*umn(il)/dep(il)
      !         ccy(il)=0.*vmn(il)/dep(il)
    end if
  end do ! loop 30
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j > msg + 1) then
        rhohat(il,j) = alpha(il,j) * rho(il,j - 1) + (1. - alpha(il,j)) * rho(il,j)
        dzf(il,j) = z(il,j - 1) - z(il,j)
        uhat(il,j) = alpha(il,j) * u(il,j - 1) + (1. - alpha(il,j)) * u(il,j)
        vhat(il,j) = alpha(il,j) * v(il,j - 1) + (1. - alpha(il,j)) * v(il,j)
      else
        rhohat(il,j) = rho(il,j)
        dzf(il,j) = zf(il,j) - z(il,j)
        uhat(il,j) = u(il,j)
        vhat(il,j) = v(il,j)
      end if
      uc(il,j) = uhat(il,j)
      vc(il,j) = vhat(il,j)
    end do
  end do ! loop 35
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j < ilev) then
        dz(il,j) = zf(il,j) - zf(il,j + 1)
      else
        dz(il,ilev) = zf(il,ilev)
      end if
    end do
  end do ! loop 40
  !     ******************************************************************
  !     au, ad,ac are actual fractional cloud area times grav
  !     ******************************************************************
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j >= jt(il)) then
        au(il,j) = mu(il,jb(il))/rhohat(il,jb(il))
      else
        au(il,j) = 0.
      end if
      if (j >= jd(il)) then
        ad(il,j) = - md(il,jd(il))/ rhohat(il,jd(il))
      else
        ad(il,j) = 0.
      end if
      ac(il,j) = au(il,j) + ad(il,j)
      if (ac(il,j) > 0.) then
        w0(il,j) = mc(il,j)/(rhohat(il,j) * ac(il,j))
      else
        w0(il,j) = 0.
      end if
    end do
  end do ! loop 75
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j < ilev) then
        acfl(il,j) = alpha(il,j) * ac(il,j) + (1. - alpha(il,j)) * ac(il,j + 1)
        aufl(il,j) = alpha(il,j) * au(il,j) + (1. - alpha(il,j)) * au(il,j + 1)
        adfl(il,j) = alpha(il,j) * ad(il,j) + (1. - alpha(il,j)) * ad(il,j + 1)
        w0fl(il,j) = alpha(il,j) * w0(il,j) + (1. - alpha(il,j)) * w0(il,j + 1)
      else
        acfl(il,j) = alpha(il,j) * ac(il,j)
        aufl(il,j) = alpha(il,j) * au(il,j)
        adfl(il,j) = alpha(il,j) * ad(il,j)
        w0fl(il,j) = alpha(il,j) * w0(il,j)
      end if
    end do
  end do ! loop 80
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j < jb(il) .and. j >= jt(il) .and. acfl(il,j) > 0.) then
        d0(il,j) = ( mc(il,j + 1) - mc(il,j) ) &
                   /(rho(il,j) * dz(il,j) * acfl(il,j))
      else
        if (j == jb(il) .and. acfl(il,j) > 0. ) then
          d0(il,j) = - mc(il,j)/(rho(il,j) * dz(il,j) * acfl(il,j))
        else
          d0(il,j) = 0.
        end if
      end if
      if (j < jb(il) .and. j >= jt(il) .and. acfl(il,j) > 0. &
          .and. adfl(il,j) > 0. .and. aufl(il,j) > 0.) then
        e(il,j) = 1./chi ** 2 * adfl(il,j)/(acfl(il,j) * rho(il,j)) &
                  * (1./(aufl(il,j) * dz(il,j)) &
                  * (mu(il,j) - mu(il,j + 1)) - 1./(adfl(il,j) * dz(il,j)) &
                  * (md(il,j) - md(il,j + 1)) ) &
                  * cshape/factor
      else
        e(il,j) = 0.
      end if
    end do
  end do ! loop 250
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j > msg + 1) then
        d0hat(il,j) = alpha(il,j) * d0(il,j - 1) + (1. - alpha(il,j)) * d0(il,j)
        ehat(il,j) = alpha(il,j) * e(il,j - 1) + (1. - alpha(il,j)) * e(il,j)
      else
        d0hat(il,j) = (1. - alpha(il,j)) * d0(il,j)
        ehat(il,j) = (1. - alpha(il,j)) * e(il,j)
      end if
    end do
  end do ! loop 275
  !     **********************************************
  !     calculate first harmonics in thermodynamics
  !     **********************************************
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j >= jd(il) .and. j <= jb(il) .and. au(il,j) > 0. &
          .and. ad(il,j) > 0. .and. ac(il,j) > 0. ) then
        w1(il,j) = cshape/factor * (mu(il,j)/au(il,j) - md(il,j)/ad(il,j)) &
                   * ad(il,j)/(ac(il,j) * rhohat(il,j))
        b1(il,j) = cshape/factor * (bu(il,j) - bd(il,j)) * ad(il,j)/ac(il,j)
      else
        w1(il,j) = 0.
        b1(il,j) = 0.
      end if
    end do
  end do ! loop 325
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j < ilev) then
        w1fl(il,j) = alpha(il,j) * w1(il,j) + (1. - alpha(il,j)) * w1(il,j + 1)
        b1fl(il,j) = alpha(il,j) * b1(il,j) + (1. - alpha(il,j)) * b1(il,j + 1)
      else
        w1fl(il,j) = alpha(il,j) * w1(il,j)
        b1fl(il,j) = alpha(il,j) * b1(il,j)
      end if
    end do
  end do ! loop 350
  !
  do n = 1,nbf
    do j = msg + 1,ilev
      do il = il1g,il2g
        if (j < jb(il) .and. j >= jt(il) .and. mc(il,j) > 0. &
            .and. mc(il,j + 1) > 0.) then
          fx(il,j,n) = 2. * rho(il,j)/bsj0(n) ** 2 &
                       * (d0(il,j) * e(il,j) * chi ** 2 * cosa(il) * alfa1(n) &
                       + 2. * w0fl(il,j) * rc/rmax ** 2 * bsj1(n) &
                       * ( (ehat(il,j) - ehat(il,j + 1))/dz(il,j) * chi * cosa(il) &
                       * bsj0chi + (u (il,j) - u (il,j + 1))/dz(il,j) ) &
                       - (d0hat(il,j) - d0hat(il,j + 1))/dz(il,j) * w1fl(il,j) * cosa(il) &
                       * (alfa2(n) - alfa1(n))    - 2. * ( (w0(il,j) - w0(il,j + 1)) &
                       * (w1(il,j) - w1(il,j + 1))/dz(il,j) ** 2 &
                       - w0fl(il,j) * w1fl(il,j) &
                       * (log(rho(il,j - 1))/(dz(il,j) * dzf(il,j)) &
                       - (1./dzf(il,j) + 1./dzf(il,j + 1))/dz(il,j) &
                       * log(rho(il,j)) &
                       + log(rho(il,j + 1))/(dz(il,j) * dzf(il,j + 1)) ) ) &
                       * cosa(il) * alfa1(n) &
                       + (rhohat(il,j) * b1(il,j) - rhohat(il,j + 1) * b1(il,j + 1)) &
                       /(dz(il,j) * rho(il,j)) * cosa(il) * alfa1(n) )
          fy(il,j,n) = 2. * rho(il,j)/bsj0(n) ** 2 &
                       * (d0(il,j) * e(il,j) * chi ** 2 * sina(il) * alfa1(n) &
                       + 2. * w0fl(il,j) * rc/rmax ** 2 * bsj1(n) &
                       * ( (ehat(il,j) - ehat(il,j + 1))/dz(il,j) * chi * sina(il) &
                       * bsj0chi + (v (il,j) - v (il,j + 1))/dz(il,j) ) &
                       - (d0hat(il,j) - d0hat(il,j + 1))/dz(il,j) * w1fl(il,j) * sina(il) &
                       * (alfa2(n) - alfa1(n))    - 2. * ( (w0(il,j) - w0(il,j + 1)) &
                       * (w1(il,j) - w1(il,j + 1))/dz(il,j) ** 2 &
                       - w0fl(il,j) * w1fl(il,j) &
                       * (log(rho(il,j - 1))/(dz(il,j) * dzf(il,j)) &
                       - (1./dzf(il,j) + 1./dzf(il,j + 1))/dz(il,j) &
                       * log(rho(il,j)) &
                       + log(rho(il,j + 1))/(dz(il,j) * dzf(il,j + 1)) ) ) &
                       * sina(il) * alfa1(n) &
                       + (rhohat(il,j) * b1(il,j) - rhohat(il,j + 1) * b1(il,j + 1)) &
                       /(dz(il,j) * rho(il,j)) * sina(il) * alfa1(n) )
        end if
      end do
    end do
  end do ! loop 500
  !
  do n = 1,nbf
    do il = il1g,il2g
      aa(il,msg + 1,n) = 0.
      bb(il,msg + 1,n) = 1./ dzf(il,msg + 2)
      cc(il,msg + 1,n) = - 1./ dzf(il,msg + 2)

      aa(il,ilev,n) = 1./ dzf(il,ilev)
      bb(il,ilev,n) = - 1./ dzf(il,ilev)
      cc(il,ilev,n) = 0.

      dx(il,msg + 1,n) = b1(il,msg + 2) * alfa1(n) * 2 &
                         * cosa(il)/bsj0(n) ** 2
      dx(il,ilev,n) = b1(il,ilev) * alfa1(n) * 2 &
                      * cosa(il)/bsj0(n) ** 2

      dy(il,msg + 1,n) = b1(il,msg + 2) * alfa1(n) * 2 &
                         * sina(il)/bsj0(n) ** 2
      dy(il,ilev,n) = b1(il,ilev) * alfa1(n) * 2 &
                      * sina(il)/bsj0(n) ** 2
    end do
  end do ! loop 525
  !
  do n = 1,nbf
    do j = msg + 2,ilev - 1
      do il = il1g,il2g
        aa(il,j,n) = 1./( dz(il,j) * dzf(il,j) )
        bb(il,j,n) = - ( 1./dz(il,j) * (1./dzf(il,j) + 1./dzf(il,j + 1)) &
                     + (tau(n)/rmax) ** 2)
        cc(il,j,n) = 1./( dz(il,j) * dzf(il,j + 1) )

        dx(il,j,n) = fx(il,j,n)
        dy(il,j,n) = fy(il,j,n)
      end do
    end do
  end do ! loop 550
  !
  do n = 1,nbf
    do il = il1g,il2g
      cc(il,msg + 1,n) = cc(il,msg + 1,n)/bb(il,msg + 1,n)
      dx(il,msg + 1,n) = dx(il,msg + 1,n)/bb(il,msg + 1,n)
      dy(il,msg + 1,n) = dy(il,msg + 1,n)/bb(il,msg + 1,n)
    end do
  end do ! loop 575
  !
  do n = 1,nbf
    do j = msg + 2,ilev
      do il = il1g,il2g
        cc(il,j,n) = cc(il,j,n)/(bb(il,j,n) - aa(il,j,n) * cc(il,j - 1,n))
        dx(il,j,n) = (dx(il,j,n) - aa(il,j,n) * dx(il,j - 1,n)) &
                     /(bb(il,j,n) - aa(il,j,n) * cc(il,j - 1,n))
        dy(il,j,n) = (dy(il,j,n) - aa(il,j,n) * dy(il,j - 1,n)) &
                     /(bb(il,j,n) - aa(il,j,n) * cc(il,j - 1,n))
      end do
    end do
  end do ! loop 600
  !
  do n = 1,nbf
    do j = ilev - 1,msg + 1, - 1
      do il = il1g,il2g
        dx(il,j,n) = dx(il,j,n) - cc(il,j,n) * dx(il,j + 1,n)
        dy(il,j,n) = dy(il,j,n) - cc(il,j,n) * dy(il,j + 1,n)
      end do
    end do
  end do ! loop 650
  !
  do n = 1,nbf
    do j = msg + 1,ilev
      do il = il1g,il2g
        hx(il,j,n) = dx(il,j,n)
        hy(il,j,n) = dy(il,j,n)
        delpx(il,j) = delpx(il,j) + hx(il,j,n) * bsj1(n)
        delpy(il,j) = delpy(il,j) + hy(il,j,n) * bsj1(n)
      end do
    end do
  end do ! loop 700
  !
  do j = msg + 1,ilev
    do il = il1g,il2g
      delpx(il,j) = delpx(il,j)/rc
      delpy(il,j) = delpy(il,j)/rc
    end do
  end do ! loop 850
  !     ************************************
  !     calculate the cloud mean wind
  !     ************************************
  do j = ilev - 1,msg + 1, - 1
    do il = il1g,il2g
      if (mc(il,j) > 0. &
          .and. j > jt(il) .and. j < jb(il)) then
        uc(il,j) = 1./( mc(il,j) - dz(il,j) * (ec(il,j)/2. - du(il,j)) &
                   * alpha(il,j + 1) ) * (mc(il,j + 1) * uc(il,j + 1) + dz(il,j) &
                   * (ec(il,j) * u(il,j) + (ec(il,j)/2. - du(il,j)) &
                   * (1. - alpha(il,j + 1)) * uc(il,j + 1) &
                   +  ec(il,j)/2. &
                   * ( - bsj0chi/(2. * chi * dz(il,j)) &
                   * (log(mc(il,j)) - log(mc(il,j + 1)) ) &
                   * w1fl(il,j) * cosa(il) &
                   + bsj0chi * chi * e(il,j) * cosa(il) ) &
                   - ec(il,j)/2. * ccx(il) &
                   - acfl(il,j) * delpx(il,j) )  )
        vc(il,j) = 1./( mc(il,j) - dz(il,j) * (ec(il,j)/2. - du(il,j)) &
                   * alpha(il,j + 1) ) * (mc(il,j + 1) * vc(il,j + 1) + dz(il,j) &
                   * (ec(il,j) * v(il,j) + (ec(il,j)/2. - du(il,j)) &
                   * (1. - alpha(il,j + 1)) * vc(il,j + 1) &
                   +  ec(il,j)/2. &
                   * ( - bsj0chi/(2. * chi * dz(il,j)) &
                   * (log(mc(il,j)) - log(mc(il,j + 1)) ) &
                   * w1fl(il,j) * sina(il) &
                   + bsj0chi * chi * e(il,j) * sina(il) ) &
                   - ec(il,j)/2. * ccy(il) &
                   - acfl(il,j) * delpy(il,j) )  )
      end if
    end do
  end do ! loop 875
  !
  rnu = 1.
  do j = msg + 1,ilev
    do il = il1g,il2g
      if (j >= jt(il) .and. j <= jb(il) ) then
        uhat(il,j) = uhat(il,j) + rnu * alpha(il,j) * dt * dudt(il,j - 1)
        vhat(il,j) = vhat(il,j) + rnu * alpha(il,j) * dt * dvdt(il,j - 1)
        dpeff = dsr(il,j) * dp(il,j)
        if (j == jt(il)) then
          dudt(il,j) = 1./(1 + rnu * alpha(il,j + 1) * mc(il,j + 1) * dt/dpeff) * &
                       mc(il,j + 1) * (uc(il,j + 1) - uhat(il,j + 1))/dpeff
          dvdt(il,j) = 1./(1 + rnu * alpha(il,j + 1) * mc(il,j + 1) * dt/dpeff) * &
                       mc(il,j + 1) * (vc(il,j + 1) - vhat(il,j + 1))/dpeff
        else if (j < jb(il)) then
          dudt(il,j) = 1./(1 + rnu * alpha(il,j + 1) * mc(il,j + 1) * dt/dpeff) &
                       * (mc(il,j + 1) * (uc(il,j + 1) - uhat(il,j + 1)) &
                       - mc(il,j) * (uc(il,j) - uhat(il,j)))/dpeff
          dvdt(il,j) = 1./(1 + rnu * alpha(il,j + 1) * mc(il,j + 1) * dt/dpeff) &
                       * (mc(il,j + 1) * (vc(il,j + 1) - vhat(il,j + 1)) &
                       - mc(il,j) * (vc(il,j) - vhat(il,j)))/dpeff
        else
          dudt(il,j) = - 1./dsubcld(il) * mc(il,j) * (uc(il,j) - uhat(il,j))
          dvdt(il,j) = - 1./dsubcld(il) * mc(il,j) * (vc(il,j) - vhat(il,j))
        end if
      end if
    end do
  end do ! loop 950
  !
  return
end subroutine moment2

!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
