!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine mkgwintr2(ug,vg,tfrow,throw, &
                     shxkj,shj,tsg,rgocp, &
                     sgj,sgbj,utendgw,vtendgw,urms, &
                     pressg,rgas,cpres,ilev,ilg,il1,il2,nhar)
  !
  !    * jun 27/2006 - m.lazare.   new version for gcm15f based on mkgwintr:
  !    *                           - work arrays are now local to the
  !    *                             routine, ie not passed in with
  !    *                             "WRK" pointers.
  !    * oct 27/1998 - a.medvedev. original version mkgwintr.
  ! isotropic case: waves in 4 directions (nwse)
  ! exact copy of mam4 version
  !
  ! purpose:
  !    interface for gravity wave drag routine (mk95, jgr95, d12)
  !
  ! author:   a. medvedev  (1996)
  !           a. medvedev  (1998):  vectorized
  !
  !---------------- input parameters: ----------------------------------
  !    ug(ilg,ilev)- row of *pf* level u-wind
  !    vg(ilg,ilev)- row of *pf* level v-wind
  ! tfrow(ilg,ilev) - row of full level temperatures (*pf*)
  ! throw(ilg,ilev) - row of half level temperatures (*ph*)
  !   sgj(ilg,ilev)- vert coord sigma (correspond to "FULL" in gwd.f)
  !  sgbj(ilg,ilev)- "BOTTOM" interface for sgj (correspond to "HALF" in gwd.f)
  !    pressg(ilg) - latitude row of surface pressure (pa)
  !
  !---------------- output parameters: --------------------------------
  ! utendgw(ilg,ilev) - gwd in u-direction
  ! vtendgw(ilg,ilev) - gwd in v-direction
  ! urms   (ilg,ilev) - rms horizontal wind

  !---------------- igw spectrum parameters: --------------------------
  ! modified desaubies spectrum

  implicit none
  real :: ampl
  real, intent(in) :: cpres  !< Heat capacity of dry air \f$[J kg^{-1} K^{-1}]\f$
  real :: dttdsf
  real :: dxkz
  real :: gcpres
  real :: grav
  real :: gwkh
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer :: ilevgws
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: l
  integer :: levgws
  integer :: ll
  integer :: lupp
  integer, intent(in) :: nhar
  real :: pi
  real :: psd
  real :: ratio
  real :: rg
  real, intent(in) :: rgas  !< Ideal gas constant for dry air \f$[J kg^{-1} K^{-1}]\f$
  real, intent(in) :: rgocp  !< Ideal gas constant divided by heat capacity for dry air \f$[unitless]\f$
  real :: tndmax
  real :: wavelen
  real :: x1
  real :: x2
  real :: xkz1
  real :: xkz2
  real :: xm

  !      parameter(nhar=15)
  parameter(xkz1 = 19.e3, xkz2 = 900.)
  parameter(ilevgws = 16)
  parameter(ampl = 200., xm = 0.006, wavelen = 3.0e5)
  parameter(tndmax = 500./86400.) !  maximum allowed tendency (in m/s/

  real, intent(in), dimension(ilg,ilev) :: ug !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: vg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: tfrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: throw   !< Physics internal field for temperature, including moon layer at atmospheric model top \f$[K]\f$
  real, intent(inout), dimension(ilg,ilev) :: utendgw !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: vtendgw !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: shxkj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: tsg !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: urms !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * internal work arrays:
  !
  real, dimension(ilg,ilev) :: uu
  real, dimension(ilg,ilev) :: vv
  real, dimension(ilg,ilev) :: pf
  real, dimension(ilg,ilev) :: ph
  real, dimension(ilg,ilev) :: dpf
  real, dimension(ilg,ilev) :: dph
  real, dimension(ilg,ilev) :: rms

  real, dimension(ilg,nhar,ilev) :: usq
  real, dimension(ilg,nhar,ilev) :: zkzr
  real   :: omeg (ilg,nhar,ilev)
  real   :: beta(ilg,nhar,ilev)

  real, dimension(ilg,nhar) :: sigma
  real, dimension(ilg,nhar) :: alpha
  real, dimension(ilg,nhar) :: om
  real   :: zkz (nhar)
  real   :: ddkz(nhar)
  real   :: usql(nhar)

  real, dimension(ilg,ilev) :: ax
  real, dimension(ilg,ilev) :: axx
  real, dimension(ilg,ilev) :: ubm
  real, dimension(ilg) :: xv
  real, dimension(ilg) :: yv
  real, dimension(ilg,ilev) :: bvfreq

  real, dimension(ilg,nhar) :: v2
  real, dimension(ilg,nhar) :: drag
  real, dimension(ilg) :: gwkhu
  real, dimension(ilg) :: vv2
  real, dimension(ilg) :: xx
  real, dimension(ilg) :: dz
  real, dimension(ilg) :: bvkh
  real, dimension(ilg) :: vv3

  grav = 9.80616
  pi = 3.1415926
  rg = rgas/grav
  gcpres = grav/cpres

  lupp = ilev

  ! wave source at 165 mb (xx km)

  levgws = ilevgws
  gwkh = 2. * pi/wavelen
  x1 = 2. * pi/xkz1
  x2 = 2. * pi/xkz2

  dxkz = (log(x2) - log(x1))/real(nhar - 1)
  dxkz = exp(dxkz)                ! set up the condensed m-grid
  zkz(1) = x2
  do l = 1,nhar - 1
    zkz(l + 1) = zkz(l)/dxkz
    ddkz(l) = zkz(l) - zkz(l + 1)
  end do
  ddkz(nhar) = zkz(nhar) * (dxkz - 1.)/dxkz

  do l = 1,nhar
    psd = ampl * zkz(l)/xm/(1. + (zkz(l)/xm) ** 4)
    usql(l) = psd * ddkz(l)
  end do
  !
  !---------------------------------------------------------------------------
  ! determine the source layer wind and unit vectors, then project winds.
  !---------------------------------------------------------------------------
  !
  ! just use the source level midpoint values for the source
  ! wind speed and direction (unit vector).
  !
  !     k=ilev+1-levgws
  !     do i = il1,il2
  !        ubm(i,k) = sqrt (ug(i,k)**2 + vg(i,k)**2)
  !        xv(i) = ug(i,k) / ubm(i,k)
  !        yv(i) = vg(i,k) / ubm(i,k)
  !     end do
  !
  ! project the local wind at midpoints onto the source wind.
  !
  !     do k = 1, ilev+1-levgws
  !        do i = il1,il2
  !           ubm(i,k) = ug(i,k) * xv(i) + vg(i,k) * yv(i)
  !        end do
  !     end do
  !
  !     * calculate  b-v frequency at all longitudes
  !
  do l = 2,ilev
    do i = il1,il2
      dttdsf = (throw(i,l)/shxkj(i,l) - throw(i,l - 1)/ &
               shxkj(i,l - 1))/(shj(i,l) - shj(i,l - 1))
      dttdsf = min(dttdsf, - 5./sgj(i,l))
      bvfreq(i,l) = sqrt( - dttdsf * sgj(i,l) * (sgj(i,l) ** rgocp)/rgas) &
                    * grav/tsg(i,l)
    end do
  end do
  !
  !     * define bvfreq at top, smooth bvfreq and set lower limit
  !
  do l = 1,ilev
    do i = il1,il2
      if (l == 1) then
        bvfreq(i,l) = bvfreq(i,l + 1)
      end if
      if (l > 1) then
        ratio = 5. * log(sgj(i,l)/sgj(i,l - 1))
        bvfreq(i,l) = (bvfreq(i,l - 1) + ratio * bvfreq(i,l)) &
                      /(1. + ratio)
      end if
      bvfreq(i,l) = max(bvfreq(i,l), 0.001)
    end do
  end do

  ! :-------------------------------------------------------------------
  !
  do l = 1,ilev                    ! prepare 1d arrays for mkgwd
    ll = ilev + 1 - l
    do i = il1,il2
      pf(i,l) = sgj(i,ll) * pressg(i)
      ph(i,l) = sgbj(i,ll) * pressg(i)
      uu(i,l) = ug(i,ll)
      vv(i,l) = vg(i,ll)
      !           utendgw(i,ll)=0.0        ! initialize gravity wave
      !           vtendgw(i,ll)=0.0        ! drag tendencies to zero
      rms(i,l) = 0.0
    end do
  end do

  do l = 1,ilev - 1
    do i = il1,il2
      dpf(i,l) = pf(i,l + 1) - pf(i,l)
    end do
  end do

  do l = 1,nhar                    ! initialize squared amplitudes of
    do i = il1,il2
      usq(i,l,levgws) = usql(l)
    end do                      ! the spectrum at the bottom, levgw
  end do                         ! (where wave source is assumed)

  ! extrawaves  (c > 0) ......................x-direction...............

  do l = 1,nhar
    do i = il1,il2
      om(i,l) = bvfreq(i,ilev + 1 - levgws) * gwkh/zkz(l)
      omeg(i,l,levgws) = om(i,l) - gwkh * uu(i,levgws)
    end do
  end do

  call mkgwd(uu,pf,ph,throw,tfrow,bvfreq,dpf,gwkh,rg, &
             ax,axx,usq,sigma,zkzr,omeg,alpha,beta, &
             rms,ilev,nhar,levgws,lupp,ilg,il1,il2, &
             v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

  do l = 1,ilev                    !   accumulate gw tendency
    do i = il1,il2
      utendgw(i,l) = utendgw(i,l) + ax(i,ilev + 1 - l)
      urms(i,l)   = rms(i,ilev + 1 - l)
    end do
  end do

  ! intrawaves  (c < 0) .................................................

  do l = 1,nhar
    do i = il1,il2
      omeg(i,l,levgws) = - om(i,l) - gwkh * uu(i,levgws)
    end do
  end do

  call mkgwd(uu,pf,ph,throw,tfrow,bvfreq,dpf,gwkh,rg, &
             ax,axx,usq,sigma,zkzr,omeg,alpha,beta, &
             rms,ilev,nhar,levgws,lupp,ilg,il1,il2, &
             v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

  do l = 1,ilev                    !   accumulate gw tendency
    do i = il1,il2
      utendgw(i,l) = utendgw(i,l) + ax(i,ilev + 1 - l)
      urms(i,l)   = urms(i,l) + rms(i,ilev + 1 - l)
      if (abs(utendgw(i,l)) > tndmax) then
        utendgw(i,l) = sign(tndmax,utendgw(i,l))
      end if
    end do
  end do

  ! extrawaves  (c > 0) .......................y-direction...............

  do l = 1,nhar
    do i = il1,il2
      omeg(i,l,levgws) = om(i,l) - gwkh * vv(i,levgws)
    end do
  end do

  call mkgwd(vv,pf,ph,throw,tfrow,bvfreq,dpf,gwkh,rg, &
             ax,axx,usq,sigma,zkzr,omeg,alpha,beta, &
             rms,ilev,nhar,levgws,lupp,ilg,il1,il2, &
             v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

  do l = 1,ilev
    do i = il1,il2
      vtendgw(i,l) = vtendgw(i,l) + ax(i,ilev + 1 - l)
      urms(i,l)   = urms(i,l) + rms(i,ilev + 1 - l)
    end do
  end do

  ! intrawaves  (c < 0) .................................................

  do l = 1,nhar
    do i = il1,il2
      omeg(i,l,levgws) = - om(i,l) -  gwkh * vv(i,levgws)
    end do
  end do

  call mkgwd(vv,pf,ph,throw,tfrow,bvfreq,dpf,gwkh,rg, &
             ax,axx,usq,sigma,zkzr,omeg,alpha,beta, &
             rms,ilev,nhar,levgws,lupp,ilg,il1,il2, &
             v2,drag,gwkhu,vv2,xx,dz,bvkh,vv3)

  do l = 1,ilev
    do i = il1,il2
      vtendgw(i,l) = vtendgw(i,l) + ax(i,ilev + 1 - l)
      urms(i,l)   = urms(i,l) + rms(i,ilev + 1 - l)
      if (abs(vtendgw(i,l)) > tndmax) then
        vtendgw(i,l) = sign(tndmax,vtendgw(i,l))
      end if
    end do
  end do
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
