!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cslms (tlakrow,t0lakrow,hicelrow,hdpthrow,expwrow, &
                  dtmprow,tkelrow,delurow,gredrow,rhomrow, &
                  luinrow,luimrow,wsnolrot, &
                  ikmos,jkmos,nlakmax,nmk,nl,nm,ilg, &
                  tlakgat,t0lakgat,hicelgat,hdpthgat,expwgat, &
                  dtmpgat,tkelgat,delugat,gredgat,rhomgat, &
                  luingat,luimgat,wsnolgat,nlakgat)
  !
  !     * dec 01/16 - m.lazare. - remove {snol,rhosl,tsnol,albsl} already done
  !     *                         in "OCEANS" routine.
  !     *                       - remove {hlak,blak,llak,nlak} which are invariant
  !     *                         AND THUS DON'T NEED TO BE SCATTERED.
  !     *                       - remove tile dimension for all fields
  !     *                         except wsno.
  !     *                       - {ikmos,jkmos}->{ikmos,jkmos} and nmw->nmk.
  !     *                       - "ROT"->"ROW" for names except wsno.
  !     *                       - renamed lakeg-> cslmg.
  !     *                       - add "LUIN" and "LUIM", ie lake ice
  !     *                         fraction and mass..
  !     * jan 19/12 - m.mackay. scatter operation for lake tile variables.
  !
  implicit none
  !
  integer, intent(in) :: nmk
  integer, intent(in) :: nl
  integer, intent(in) :: nm
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: l
  integer, intent(in) :: nlakmax
  !
  !     * lake tile parameters
  !
  real, intent(inout), dimension(nl) :: t0lakrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: hicelrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: hdpthrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: expwrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: dtmprow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: tkelrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: delurow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: gredrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: rhomrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: luinrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl) :: luimrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl,nm) :: wsnolrot !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nl,nlakmax) :: tlakrow !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilg) :: t0lakgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: hicelgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: hdpthgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: expwgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: dtmpgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: tkelgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: delugat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gredgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: rhomgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: luingat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: luimgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: wsnolgat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,nlakmax) :: tlakgat !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: nlakgat !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(ilg) :: ikmos !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilg) :: jkmos !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !----------------------------------------------------------------------
  !     * scatter lake relevant variables out of
  !     * water-tile gat arrays
  !
  do k = 1,nmk
    t0lakrow(ikmos(k)) = t0lakgat(k)
    hicelrow(ikmos(k)) = hicelgat(k)
    hdpthrow(ikmos(k)) = hdpthgat(k)
    expwrow (ikmos(k)) = expwgat (k)
    dtmprow (ikmos(k)) = dtmpgat (k)
    tkelrow (ikmos(k)) = tkelgat (k)
    delurow (ikmos(k)) = delugat (k)
    gredrow (ikmos(k)) = gredgat (k)
    rhomrow (ikmos(k)) = rhomgat (k)
    luinrow (ikmos(k)) = luingat (k)
    luimrow (ikmos(k)) = luimgat (k)
    do l = 1,nlakgat(k)
      tlakrow(ikmos(k),l) = tlakgat(k,l)
    end do
  end do ! loop 200
  !
  do k = 1,nmk
    wsnolrot(ikmos(k),jkmos(k)) = wsnolgat(k)
  end do ! loop 300
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
