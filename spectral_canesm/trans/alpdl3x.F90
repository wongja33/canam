!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine alpdl3x(delalp,alp,lsr,lm,nlat,iram)
  !
  !     * oct 29/03 - m. lazare. reverse order of polynomials
  !     *                        and do over all latitudes.
  !     * oct 15/92 - previous version alpdl3.
  !     * jul 14/92 - previous version alpdl2.
  !
  !     * computes laplacian of legendre polynomials.
  !     * lsr contains row length info.
  !
  implicit none
  real*8 :: fns
  integer, intent(in) :: iram
  integer :: k
  integer :: kl
  integer :: kr
  integer, intent(in) :: lm
  integer :: m
  integer :: nj
  integer, intent(in) :: nlat
  integer :: ns
  real*8, intent(inout) , dimension(nlat,iram) :: delalp !< Variable description\f$[units]\f$
  real*8, intent(in) , dimension(nlat,iram) :: alp !< Variable description\f$[units]\f$
  integer, intent(in) :: lsr(2,lm+1) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  do m=1,lm
    kl=lsr(2,m)
    kr=lsr(2,m+1)-1
    do k=kl,kr
      ns=(m-1)+(k-kl)
      fns=float(ns)
      do nj=1,nlat
        delalp(nj,k)=-fns*(fns+1.)*alp(nj,k)
      end do ! loop 210
    end do ! loop 220
  end do ! loop 300
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
