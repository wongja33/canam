# NOTE:
#   - note tested
FC = ftn
LD = ftn

# TODO: add netcdf consideration if it can't be handled by the module system
INCLUDE_FLAGS =
LDFLAGS =

# create FFLAGS depending on debug/optimization settings
FFLAGS_DEBUG = -g -O0 -fbacktrace -ffpe-trap=invalid,zero,overflow -fcheck=all -fno-automatic -Wall -Wextra
FFLAGS_OPT = -g -O2 -march=native
FFLAGS = -cpp
ifeq ($(OPTIMIZATION_LVL),0)
FFLAGS += $(FFLAGS_DEBUG)
else
ifeq ($(OPTIMIZATION_LVL),1)
FFLAGS += $(FFLAGS_OPT)
endif
endif

# add include flags to FFLAGS
FFLAGS += $(INCLUDE_FLAGS)
