!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine nlclmxtke(xrow,qh,th,p,z,zf,dshj,shxkj,sqflx,tqflx, &
                           shflx,thflx,cdm,cvsg,sigma,pressg,x, &
                           dt,tice,msg,il1,il2,ilg,ilev, &
                           lev,ntrac,qt,hmn,dsr,chi,rrl,cpm,dsmix,hint, &
                           qint,tvppbl,tau,xint,sxflx, &
                           bcr,zer,qtn,hmnn, &
                           qcw,zclf,zcraut,ssh,vmodl, &
                           almix,  dhldz,   drwdz,   hmnmin,    qtmin, &
                           imxl,cndrol,deprol,xlwrol,xicrol,isavls, &
                           xlmtke,cld_he)
  !
  !     * sep 2020 - y.he,m.lazare,n. mcfarlane. updated with tke related mixing length
  !     *                                        and multi-layer clouds, et. al.
  !     * may 03/2018 - n.mcfarlane. added code to prevent statcld crashes.
  !     *                            removed unused {esw,esi} statement functions.
  !     * feb 10/2015 - m.lazare/      new version for gcm18:
  !     *               k.vonsalzen:   - comment-out lower bound on total
  !     *                                water (don't think we need it
  !     *                                any more due to implemented
  !     *                                improvements and bugfixes else where).
  !     * jul 31/2013 - m.lazare.    previous version nlclmx7 for gcm17:
  !     *                            cosmetic changes (since qtn>=0. in
  !     *                            our tests) to:
  !     *                            - print out warning if qtn<0 before
  !     *                              calling statcld5, then limit
  !     *                              it to be non-negative (2x).
  !     *                            - pass in lvl and icall to statcld5
  !     *                              to aid in future debugging.
  !     * jun 26/2013 - k.vonsalzen. new version for gcm17:
  !     *                            - calls new statcld5.
  !     * apr 29/2012 - k.vonsalzen/ previous version nlclmx6 for gcm16:
  !     *               m.lazare.    - calls new statcld4.
  !     * jan 17/2008 - k.vonsalzen/ previous version nlclmx5 for gcm15g/h/i:
  !     *               m.lazare.    - calls new statcld3.
  !     *                            - bugfix in expression for ustar to
  !     *                              properly include wind speed.
  !     *                            - modified calculation of adjustment
  !     *                              timescale in mixed layer (tau).
  !     *                            - uses clear-sky and all-sky mixing
  !     *                              lengths (almc and almx, respectively)
  !     *                              to define a consistent total
  !     *                              mixing length (almix) passed to
  !     *                              statcld3.
  !     *                            - passes dt(=adelt from physics)
  !     *                              to new statcld3 in place of
  !     *                              hard-coded 2.*delt.
  !     *                            - gustiness effect now included
  !     *                              since is now already contained in zspd
  !     *                              from physics.
  !     * jan 13/2007 - k.vonsalzen. previous version nlclmx4 for gcm15f.
  !     *                            - modified call to statcld2, in
  !     *                              conjunction with changes to add
  !     *                              "QCWVAR".
  !     * nov 28/2006 - m.lazare/    - add calculation for cndrol,deprol
  !     *               k.vonsalzen.   under control of "ISAVLS".
  !     *                            - move mixing of tracers to before
  !     *                              calculation of mlse and total
  !     *                              water, so profiles are well-mixed
  !     *                              before being processed.
  !     *                              note that now cloud water and ice
  !     *                              are mixed as well.
  !     * jun 19/2006 - m.lazare.    - calls new version statcld2.
  !     *                            - cosmetic: use variable instead of
  !     *                              constant in intrinsics such as "MAX",
  !     *                              so that can compile in 32-bit mode
  !     *                              with real*8.
  !     * may 06/06 - k.vonsalzen/ previous version nlclmx3 for gcm15e:
  !     *             m.lazare.    - use new common subroutine statcld for
  !     *                            unravelling.
  !     *                          - since similar mixing now also done in new
  !     *                            subroutine vrtdf14 (ie also calls
  !     *                            statcld), pass in required input fields
  !     *                            instead of re-deriving them here.
  !     *                          - reordering of terms and limitation of
  !     *                            fluxes at top of mixed layer to improve
  !     *                            numerical robustness.
  !     *                          - revisions to almix and convective
  !     *                            contribution to variance, consistent with
  !     *                            cond3.
  !     * dec 14/05 - k.vonsalzen/ previous version nlclmx2 for gcm15d.
  !     *             m.lazare.
  !     *
  !     * performs non-local mixing in planetary boundary layer based
  !     * on simple adjustment scheme. the mixing is performed for liquid
  !     * water static energy and total water. temperature and specific
  !     * humidity are obtained from unravelling procedure. a
  !     * semi-implicit method is used.
  !
  use phys_consts, only : grav, rgas, hs, hv, tvfa
  use tracers_info_mod, only: modl_tracers, iiwc, ilwc

  implicit none
  real, intent(in) :: dt
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: isavls  !< Switch to save extra cloud microphysic diagnostics (0 = don't save, 1 = save) \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: msg
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: tice
  !
  real, intent(inout), dimension(ilg,lev,ntrac) :: xrow !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: qh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: th !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg,ilev) :: z !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: p !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: zf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: shxkj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: cvsg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: sigma !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: sqflx !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: tqflx !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: shflx !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: thflx !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: cdm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(inout), dimension(ilg) :: x !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zer !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qtn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hmnn !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: qcw !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zclf !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zcraut !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ssh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: almix !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dhldz !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: drwdz !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hmnmin !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qtmin !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: vmodl !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dsr !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: chi !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: cpm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: rrl !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: qt !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: hmn !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: cndrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: deprol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: xlwrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: xicrol !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: xlmtke !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: cld_he !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg) :: dsmix !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hint !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qint !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tvppbl !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tau !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bcr !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ntrac) :: xint !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ntrac) :: sxflx !< Variable description\f$[units]\f$

  integer, intent(inout), dimension(ilg) :: imxl !< Variable description\f$[units]\f$

  real :: acor1
  real :: acort
  real :: aratio
  real :: buoy
  real :: csigma
  real :: denf1
  real :: denf2
  real :: dpbl
  real :: dq1
  real :: dq2
  real :: dqsum
  real :: drdqv
  real :: dst
  real :: dz
  real :: eta
  real :: eta1
  real :: expm1
  real :: fac
  integer :: icall
  integer :: icvsg
  integer :: idum
  integer :: il
  integer :: isubg
  integer :: l
  integer :: n
  real :: qcwe
  real :: qcwx
  real :: qiwc
  real :: qlwc
  real :: qtnadj
  real :: qv
  real :: qve
  real :: qvn
  real :: qvnef
  real :: rfx
  real :: rho
  real :: tn
  real :: tpot
  real :: tpv
  real :: tv
  real :: tvpot
  real :: ustaref
  real :: vptflx
  real :: weigh
  real :: xintef
  real :: yfr
  real :: zepclc
  real :: zsecfrl
  !
  !     internal work arrays.
  !
  real, dimension(ilg,ilev) :: wc !< Variable description\f$[units]\f$
  real, dimension(ilg,ilev) :: fcwgt !< Variable description\f$[units]\f$
  real, dimension(ilg) :: acor !< Variable description\f$[units]\f$
  real, dimension(ilg) :: qcwvar !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wcinv !< Variable description\f$[units]\f$
  real, dimension(ilg) :: qtnef !< Variable description\f$[units]\f$
  real, dimension(ilg) :: hmnnef !< Variable description\f$[units]\f$
  real, dimension(ilg) :: ustar !< Variable description\f$[units]\f$
  real, dimension(ilg) :: wstar !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: qcwcrit = 0.
  real, parameter :: tauprp = 1.0
  real, parameter :: ysec = 0.999
  !*oct19/28
  real, parameter :: cstar=0.6
  real, parameter :: alph=0.6
  !*oct19/28
  !
  real, parameter :: pz3 = 0.03
  !
  !=======================================================================
  !     * constants.
  !
  real, parameter :: zero = 0.
  real, parameter :: one = 1.
  !
  expm1=exp(-1.)
  csigma=0.2
  icvsg=0
  isubg=1
  yfr=0.50
  zsecfrl=1.e-7
  zepclc=1.e-3
  !
  !     * initial profiles of thermodynamic properties:
  !     * total water mixing ratio and liquid water static energy,
  !
  do l=msg+1,ilev
    do il=il1,il2
      qcwx=(xrow(il,l+1,ilwc)+xrow(il,l+1,iiwc))*dsr(il,l)
      qv=qh(il,l)*dsr(il,l)
      qt(il,l)=qv+qcwx
      dst=cpm(il,l)*th(il,l)+grav*z(il,l)
      hmn(il,l)=dst-rrl(il,l)*qcwx
      fcwgt(il,l)=0.
      wc(il,l)=pz3
    end do
  end do ! loop 50
  !
  !=======================================================================
  !     * determine depth of mixed layer from adjustment scheme for
  !     * mixed layer with fluxes across top and bottom layers.
  !
  l=ilev
  do il=il1,il2
    tvpot=th(il,l)*(1.+tvfa*qh(il,l))/shxkj(il,l)
    tvppbl(il)=tvpot*zf(il,l)
    hint(il)=hmn(il,l)*dshj(il,l)
    qint(il)=qt (il,l)*dshj(il,l)
    dsmix(il)=dshj(il,l)
    imxl(il)=l
    bcr(il)=9.e+20
    hmnmin(il)=hmn(il,l)
    qtmin(il)=qt(il,l)
    wcinv(il)=0.
    !*oct19
    hmnn(il)=hmn(il,l)
    qtn(il) =qt(il,l)
    ustar(il)=max(sqrt(cdm(il))*vmodl(il),pz3)
    !*oct19
  end do ! loop 100
  !
  !     * perform semi-implicit time integration to determine
  !     * thermodynamic properties at the top of the virtual mixed
  !     * layer. use buoyancy in that layer (relative to layer above)
  !     * to determine depth of mixed layer.
  !
  do l=ilev-1,msg+2,-1
    do il=il1,il2
      if (imxl(il)==(l+1) ) then
        !
        !         * adjustment time scale in mixed layer.
        !
        rho=100.*p(il,ilev)/(rgas*th(il,ilev))
        drdqv=1.
        rfx=sqflx(il)*drdqv/rho
        tpot=th(il,ilev)/shxkj(il,ilev)
        vptflx=(shflx(il)/(rho*cpm(il,ilev)))*(1.+tvfa*qh(il,ilev)) &
            +tvfa*rfx*tpot
        dpbl=zf(il,l+1)
        !         wstar(il)=max(grav*vptflx*dpbl/(tvppbl(il)/dpbl),0.)**(1./3.)
        wstar(il)=max(grav*vptflx*dpbl/(tvppbl(il)/dpbl),pz3**3)** &
               (1./3.)
        !*oct19          ustar=sqrt(cdm(il))*vmodl(il)
        !*fc         tau(il)=tauprp*dpbl/max(sqrt(0.6*wstar**2+ustar**2),pz3)
        !*oct19          wc(il,l+1)=max((ustar**3
        !*oct19     1                  +0.6*(z(il,l+1)/dpbl)*wstar**3)**(1./3.),pz3)
        !*oct19          wcinv(il)=wcinv(il)
        !*oct19     1                  +rgas*th(il,ilev)*dshj(il,l+1)/(grav*wc(il,l+1))
        !*oct19          tau(il)=tauprp*wcinv(il)
        !*oct19          fcwgt(il,l+1)=(1./wc(il,l+1)
        !*oct19     1                  -wcinv(il)*grav/(rgas*th(il,ilev)*dsmix(il)))

        !*oct20 revised definition of the convective velocity variable
        eta=min(zf(il,l+1)/dpbl,1.)
        eta1=eta**(1./3.)
        ustaref=sqrt(ustar(il)**2+0.55*cdm(il)*wstar(il)**2)
        aratio=0.4*ustaref/(3.*cstar*max(wstar(il),pz3))
        denf1=(1.+0.5*eta)/eta1
        denf2=log((1.+aratio*denf1)/(1.+alph*aratio*tanh(aratio)))
        wc(il,l+1)=cstar*wstar(il)*aratio/denf2
        !
        !         * liq. water static energy and total water at top of mixed layer.
        !
        fac=grav/(pressg(il)*dsmix(il))
        !*fc         weigh=min(max(tau(il)/(tau(il)+dt),zero),one)
        !*oct19          weigh=exp(-dt/tau(il))
        weigh=0.
        !*fc
        !*oct19          hmnmin(il)=min(hmnmin(il),hmn(il,l+1))
        !*oct19          thflxt=min(thflx(il),shflx(il)+ysec*hmnmin(il)/(dt*fac))
        !*oct19          qtmin(il)=min(qtmin(il),qt(il,l+1))
        !*oct19          tqflxt=min(tqflx(il),sqflx(il)+ysec*qtmin(il)/(dt*fac))
        hmnn(il)=weigh*hmn(il,l+1)+(1.-weigh)*hint(il)/dsmix(il)
        !*oct19     1            +dt*fac*(shflx(il)-thflxt)
        qtn (il)=weigh*qt (il,l+1)+(1.-weigh)*qint(il)/dsmix(il)
        !*oct19     1            +dt*fac*(sqflx(il)-tqflxt)
        if (qtn(il)<=0. ) call wrn('NLCLMXTKE',-1)
        !            qt(il,l)=max(qt(il,l),0.)
        !
        dz       =z   (il,l-1)-z  (il,l)
        dhldz(il)=(hmn(il,l-1)-hmn(il,l))/dz
        drwdz(il)=(qt (il,l-1)-qt (il,l))/dz
        !
        almix(il)=xlmtke(il,l)
        x(il)=1.
      else
        x(il)=0.
      end if
      !*fc
      !*oct19       qtnef (il)=qtn (il)+(1.-weigh)*sqflx(il)*fcwgt(il,l+1)
      !*oct19       hmnnef(il)=hmnn(il)+(1.-weigh)*shflx(il)*fcwgt(il,l+1)
      qtnef(il)=qtn  (il)+sqflx(il)/wc(il,l+1)
      hmnnef(il)=hmnn(il)+shflx(il)/wc(il,l+1)
    end do ! loop 125
    !
    idum=0
    icall=3
    call statcld5(qcw,zclf,sigma(1,l),zcraut,qcwvar,ssh, &
                     cvsg(1,l),qtnef,hmnnef,chi(1,l),cpm(1,l), &
                     p(1,l),z(1,l),rrl(1,l),zer,x, &
                     almix,dhldz,drwdz,idum, &
                     dt,ilev,ilg,il1,il2,icvsg,isubg, &
                     l,icall)
    !
    do il=il1,il2
      if (imxl(il)==(l+1) ) then
        !
        !         * cloud liquid and ice water contents from diagnostic
        !         * relationship.
        !
        qlwc=(1.-chi(il,l))*qcw(il)
        qiwc=chi(il,l)*qcw(il)
        !
        !         * adjusted water vapour mixing ratio and temperature.
        !
        qvn=qtnef(il)-qcw(il)
        tn =(hv*qlwc+hs*qiwc-grav*z(il,l)+hmnnef(il))/cpm(il,l)
        !
        !         * buoyancy for air at top of virtual mixed layer in ambient
        !         * air at that level.
        !
        qcwe=(xrow(il,l+1,ilwc)+xrow(il,l+1,iiwc))*dsr(il,l)
        qve=qh(il,l)*dsr(il,l)
        tv =th(il,l)*(1.+tvfa*qve-(1.+tvfa)*qcwe)
        tpv=tn      *(1.+tvfa*qvn-(1.+tvfa)*qcw(il) )
        buoy=grav*( tpv-tv)/tv
        !
        !         * test for buoyancy.
        !
        if (buoy>=0. .and. &
            .not.(buoy>bcr(il).and.qcw(il)>qcwcrit)   ) then
          imxl(il)=l
          !
          !         * vertically integrated liq. water energy and total water,
          !         * and depth of mixed layer.
          !
          tvpot=th(il,l)*(1.+tvfa*qh(il,l))/shxkj(il,l)
          tvppbl(il)=tvppbl(il)+tvpot*(zf(il,l)-zf(il,l+1))
          hint(il)=hint(il)+hmn(il,l)*dshj(il,l)
          qint(il)=qint(il)+qt (il,l)*dshj(il,l)
          dsmix(il)=dsmix(il)+dshj(il,l)
          bcr(il)=buoy
        end if
      end if
    end do ! loop 150
  end do ! loop 200
  !*oct19
  do l=ilev,msg+1,-1
    do il=il1,il2
      if (l>=imxl(il)) then
        eta=min(zf(il,l)/zf(il,imxl(il)),1.)
        eta1=eta**(1./3.)
        ustaref=sqrt(ustar(il)**2+0.55*cdm(il)*wstar(il)**2)
        aratio=0.4*ustaref/(3.*cstar*max(wstar(il),pz3))
        denf1=(1.+0.5*eta)/eta1
        denf2=log((1.+aratio*denf1)/(1.+alph*aratio*tanh(aratio)))
        wc(il,l)=cstar*wstar(il)*aratio/denf2
        wcinv(il)=wcinv(il) &
               +rgas*th(il,ilev)*dshj(il,l)/(grav*wc(il,l))
      end if
    end do
  end do ! loop 205
  !
  do il=il1,il2
    tau(il)=tauprp*wcinv(il)
  end do ! loop 206
  !*oct19
  !*fc
  !*fc update the free convective increment profile function.
  !*fc
  do l=ilev,msg+1,-1
    do il=il1,il2
      if (l>=imxl(il)) then
        fcwgt(il,l)=(1./wc(il,l) &
         -wcinv(il)*grav/(rgas*th(il,ilev)*dsmix(il)))
      end if
    end do
  end do ! loop 210
  !*fc
  !
  !     tracers.
  !
  do n=1,ntrac
    if (modl_tracers(n)%phs /=0 .or. n==ilwc .or. n==iiwc) then
      do il=il1,il2
        xint (il,n)=0.
        sxflx(il,n)=0.
      end do ! loop 420
      do l=ilev,msg+1,-1
        do il=il1,il2
          if (l>=imxl(il) ) then
            xint(il,n)=xint(il,n)+xrow(il,l+1,n)*dshj(il,l)
          end if
        end do
      end do ! loop 440
      do l=ilev,msg+1,-1
        do il=il1,il2
          if (l>=imxl(il) ) then
            !
            !             * perform semi-implicit time integration to determine
            !             * thermodynamic properties.
            !
            fac=grav/(pressg(il)*dsmix(il))
            !*fc          weigh=min(max(tau(il)/(tau(il)+dt),0.),1.)
            !*fc          xrow(il,l+1,n)=weigh*xrow(il,l+1,n)+(1.-weigh)*xint(il,n)
            !*fc     1                                /dsmix(il)+dt*fac*sxflx(il,n)
            weigh=exp(-dt/tau(il))
            xintef=xint(il,n)+ sxflx(il,n)*fcwgt(il,l)*dsmix(il)
            xrow(il,l+1,n)=weigh*xrow(il,l+1,n)+(1.-weigh)*xintef &
                                /dsmix(il)+dt*fac*sxflx(il,n)
          end if
        end do
      end do ! loop 460
    end if
  end do ! loop 400
  !
  !     * save cloud water/ice profiles after mixing as passive tracers.
  !
  if (isavls/=0) then
    do l=msg+1,ilev
      do il=il1,il2
        xlwrol(il,l)=xrow(il,l+1,ilwc)
        xicrol(il,l)=xrow(il,l+1,iiwc)
      end do
    end do ! loop 470
  end if
  !
  do il=il1,il2
    acor(il)=1.
  end do ! loop 480

  do l=ilev,msg+1,-1
    do il=il1,il2
      if (l>=imxl(il) ) then
        fac=grav/(pressg(il)*dsmix(il))
        !*fc      weigh=min(max(tau(il)/(tau(il)+dt),zero),one)
        weigh=exp(-dt/tau(il))
        dq1=dt*fac*sqflx(il)
        dq2=(1.-weigh)*(qint(il)/dsmix(il)-qt(il,l))
        dqsum=dq1+dq2
        if (dqsum+qt(il,l)<0.) then
          call wrn('NLCLMXTKE',-3)
          acort=-max(qt(il,l),0.)/dqsum
          acor1=max(min(acor(il),acort),0.)
          acor(il)=(3.-2.*acor1)*acor1**3
        end if
      end if
    end do
  end do ! loop 485
  !
  !=======================================================================
  !     * calculate new thermodynamic values in mixed layer for given
  !     * mixed layer depth and surface fluxes. effects of fluxes
  !     * across top of mixed layer are accounted for in parameterizations
  !     * of convection and therefore do not affect these calculation.
  !
  do l=ilev,msg+1,-1
    do il=il1,il2
      if (l>=imxl(il) ) then
        !
        !         * liq. water static energy and total water.
        !
        fac=grav/(pressg(il)*dsmix(il))
        !*fc      weigh=min(max(tau(il)/(tau(il)+dt),zero),one)
        weigh=exp(-dt/tau(il))
        hmnn(il)=weigh*hmn(il,l)+(1.-weigh)*hint(il)/dsmix(il) &
             +dt*fac*shflx(il)
        qtnadj=qt(il,l)+acor(il)*((1.-weigh)*(qint(il)/ &
            dsmix(il)-qt(il,l))+dt*fac*sqflx(il))
        qtn (il)=qtnadj
        if (qtn(il)<=0. ) call wrn('NLCLMXTKE',-2)
        !         qtn(il)=max(qtn(il),0.)
        !
        dz       =z   (il,l-1)-z  (il,l)
        dhldz(il)=(hmn(il,l-1)-hmn(il,l))/dz
        drwdz(il)=(qt (il,l-1)-qt (il,l))/dz
        almix(il)=xlmtke(il,l)
        x(il)=1.
      else
        x(il)=0.
      end if
      !*gc
      qtnef (il)=qtn (il)+(1.-weigh)*sqflx(il)*fcwgt(il,l)
      hmnnef(il)=hmnn(il)+(1.-weigh)*shflx(il)*fcwgt(il,l)
      !*fc
    end do ! loop 525
    !
    idum=0
    icall=4
    call statcld5(qcw,zclf,sigma(1,l),zcraut,qcwvar,ssh, &
                    cvsg(1,l),qtnef,hmnnef,chi(1,l),cpm(1,l), &
                    p(1,l),z(1,l),rrl(1,l),zer,x, &
                    almix,dhldz,drwdz,idum, &
                    dt,ilev,ilg,il1,il2,icvsg,isubg, &
                    l,icall)
    !
    do il=il1,il2
      !***
      cld_he(il,l)=zclf(il)
      !***
      if (l>=imxl(il) ) then
        !
        !         * cloud liquid and ice water contents from diagnostic
        !         * relationship.
        !
        qlwc=(1.-chi(il,l))*qcw(il)
        qiwc=chi(il,l)*qcw(il)
        xrow(il,l+1,ilwc)=qlwc/dsr(il,l)
        xrow(il,l+1,iiwc)=qiwc/dsr(il,l)
        !
        !         * adjusted water vapour mixing ratio and temperature.
        !
        !*fc      qvn=qtn(il)-qcw(il)
        !*fc      qh(il,l)=qvn/dsr(il,l)
        !*fc      th(il,l)=(hv*qlwc+hs*qiwc-grav*z(il,l)+hmnn(il))/cpm(il,l)
        !*fc
        weigh=exp(-dt/tau(il))
        qtnef (il)=qtn (il)+(1.-weigh)*sqflx(il)*fcwgt(il,l)
        hmnnef(il)=hmnn(il)+(1.-weigh)*shflx(il)*fcwgt(il,l)
        qvnef=qtnef(il)-qcw(il)
        qh(il,l)=qvnef/dsr(il,l)
        th(il,l)=(hv*qlwc+hs*qiwc-grav*z(il,l)+hmnnef(il))/cpm(il,l)
      end if
    end do ! loop 540
  end do ! loop 550
  !
  !     * save condensation/deposition tendencies.
  !
  if (isavls/=0) then
    do l=msg+1,ilev
      do il=il1,il2
        cndrol(il,l)=(xrow(il,l+1,ilwc)-xlwrol(il,l))/dt
        deprol(il,l)=(xrow(il,l+1,iiwc)-xicrol(il,l))/dt
      end do
    end do ! loop 600
  end if
  !
  return
  !-----------------------------------------------------------------------
end subroutine nlclmxtke
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
