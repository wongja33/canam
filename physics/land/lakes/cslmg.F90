!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cslmg (hlakgat,llakgat,blakgat,nlakgat, &
                  tlakgat,t0lakgat,hicelgat,hdpthgat, &
                  expwgat,dtmpgat,tkelgat,delugat,gredgat,rhomgat, &
                  snogat,rhongat,tngat,angat,wsnogat, &
                  fsvgat,fsigat,fdlgat,fsggat,flggat,cszgat, &
                  ulgat,vlgat,tagat,qagat,rhoagat,padrgat,presgat, &
                  zrfmgat,zrfhgat,zdmgat,zdhgat,rpcpgat, &
                  trpcgat,spcpgat,tspcgat,rhsigat,radjgat, &
                  asvdgat,asidgat,fsdbgat,fsfbgat,fssbgat, &
                  refgat,bcsngat,depbgat, &
                  ikmos,jkmos,nlakmax,nmk,nl,nm,ilg,nbs, &
                  hlakrow,llakrow,blakrow,nlakrow, &
                  tlakrow,t0lakrow,hicelrow,hdpthrow, &
                  expwrow,dtmprow,tkelrow,delurow,gredrow,rhomrow, &
                  snorot,rhonrot,tnrot,anrot,wsnorot, &
                  fsvrot,fsirot,fdlrot,fsgrot,flgrot,cszrow, &
                  ulrow,vlrow,tarow,qarow,rhoarow,padrrow,presrow, &
                  zrfmrow,zrfhrow,zdmrow,zdhrow,rpcprow, &
                  trpcrow,spcprow,tspcrow,rhsirow,radjrow, &
                  asvdrot,asidrot,fsdbrot,fsfbrot,fssbrot, &
                  refrot,bcsnrot,depbrow)
  !
  !     * dec 04/16 - m.lazare. - remove tile dimension for several lake fields.
  !     *                       - change "ROT" -> "ROW" for non-tiled fields.
  !     *                       - renamed lakeg-> cslmg.
  !     *                       - add "DEPB".
  !     * apr 13/15 - d.verseghy. local version for offline testing.
  !     * jan 18/12 - m.mackay.  gather operation for lake tile variables.
  !
  implicit none
  !
  integer, intent(in) :: nmk
  integer, intent(in) :: nl
  integer, intent(in) :: nm
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: k
  integer :: l
  integer, intent(in) :: nlakmax
  integer, intent(in) :: nbs  !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  !
  integer, intent(in), dimension(ilg) :: ikmos
  integer, intent(in), dimension(ilg) :: jkmos
  !
  !     * lake and snow fields.
  !
  real, intent(in), dimension(nl) :: hlakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: llakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: blakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: t0lakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: hicelrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: hdpthrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: expwrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: dtmprow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: tkelrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: delurow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: gredrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rhomrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: depbrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: nlakrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nlakmax) :: tlakrow !< Variable description\f$[units]\f$

  real, intent(in), dimension(nl,nm) :: snorot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: rhonrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: tnrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: anrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: wsnorot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsvrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsirot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fdlrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: fsgrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: flgrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: asvdrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: asidrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: refrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm) :: bcsnrot !< Variable description\f$[units]\f$
  !
  real, intent(inout), dimension(ilg) :: hlakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: llakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: blakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: t0lakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hicelgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: hdpthgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: expwgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: dtmpgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tkelgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: delugat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: gredgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhomgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: depbgat !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(ilg) :: nlakgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nlakmax) :: tlakgat !< Variable description\f$[units]\f$

  real, intent(inout), dimension(ilg) :: snogat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhongat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tngat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: angat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: wsnogat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fsvgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fsigat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fdlgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: fsggat !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: flggat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: asvdgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: asidgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: refgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: bcsngat !< Variable description\f$[units]\f$
  !
  !     * atmospheric and grid-constant input variables.
  !
  real, intent(in), dimension(nl) :: cszrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: ulrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: vlrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: tarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: qarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: presrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rhoarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: padrrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zrfmrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zrfhrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zdmrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: zdhrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rpcprow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: trpcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: spcprow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: tspcrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: rhsirow !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl) :: radjrow !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(nl,nm,nbs) :: fsdbrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: fsfbrot !< Variable description\f$[units]\f$
  real, intent(in), dimension(nl,nm,nbs) :: fssbrot !< Variable description\f$[units]\f$
  !
  real, intent(inout), dimension(ilg) :: cszgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: ulgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: vlgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: qagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: presgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhoagat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: padrgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zrfmgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zrfhgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zdmgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: zdhgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rpcpgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: trpcgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: spcpgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: tspcgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: rhsigat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: radjgat !< Variable description\f$[units]\f$
  !
  real, intent(inout), dimension(ilg,nbs) :: fsdbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nbs) :: fsfbgat !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nbs) :: fssbgat !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !----------------------------------------------------------------------
  if (nmk > 0) then
    !
    !       * gather lake relevant variables into
    !       * lake-tile gat arrays
    !
    do k = 1,nmk
      snogat (k) = snorot (ikmos(k),jkmos(k))
      rhongat(k) = rhonrot(ikmos(k),jkmos(k))
      tngat  (k) = tnrot  (ikmos(k),jkmos(k))
      angat  (k) = anrot  (ikmos(k),jkmos(k))
      asvdgat(k) = asvdrot(ikmos(k),jkmos(k))
      asidgat(k) = asidrot(ikmos(k),jkmos(k))
      refgat (k) = refrot (ikmos(k),jkmos(k))
      bcsngat(k) = bcsnrot(ikmos(k),jkmos(k))
      wsnogat(k) = wsnorot(ikmos(k),jkmos(k))
      fsvgat (k) = fsvrot (ikmos(k),jkmos(k))
      fsigat (k) = fsirot (ikmos(k),jkmos(k))
      fdlgat (k) = fdlrot (ikmos(k),jkmos(k))
      fsggat (k) = fsgrot (ikmos(k),jkmos(k))
      fsigat (k) = fsirot (ikmos(k),jkmos(k))
    end do ! loop 100
    !
    do k = 1,nmk
      hlakgat (k) = hlakrow (ikmos(k))
      llakgat (k) = llakrow (ikmos(k))
      blakgat (k) = blakrow (ikmos(k))
      nlakgat (k) = nint(nlakrow (ikmos(k)))
      t0lakgat(k) = t0lakrow(ikmos(k))
      hicelgat(k) = hicelrow(ikmos(k))
      hdpthgat(k) = hdpthrow(ikmos(k))
      expwgat (k) = expwrow (ikmos(k))
      dtmpgat (k) = dtmprow (ikmos(k))
      tkelgat (k) = tkelrow (ikmos(k))
      delugat (k) = delurow (ikmos(k))
      gredgat (k) = gredrow (ikmos(k))
      rhomgat (k) = rhomrow (ikmos(k))
      depbgat (k) = depbrow (ikmos(k))
      do l = 1,nlakgat(k)
        tlakgat(k,l) = tlakrow(ikmos(k),l)
      end do
    end do ! loop 200

    !       * atmospheric forcing variables needed for lake tiles
    !
    do k = 1,nmk
      cszgat (k) = cszrow (ikmos(k))
      ulgat  (k) = ulrow  (ikmos(k))
      vlgat  (k) = vlrow  (ikmos(k))
      tagat  (k) = tarow  (ikmos(k))
      qagat  (k) = qarow  (ikmos(k))
      rhoagat(k) = rhoarow(ikmos(k))
      padrgat(k) = padrrow(ikmos(k))
      presgat(k) = presrow(ikmos(k))
      zrfmgat(k) = zrfmrow(ikmos(k))
      zrfhgat(k) = zrfhrow(ikmos(k))
      zdmgat (k) = zdmrow (ikmos(k))
      zdhgat (k) = zdhrow (ikmos(k))
      rpcpgat(k) = rpcprow(ikmos(k))
      trpcgat(k) = trpcrow(ikmos(k))
      spcpgat(k) = spcprow(ikmos(k))
      tspcgat(k) = tspcrow(ikmos(k))
      rhsigat(k) = rhsirow(ikmos(k))
      radjgat(k) = radjrow(ikmos(k))
      do l = 1,nbs
        fsdbgat(k,l) = fsdbrot(ikmos(k),jkmos(k),l)
        fsfbgat(k,l) = fsfbrot(ikmos(k),jkmos(k),l)
        fssbgat(k,l) = fssbrot(ikmos(k),jkmos(k),l)
      end do
    end do ! loop 300
  end if

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
