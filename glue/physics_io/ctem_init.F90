subroutine ctem_init(nlu,newfrac,prefrac,c_clim_time, &
                     month,iyear,iday, &
                     lndcvryr1,lndcvryr2,lndcvrmod,lndcvr_offset, &
                     lctem,icc,ntld,nlon,nlat,ijpak,gg)
  !
  !     * feb 29, 2016 - m.lazare. new routine based on first part of ctem_init.dk
  !     *                          to get ctem land fractions for present and
  !     *                          next month.
  !     *                          as well, we calculate ctem time-counting variables
  !     *                          and place them in a common block passed
  !     *                          to routine intctemf.
  !
  implicit none
  !
  integer, intent(in) :: nlu ! i/o unit number for file that contains land cover + may be other variables
  !
  !     * output fields:
  !
  real, intent(inout), dimension(ijpak,ntld,icc) :: newfrac
  real, intent(in), dimension(ijpak,ntld,icc) :: prefrac
  real, intent(inout), dimension(3,2) :: c_clim_time
  !
  !     * rest.
  !
  integer, intent(in), dimension(icc) :: lctem

  integer*4 :: mynode
  common /mpinfo/ mynode
  !
  !     * i/o work array.
  !
  real, intent(in) :: gg( * )
  !
  integer, dimension(12) :: mid_month_days
  !
  integer, intent(in) :: iyear ! compressed time year    (... -2, -1, 0, 1, 2....)
  integer, intent(in) :: iday ! compressed time day     (1, 2, 3...   364, 365)  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: month ! extended time month     (1, 2, 3...  12)
  !
  integer, intent(in) :: lndcvryr1
  integer, intent(inout) :: lndcvryr2
  integer, intent(in) :: lndcvrmod
  integer, intent(in) :: lndcvr_offset
  integer :: monthl1
  integer :: tmestmp1
  integer :: tmestmp2
  integer :: nc4to8
  integer, intent(in) :: ijpak
  integer, intent(in) :: ntld
  integer :: k
  integer :: m
  integer, intent(in) :: nlon
  integer, intent(in) :: nlat
  integer, intent(in) :: icc
  !
  data mid_month_days /15,46,74,105,135,166,196,227,258,288,319, &
 349/
  !-----------------------------------------------------------------------------
  if (lndcvrmod == 12345) then
    lndcvryr2 = iyear + lndcvr_offset ! year for which we want to get the land cover
    !
    monthl1 = month - 1
    if (monthl1 /= 0) then
      tmestmp1 = lndcvryr2 * 100 + monthl1
      c_clim_time(1,1) = real(lndcvryr2)
      c_clim_time(2,1) = real(mid_month_days(monthl1))
      c_clim_time(3,1) = 0.0
    else if (monthl1 == 0) then
      tmestmp1 = (lndcvryr2 - 1) * 100 + 12 ! december of last year
      c_clim_time(1,1) = real(lndcvryr2 - 1)
      c_clim_time(2,1) = real(mid_month_days(12))
      c_clim_time(3,1) = 0.0
    end if
    tmestmp2 = lndcvryr2 * 100 + month
    c_clim_time(1,2) = real(lndcvryr2)
    c_clim_time(2,2) = real(mid_month_days(month))
    c_clim_time(3,2) = 0.0
  else
    !
    !        *  get land cover for the particular year, lndcvrmod. since land
    !        *  cover data are at monthly resolution we get the values for the
    !        *  1st of july for this year
    !
    lndcvryr2 = lndcvrmod
    !
    !        *  also both time stamps are same because when running ctem for a
    !        *  GIVEN YEAR OVER AND OVER AGAIN WE DON'T WANT ANY LAND USE CHANGE
    !        *  to be happening
    !
    tmestmp1 = lndcvryr2 * 100 + 7   ! 7 for july
    tmestmp2 = lndcvryr2 * 100 + 7
    !
    !        * HOWEVER, FOR INTERPOLATION USING WARREN'S SUBROUTINE I DO NEED
    !        * c_clim_time so lets set them as the start and end of the given
    !        * year
    !
    c_clim_time(1,1) = real(lndcvryr2)
    c_clim_time(2,1) = 1.0
    c_clim_time(3,1) = 0.0
    !
    c_clim_time(1,2) = real(lndcvryr2 + 1)
    c_clim_time(2,2) = 1.0
    c_clim_time(3,2) = 0.0

  end if
  !
  !     * if any time stamp is greater than 210012 than set it to
  !     * 210012 so that after 1st december, 2100 the land cover
  !     * DOESN'T CHANGE.
  !
  if (tmestmp1 > 210012) then
    tmestmp1 = 210012
  end if
  if (tmestmp2 > 210012) then
    tmestmp2 = 210012
  end if
  !
  !     * if any time stamp is less than 185001 then set it to
  !     * 185001 so that before 1st january, 1850 the land cover
  !     * used is that of 1850
  !
  if (tmestmp1 < 185001) then
    tmestmp1 = 185001
  end if
  if (tmestmp2 < 185001) then
    tmestmp2 = 185001
  end if
  !
  !     * extract land cover data for this and the next month
  !
  rewind nlu
  do k = 1, icc
    do m = 1, ntld
      call getggbx(prefrac(1,m,k),nc4to8("FRAC"),nlu,nlon,nlat, &
                   tmestmp1,lctem(k),gg)
    end do
  end do
  !
  if (lndcvrmod == 12345 .and. tmestmp1 /= tmestmp2) then
    do k = 1, icc
      do m = 1, ntld
        call getggbx(newfrac(1,m,k),nc4to8("FRAC"),nlu,nlon,nlat, &
                     tmestmp2,lctem(k),gg)
      end do
    end do
  else
    newfrac(:,:,:) = prefrac(:,:,:)
  end if

  return
end
