!> !> !! !! !
subroutine init_snow_rt_lut_gem(F_iu, mynode)

  !
  !     * Jun 04/2018 - Y.Jiao. Modified to be used by GEM4
  !     * Feb 10/2015 - J.Cole. New version for gcm18:
  !                             - NBC increased from 12 to 20.
  !     * JAN 24/2013 - J.COLE. Previous version for gcm17:
  !                    - READ IN SNOW ALBEDO AND TRANSMISSIVITY LOOK UP TABLE
  !                      REQUIRED FOR NEW ALBEDO PARAMETERIZATIONS.

  implicit none

  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ! this subroutine reads in a single cccma formatted file that contains
  ! lookup tables for each band in the cccma solar radiative transfer model
  ! for snow albedo and tranmissivity.  for both variables there are two tables
  ! one for incident direct beam radiation and diffuse radiation.  the
  ! tables are a function of the cosine of solar zenith angle, underlying
  ! surface albedo, snow water equivalent, snow grain size and black carbon
  ! concentration.  the data in the file has been unrolled into a 1xn vector
  ! of values which needs to be reshaped into five dimensional arrays.
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  integer, intent(in) :: F_iu
  integer, intent(in) :: mynode

  !--- table dimensions
  ! nsmu ... number of cosine of the solar zenith angle
  ! nsfa ... number of surface albedos
  ! nbc  ... number of black carbon concentrations
  ! nsgs ... number of snow grain sizes
  ! nswe ... number of snow water equivalents
  ! nbnd ... number of solar radiation bands

  integer, parameter :: nsmu = 10,  &
                        nsfa = 11,  &
                        nbc  = 20,  &
                        nsgs = 10,  &
                        nswe = 11,  &
                        nbnd = 4,   &
                        irec = nsmu*nsfa*nbc*nsgs*nswe

  integer :: i, ier,ni,nj,nk
  integer, external :: fstlir, fstopc

  logical :: ok

  integer :: snow_alb_lut_init,snow_tran_lut_init

  character(len=4) :: cnam
  character(len=1) :: ich

  ! Need four look up tables.
  real, dimension(nbc,nswe,nsgs,nsmu,nsfa,nbnd) :: &
                 albdif, albdir, trandif, trandir

  common /snowalblut/ albdif,albdir,snow_alb_lut_init
  common /snowtranlut/ trandif,trandir,snow_tran_lut_init

  snow_alb_lut_init  = 0
  snow_tran_lut_init = 0

  ! Read the albedo lookup tables
  if (mynode.eq.0) then
    write(6,*)'INIT_SNOW_SURFACE_ALBEDO: Initializing snow albedo' &
              ,' lookup table with data from a file.'
  endif

  ier = fstopc('MSGLVL', 'WARNIN', 0)

  !.....Read the albedo lookup table
  do i=1,nbnd
     write(ich, '(i1)') i

     cnam = "ADF" // ich
     ier = fstlir( albdif(1,1,1,1,1,i), &
                   F_iu,ni,nj,nk,-1,'SNOW_ALBEDO', &
                   -1,-1,-1,' ',cnam )
     if ((ier < 0) .or. (ni*nj*nk /= irec)) &
        call xit('INIT_SNOW_RT_LUT', -2)

     cnam = "ADR" // ich
     ier = fstlir( albdir(1,1,1,1,1,i), &
                   F_iu,ni,nj,nk,-1,'SNOW_ALBEDO', &
                   -1,-1,-1,' ',cnam )
     if ((ier < 0) .or. (ni*nj*nk /= irec)) &
        call xit('INIT_SNOW_RT_LUT', -3)

     cnam = "TDF" // ich
     ier = fstlir( trandif(1,1,1,1,1,i), &
                   F_iu,ni,nj,nk,-1,'SNOW_ALBEDO', &
                   -1,-1,-1,' ',cnam )
     if ((ier < 0) .or. (ni*nj*nk /= irec)) &
        call xit('INIT_SNOW_RT_LUT', -4)

     cnam = "TDR" // ich
     ier = fstlir( trandir(1,1,1,1,1,i), &
                   F_iu,ni,nj,nk,-1,'SNOW_ALBEDO', &
                   -1,-1,-1,' ',cnam )
     if ((ier < 0) .or. (ni*nj*nk /= irec)) &
        call xit('INIT_SNOW_RT_LUT', -5)
  enddo

  snow_alb_lut_init  = 1
  snow_tran_lut_init = 1

  return
end subroutine init_snow_rt_lut_gem
!> !> !! !! ! !! ! !! ! !! ! !! ! !! !
