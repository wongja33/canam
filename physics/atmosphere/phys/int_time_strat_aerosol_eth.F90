!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine int_time_strat_aerosol_eth(sw_ext_sa_row, & ! output
                                      sw_ssa_sa_row, &
                                      sw_g_sa_row, &
                                      lw_ext_sa_row, &
                                      lw_ssa_sa_row, &
                                      w055_ext_sa_row, &
                                      w110_ext_sa_row, &
                                      pressure_sa_row, &
                                      sw_ext_sa_rol, & ! input
                                      sw_ssa_sa_rol, &
                                      sw_g_sa_rol, &
                                      lw_ext_sa_rol, &
                                      lw_ssa_sa_rol, &
                                      w055_ext_sa_rol, &
                                      w110_ext_sa_rol, &
                                      pressure_sa_rol, &
                                      ilg, &
                                      il1, &
                                      il2, &
                                      nlev, &
                                      nbs, &
                                      nbl, &
                                      delt, &
                                      gmt, &
                                      iday, &
                                      mday)

  !
  !     * jan 10/17 - j. cole
  !
  !     * interpolate stratospheric aerosol data from ethz down to
  !     * current timestep.
  !
  !     method:
  !
  !     basically a simple linear average in time.  for the optical properties
  !     need to average using the approach of xxx to maintain similarity of
  !     radiative transfer solutions.

  implicit none

  ! parameters
  real, parameter :: sec_per_day = 86400.0
  real, parameter :: day_per_year = 365.0

  ! input

  real, intent(in) , dimension(ilg,nlev,nbs) :: sw_ext_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev,nbs) :: sw_ssa_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev,nbs) :: sw_g_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev,nbl) :: lw_ext_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev,nbl) :: lw_ssa_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev) :: w055_ext_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev) :: w110_ext_sa_rol !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,nlev) :: pressure_sa_rol !< Variable description\f$[units]\f$

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: nlev !< Variable description\f$[units]\f$
  integer, intent(in) :: nbs   !< Number of wavelength intervals for solar radiative transfer \f$[unitless]\f$
  integer, intent(in) :: nbl   !< Number of wavelength intervals for thermal radiative transfer \f$[unitless]\f$
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: mday   !< Julian calendar day of next mid-month \f$[day]\f$

  real, intent(in) :: delt   !< Timestep for atmospheric model \f$[seconds]\f$
  real, intent(in) :: gmt   !< Real value of number of elapsed seconds in current day \f$[seconds]\f$

  ! output

  real, intent(out) , dimension(ilg,nlev,nbs) :: sw_ext_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev,nbs) :: sw_ssa_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev,nbs) :: sw_g_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev,nbl) :: lw_ext_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev,nbl) :: lw_ssa_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev) :: w055_ext_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev) :: w110_ext_sa_row !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,nlev) :: pressure_sa_row !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  ! local

  integer :: il
  integer :: ilev
  integer :: ib

  real :: e
  real :: e1
  real :: e2
  real :: s
  real :: s1
  real :: s2
  real :: g
  real :: g1
  real :: g2
  real :: w1
  real :: w2
  real :: sec0
  real :: fmsec
  real :: secsm

  !
  ! compute the number of timestesps from here to mday
  !

  sec0  = real(iday) * sec_per_day + gmt
  fmsec = real(mday) * sec_per_day

  if (fmsec < sec0) fmsec = fmsec + day_per_year * sec_per_day

  secsm = fmsec - sec0

  w1 = (secsm - delt)/secsm
  w2 = delt/secsm
  !
  ! do the interpolation
  !

  ! solar

  do ib = 1, nbs
    do ilev = 1, nlev
      do il = il1, il2

        e1 = sw_ext_sa_row(il,ilev,ib)
        e2 = sw_ext_sa_rol(il,ilev,ib)
        s1 = sw_ssa_sa_row(il,ilev,ib)
        s2 = sw_ssa_sa_rol(il,ilev,ib)
        g1 = sw_g_sa_row(il,ilev,ib)
        g2 = sw_g_sa_rol(il,ilev,ib)

        g = w1 * e1 * s1 * g1 + w2 * e2 * s2 * g2
        s = w1 * e1 * s1 + w2 * e2 * s2
        e = w1 * e1 + w2 * e2

        sw_g_sa_row(il,ilev,ib)   = g/s
        sw_ssa_sa_row(il,ilev,ib) = s/e
        sw_ext_sa_row(il,ilev,ib) = e

      end do ! il
    end do ! ilev
  end do ! ib

  ! thermal

  do ib = 1, nbl
    do ilev = 1, nlev
      do il = il1, il2

        e1 = lw_ext_sa_row(il,ilev,ib)
        e2 = lw_ext_sa_rol(il,ilev,ib)
        s1 = lw_ssa_sa_row(il,ilev,ib)
        s2 = lw_ssa_sa_rol(il,ilev,ib)

        s = w1 * e1 * s1 + w2 * e2 * s2
        e = w1 * e1 + w2 * e2

        lw_ssa_sa_row(il,ilev,ib) = s/e
        lw_ext_sa_row(il,ilev,ib) = e

      end do ! il
    end do ! ilev
  end do ! ib

  ! diagnostics

  do ilev = 1, nlev
    do il = il1, il2

      w055_ext_sa_row(il,ilev) = w1 * w055_ext_sa_row(il,ilev) &
                                 + w2 * w055_ext_sa_rol(il,ilev)

      w110_ext_sa_row(il,ilev) = w1 * w110_ext_sa_row(il,ilev) &
                                 + w2 * w110_ext_sa_rol(il,ilev)

      pressure_sa_row(il,ilev) = w1 * pressure_sa_row(il,ilev) &
                                 + w2 * pressure_sa_rol(il,ilev)

    end do ! il
  end do ! ilev

  return

end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
