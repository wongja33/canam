!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine dyncal4d (ruj, rvj, rtj, rqj, rpsj, rxj, xuj, xvj, &
                    ej, tuj, tvj, quj, qvj, &
                    ni2, ni, nk, nm, itrac, ntrac, &
                    zj, dj, uj, vj, tj, qj, psj, pslmj, psthj, xj, &
                    vgrpsj, sgdtjk, ssdrjk, phijk, &
                    omspj, hsj, dbar, ubar, vbar, &
                    dsgj, dshj, dlnsgj, d1sgj, &
                    a1sgj, b1sgj, d2sgj, a2sgj, b2sgj, &
                    db, d2b, fj, r, rv, cpd, cpv, grav, jlat)
  !
  !     * feb 04/2004 - m.lazare.   new version for gcm13d onward, which
  !     *                           uses additional work arrays "DBAR",
  !     *                           "UBAR" and "VBAR" to eliminate
  !     *                           unnecessary repeated calculations near
  !     *                           the end of this routine.
  !     * jan 08/2004 - m.lazare.   final installed version for ibm (dyncal4c):
  !     *                           - "ISTR" added to "INTVLS" common block.
  !     *                           - modify lengths of character strings
  !     *                             passed in prntrow calls to conform
  !     *                             to proper length expected by the
  !     *                             character declarations in the subroutine,
  !     *                             so that the printout will not be garbled.
  !     *                           - "FIZDYN" option removed (including
  !     *                             called common decks) and hence
  !     *                             "SHUMREF" and "MOIST" in call as well.
  !     * jul 10/2003 - m.lazare.   new working version for ibm (dyncal4b).
  !     *                           - add use of "DYN_PAKFLDS_13B" and
  !     *                             "DYNTEND_ROWFLDS_13B" modules, along
  !     *                             with "PACK_FIZDYN_13B" common deck.
  !     *                           - pass in "SHUMREF", "PSJ" and do all
  !     *                             "DYNTEND" calculations, including "DQDSROW"
  !     *                             and zeroing of missing moisture levels,
  !     *                             in this routine.
  !     *                           - "FIZDYN"->"DYNTEND".
  !     *                           - "NC4TO8" replaces holleriths and implicit
  !     *                             statement for f90 added.
  !     * may 15/96 - m.lazare.     previous version dyncal4 for gcm13a.
  !     *                           - dynamics for moisture variable now
  !     *                             only done if nm/=0 (i.e. bracketed
  !     *                             by "IF" blocks in 3 places, for s/l).
  !     * jan 10/94 - m.lazare.     - wrow only saved for fizdyn option.
  !     *                           - calls new comfizd2 which has
  !     *                             wrow dimension added.
  !     * dec 27/90 - m.lazare.     previous version dyncal3.

  !     * dynamique du modele spectral semi-implicite,
  !     * aux elements finis constants par morceaux.
  !     * calcul de la contribution des termes non-lineaires
  !     * aux cotes droits des equations pour la latitude j.
  !     * toutes les variables impliquees sont definees au temps t.

  !     * the "N"-tracer formulation involves exterior looping over
  !     * 1,n tracers for all necessary calculations.

  !     * right-hand sides are initialized from physics tendencies.
  !     * the right-hand sides are calculated for the momentum (u,v)
  !     * equations; conversion to vorticity and divergence is
  !     * done during the spectral transform.

  !     * nk         : nombre de couches.
  !     * NM         : NOMBRE DE COUCHES D'HUMIDITE.
  !     * ni         : nombre de longitudes.
  !     * ni2        : longueur des vecteurs

  !     * fj         : parametre de coriolis.
  !     * R          : CONSTANTE DES GAZ POUR L'AIR SEC.
  !     * RV         : CONSTANTE DES GAZ POUR LA VAPEUR D'EAU.
  !     * CPD        : CHALEUR SPECIFIQUE DE L'AIR SEC.
  !     * CPV        : CHALEUR SPECIFIQUE DE LA VAPEUR D'EAU.
  !     * grav       : gravitational constant.
  !     * moist      : logical :: switch indicating moisture variable.
  !     * jlat       : gaussian latitude loop index for debugging.

  !     * on calcule les quantites:                                couche
  !     * ------------------------                           (g: vents)
  !     *                                                    (h: thermo.)

  !     * ruj(i,k)   : partie du cote droit, eqn du vent zonal.          g
  !     * rvj(i,k)   : partie du cote droit, eqn du vent meridional.     g
  !     * rtj(i,k)   : partie du cote droit, eqn thermodynamique.        h
  !     * RQJ(I,KM)  : PARTIE DU COTE DROIT, EQUATION DE L'HUMIDITE.     H
  !     * rxj(i,k,n) : partie du cote droit, eqn du traceur.             h
  !     * RPSJ(I)    : COTE DROIT DE L'EQUATION DE CONTINUITE.
  !     * EJ(I,K)    : TERMES D'ENERGIE DANS L'EQN DE LA DIVERGENCE.     G
  !     * xuj(i,k,n) : le produit  x*u .                                 h
  !     * xvj(i,k,n) : le produit  x*v .                                 h
  !     * tuj(i,k)   : le produit  t*u .                                 h
  !     * tvj(i,k)   : le produit  t*v .                                 h
  !     * quj(i,km)  : le produit  q*u .                                 h
  !     * qvj(i,km)  : le produit  q*v .                                 h
  !     * omspj(i,k) : omega sur p, moyenne de couche                    h

  !     * A PARTIR DES DONNEES D'ENTREE:
  !     * -----------------------------

  !     * zj(i,k)    : tourbillon relatif a la latitude j.               g
  !     * dj(i,k)    : divergence a la latitude j.                       g
  !     * uj(i,k)    : vent zonal a la latitude j.                       g
  !     * vj(i,k)    : vent meridional a la latitude j.                  g
  !     * tj(i,k)    : temperature a la latitude j.                      h
  !     * QJ(I,KM)   : VARIABLE D'HUMIDITE A LA LATITUDE J.              H
  !     * xj(i,k,n)  : f(traceur) a la latitude j .                      h
  !     * hsj(i,k)   : humidite specific a la latitude j.                h
  !     * psj(i)     : log de la pression de surface a la latitude j
  !     * pslmj(i)   : derivee longitudinale a la latitude j
  !     *              du log de la pression de surface (lps) .
  !     * psthj(i)   : derivee latitudinale a la latitude j
  !     *              du log de la pression de surface (lps) .

  !     * on se sert des champs de travail suivants:
  !     * -----------------------------------------

  !     * vgrpsj(i,k): v.grad(lps) .
  !     * dbar(i,k)  : mean divergence                                   g
  !     * ubar(i,k)  : mean u-wind                                       g
  !     * vbar(i,k)  : mean v-wind                                       g
  !     * sgdtjk(i)  : sigma dot = eta dot * (d p/d eta) / pres.surf.
  !     *              a la base des couches du vent                     g
  !     * shdtjki    : sigma dot = eta dot * (d p/d eta) / pres.surf.
  !     *              a la base des couches thermodynamiques            h
  !     * ssdrjk(i)  : somme des couches 1 a k de "D ROND" * dsigma.     g
  !     * phijk(i)   : somme des couches k a nk de r*t*dlnsg.            h

  !     * on se sert aussi des parametres suivants:
  !     * ----------------------------------------

  !     * dsgj(i,k)     : epaisseur sigma des couches du vents.          g
  !     * dshj(i,k)     : epaisseur sigma des couches pour t et q.       h
  !     * dlnsgj(i,k)   : d ln(sigma) des couches thermodynamiques.      h
  !     * d1sgj(i,k)    : premiere demi-couche
  !     * a1sgj(i,k)    : parametre alpha1/pres.surf.
  !     * b1sgj(i,k)    : parametre beta1
  !     * d2sgj(i,k)    : deuxieme demi-couche
  !     * a2sgj(i,k)    : parametre alpha2/pres.surf.
  !     * b2sgj(i,k)    : parametre beta2
  !     *
  !     * k-1/2 ---------------------
  !     *            .
  !     *            .        d2sg
  !     *            .            k
  !     *            v        ------------------ k-1/2
  !     *             k                  .
  !     *            .        d1sg       .
  !     *            .            k      .
  !     * k+1/2 ---------------------    t
  !     *            .                    k
  !     *            .        d2sg       .
  !     *            .            k+1    .
  !     *            v        ------------------ k+1/2
  !     *             k+1                .
  !     *            .        d1sg       .
  !     *            .            k+1    .
  !     * k+3/2 ---------------------    t
  !     *                                 k+1
  !     *                     d2sg       .
  !     *                         k+2    .
  !     *                     ------------------ k+3/2
  !     *

  implicit none
  real, intent(in) :: cpd
  real, intent(in) :: cpv
  real, intent(in) :: fj
  real, intent(in) :: grav  !< Gravity on Earth \f$[m s^{-2}]\f$
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(in) :: jlat
  integer, intent(in) :: ni
  integer, intent(in) :: ni2
  integer, intent(in) :: nk
  integer, intent(in) :: nm
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: r
  real, intent(in) :: rv
  !     * sorties:

  real, intent(inout), dimension(ni2,nk) :: ruj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: rvj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: rtj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nm) :: rqj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2) :: rpsj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk,ntrac) :: rxj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: ej !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: tuj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: tvj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nm) :: quj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nm) :: qvj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk,ntrac) :: xuj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk,ntrac) :: xvj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: omspj !< Variable description\f$[units]\f$

  !     * entrees:

  real, intent(in), dimension(ni2,nk) :: zj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: dj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: uj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: vj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: tj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nm) :: qj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: hsj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk,ntrac) :: xj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2) :: psj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2) :: pslmj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2) :: psthj !< Variable description\f$[units]\f$

  !     * champs de travail:

  real, intent(inout), dimension(ni2,nk) :: vgrpsj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: dbar !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: ubar !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2,nk) :: vbar !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2) :: sgdtjk !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2) :: ssdrjk !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ni2) :: phijk !< Variable description\f$[units]\f$

  !     * parametres de la discretisation verticale:

  real, intent(in), dimension(ni2,nk) :: dsgj   !< Thickness of momentum layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ni2,nk) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ni2,nk) :: dlnsgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: d1sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: a1sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: b1sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: d2sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: a2sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(ni2,nk) :: b2sgj !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: db !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: d2b !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: drjki
  real :: drjkpi
  integer :: i
  integer :: iclw
  integer :: icsw
  integer :: iepr
  integer :: isbeg
  integer :: isen
  integer :: isgg
  integer :: israd
  integer :: issp
  integer :: istr
  integer :: jlatpr
  integer :: k
  integer :: km
  integer :: msg
  integer :: n
  real :: qijk
  real :: qijkm1
  real :: ra1kv
  real :: ra2kv
  real :: rb1ki
  real :: rb2ki
  real :: rdlnk
  real :: rscpv
  real :: shdtjki

  common /intvls/ issp,isgg,israd,isbeg,isen,istr,iepr,icsw,iclw
  !-----------------------------------------------------------------------
  msg = nk - nm
  !
  !-----------------------------------------------------------------------
  !            etape 1 :  initialisation de divers champs.
  !     ------------------------------------------------------------------

  do i = 1,ni
    ssdrjk (i)= 0.
    sgdtjk (i)= 0.
    rpsj   (i)= 0.
    phijk  (i)= 0.
  end do ! loop 1001

  !     -----------------------------------------------------------------
  !            etape 2 :    calcul de rps
  !     -----------------------------------------------------------------

  do k = 1,nk
    do i = 1,ni

      vgrpsj(i,k) =  uj(i,k)*pslmj(i) + vj(i,k)*psthj(i)
      drjki       =  dj(i,k) + vgrpsj(i,k)*db(k)/dsgj(i,k)
      rpsj(i)     =  rpsj(i) - drjki*dsgj(i,k)

    end do
  end do ! loop 2001

  !     -----------------------------------------------------------------
  !            etape 3a:    contribution de sigma dot dans ru,rv,rt.
  !     -----------------------------------------------------------------

  do k=1,nk-1
    do i=1,ni

      sgdtjk(i) = sgdtjk(i)-(rpsj(i)+vgrpsj(i,k))*db(k) &
                        -dj(i,k)*dsgj(i,k)

      ruj(i,k)  = ruj(i,k) &
                        - (sgdtjk(i)*(uj(i,k+1)-uj(i,k)))/(2.*dsgj(i,k))
      ruj(i,k+1)= ruj(i,k+1) &
                        - (sgdtjk(i)*(uj(i,k+1)-uj(i,k)))/(2.*dsgj(i,k+1))

      rvj(i,k)  = rvj(i,k) &
                        - (sgdtjk(i)*(vj(i,k+1)-vj(i,k)))/(2.*dsgj(i,k))
      rvj(i,k+1)= rvj(i,k+1) &
                        - (sgdtjk(i)*(vj(i,k+1)-vj(i,k)))/(2.*dsgj(i,k+1))

      shdtjki   = sgdtjk(i)-(rpsj(i)+vgrpsj(i,k+1))*d2b(k+1) &
                        -dj(i,k+1)*d2sgj(i,k+1)

      rtj(i,k)  = rtj(i,k) &
                        -   (shdtjki*(tj(i,k+1)-tj(i,k)))/(2.*dshj(i,k))

      rtj(i,k+1)= rtj(i,k+1) &
                        -   (shdtjki*(tj(i,k+1)-tj(i,k)))/(2.*dshj(i,k+1))

    end do
  end do ! loop 3001

  !     -----------------------------------------------------------------
  !            etape 3b:    contribution de sigma dot dans rq.
  !     -----------------------------------------------------------------

  if (nm>0) then

    do i=1,ni
      sgdtjk(i) = 0.
    end do ! loop 3101

    do km=1,nm-1
      k = km+(nk-nm)
      do i=1,ni

        sgdtjk(i) = sgdtjk(i)-(rpsj(i)+vgrpsj(i,k))*db(k) &
                        -dj(i,k)*dsgj(i,k)
        shdtjki   = sgdtjk(i)-(rpsj(i)+vgrpsj(i,k+1))*d2b(k+1) &
                        -dj(i,k+1)*d2sgj(i,k+1)

        rqj(i,km)  = rqj(i,km) &
                         -(shdtjki*(qj(i,km+1)-qj(i,km)))/(2.*dshj(i,k))

        rqj(i,km+1)= rqj(i,km+1) &
                         -(shdtjki*(qj(i,km+1)-qj(i,km)))/(2.*dshj(i,k+1))


      end do
    end do ! loop 3301

  end if
  !     ------------------------------------------------------------------
  !            etape 3c:     contribution de sigma dot dans rx.
  !     ------------------------------------------------------------------

  if (itrac/=0) then

    do n=1,ntrac
      do i=1,ni
        sgdtjk(i) = 0.
      end do ! loop 3303
      do k=1,nk-1
        do i=1,ni

          sgdtjk(i) = sgdtjk(i)-(rpsj(i)+vgrpsj(i,k))*db(k) &
                        -dj(i,k)*dsgj(i,k)
          shdtjki   = sgdtjk(i)-(rpsj(i)+vgrpsj(i,k+1))*d2b(k+1) &
                        -dj(i,k+1)*d2sgj(i,k+1)

          rxj(i,k,n)  = rxj(i,k,n)-(shdtjki* &
                          (xj(i,k+1,n)-xj(i,k,n)))/(2.*dshj(i,k))
          rxj(i,k+1,n)= rxj(i,k+1,n)-(shdtjki* &
                          (xj(i,k+1,n)-xj(i,k,n)))/(2.*dshj(i,k+1))

        end do
      end do
    end do ! loop 3304

  end if

  !     -----------------------------------------------------------------
  !      etape 4 :    calcul de omspj (omega sur p) and diagnostic w
  !     -----------------------------------------------------------------

  do k=1,nk-1
    do i = 1,ni

      drjki      =  dj(i,k) + vgrpsj(i,k)*db(k)/dsgj(i,k)
      drjkpi     =  dj(i,k+1) + vgrpsj(i,k+1)*db(k+1)/dsgj(i,k+1)

      omspj(i,k) = ( - ssdrjk(i)*dlnsgj(i,k) &
                         - drjki*a1sgj(i,k) - drjkpi*a2sgj(i,k+1) &
                         + vgrpsj(i,k)*b1sgj(i,k) &
                         + vgrpsj(i,k+1)*b2sgj(i,k+1) )/dshj(i,k)
      ssdrjk(i)  = ssdrjk(i) +drjki*dsgj(i,k)

    end do
  end do ! loop 4001

  !     -----------------------------------------------------------------

  k=nk
  do i = 1,ni

    drjki      =  dj(i,k) + vgrpsj(i,k)*db(k)/dsgj(i,k)

    omspj(i,k) = ( - ssdrjk(i)*dlnsgj(i,k) - drjki*a1sgj(i,k) &
                         + vgrpsj(i,k)*b1sgj(i,k) )/dshj(i,k)
  end do ! loop 4002

  !     -----------------------------------------------------------------
  !            etape 5 : 1. contribution des autres termes dans ru,rv
  !                      2. contribution du terme adiabatique dans rt
  !     -----------------------------------------------------------------

  k=1
  do i = 1,ni

    qijk  = hsj(i,k)
    rscpv = (r+(rv-r)*qijk)/(cpd+(cpv-cpd)*qijk)
    rb1ki = (r+(rv-r)*qijk)*(b1sgj(i,k)/dsgj(i,k))

    ruj(i,k)   = ruj(i,k) +(zj(i,k)+fj)*vj(i,k) &
                         - rb1ki*tj(i,k)*pslmj(i)

    rvj(i,k)   = rvj(i,k) -(zj(i,k)+fj)*uj(i,k) &
                         - rb1ki*tj(i,k)*psthj(i)

    rtj(i,k)   = rtj(i,k) + rscpv*tj(i,k)*omspj(i,k)

  end do ! loop 5001

  !     -----------------------------------------------------------------

  do k=2,nk
    do i = 1,ni

      qijk  = hsj(i,k)
      qijkm1= hsj(i,k-1)
      rscpv = (r+(rv-r)*qijk)/(cpd+(cpv-cpd)*qijk)
      rb1ki = (r+(rv-r)*qijk)*(b1sgj(i,k)/dsgj(i,k))
      rb2ki = (r+(rv-r)*qijkm1)*(b2sgj(i,k)/dsgj(i,k))

      ruj(i,k)   = ruj(i,k) +(zj(i,k)+fj)*vj(i,k) &
                         -(rb2ki*tj(i,k-1)+ rb1ki*tj(i,k))*pslmj(i)

      rvj(i,k)   = rvj(i,k) -(zj(i,k)+fj)*uj(i,k) &
                         -(rb2ki*tj(i,k-1)+ rb1ki*tj(i,k))*psthj(i)

      rtj(i,k)   = rtj(i,k) + rscpv*tj(i,k)*omspj(i,k)

    end do
  end do ! loop 5002
  !     -----------------------------------------------------------------
  !            etape 6 :    contribution de td et qd dans rt et rq
  !                         et de xd dans rx.
  !                         calcul de tu,qu,tv,qv
  !                         et de xu,xv.
  !     -----------------------------------------------------------------
  !
  !     * first generate dbar,ubar,vbar arrays which are common to the
  !     * ensuing set of calculations.
  !
  do k=1,nk-1
    do i=1,ni
      dbar(i,k) = (dj(i,k)*d1sgj(i,k)+dj(i,k+1)*d2sgj(i,k+1))/dshj(i,k)
      ubar(i,k) = (uj(i,k)*d1sgj(i,k)+uj(i,k+1)*d2sgj(i,k+1))/dshj(i,k)
      vbar(i,k) = (vj(i,k)*d1sgj(i,k)+vj(i,k+1)*d2sgj(i,k+1))/dshj(i,k)
    end do
  end do
  !
  k=nk
  do i = 1,ni
    dbar(i,k) = dj(i,k)*d1sgj(i,k)/dshj(i,k)
    ubar(i,k) = uj(i,k)*d1sgj(i,k)/dshj(i,k)
    vbar(i,k) = vj(i,k)*d1sgj(i,k)/dshj(i,k)
  end do
  !
  do k=1,nk-1
    do i=1,ni
      rtj(i,k) = rtj(i,k) + tj(i,k)*dbar(i,k)
      tuj(i,k) = tj(i,k)*ubar(i,k)
      tvj(i,k) = tj(i,k)*vbar(i,k)
    end do
  end do ! loop 6001

  !     -----------------------------------------------------------------
  if (nm>0) then
    do km=1,nm-1
      k = km+(nk-nm)
      do i=1,ni
        rqj(i,km) = rqj(i,km) + qj(i,km)*dbar(i,k)
        quj(i,km) = qj(i,km)*ubar(i,k)
        qvj(i,km) = qj(i,km)*vbar(i,k)
      end do
    end do ! loop 6002
  end if
  !     -----------------------------------------------------------------

  k=nk
  do i = 1,ni
    rtj(i,k) = rtj(i,k) + tj(i,k)*dbar(i,k)
    tuj(i,k) = tj(i,k)*ubar(i,k)
    tvj(i,k) = tj(i,k)*vbar(i,k)
  end do ! loop 6003

  !     -----------------------------------------------------------------
  if (nm>0) then
    km = nm
    k  = nk
    do i = 1,ni
      rqj(i,km)  = rqj(i,km) + qj(i,km)*dbar(i,k)
      quj(i,km)  = qj(i,km)*ubar(i,k)
      qvj(i,km)  = qj(i,km)*vbar(i,k)
    end do ! loop 6004
  end if
  !     ------------------------------------------------------------------
  if (itrac/=0) then

    do n=1,ntrac
      do k=1,nk-1
        do i=1,ni
          rxj(i,k,n) = rxj(i,k,n) + xj(i,k,n)*dbar(i,k)
          xuj(i,k,n) = xj(i,k,n)*ubar(i,k)
          xvj(i,k,n) = xj(i,k,n)*vbar(i,k)
        end do
      end do
    end do ! loop 6005

    !     ------------------------------------------------------------------

    k=nk
    do n=1,ntrac
      do i = 1,ni
        rxj(i,k,n) = rxj(i,k,n) + xj(i,k,n)*dbar(i,k)
        xuj(i,k,n) = xj(i,k,n)*ubar(i,k)
        xvj(i,k,n) = xj(i,k,n)*vbar(i,k)
      end do
    end do ! loop 6006

  end if

  !     -----------------------------------------------------------------
  !            etape 7 :    calcul de e (energie cinetique + potentielle)
  !     -----------------------------------------------------------------

  do k=nk,2,-1
    do i=1,ni

      qijk  = hsj(i,k)
      qijkm1= hsj(i,k-1)
      rdlnk =(r+(rv-r)*qijk)*dlnsgj(i,k)
      ra1kv =(r+(rv-r)*qijk)*(a1sgj(i,k)/dsgj(i,k))
      ra2kv =(r+(rv-r)*qijkm1)*(a2sgj(i,k)/dsgj(i,k))

      ej(i,k) = 0.5*(uj(i,k)*uj(i,k) + vj(i,k)*vj(i,k)) &
                      +phijk(i) +ra1kv*tj(i,k)+ra2kv*tj(i,k-1)

      phijk(i)= phijk(i)+rdlnk*tj(i,k)

    end do
  end do ! loop 7001

  !     -----------------------------------------------------------------

  k=1
  do i=1,ni

    qijk  = hsj(i,k)
    ra1kv =(r+(rv-r)*qijk)*(a1sgj(i,k)/dsgj(i,k))

    ej(i,k) = 0.5*(uj(i,k)*uj(i,k) + vj(i,k)*vj(i,k)) &
                      +phijk(i) +ra1kv*tj(i,k)

  end do ! loop 7002

  return
  !-----------------------------------------------------------------------
  6000 format('1INSIDE DYNAMICS:')
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
