!> \file
!>\brief Calculate optical properties of dust in bulk aerosol code
!!
!! @author Jiangnan Li
!
subroutine dustaero3 (exta, exoma, exomga, fa, absa, &
                      exta055, oma055, ga055, &
                      extb, odfb, ssab, absb, extb055,odf055, &
                      ssa055, abs055, &
                      aload1, aload2, il1, il2, ilg, lay)
  !
  !     * apr 23,2012 - j.li & y.peng. new version for gcm16:
  !     *                              - add the optical depth calculation
  !     *                                for 0.55 um
  !     * dec 05,2007 - m.lazare. previous version dustaero2 for gcm15g/h/i:
  !     *                         - sload1,sload2 now internal work arrays.
  !     * apr 25,2003 - j.li. previous version dustaero for gcm15e/f.
  !----------------------------------------------------------------------c
  !     calculation of optical properties for dust with two modes        c
  !                                                                      c
  !     exta:   extinction coefficient                                   c
  !     exoma:  extinction coefficient times single scattering albedo    c
  !     exomga: exoma times asymmetry factor                             c
  !     fa:     square of asymmetry factor                               c
  !     absa:   absorption coefficient                                   c
  !     exta055:extinction coefficient at 0.55 um                        c
  !     oma055: single scattering albedo at 0.55 um                      c
  !     ga055:  asymmetry factor at 0.55 um                              c
  !     aload1: dust loading for fine mode                               c
  !     aload2: dust loading for coarse mode                             c
  !----------------------------------------------------------------------c
  !
  implicit none
  real :: exom1
  real :: exom2
  real :: exomg1
  real :: exomg2
  real :: ext0551
  real :: ext0552
  real :: ext1
  real :: ext2
  real :: ga0551
  real :: ga0552
  integer :: i
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: j
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: nbl
  integer :: nbs
  real :: om0551
  real :: om0552
  real :: tmass
  !
  parameter (nbs = 4, nbl = 9)
  !
  !     * output arrays.
  !
  real, intent(inout), dimension(ilg,lay,nbs) :: exta !< Extinction coefficient at 0.5\f$\mu\f$m for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exoma !< EXTA*single scattering albedo for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: exomga !< EXOMA*asymetry factor for all aersools \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: fa !< EXOMGA*asymetry factor for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: absb !< Absorption coefficient for fine+coarse dust \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: odfb !< Extinction coefficient per mass for fine dust \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: extb !< Extinction coefficient per mass at 0.5\f$\mu\f$m for fine+coarse dust \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbs) :: ssab !< Single scattering albedo for fine+coarse dust \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: exta055 !< Extinction coefficient at 0.5\f$\mu\f$m for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: oma055 !< EXTA055 * single scattering albedo at 0.5\f$\mu\f$m for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ga055 !< OMA055 * asymmetry factor at 0.5\f$\mu\f$m for all aerosols \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: extb055 !< Extinction coefficient per mass at 0.5\f$\mu\f$m for fine+coarse dust \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: odf055 !< Extinction coefficient per mass at 0.5\f$\mu\f$m for fine dust \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay) :: ssa055 !< Single scattering albedo at 0.5\f$\mu\f$m for fine+coarse dust \f$[1]\f$
  real, intent(inout), dimension(ilg,lay) :: abs055 !< Absorption coefficient at 0.5\f$\mu\f$m for fine+coarse dust \f$[m^2/gram]\f$
  real, intent(inout), dimension(ilg,lay,nbl) :: absa !< Longwave absorption coefficient for fine+coarse mode \f$[m^2/gram]\f$
  !
  !     * input arrays.
  !
  real, intent(in), dimension(ilg,lay) :: aload1 !< Aerosol loading for fine mode dust \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: aload2 !< Aerosol loading for coarse mode dust \f$[gram/gram]\f$
  real, dimension(ilg,lay) :: aload !< Aerosol loading for fine+coarse \f$[gram/gram]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * internal work arrays.
  !
  real, dimension(ilg,lay) :: sload1
  real, dimension(ilg,lay) :: sload2
  real, dimension(nbs,2) :: se
  real, dimension(nbs,2) :: sw
  real, dimension(nbs,2) :: sg
  real, dimension(nbl,2) :: sa
  !
  real, dimension(2) :: se055
  real, dimension(2) :: sw055
  real, dimension(2) :: g055
  real, dimension(ilg,lay) :: sload551
  real, dimension(ilg,lay) :: sload552
  real   :: facdu
  !
  !        data ((se(j,k), j = 1, nbs), k= 1, 2)          /
  !     1  0.5472e+00, 0.6015e+00, 0.6026e+00, 0.4516e+00,
  !     2  0.7444e-01, 0.7657e-01, 0.7999e-01, 0.8605e-01 /
  ! c
  !        data ((sw(j,k), j = 1, nbs), k= 1, 2)          /
  !     1  0.8225, 0.9368, 0.9443, 0.8790,
  !     2  0.6333, 0.7635, 0.7812, 0.7016                 / ! ????
  ! c
  !        data ((sg(j,k), j = 1, nbs), k= 1, 2)          /
  !     1  0.7552, 0.6976, 0.6887, 0.6941,
  !     2  0.8999, 0.8396, 0.8077, 0.8048                 /
  ! c
  !        data ((sa(j,k), j = 1, nbl), k= 1, 2)                    /
  !     1  1.396e-02,  2.372e-02,  5.294e-02,  9.146e-02, 1.842e-01,
  !     1  1.145e-01,  8.252e-02,  7.634e-02,  4.076e-02,
  !     2  1.178e-02,  1.816e-02,  3.068e-02,  3.588e-02, 4.513e-02,
  !     2  4.572e-02,  4.220e-02,  4.700e-02,  4.146e-02            /
  !
  ! ccc jiangnan corrected parameters for band calculation
  !
  !        data ((se(j,k), j = 1, nbs), k= 1, 2)          /
  !     1  1.9827e+00, 1.5467e+00, 5.8710e-01, 6.0982e-02,
  !     2  3.5795e-01, 3.8706e-01, 3.7115e-01, 1.6996e-01 /
  !
  !        data ((sw(j,k), j = 1, nbs), k= 1, 2)          /
  !     1  9.2771e-01, 9.4909e-01, 9.2426e-01, 6.6900e-01,
  !     2  7.8074e-01, 8.5087e-01, 9.0524e-01, 8.7040e-01 /
  !
  !        data ((sg(j,k), j = 1, nbs), k= 1, 2)          /
  !     1  6.9309e-01, 6.7709e-01, 6.8139e-01, 6.0267e-01,
  !     2  7.9170e-01, 7.4094e-01, 7.8688e-01, 8.7738e-01 /
  !
  !        data ((sa(j,k), j = 1, nbl), k= 1, 2)                     /
  !     1  1.5186e-02, 1.5452e-02, 2.7977e-02, 4.2728e-02, 5.7816e-02,
  !     1  3.8332e-02, 2.5348e-02, 3.4011e-02, 3.9038e-02,
  !     2  1.8447e-02, 1.8589e-02, 2.8312e-02, 4.9108e-02, 7.9832e-02,
  !     2  5.3812e-02, 3.4652e-02, 4.5685e-02, 4.6972e-02            /
  !
  data ((se(j,k), j = 1, nbs), k = 1, 2)          / &
                      0.1983e+01, 0.1547e+01, 0.5870e+00, 0.6095e-01, &
                      0.3579e+00, 0.3871e+00, 0.3711e+00, 0.1700e+00 /
  !
  data ((sw(j,k), j = 1, nbs), k = 1, 2)          / &
                      0.9277e+00, 0.9491e+00, 0.9243e+00, 0.6690e+00, &
                      0.7807e+00, 0.8509e+00, 0.9052e+00, 0.8704e+00 /
  !
  data ((sg(j,k), j = 1, nbs), k = 1, 2)          / &
                      0.6931e+00, 0.6771e+00, 0.6814e+00, 0.6027e+00, &
                      0.7917e+00, 0.7409e+00, 0.7869e+00, 0.8774e+00 /
  !
  data ((sa(j,k), j = 1, nbl), k = 1, 2)                    / &
                      0.1519e-01, 0.1545e-01, 0.2797e-01, 0.4272e-01, 0.5782e-01, &
                      0.3833e-01, 0.2535e-01, 0.3402e-01, 0.3904e-01, &
                      0.1847e-01, 0.1861e-01, 0.2836e-01, 0.4921e-01, 0.8002e-01, &
                      0.5393e-01, 0.3471e-01, 0.4577e-01, 0.4706e-01      /
  !
  !
  data (se055(k),k = 1, 2)                       / &
                     1.95913, 0.36309                               /
  !
  data (sw055(k),k = 1, 2)                       / &
                     0.93730, 0.79935                               /
  !
  data (g055(k),k = 1, 2)                        / &
                    0.68922, 0.77746                               /
  !
  !----------------------------------------------------------------------c
  !     dust with two modes: accumulation mode with model radius         c
  !     0.39 um, sigma 2.0; coarse mode with model radius 1.9 um,        c
  !     sigma 2.15. dry density 2.6 kg/m^3                               c
  !                                                                      c
  !     solar, se: specific extinction, oma: single scattering albedo,   c
  !     ga: asymmetry factor, exta: extinction coefficient               c
  !     factor 10000 because the unit of specific extinction for aerosol c
  !     is m^2/gram, while for gas is cm^2/gram, in raddriv we use same  c
  !     dp (air density * layer thickness) is used for both ga and       c
  !     aerosol. aload dust aerosol loading in unit g (aerosol) / g      c
  !     (air). tau = aload * specific extinction * air density *         c
  !     layer thickness                                                  c
  !     data by g. lensins, code by j. li (2002, 2)                      c
  !----------------------------------------------------------------------c
  !
  facdu             =  1.0
  do k = 1, lay
    do i = il1, il2
      sload1(i,k)     =  10000.0 * aload1(i,k)
      sload2(i,k)     =  10000.0 * aload2(i,k)
      aload(i,k)      =  aload1(i,k) + aload2(i,k)
      sload551(i,k)   =  10000.0 * aload1(i,k) * facdu
      sload552(i,k)   =  10000.0 * (aload2(i,k) + &
                        aload1(i,k) * (1.0 - facdu))
    end do
  end do ! loop 100
  do j = 1, nbs
    do k = 1, lay
      do i = il1, il2
        ext1            =  sload551(i,k) * se(j,1)
        ext2            =  sload552(i,k) * se(j,2)
        exom1           =  ext1 * sw(j,1)
        exom2           =  ext2 * sw(j,2)
        exomg1          =  exom1 * sg(j,1)
        exomg2          =  exom2 * sg(j,2)
        !
        exta(i,k,j)     =  exta(i,k,j) + ext1 + ext2
        exoma(i,k,j)    =  exoma(i,k,j) + exom1 + exom2
        exomga(i,k,j)   =  exomga(i,k,j) + exomg1 + exomg2
        fa(i,k,j)       =  fa(i,k,j) + exomg1 * sg(j,1) + &
                          exomg2 * sg(j,2)
        !
        absb(i,k,j)     =  ext1 * (1.0 - sw(j,1)) + &
                          ext2 * (1.0 - sw(j,2))
        !
        tmass           =  10000.0 * aload(i,k)
        if (tmass > 1.e-15) then
          extb(i,k,j)    = (ext1 + ext2) / tmass
        else
          extb(i,k,j)    =  0.0
        end if
        if (aload1(i,k) > 1.e-19) then
          odfb(i,k,j)    =  ext1 / (10000.0 * aload1(i,k))
        else
          odfb(i,k,j)    =  0.0
        end if
        !
        if ((ext1 + ext2) > 1.e-20) then
          ssab(i,k,j)    = (exom1 + exom2) / (ext1 + ext2)
        else
          ssab(i,k,j)    =  0.0
        end if
        !
      end do
    end do
  end do ! loop 200
  do k = 1, lay
    do i = il1, il2
      ext0551         =  sload551(i,k) * se055(1)
      ext0552         =  sload552(i,k) * se055(2)
      exta055(i,k)    =  exta055(i,k) + ext0551 + ext0552
      om0551          =  sw055(1) * ext0551
      om0552          =  sw055(2) * ext0552
      oma055(i,k)     =  oma055(i,k) + om0551 + om0552
      ga0551          =  g055(1) * om0551
      ga0552          =  g055(2) * om0552
      ga055(i,k)      =  ga055(i,k) + ga0551 * g055(1) + ga0552 * g055(2)
      !
      abs055(i,k)     =  ext0551 * (1.0 - sw055(1)) + &
                        ext0552 * (1.0 - sw055(2))
      tmass           =  10000.0 * aload(i,k)
      if (tmass > 1.e-15) then
        extb055(i,k)   = (ext0551 + ext0552) / tmass
      else
        extb055(i,k)   =  0.0
      end if
      if (aload1(i,k) > 1.e-19) then
        odf055(i,k)    =  ext0551 / (10000.0 * aload1(i,k))
      else
        odf055(i,k)    =  0.0
      end if
      !
      if ((ext0551 + ext0552) > 1.e-20) then
        ssa055(i,k)    = (om0551 + om0552) / (ext0551 + ext0552)
      else
        ssa055(i,k)    =  0.0
      end if
      !
    end do
  end do ! loop 250
  !
  !----------------------------------------------------------------------c
  !     infrared, sa: specific absorptance, abs: absorption              c
  !----------------------------------------------------------------------c
  !
  do j = 1, nbl
    do k = 1, lay
      do i = il1, il2
        absa(i,k,j)     =  absa(i,k,j) + sload551(i,k) * sa(j,1) + &
                          sload552(i,k) * sa(j,2)
      end do
    end do
  end do ! loop 400
  !
  return
end
!> \file
!> Calculation of optical properties for dust in bulk aerosol mode.
!! The dust is assumed to be composed of two modes with both modelled as log normal
!! size distributions (?).  The accumulation mode has a mode(?) radius of 0.39 \f$\mu\f$um
!! and and a \f$\sigma\f$ of 2.0.  The coarse mode has a mode(?) radius 1.9 \f$\mu\f$um,
!! and a \f$\sigma\f$ of 2.15. Both assume that dust has a dry density 2.6 kg/m\f$^3\f$.