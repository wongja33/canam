!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cld_gen_driver3(ncldy, clw_sub,cic_sub, & ! output
                           rlc_cf, rlc_cw, sigma_qcw, ipph, & ! input
                           ioverlap, cld, shj, shtj, clw, cic, t, &
                           ps, iseedrow, &
                           il1, il2, ilg, lay, lev, nx_loc, mtop)

  !      * apr 30/2012 - jason cole.   new version for gcm16:
  !      *                             remove unused internal arrays
  !      *                             {x,x1,x2,y,y1,y2}, including call
  !      *                             in new cld_generator3.
  !      * dec 12/2007 - jason cole.   previous version cld_gen_driver2 for
  !      *                             gcm15g/h/i:
  !      *                             - handle 3 different overlap
  !      *                               methods (defined using ioverlap).
  !      *                             - remove code related to setting
  !      *                               lpph and imaxran since now using
  !      *                               ipph and ioverlap.
  !      *                             - calls new version cld_generator2.
  !      * jan 09/2007 - jason cole.   previous version cld_gen_driver for gcm15f.
  ! --------------------------------------------------------------------
  ! driver to call stochastic cloud generator
  ! --------------------------------------------------------------------
  ! jnsc gcm15d now carries extra fields for the shallow convection cloud amount,
  ! jnsc cloud water content and cloud droplet number concentration.  right now
  ! jnsc i am just using the defacto method from gcm15d.  this method can likely
  ! jnsc be improved using mcica and sampling

  use phys_consts, only : grav, rgas
  implicit none
  ! note: lay    => number of layers
  ! note: ilg    => number of gcm columns
  ! note: nx_loc => number of subcolumns to generate

  !
  ! input data
  !

  real, intent(in) , dimension(ilg, lay) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(in) , dimension(ilg,lay) :: t !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: cld !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: ps !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: rlc_cf !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: rlc_cw !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: sigma_qcw !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: clw !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: cic !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg,lev) :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev   !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: nx_loc   !< Number of cloud subcolumns in an atmospheric column \f$[unitless]\f$
  integer, intent(in) :: mtop !< Variable description\f$[units]\f$

  integer, intent(in) :: ioverlap !< Overlap flag\f$[units]\f$
  integer, intent(in) :: ipph !< Plane-parallel homogeneous flag\f$[units]\f$

  integer(4), intent(inout) , dimension(ilg,4) :: iseedrow ! seed for kissvec rng

  !
  ! output data
  !

  real, intent(out) , dimension(ilg,lay,nx_loc) :: clw_sub !< Subcolumn of cloud liquid water contents       (g/m^3)\f$[units]\f$
  real, intent(out) , dimension(ilg,lay,nx_loc) :: cic_sub !< Subcolumn of cloud ice water contents          (g/m^3)\f$[units]\f$
  integer,intent(out) , dimension(ilg) :: ncldy !< Number of cloudy subcolumns\f$[units]\f$
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: m2km = 1.0/1000.0 ! convert meters to kilometers
  real, parameter :: cut  = 0.001
  !==================================================================
  !
  ! work arrays
  !

  real, dimension(ilg,lay) :: qi_prof
  real, dimension(ilg,lay) :: qc_prof
  real, dimension(ilg,lay) :: zm

  real, dimension(ilg,lay) :: alpha ! fraction of maximum/random cloud overlap
  real, dimension(ilg,lay) :: rcorr ! fraction of maximum/random cloud condensate overlap

  integer, dimension(ilg) :: i_loc ! place the new subcolumns into the arrays starting from the front

  logical, dimension(ilg) :: l_cld

  !
  ! local data (scalar)
  !

  integer :: il ! counter over gcm columns
  integer :: ii ! counter over nx subcolumns
  integer :: kk ! counter over lay vertical layers

  real :: rho ! density of air                               (g/m^3)
  real :: p ! gcm column pressure at layer midpoint        (pa)
  real :: rog ! total gas constant/gravity
  real :: dmult

  real :: p1
  real :: p2

  ! zero out fields
  do il = il1,il2
    ncldy(il)  = 0
  end do ! il

  do kk = 1, lay
    do il = il1,il2
      qc_prof(il,kk) = 0.0
      qi_prof(il,kk) = 0.0
      alpha(il,kk)   = 0.0
      rcorr(il,kk)   = 0.0
    end do ! il
  end do ! kk

  do ii = 1, nx_loc
    do kk = 1 , lay
      do il = il1, il2
        cic_sub(il,kk,ii) = 0.0
        clw_sub(il,kk,ii) = 0.0
      end do
    end do
  end do

  ! compute the heights of mid-layers
  ! needed if decorrelation lengths are used
  rog = rgas/grav

  do il = il1, il2
    p          = shj(il,lay) * ps(il) ! since using ratios
    zm(il,lay) = rog * t(il,lay) * log(ps(il)/p) * m2km
  end do ! il

  do kk = lay - 1, 1, - 1
    do il = il1, il2
      p1 = shj(il,kk) * ps(il)
      p2 = shj(il,kk + 1) * ps(il)
      zm(il,kk) = zm(il,kk + 1) + rog * t(il,kk) * log(p2/p1) * m2km
    end do ! il
  end do ! kk

  ! compute the cloud fraction and layer mean liquid cloud condensate accounting for shallow
  ! convection

  ! convert the cloud condensate from kg/kg to g/m^3 since these are the units used to generate
  ! cloud optical properties
  !
  ! compute the cloud fraction and layer mean liquid cloud condensate accounting for shallow
  ! convection
  do kk = 1, lay
    do il = il1, il2
      p = shj(il,kk) * ps(il)
      if (cld(il,kk) >= cut) then
        ! rho in g/m^3
        rho            = 1000.0 * p/(rgas * t(il,kk))
        dmult          = rho /cld(il,kk)
        qi_prof(il,kk) = cic(il,kk) * dmult
        qc_prof(il,kk) = clw(il,kk) * dmult
      end if
    end do ! il
  end do ! kk

  ! call cloud generator
  call cld_generator3(cic_sub, clw_sub, ncldy, &
                      zm, cld, qc_prof, qi_prof, &
                      rlc_cf, rlc_cw, sigma_qcw, &
                      ilg, il1, il2, lay, nx_loc, &
                      alpha, rcorr, iseedrow, &
                      i_loc, l_cld, ipph, ioverlap)

  return
end subroutine cld_gen_driver3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
