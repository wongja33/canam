!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getwildf2(ebwapak,ebwapal,eowapak,eowapal, &
                     eswapak,eswapal, &
                     nlon,nlat,levwf,incd,iday,mday,mday1, &
                     kount,ijpak,nuan,lf,gg)
  !
  !     * may 10/2012 - m.lazare. new version for gcm16:
  !     *                         - mdayt,mdayt1 passed in (as
  !     *                           "MDAY" and "MDAY1", respectively),
  !     *                           from model driver, instead of
  !     *                           being calculated inside this routine.
  !     * mar 06/2007 - x.ma.     previous version getwildf.
  !
  !     * read in wildfire emissions from aerocom
  !
  implicit none
  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer :: jour
  integer :: jour1
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: levwf  !< Variable description\f$[units]\f$
  integer :: lon
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  integer, intent(in) :: mday1 !< Variable description\f$[units]\f$
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuan
  integer :: nc4to8
  real :: w1
  real :: w2
  !
  !     * surface emissions/concentrations.
  !
  !     * multi-level species.
  !
  real, intent(inout), dimension(ijpak,levwf) :: ebwapak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levwf) :: ebwapal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levwf) :: eowapak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levwf) :: eowapal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levwf) :: eswapak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levwf) :: eswapal !< Variable description\f$[units]\f$
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(levwf) :: lf !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer, dimension(6) :: levf
  integer*4 :: mynode
  !
  common /mpinfo/ mynode
  !
  data  levf/ 100, 500, 1000, 2000, 3000, 6000/
  !----------------------------------------------------------------------
  jour = mday
  jour1 = mday1
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      rewind nuan
      do   l = 1,levwf
        call getggbx(ebwapak(1,l),nc4to8("EBWA"),nuan,nlon,nlat,iday, &
                     levf(l),gg)
      end do ! loop 8
      !
      do   l = 1,levwf
        call getggbx(eowapak(1,l),nc4to8("EOWA"),nuan,nlon,nlat,iday, &
                     levf(l),gg)
      end do ! loop 9
      !
      do  l = 1,levwf
        call getggbx(eswapak(1,l),nc4to8("ESWA"),nuan,nlon,nlat,iday, &
                     levf(l),gg)
      end do ! loop 10
      !
      do l = 1,levwf
        do i = 1,ijpak
          ebwapal(i,l) = ebwapak(i,l)
          eowapal(i,l) = eowapak(i,l)
          eswapal(i,l) = eswapak(i,l)
        end do
      end do ! loop 16

    else
      !
      !         * not a new integration. no new fields required.
      !
    end if

  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !        * start-up time. get fields for previous and target mid-month days.
      !
      !
      rewind nuan
      !
      do  l = 1,levwf
        call getggbx(ebwapak(1,l),nc4to8("EBWA"),nuan,nlon,nlat,jour1, &
                     levf(l),gg)
      end do ! loop 20
      !
      do  l = 1,levwf
        call getggbx(eowapak(1,l),nc4to8("EOWA"),nuan,nlon,nlat,jour1, &
                     levf(l),gg)
      end do ! loop 21
      !
      do  l = 1,levwf
        call getggbx(eswapak(1,l),nc4to8("ESWA"),nuan,nlon,nlat,jour1, &
                     levf(l),gg)
      end do ! loop 22
      !
      rewind nuan
      !
      do  l = 1,levwf
        call getggbx(ebwapal(1,l),nc4to8("EBWA"),nuan,nlon,nlat,jour, &
                     levf(l),gg)
      end do ! loop 30
      !
      do  l = 1,levwf
        call getggbx(eowapal(1,l),nc4to8("EOWA"),nuan,nlon,nlat,jour, &
                     levf(l),gg)
      end do ! loop 31
      !
      do  l = 1,levwf
        call getggbx(eswapal(1,l),nc4to8("ESWA"),nuan,nlon,nlat,jour, &
                     levf(l),gg)
      end do ! loop 32
      !
      lon = nlon - 1
      day1 = real(mday1)
      day2 = real(iday)
      day3 = real(mday)
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) write(6,6000) iday,mday1,mday,w1,w2
      !
      do l = 1,levwf
        do i = 1,ijpak
          ebwapak(i,l) = w1 * ebwapal(i,l) + w2 * ebwapak(i,l)
          eowapak(i,l) = w1 * eowapal(i,l) + w2 * eowapak(i,l)
          eswapak(i,l) = w1 * eswapal(i,l) + w2 * eswapak(i,l)
        end do
      end do ! loop 150

    else
      !
      !         * this is in the middle of a run.
      !
      rewind nuan
      !
      do  l = 1,levwf
        call getggbx(ebwapal(1,l),nc4to8("EBWA"),nuan,nlon,nlat,jour, &
                     levf(l),gg)
      end do ! loop 230
      !
      do  l = 1,levwf
        call getggbx(eowapal(1,l),nc4to8("EOWA"),nuan,nlon,nlat,jour, &
                     levf(l),gg)
      end do ! loop 231
      !
      do  l = 1,levwf
        call getggbx(eswapal(1,l),nc4to8("ESWA"),nuan,nlon,nlat,jour, &
                     levf(l),gg)
      end do ! loop 232
    end if
    !

  end if
  return
  !---------------------------------------------------------------------
6000 format(' INTERPOLATING FOR', i5, ' BETWEEN', i5, ' AND', &
        i5, ' WITH WEIGHTS = ', 2f7.3)
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
