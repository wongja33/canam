!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine swapf2x(f,ilev,nlat,ilh,lon,g)
  !
  !     * AUG 26/2003 - M.LAZARE. New version for revised FFT's.
  !     * nov 12/92 a.j. stacey.  previous version swapf2.
  !     *
  !     * store f(2,ilh,nlat,ilev) into g(2*ilh*nlat+1,ilev).
  !     *
  !     * note: this means that the 2 extra memory locations for each
  !     * transform, unused by the physics, are extracted here before the
  !     * data is passed to the physics (note that lon+2 = 2*ilh = ilg).
  !     *
  !     * note that it is very important that the output array be dimensioned
  !     * with the unused memory locations explicitly allocated, or else
  !     * the starting addresses according to level and variable will not be
  !     * maintained. to avoid memory bank conflicts, the inner dimension of
  !     * "g" is augmented by one over what is used.
  !
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilh
  integer :: j
  integer :: k
  integer :: lat
  integer :: lev
  integer, intent(in) :: lon
  integer, intent(in) :: nlat
  !
  real, intent(in) :: f(2*ilh,nlat,ilev) !< Variable description\f$[units]\f$
  real, intent(inout) :: g(2*ilh*nlat+1,ilev) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------------
  do lev=1,ilev
    k = 1
    do lat=1,nlat
      do j=1,lon
        g(k,lev) = f(j,lat,lev)
        k = k + 1
      end do
    end do ! loop 100
  end do ! loop 200
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
