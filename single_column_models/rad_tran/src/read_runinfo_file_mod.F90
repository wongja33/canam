!>\file
!>\brief Read in the information from the file with the run information
!!
!! @author Jason Cole
!

module read_runinfo_file_mod
    implicit none
contains
  
  subroutine read_runinfo_file(runinfo_filename,  & ! input
                               cfg_filename,      & ! output
                               input_filename,    &
                               output_dir,        &
                               output_filename    )
                                   
    use parameters_mod, only : maxlen_char, &
                               exit_code_runinfo

    implicit none
    
    ! input 
    character(len=maxlen_char), intent(in) :: runinfo_filename !< Name of the run information file
            
    ! output
    character(len=maxlen_char), intent(out) :: cfg_filename    !< Name of the configuration file
    character(len=maxlen_char), intent(out) :: input_filename  !< Name of the input file
    character(len=maxlen_char), intent(out) :: output_dir      !< name of the input file
    character(len=maxlen_char), intent(out) :: output_filename !< Name of the output file
    
    ! local
    integer :: iu
    logical :: ok
    
    ! define the configuration namelist
    namelist /runinfo/ cfg_filename,   &
                       input_filename, &
                       output_dir,     &
                       output_filename
                             
    ! read in the configuration file namelist.
    
    inquire(file=trim(runinfo_filename),exist=ok)
    
    if (ok) then
      iu = 16
      open(iu,file=trim(runinfo_filename),form='formatted')
      read(iu,nml=runinfo)
      close(iu)
    else
      write(6,*) 'Error opening run information file ',trim(runinfo_filename)
      stop exit_code_runinfo
    endif ! ok
        
    return
    
  end subroutine read_runinfo_file
  
  
end module read_runinfo_file_mod

!> \file
!> Read in the information from the run information file.  The information is 
!! provided in a namelist.
