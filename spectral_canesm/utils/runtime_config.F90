!> Handle the runtime configuration and storage of parameters controlling the algorithmic flow of the model
!! The following provides an example for the intended use of this module
!!    subroutine example( )
!!      use agcm_config_mod, only : agcm_config
!!      integer :: seed
!!
!!      ! Example for branching code
!!      if (agcm_config%random_number_seed == 0) then
!!        ...
!!      endif
!!
!!      ! Example for tuneable parameters
!!      call set_random_number_seed(agcm_config%random_number_seed)
!!      ! Alternate example for a tuneable parameter
!!      seed = agcm_config%random_number_seed
!!
!!    end subroutine example
module agcm_config_mod

implicit none; private

!> Holds all runtime configurable parameters used to control the behavior of the AGCM
type, public :: agcm_config_type

  integer :: random_number_seed = 0 !< Seed used for the random number generator

end type agcm_config_type

type, public :: phys_parm_type

end type phys_parm_type

type, public :: diag_config_type

end type diag_config_type

type(agcm_config_type), save :: agcm_config
type(phys_parm_Type),   save :: phys_parm
type(diag_config_type), save :: diag_config

public :: init_runtime_config

contains

subroutine init_runtime_config()

  ! Read namelists

end subroutine init_runtime_config

end module agcm_config_mod