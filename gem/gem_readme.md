# CanAM5 GEM

**WARNING** : this document and code is in alpha. There are no guarantees whatsoever!

These are initial instructions to attempt to compile CanAM5-GEM off the `canam_gem` branch.

1. **Basic profile setup.**
    See One-time setup below

2. **Clone the CanESM repo.**
    Typically we put it in /home/ords/crd/ccrn/$USER:

    ```bash
    # note that we clone down the super repo for dependencies in the CanDIAG and CanCPL repos
    git clone --recursive git@gitlab.science.gc.ca:CanESM/CanESM5.git mytest

    # checkout the canam gem development branch
    cd mytest
    git scheckout canam_gem     # note the 's'
    ```
    This pulls down the CanESM5 super-repo from the central repo. and checks out the `canam_gem` branch.
    You might use your fork instead in future.

3. **Setup for compilation**
    Compilation is typically on the backend (banting/daley).

    ```bash
     cd CanAM
     . r.load.dot eccc/mrd/rpn/MIG/GEM/5.1.0 # set GEM environment

    # Where to scan for code to compile.
    export rpnphy=$(pwd)

    # Where the build is done. Change as appropriate for you. Gets large!
    export storage_model=/space/hall3/work/eccc/crd/ccrn/users/ncs001/storage_model/
    ```

4. **Compile**

    ```bash
    cd gem
    ouv_exp_gem -v           # setup experiment, deps
    linkit -v                # bring in rpn scripts
    make dep                 # build Makefile.dep.mk
    make -j6 libcccma        # CCCma physics lib build. Note: this builds a gem_* directory in $storage_model. Delete it using "make buildclean" if you want your updated src code within your repo to be used
    make gemdm               # GEM build
    ```


5. **Lunch with maestro**

    ```bash
    cd ~/.suites/gem/5.1.0
    ln -sfn /home/ords/crd/ccrn/$USER/mytest/CanAM/gem/maestrofiles mytest
    cd mytest
    export SEQ_EXP_HOME=$(pwd)
    mkdir hub listings sequencing logs
    makelinks -f

    # Our masestro suites a link to maestrofiles, it's important to export SEQ_EXP_HOME correctly whenever you launch xflow.
    cd ~/.suites/gem/5.1.0/mytest; export SEQ_EXP_HOME=$(pwd); xflow &
    ```

6. **Create links to run CCCma diagnostics/pooling/ploting job string**

   ```bash
   cd ~/bin
   ln -sf ~ryj001/bin/mmerged_diag_deck.sh   # modified/simplified from CanESM5 merged_diag_deck.sh for GEM output on GU grid
   ln -sf ~ryj001/bin/mmerged_diag_deck.dk   # modified/simplified from CanESM5 merged_diag_deck.dk for GEM output on GU grid

   ln -sf ~ryj001/bin/plotmix15gu.dk         # plotmix deck on GU grid
   ln -sf ~ryj001/bin/anomaly15gu.dk         # anomaly deck on GU grid
   ln -sf ~ryj001/bin/heatrans16.dk          # standard deck, nothing changed
   ```
### One-time setup

The nominal things you need are, in `~./profile` make sure you have ORDENV setup, eg:

   ```bash
   export ORDENV_SITE_PROFILE=20210129
   export ORDENV_COMM_PROFILE=eccc/20210309
   export ORDENV_GROUP_PROFILE=eccc/cmc/1.9.4
   . /fs/ssm/main/env/ordenv-boot-20201118.sh
   ```

You need this to be able to get the GEM lib via `r.load`. You might also choose to
add the `storage_model` variable to avoid defining it every time.

### More comprehensive setup

Something like this will be needed in the future, but is likely not needed just for testing.

If this is your first time running CanAM_GEM, you will need to do the one-time setup instructions: [OneTimeSetup\_readme.md] (OneTimeSetup_readme.md).

In the future, you may also try running Yanjun's one-time set up script (needs updating I think):
```
~ryj001/bin/gem4.setenv.once
```

### Development Notes

As of June 2020, `gem` integration into `CanAM5` and `CanESM5` is _currently ongoing_, with the main development happening
on the `canam_gem` branch of the [central CanESM5 repository](https://gitlab.science.gc.ca/CanESM/CanESM5), with most of the
development occuring in the [CanAM submodule](https://gitlab.science.gc.ca/CanESM/CanAM).

During this time, you may be working on this branch while _another_ developer is also working on it - if this is the case,
you may get the following error when trying to push your changes:

#### Developing in the CanESM framework
As this work is happening within the `CanESM5` super-repo, it requires developers to work with multiple "sub-repos".
For interested parties, they can read up on sub-repos and submodules [here](https://git-scm.com/book/en/v2/Git-Tools-Submodules),
[here](https://www.vogella.com/tutorials/GitSubmodules/article.html), and many other locations on the internet. However,
to aid developers in working with submodules, we have prepared many `git-s*` commands that can be used to easily interact
with submodules, and using them in the context of `CanESM5` are documented
[here](https://gitlab.science.gc.ca/CanESM/CanESM5/blob/develop_canesm/Developing_readme.md#modifying-the-code).
In specific relation to the `CanAM` `GEM` development, since this work is occuring on an existing branch of the main repo,
developers only need to worry about directions to "**Add and commit the changes**" and "**Push changes back to gitlab**",
but it should be noted that using the `git sadd`, `git scommit`, and `git spush` **requires users to be at the top level**
**of the repo** when they do (i.e. the level that contains the `CanAM` directory).

#### Syncing Changes with Other Developers

```
>> git spush origin canam_gem
...
To gitlab.science.gc.ca:CanESM/CanAM.git
 ! [rejected]        canam_gem -> canam_gem (fetch first)
nt: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
...
```
If this happens, this means that another developer has pushed changed to this branch. Fortunately, all is not lost! You
simply need to pull down the changes and merge them with yours before you can push up the new updates. To do this, run

```bash
git sfetch origin canam_gem
git pull
```
then deal with any conflicts that come up (if any) and then try pushing the branch again.
