!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine thermdat2(xvf, pvf, o2vf, cn2vf, ovf, amuvf, &
                           cpvf, gvf, &
                           effh, effsrc, to2, effo2nir, &
                           x1chem, x2chem, dxchem, wchem, k1chem, &
                           wwvs2, wwvs3, wco2s, wco2st, &
                           wwvl3, wch4l, wo3l, k1co2s, k2co2s, &
                           etag, lay)

  !***********************************************************************
  !*                                                                     *
  !*              subroutine thermdat2                                   *
  !*                                                                     *
  !***********************************************************************

  ! modified from thermdat to be used with the ckd scheme
  !        v. fomichev, november, 2004
  !
  ! 1) to define input data on concentrations of o, o2, n2, as well as data
  !    on the air molecular weight and specific heat to be used in the
  !    radiative scheme for the lower thermosphere (taken from the msis
  !    global average model
  ! 2) to calculate solar heating efficiency in hartley (ha) o3 band,
  !    schumann-runge o2 continuum (src) (srbands efficiency = 1).
  !    method by mlynczack &solomon (jgr, vol 98, p 10517, 1993) is used
  ! 3) to calculate o2 column number density at the model mid-layer levels
  !    used for calculation of the o2 solar heating in src and srb.
  ! 4) to define parameters used for the chemical heating calculation.
  ! 5) to define indexes and weights used to smoothly indroduce the
  !    upper atmosphere radiation scheme.
  ! 6) to estimate the solar heating efficiency in o2 bands: .762, .688, .629
  !    and 1.27 micron. approximation based on results shown on fig. 1 of
  !    mlynczak &marshall (grl, 1996, vol. 23, p. 657). efficiency for .762 um
  !    is used for its giving the dominating contribution.
  !
  !       v. fomichev, november, 1997
  !                    october,  1999 - modified by inclusion (4) - vf.
  !                    november, 2004 - (5) and (6) included
  !
  ! called by model driver outside of parallel region
  ! calls a18lin

  implicit none
  real :: a18lin
  real :: ana
  real :: g1
  real :: g2
  integer :: i
  integer :: k
  integer :: kk
  integer :: kmxm
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  real :: p
  real :: x
  real :: x1ch4l
  real :: x1co2s
  real :: x1o3l
  real :: x1wvl3
  real :: x1wvs2
  real :: x1wvs3
  real :: x2ch4l
  real :: x2co2s
  real :: x2o3l
  real :: x2wvl3
  real :: x2wvs2
  real :: x2wvs3
  real :: x3co2s
  real :: x4co2s
  real :: xk

  ! input:  etag(lay) - model hybrid levels [p/p(surface)] for the mid-layers
  !                     levels ordered from the top (1 - moon layer) down.

  real, intent(in), dimension(lay) :: etag !< Variable description\f$[units]\f$

  ! output: all information is giving at log-pressure grid defined for
  !         "mid-layer" levels. ordered from the surface up.
  !
  !  output results are passed throught the following common blocks in
  !  the driver (fields are passed out to the driver):
  !  /therdat/:
  !         pvf - pressure (mb); xvf - log-pressure height
  !         O2vf,CN2vf,Ovf - vmr's for O2, N2, and O
  !         amuvf - air molecule weight (g/mol)
  !         cpvf -specific heat (erg/g/k); gvf - gravity acceleration (m s-2)
  !         co2vf - vmr for co2 is determined in the vmrco2 subroutine
  !  /thersh/:
  !  effh - solar heating efficiency in the hartley o3 band
  !  effsrc - solar heating efficiency for src
  !  to2 - o2 column number density (cm-2) at the model mid-layer levels
  !  effo2nir - solar heating efficiency in the "NIR" o2 bands (.629-1.27um)
  !  /chhdat/:
  !  x1chem, x2chem, dxchem - merging region boundaries (to implement chem.heat)
  !  k1chem, wchem - model index for x1chem and weights at x > x2chem
  !  /radidx/:
  ! indexes and weights from this block are used to smoothly indroduce the
  ! upper atmosphere radiation scheme (similar to k1chem, wchem)
  ! wwvs2 - weights for h2o heating in 4200-8400 cm-1
  ! wwvs3 - weights for h2o heating in 2500-4200 cm-1
  ! k1co2s,k2co2s,wco2s,wco2st - indexes and weights for the nir co2 bands
  ! wwvl3 - weights for h2o ir heating in 1400-1900 cm-1
  ! wch4l - weights for ch4 ir heating in 1100-1400 cm-1
  ! wo3l  - weights for o3 ir heating in 980-1100 cm-1 (9.6um)

  real, intent(inout), dimension(lay) :: xvf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: pvf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: o2vf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: cn2vf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: ovf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: amuvf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: cpvf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: gvf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: effh !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: effsrc !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: to2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: effo2nir !< Variable description\f$[units]\f$
  real, intent(inout) :: x1chem !< Variable description\f$[units]\f$
  real, intent(inout) :: x2chem !< Variable description\f$[units]\f$
  real, intent(inout) :: dxchem !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wchem !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wwvs2 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wwvs3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wco2s !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wco2st !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wwvl3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wch4l !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lay) :: wo3l !< Variable description\f$[units]\f$
  integer, intent(inout) :: k1chem !< Variable description\f$[units]\f$
  integer, intent(inout) :: k1co2s !< Variable description\f$[units]\f$
  integer, intent(inout) :: k2co2s !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  ! initial input info is taken from the msis global average model:
  ! muo(g) - air mol. weight; oo,n2o,o2o - vmr; cpo(j/kg) - specific heat
  ! go(m s-2) gravity acceleration.
  real :: xo(45) !<
  real :: muo(45) !<
  real :: oo(45) !<
  real :: n2o(45) !<
  real :: o2o(45) !<
  real :: cpo(45) !<
  real :: go(45) !<
  real :: muoo(45) !<
  real :: ooo(45) !<
  real :: n2oo(45) !<
  real :: o2oo(45) !<
  real :: cpoo(45) !<
  real :: goo(45) !<

  ! efficiency for 762 nm o2 (taken from mlynczak &marshall, grl, 1996)
  real :: xo2nir(10) !<
  real :: effo2o(10) !<

  ! coefficients for the chebyshev polynomial fit for the heating efficiency
  real :: cho3l(4) !<
  real :: cho3h(4) !<
  real :: cho2(4) !<
  ! ----- for the ha band:
  data cho3l/0.926210, 0.133960,-0.076863, 0.006897/
  data cho3h/0.669650,-0.009682, 0.033093, 0.017938/
  ! ----- for the src:
  data cho2/0.75349, 0.0036, 0.059468, -0.022795/

  ! ----- efficiency for 762 nm o2 (taken from mlynczak &marshall, grl, 1996):
  data (xo2nir(i), i=1,10)/ &
  4.44, 5.84, 7.12, 8.44, 9.90,11.50,13.24,15.07,16.62,17.18/
  data (effo2o(i), i=1,10)/ &
  1.,  0.992,0.976,0.919,0.732,0.455,0.301,0.175,0.050,0.0/

  ! avogadro number * 10 (only for to2 calculation)
  data ana/6.022045e24/

  ! ----- msis data:
  data (xo(i), i=1,45)/ &
   0.0,     0.5,     1.0,     1.5,     2.0,     2.30000, 2.55259, &
   3.05259, 3.55259, 4.05259, 4.55259, 5.05259, 5.55259, 6.05259, &
   6.55259, 7.05259, 7.55259, 8.05259, 8.55259, 9.05259, 9.55259, &
  10.05260,10.55260,11.05260,11.55260,12.05260,12.55260,13.05260, &
  13.55260,14.05260,14.55260,15.05260,15.55260,16.05260,16.55260, &
  17.05260,17.55260,18.05260,18.55260,19.05260,19.55260,20.05260, &
  20.55260,21.05260,21.30000/
  data (muo(i), i=1,45)/ &
  28.95,28.95,28.95,28.95,28.95,28.95,28.95,28.95,28.95,28.95, &
  28.95,28.95,28.95,28.95,28.95,28.95,28.95,28.95,28.95,28.95, &
  28.95,28.95,28.94,28.94,28.94,28.93,28.92,28.89,28.83,28.73, &
  28.58,28.38,28.13,27.80,27.39,26.89,26.30,25.65,24.97,24.27, &
  23.53,22.73,21.89,21.03,20.60/
  data (oo(i), i=1,45)/ &
  0.000e+00,0.000e+00,0.000e+00,0.000e+00,0.000e+00,0.000e+00, &
  0.000e+00,0.000e+00,0.000e+00,0.000e+00,0.000e+00,0.000e+00, &
  0.000e+00,0.000e+00,0.000e+00,0.000e+00,0.000e+00,0.000e+00, &
  0.000e+00,0.000e+00,0.000e+00,0.240e-08,0.127e-06,0.176e-05, &
  0.177e-04,0.125e-03,0.615e-03,0.217e-02,0.575e-02,0.122e-01, &
  0.219e-01,0.350e-01,0.520e-01,0.736e-01,0.101e+00,0.135e+00, &
  0.177e+00,0.224e+00,0.275e+00,0.329e+00,0.388e+00,0.452e+00, &
  0.519e+00,0.589e+00,0.623e+00/
  data (n2o(i), i=1,45)/ &
  0.781,0.781,0.781,0.781,0.781,0.781,0.781,0.781,0.781,0.781, &
  0.781,0.781,0.781,0.781,0.781,0.781,0.781,0.781,0.781,0.781, &
  0.782,0.782,0.782,0.783,0.783,0.784,0.785,0.786,0.786,0.785, &
  0.782,0.778,0.774,0.767,0.758,0.745,0.726,0.698,0.662,0.618, &
  0.568,0.512,0.452,0.389,0.357/
  data (o2o(i), i=1,45)/ &
  0.210,0.210,0.210,0.210,0.210,0.210,0.210,0.210,0.210,0.210, &
  0.210,0.210,0.210,0.210,0.210,0.210,0.210,0.210,0.210,0.209, &
  0.209,0.209,0.209,0.208,0.208,0.207,0.205,0.203,0.200,0.195, &
  0.188,0.179,0.168,0.153,0.136,0.116,0.094,0.076,0.061,0.051, &
  0.043,0.036,0.029,0.022,0.020/
  data (cpo(i), i=1,45)/ &
  1002.5,1002.5,1002.5,1002.5,1002.5,1002.5,1002.5,1002.5,1002.5, &
  1002.5,1002.5,1002.5,1002.5,1002.5,1002.5,1002.5,1002.5,1002.5, &
  1002.5,1002.6,1002.6,1002.7,1002.8,1002.9,1003.0,1003.2,1003.5, &
  1004.1,1005.2,1006.9,1009.4,1012.8,1017.3,1023.0,1030.3,1039.2, &
  1049.7,1061.3,1073.3,1085.8,1099.4,1114.8,1132.1,1151.0,1160.9/
  data (go(i), i=1,45)/ &
  9.8,9.79,9.78,9.77,9.765,9.76,9.75,9.74,9.73,9.72,9.71,9.70, &
  9.69,9.68,9.67,9.66,9.64,9.63,9.62,9.61,9.60,9.59,9.58,9.57, &
  9.56,9.55,9.55,9.54,9.53,9.52,9.51,9.51,9.50,9.49,9.48,9.46, &
  9.45,9.43,9.40,9.37,9.33,9.29,9.23,9.18,9.15/

  ! ----- to form therdat common block:
  !       note: co2vf is determined in the vmrco2 subroutine

  do i = 1,lay
    pvf(i)  = etag(lay-i+1)*1000.0
    xvf(i) = alog(1000/pvf(i))
  end do ! loop 1

  do i = 1, 45
    muoo(i) = alog(muo(i))
    ooo(i)  = alog(oo(i)+1.)
    n2oo(i) = alog(n2o(i))
    o2oo(i) = alog(o2o(i))
    cpoo(i) = alog(cpo(i))
    goo(i)  = alog(go(i))
  end do ! loop 2

  do i = 1, lay
    amuvf(i) = exp(a18lin(xvf(i),xo,muoo,1,45))
    ovf(i)   = exp(a18lin(xvf(i),xo,ooo,1,45)) - 1.
    cn2vf(i) = exp(a18lin(xvf(i),xo,n2oo,1,45))
    o2vf(i)  = exp(a18lin(xvf(i),xo,o2oo,1,45))
    ! cpvf in (erg/g/k)
    cpvf(i)  = exp(a18lin(xvf(i),xo,cpoo,1,45))*10000.
    gvf(i)   = exp(a18lin(xvf(i),xo,goo,1,45))
  end do ! loop 3

  ! ----- to calculate effh
  ! efficiency in the o3 hartley band. the efficiency coefficient should be
  ! applied to the net (i.e., total - chemical potential) heating.

  do k=1,lay
    p = pvf(k)
    if (p>1.) then
      effh(k)=1.
    else if (p>1.e-2) then
      x = alog10(p)+1
      effh(k)=cho3l(1)+cho3l(2)*x+cho3l(3)*x*x+cho3l(4)*x*x*x
    else if (p>1.e-4) then
      x = alog10(p)+3
      effh(k)=cho3h(1)+cho3h(2)*x+cho3h(3)*x*x+cho3h(4)*x*x*x
    else
      effh(k) = 0.6944
    end if
  end do ! loop 10

  ! ----- to calculate the src heating efficiency (for sr band efficiency = 1.)

  do k = 1, lay
    p = pvf(k)
    if (p>1.e-2) then
      effsrc(k) = 0.7938
    else if (p>1.e-4) then
      x = alog10(p)+3
      effsrc(k) = cho2(1) + cho2(2)*x + cho2(3)*x*x + cho2(4)*x*x*x
    else
      effsrc(k) = 0.8320
    end if
  end do ! loop 20

  ! ----- to define the o2 heating efficiency for 762 nm

  do k = 1, lay
    xk=xvf(k)
    if (xk<=xo2nir(1)) then
      effo2nir(k) = 1.
    else if (xk<= xo2nir(10)) then
      effo2nir(k) = a18lin(xk,xo2nir,effo2o,1,10)
    else
      effo2nir(k) = 0.0
    end if
  end do ! loop 30

  ! ----- to calculate to2(cm-2)

  g1=o2vf(lay)*ana*pvf(lay)/(amuvf(lay)*gvf(lay))
  to2(lay)=g1*amuvf(lay)/32.

  kmxm = lay-1
  do kk=1,kmxm
    k=lay-kk
    g2=o2vf(k)*ana*pvf(k)/(amuvf(k)*gvf(k))
    to2(k)=to2(k+1)+(g1+g2)*0.5*(xvf(k+1)-xvf(k))
    g1=g2
  end do ! loop 25

  ! ----- to form chhdat common block:
  !       merging region for chemical heating and o2 (src &srb) o2 heating

  x1chem = 8.3
  x2chem = 9.8
  dxchem = x2chem - x1chem

  do k=1,lay
    if (xvf(k)>=x1chem) exit
  end do ! loop 4
  k1chem=k
  do k=1,lay
    if (xvf(k)<=x1chem) then
      wchem(k)=0.
    else if (xvf(k)<=x2chem) then
      wchem(k)=(xvf(k)-x1chem)/dxchem
    else
      wchem(k)=1.
    end if
  end do ! loop 6

  ! ----- to form radidx common block:
  !       indexes and weights to merge the upper atmosphere rad. scheme.

  ! * solar h2o:
  x1wvs2 = 2.0
  x2wvs2 = 3.0
  x1wvs3 = 7.5
  x2wvs3 = 12.25
  ! * weights:
  do k=1,lay
    if (xvf(k)<=x1wvs2) then
      wwvs2(k)=1.
    else if (xvf(k)<=x2wvs2) then
      wwvs2(k)=(x2wvs2-xvf(k))/(x2wvs2-x1wvs2)
    else
      wwvs2(k)=0.
    end if
    if (xvf(k)<=x1wvs3) then
      wwvs3(k)=1.
    else if (xvf(k)<=x2wvs3) then
      wwvs3(k)=(x2wvs3-xvf(k))/(x2wvs3-x1wvs3)
    else
      wwvs3(k)=0.
    end if
  end do

  ! * nir co2:
  x1co2s = 7.0
  x2co2s = 8.5
  x3co2s = 13.75
  x4co2s = 15.50
  ! * indexes:
  do k=1,lay
    if (xvf(k)>=x1co2s) exit
  end do
  k1co2s=k
  do k=k1co2s,lay
    if (xvf(k)>=x3co2s) exit
  end do
  k2co2s=k
  ! * weights:
  do k=1,lay
    if (xvf(k)<=x1co2s) then
      wco2s(k) =1.
      wco2st(k)=1.
    else if (xvf(k)<=x2co2s) then
      wco2s(k) =(x2co2s-xvf(k))/(x2co2s-x1co2s)
      wco2st(k)=1.
    else if (xvf(k)<=x3co2s) then
      wco2s(k) =0.
      wco2st(k)=1.
    else if (xvf(k)<=x4co2s) then
      wco2s(k) =0.
      wco2st(k)=(x4co2s-xvf(k))/(x4co2s-x3co2s)
    else
      wco2s(k) =0.
      wco2st(k)=0.
    end if
  end do

  ! * h2o ir bands:
  x1wvl3 = 11.5
  x2wvl3 = 14.0
  ! * weights:
  do k=1,lay
    if (xvf(k)<=x1wvl3) then
      wwvl3(k)=1.
    else if (xvf(k)<=x2wvl3) then
      wwvl3(k)=(x2wvl3-xvf(k))/(x2wvl3-x1wvl3)
    else
      wwvl3(k)=0.
    end if
  end do

  ! * ch4 ir band:
  x1ch4l = 10.5
  x2ch4l = 13.0
  ! * weights:
  do k=1,lay
    if (xvf(k)<=x1ch4l) then
      wch4l(k)=1.
    else if (xvf(k)<=x2ch4l) then
      wch4l(k)=(x2ch4l-xvf(k))/(x2ch4l-x1ch4l)
    else
      wch4l(k)=0.
    end if
  end do

  ! * o3 9.6um band:
  x1o3l = 10.0
  x2o3l = 15.0
  ! * weights:
  do k=1,lay
    if (xvf(k)<=x1o3l) then
      wo3l(k)=1.
    else if (xvf(k)<=x2o3l) then
      wo3l(k)=(x2o3l-xvf(k))/(x2o3l-x1o3l)
    else
      wo3l(k)=0.
    end if
  end do

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

