!>\file
!>\brief This module defines all the tracers that can be used in the model; one integer index per tracer
!!
!! @author Deji Akingunola (Spring, 2021)
!

module tracers_info_mod

  ! Defines the tracer attributes used in the atmospheric (Physics and chemistry) model
  type :: phys_tracer_info
    character(len=4)  :: name = ''     ! Tracer short name
    integer           :: phs = -1      ! Flag to indicate if physics is applied to the tracer (I/0)
    integer           :: srf = -1      ! Flag to indicate availability of surface fluxes;
                                         ! chem (generic?) tracers have special flags -1 and -2
    integer           :: wet = -1      ! Flag to indicate if tracer undergoes wet deposition
    integer           :: dry = -1      ! Flag to indicate if tracer undergoes dry deposition
    integer           :: cch = -1      ! Flag to indicate if tracer undergoes convective transport
    integer           :: ic = -1       ! Flag to indicate if initial conditions are available for the tracer
    real              :: tmin = 0.0    ! Minimum tracer value (set through xref and xpow)
    real              :: mw = 0.0      ! Molecular weight
    real              :: bndc = 0.0    ! Tracer boundary condition concentration (for generic/passive tracers)
  end type

  ! Extension of the atmospheric tracer attributes, including the ones used in the spectral core
  type, extends(phys_tracer_info) :: tracer_info
    character(len=80) :: lng_name = '' ! Tracer descriptive name
    integer           :: adv = -1      ! Is the tracer advected (1/0)
    real              :: xref = 0.0    !
    real              :: xpow = 1.0    !
  end type

  ! Allocated in model initialization routine ('trinfo')
  type(phys_tracer_info), allocatable, dimension(:) :: modl_tracers
  type(tracer_info),      allocatable, dimension(:) :: init_agcm_tracers

  ! Process-independent tracers
  integer :: ico2 = 0
  integer :: ilwc = 0
  integer :: iiwc = 0
  integer :: ihpo = 0
  integer :: ih2o = 0
  integer :: idms = 0
  integer :: iso2 = 0

  ! Bulk aerosol tracers
  integer :: iso4 = 0
  integer :: ibco = 0
  integer :: ibcy = 0
  integer :: ioco = 0
  integer :: iocy = 0
  integer :: issa = 0
  integer :: issc = 0
  integer :: idua = 0
  integer :: iduc = 0

  ! Other PAM/PLA tracers
  integer :: igs6 = 0
  integer :: igsp = 0
  !
  ! Gas-phase chemistry
  integer :: ntracgas = 0   !< Number of gas-phase chemistry tracers
  ! Tracer index values for gas-phase chemistry tracers
  ! The values are set in gaschem_tracers_init.F90 when switch_gas_chem=.true.
  integer :: itox = 0
  integer :: itnx = 0
  integer :: itc9 = 0
  integer :: itbx = 0
  integer :: itn5 = 0
  integer :: ithx = 0
  integer :: itn7 = 0
  integer :: itt5 = 0
  integer :: ithb = 0
  integer :: itmm = 0
  integer :: itco = 0
  integer :: itha = 0
  integer :: itmh = 0
  integer :: ithd = 0
  integer :: itc7 = 0
  integer :: itc4 = 0
  integer :: itf1 = 0
  integer :: itf2 = 0
  integer :: itf3 = 0
  integer :: itf4 = 0
  integer :: itf5 = 0
  integer :: itf6 = 0
  integer :: itcb = 0
  integer :: itbf = 0
  integer :: itdb = 0
  integer :: itny = 0
  integer :: itcy = 0
  integer :: itcz = 0
  integer :: itbz = 0
  integer :: itta = 0
  integer :: ittb = 0
  integer :: itx1 = 0
  integer :: itx2 = 0
  integer :: itx3 = 0
  integer :: ito1 = 0
  integer :: itoo = 0
  integer :: ito3 = 0
  integer :: itn1 = 0
  integer :: itn2 = 0
  integer :: itn3 = 0
  integer :: itn4 = 0
  integer :: itc1 = 0
  integer :: itc6 = 0
  integer :: itoc = 0
  integer :: itc3 = 0
  integer :: itc5 = 0
  integer :: itbr = 0
  integer :: itbo = 0
  integer :: itob = 0
  integer :: itbc = 0
  integer :: itbn = 0
  integer :: itt6 = 0
  integer :: ith0 = 0
  integer :: ithh = 0
  integer :: ith1 = 0
  integer :: ith2 = 0
  integer :: itm2 = 0
  integer :: itc2 = 0
  integer :: itbga1 = 0
  integer :: itshno = 0
  integer :: itsh2o = 0

  ! CMAM gas-phase mechanism tracers
!   integer :: itch4 = 0
  !
  !  Aerosol tracers indices for PLA calculations
  integer :: iaindt = -1  ! First index for PLA/PAM tracers
  integer :: ntracp = 0   ! Number of PLA/PAM tracers

  !  Total number of active tracers
  integer :: num_agcm_tracers = 0
  integer :: num_phys_tracers = 0

  !  Summed indices to determine whether there are any tracers with prescribed 
  !  surface fluxes or initial conditions. If not, certain parts of the code 
  !  can be skipped to improve performance.
  integer :: itrinit = 0
  integer :: itraflx = 0
  integer :: tracers_indx_last = 0

  ! CO2 correction computed as the difference between CO2 burdens and 
  ! cumulative surface flux.
  real*8 :: co2_xsfxcorr = 0.0d0

  contains

  !@objective
  ! Assign tracer species index, and add the tracer and its attributes to the
  ! Physics tracer_info structure
  !
  subroutine tracer_add(idx, tr_indx, name, descr, adv, phs, srf, wet, dry, &
                        cch, ic, mw, xref, xpow)
  implicit none

  integer,           intent(inout) :: idx
  integer,           intent(out)   :: tr_indx
  integer,           intent(in)    :: adv, phs, srf, wet, dry, cch, ic
  real,              intent(in)    :: mw, xref, xpow
  character(len=4),  intent(in)    :: name
  character(len=26), intent(in)    :: descr

  idx = idx + 1
  tr_indx = idx
  init_agcm_tracers(idx) % name     = name
  init_agcm_tracers(idx) % lng_name = descr
  init_agcm_tracers(idx) % adv      = adv
  init_agcm_tracers(idx) % phs      = phs
  init_agcm_tracers(idx) % srf      = srf
  init_agcm_tracers(idx) % wet      = wet
  init_agcm_tracers(idx) % dry      = dry
  init_agcm_tracers(idx) % cch      = cch
  init_agcm_tracers(idx) % ic       = ic
  init_agcm_tracers(idx) % mw       = mw
  init_agcm_tracers(idx) % xref     = xref
  init_agcm_tracers(idx) % xpow     = xpow
  init_agcm_tracers(idx) % bndc     = 0.0

  return
  end subroutine tracer_add

! ---------------------------------------------------------------------------

  function tracer_properties(F_attributes_S, F_adv, F_phs, F_srf, F_wet, &
                             F_dry, F_cch, F_ic, F_xref, F_xpow, F_mw,   &
                             F_bndc) result(F_istat)
  !@objective:
  ! Split each user-defined tracer attribute namelist input into individual 
  ! variables. The input 'F_attributes_S' is a string of comma-separated 
  ! tracer key-value pairs.
  ! The permitted keys are as follows;
  ! ADV:  Integer flag: is tracer advected (1) or not (0)
  ! PHS:  Integer flag: are physics applied to the tracer (1) or not (0)
  ! SRF:  Integer flag: is a prescribed surface flux available (1) or not (0)
  !       Flags -1 and -2 are reserved for gas-phase tracers
  ! WET:  Integer flag: does the tracer undergo wet deposition (1) or not (0)
  ! DRY:  Integer flag: does the tracer undergo dry deposition (1) or not (0)
  ! CCH:  Integer flag: does the tracer undergo convective transport
  ! XREF: Reference value for tracer
  ! XPOW: Reference power value for hybrid tracer
  ! MW:   Molecular weight (-1: no chemistry is applied and tracer is not used)
  ! BNDC: Real-valued tracer boundary condition concentration for generic or
  !       passive tracers; formerly CONTRAC

  use strings, only: str_tab2space, str_toint, str_toreal, upperc, &
                     split_on_sep, split_on_delim

  implicit none

  character(len=*), intent(in) :: F_attributes_S
  integer, intent(out), optional :: F_adv, F_phs, F_srf, F_wet
  integer, intent(out), optional :: F_dry, F_cch, F_ic
  real,    intent(out), optional :: F_xref, F_xpow, F_mw, F_bndc
  !@return
  integer :: F_istat
  !*@/
  integer, parameter :: NMAX = 32

  character(len=4), save :: default_name = "    "
  integer, save :: default_adv  = -1
  integer, save :: default_phs  = -1
  integer, save :: default_srf  = -1
  integer, save :: default_wet  = -1
  integer, save :: default_dry  = -1
  integer, save :: default_cch  = -1
  integer, save :: default_ic   = -1
  real, save :: default_xref  = 0.0
  real, save :: default_xpow  = 1.0
  real, save :: default_mw    = 0.0
  real, save :: default_bndc  = 0.0

  character(len=120) :: attributes_S, list_S(NMAX), key_S, val_S
  integer :: istat, n, n0, nlist
  integer :: adv, phs, srf, wet, dry, cch, ic
  real    :: xref, xpow, mw, bndc
  logical :: change_default_L
  !---------------------------------------------------------------

  F_istat = 0

  attributes_S = F_attributes_S
  call str_tab2space(attributes_S)
  attributes_S = adjustl(attributes_S)
  attributes_S = upperc(attributes_S)

  list_S = ' '
  call split_on_delim(list_S, nlist, attributes_S, ',')

  change_default_L = (list_S(1) == 'DEFAULT')
  n0 = 1
  if (change_default_L) n0 = 2

  adv  = default_adv
  phs  = default_phs
  srf  = default_srf
  wet  = default_wet
  dry  = default_dry
  cch  = default_cch
  ic   = default_ic
  xref = default_xref
  xpow = default_xpow
  mw   = default_mw
  bndc = default_bndc

  LOOP1: do n = n0, NMAX
    if (list_S(n) == ' ') cycle
    call split_on_sep(key_S, val_S, list_S(n), '=')

    select case(key_S)
      case('ADV')
        F_istat = min(F_istat, str_toint(adv, val_S))
      case('PHS')
        F_istat = min(F_istat, str_toint(phs, val_S))
      case('SRF')
        F_istat = min(F_istat, str_toint(srf, val_S))
      case('WET')
        F_istat = min(F_istat, str_toint(wet, val_S))
      case('DRY')
        F_istat = min(F_istat, str_toint(dry, val_S))
      case('CCH')
        F_istat = min(F_istat, str_toint(cch, val_S))
      case('IC')
        F_istat = min(F_istat, str_toint(ic, val_S))
      case('XREF')
        F_istat = min(F_istat, str_toreal(xref, val_S))
      case('XPOW')
        F_istat = min(F_istat, str_toreal(xpow, val_S))
      case('MW')
        F_istat = min(F_istat, str_toreal(mw, val_S))
      case('BNDC')
        F_istat = min(F_istat, str_toreal(bndc, val_S))
      case default
        write(*, *) 'CanAM tracer_properties unknown key: ', trim(key_S)
        F_istat = -1
    end select
  end do LOOP1

  if (change_default_L) then
    default_adv  = adv
    default_phs  = phs
    default_srf  = srf
    default_wet  = wet
    default_dry  = dry
    default_cch  = cch
    default_ic   = ic
    default_xref = xref
    default_xpow = xpow
    default_mw   = mw
    default_bndc = bndc
  end if

  if (present(F_adv )) F_adv  = adv
  if (present(F_phs )) F_phs  = phs
  if (present(F_srf )) F_srf  = srf
  if (present(F_wet )) F_wet  = wet
  if (present(F_dry )) F_dry  = dry
  if (present(F_cch )) F_cch  = cch
  if (present(F_ic  )) F_ic   = ic
  if (present(F_xref)) F_xref = xref
  if (present(F_xpow)) F_xpow = xpow
  if (present(F_mw  )) F_mw   = mw
  if (present(F_bndc)) F_bndc = bndc
  !---------------------------------------------------------------

  return
  end function tracer_properties

end module tracers_info_mod
!> \file
