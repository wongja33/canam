!>\file
!>\brief Purpose: Chemistry solver
!!
!! @author David Plummer, J.de Grandpre, S.R.Beagley
!
module mamchmf2_mod

implicit none ; private

public :: mamchmf2

contains

  subroutine mamchmf2 (arj, ark, chltrow, chlsrow, chdgrow, csadrow, wflxrow, &
                       emisln, acemphs, sfemrow, vddprow, vdsfrow, sfsaphs,   &
                       ssadphs, qtemp, airrow, delm, delzi, oxlfr, indxtrp,   &
                       indxlst, dtadv, savetr, kount, il1, il2, msg, lpphc,   &
            qOx, qNOx   , qClOx  , qBrOx   , qHNO3  , qHOx  , qHNO4 , qN2O,   &
           qHBr, qCH4   , qCO    , qCH2O   , qCH3OOH, qH2   , qHCl  , qClONO2,&
         qCFC11, qCFC12 , qCCl4  , qCH3CCl3, qHCFC22, qCH3Cl, qCH3Br, qCHBr3, &
        qCH2Br2, qO1D   , qO3P   , qO3     , qNO    , qNO2  , qNO3  , qN2O5,  &
            qCl, qHOCl  , qOClO  , qClO    , qCl2O2 , qBr   ,  qBrO , qHOBr,  &
          qBrCl, qBrONO2, qH2O   , qH      , qH2O2  , qOH   , qHO2  , qCH3O2, &
           qCl2, qAE1   , qSHNO3 , qSH2O)

  ! ---------------------------------------------------------------------------
  ! Authors (original routine): J.de Grandpre, S.R.Beagley
  ! Modified for GCM/MAM use (March - September 1994).
  !
  ! Chemical solver: D.Plummer, D.Chartrand, J.C. McConnell
  !
  ! Purpose: A troposphere / stratosphere / mesosphere chemistry solver.
  !
  ! Method : Full-newton solver
  !          Provides advected species tendencies for time T+1 and
  !          non-advected VMRs for time T+1 to the GCM.
  !
  ! New chemical solver:
  !
  ! Purpose : Forward integration in time of chemical set
  ! Revision: David Plummer, Edna Templeton, York University 1995
  !
  ! Called by: chem3.F90.
  ! Calls    : setrk05, n2o5hydro, coefsetv2, dgetrf, dgetrs,
  !            semimpv2, chdiag, hetchem
  ! ----------------------------------------------------------------------------

  use msizes, only          : ilg, ilev, ntrac
  use chem_params_mod, only : jpntjv, jpnrks, nsfcem, nvdep, nchdg, nwflx, &
                              jpspcs, jpnewt, jphnewt, jpsrxn, jppsc2,     &
                              jphcsta, jphcsto, vpbltz, vppi
  use mam_chem_mod
  use chemistry_mod, only   : hetchem
  use radcons_mod, only : co2_ppm

  implicit none

  integer, intent(in) :: il1, il2, kount, msg
  integer, dimension(ilg), intent(in) :: indxlst, indxtrp

  real, intent(in) :: dtadv     !< Advective timestep for CanAM5
  real, intent(in) :: savetr
  real, dimension(ilg), intent(in) :: delzi
  real, dimension(ilg), intent(in) :: vdsfrow  !< surface (2-D) emission field (molecules/cm^2/sec)
  real, dimension(ilg,ilev), intent(in) :: acemphs !< aircraft NOx emissions (molecules/cm^3/sec)
  real, dimension(ilg,ilev), intent(in) :: emisln  !< lightning NOx emission field (molecules/sec/molecule air)
  real, dimension(ilg,ilev), intent(in) :: sfsaphs !< sulphate (SO4) emission field
  real, dimension(ilg,ilev), intent(in) :: ssadphs !< stratospheric aerosol
  real, dimension(ilg,nsfcem), intent(in) :: sfemrow  !< nsfcem different emission fields for CO and NOx arranged in a preassumed order
  real, dimension(ilg,nvdep), intent(in) :: vddprow  !<
  real, dimension(ilg,ilev), intent(in) :: airrow, delm, qtemp

  logical, intent(in) :: lpphc

  real, dimension(ilg,nwflx), intent(inout) :: wflxrow
  real, dimension(ilg,ntrac), intent(inout) :: chltrow, chlsrow
  real, dimension(ilg,ilev,nchdg), intent(inout) :: chdgrow
  real, dimension(jpntjv,ilg,ilev), intent(inout) :: arj
  real, dimension(jpnrks,ilg,ilev), intent(inout) :: ark

  real, dimension(ilg,ilev), intent(out) :: oxlfr
  real, dimension(ilg,ilev,2), intent(out) :: csadrow

  ! Advected gas-phase chemistry species/families
  real, dimension(ilg,ilev), intent(inout) :: qOx, qNOx, qClOx, qBrOx, qHNO3
  real, dimension(ilg,ilev), intent(inout) :: qHOx, qHNO4, qN2O, qHBr, qCH4, qCO
  real, dimension(ilg,ilev), intent(inout) :: qCH2O, qCH3OOH, qH2, qHCl, qClONO2
  real, dimension(ilg,ilev), intent(inout) :: qCFC11, qCFC12, qCCl4, qCH3CCl3
  real, dimension(ilg,ilev), intent(inout) :: qHCFC22, qCH3Cl, qCH3Br, qCHBr3
  real, dimension(ilg,ilev), intent(inout) :: qCH2Br2

  ! Non-advected gas-phase chemistry species
  real, dimension(ilg,ilev), intent(inout) :: qO1D, qO3P, qO3, qNO, qNO2, qNO3
  real, dimension(ilg,ilev), intent(inout) :: qN2O5, qCl, qHOCl, qOClO, qClO
  real, dimension(ilg,ilev), intent(inout) :: qCl2O2, qBr, qBrO, qHOBr, qBrCl
  real, dimension(ilg,ilev), intent(inout) :: qBrONO2, qH2O, qH, qH2O2,qOH,qHO2
  real, dimension(ilg,ilev), intent(inout) :: qCH3O2, qCl2, qSHNO3, qSH2O
  real, dimension(ilg,ilev), intent(out) :: qAE1

  ! Local parameters
  integer, parameter :: maxitr = 50    !< maximum number of iterations
  real, parameter :: qcpmin = 1.0e-35  !< minimum mixing ratio
  real, parameter :: xtol = 5.0e-5     !< convergence criteria
  real, parameter :: eps = 1.0e-30     !< minimum diagonal element of Jacobian

  ! Locally defined work arrays
  integer, dimension(100) :: nleft
  integer, dimension(ilg,ilev) :: iconv
  integer :: ii

  real, dimension(jpspcs,ilg,ilev) :: an0, an2   !< gas-phase species arrays
  real, dimension(ilg,ilev,12) :: an3  !< heterogeneous chemistry species array
  real, dimension(ilg,ilev) :: h2ocpy
  real, dimension(jpnewt,jpnewt+1) :: fb
  real, dimension(2,ilg,ilev) :: csadwrk

  ! Variables for BLAS/LAPACK routines
  !integer (kind=4) :: ijacdim, info, inrhs, ipiv(100)    !< for Dgetrs, Dgetrf
  integer :: ijacdim, info, inrhs, ipiv(100)    !< for Sgetrs, Sgetrf
  !
  integer :: i, iclcl, idamp, it, j, k, n, nrep
  real :: cOxold, cHOxold, cNOxold, cClOxold, cBrOxold, hcmin, xf1, xf2, xmin

! ------------------------------------------------------------------------------

  if (kount == 0 .or. kount == 1) then
    do k = 1, ilev
      do i = il1, il2
        qO1D(i,k)   = 0.0
        qO3P(i,k)   = 0.0
        qO3(i,k)    = qOx(i,k)
        qNO(i,k)    = 0.0
        qNO2(i,k)   = qNOx(i,k)
        qNO3(i,k)   = 0.0
        qN2O5(i,k)  = 0.0
        qH2O2(i,k)  = 0.0
        qHO2(i,k)   = 0.0
        qH(i,k)     = 0.0
        qOH(i,k)    = qHOx(i,k)
        qHOCl(i,k)  = 0.0
        qCl(i,k)    = 0.0
        qOClO(i,k)  = qClOx(i,k)
        qCl2O2(i,k) = 0.0
        qCl2(i,k)   = 0.0
        qClO(i,k)   = 0.0
        qBrCl(i,k)  = 0.0
        qBrONO2(i,k)= 0.0
        qBr(i,k)    = 0.0
        qHOBr(i,k)  = 0.0
        qBrO(i,k)   = qBrOx(i,k)
        qCH3O2(i,k) = 0.0
        qSHNO3(i,k) = 0.0
        qSH2O(i,k)  = 0.0
      enddo
    enddo
  endif

  do k = 1, ilev
    do i = il1, il2
      cOxold   = qO3(i,k) + qO3P(i,k) + qO1D(i,k)
      cHOxold  = qH(i,k) + qOH(i,k) + qHO2(i,k) + 2.0*qH2O2(i,k)
      cNOxold  = qNO(i,k) + qNO2(i,k) + qNO3(i,k) + 2.0*qN2O5(i,k)
      cClOxold = qClO(i,k) + qHOCl(i,k) + qCl(i,k) + qOClO(i,k)   &
               + 2.0*(qCl2(i,k) + qCl2O2(i,k))
      cBrOxold = qBrO(i,k) + qHOBr(i,k) + qBr(i,k) + qBrCl(i,k) + qBrONO2(i,k)

      an2(1,i,k)  = qO3P(i,k)*qOx(i,k)/cOxold
      an2(2,i,k)  = qO3(i,k)*qOx(i,k)/cOxold
      an2(3,i,k)  = qO1D(i,k)*qOx(i,k)/cOxold
      an2(4,i,k)  = qH(i,k)*qHOx(i,k)/cHOxold
      an2(5,i,k)  = qOH(i,k)*qHOx(i,k)/cHOxold
      an2(6,i,k)  = qHO2(i,k)*qHOx(i,k)/cHOxold
      an2(7,i,k)  = qHNO4(i,k)
      an2(8,i,k)  = qNO(i,k)*qNOx(i,k)/cNOxold
      an2(9,i,k)  = qNO2(i,k)*qNOx(i,k)/cNOxold
      an2(10,i,k) = qNO3(i,k)*qNOx(i,k)/cNOxold
      an2(11,i,k) = qN2O5(i,k)*qNOx(i,k)/cNOxold
      an2(12,i,k) = qCl(i,k)*qClOx(i,k)/cClOxold
      an2(13,i,k) = qClO(i,k)*qClOx(i,k)/cClOxold
      an2(14,i,k) = qClONO2(i,k)
      an2(15,i,k) = qHOCl(i,k)*qClOx(i,k)/cClOxold
      an2(16,i,k) = qHCl(i,k)
      an2(17,i,k) = qCl2O2(i,k)*qClOx(i,k)/cClOxold
      an2(18,i,k) = qCl2(i,k)*qClOx(i,k)/cClOxold
      an2(19,i,k) = qOClO(i,k)*qClOx(i,k)/cClOxold
      an2(20,i,k) = qBr(i,k)*qBrOx(i,k)/cBrOxold
      an2(21,i,k) = qBrO(i,k)*qBrOx(i,k)/cBrOxold
      an2(22,i,k) = qBrCl(i,k)*qBrOx(i,k)/cBrOxold
      an2(23,i,k) = qBrONO2(i,k)*qBrOx(i,k)/cBrOxold
      an2(24,i,k) = qHBr(i,k)
      an2(25,i,k) = qHOBr(i,k)*qBrOx(i,k)/cBrOxold
      an2(26,i,k) = qCH3O2(i,k)
      an2(27,i,k) = qH2O(i,k)
      an2(28,i,k) = qHNO3(i,k)
      an2(29,i,k) = qH2O2(i,k)*qHOx(i,k)/cHOxold
      an2(30,i,k) = qN2O(i,k)
      an2(31,i,k) = qCH4(i,k)
      an2(32,i,k) = qCH2O(i,k)
      an2(33,i,k) = qCH3OOH(i,k)
      an2(34,i,k) = qCO(i,k)
      an2(35,i,k) = qH2(i,k)
      an2(36,i,k) = qCFC11(i,k)
      an2(37,i,k) = qCFC12(i,k)
      an2(38,i,k) = qCCl4(i,k)
      an2(39,i,k) = qCH3CCl3(i,k)
      an2(40,i,k) = qHCFC22(i,k)
      an2(41,i,k) = qCH3Cl(i,k)
      an2(42,i,k) = qCH3Br(i,k)
      an2(43,i,k) = qCHBr3(i,k)
      an2(44,i,k) = qCH2Br2(i,k)
    enddo
  enddo

  ! Copy the incoming SSAD field onto the old BGA1 field for output
  do k = 1, ilev
    do i = il1, il2
      qAE1(i,k) = ssadphs(i,k)
    enddo
  enddo

  ! Take a copy of the incoming H2O field (converted to mixing ratio) to use
  ! in deciding when to run HETCHEM and when to use the tropospheric N2O5
  ! hydrolysis parameterization.
  do k = 1, ilev
    do i = il1, il2
      h2ocpy(i,k) = qH2O(i,k)/airrow(i,k)
    enddo
  enddo

  ! Work array to collect diagnostic of PSC surface area density
  csadwrk(:,:,:) = 0.0

  ! For the range of model levels where heterogeneous chemistry is active,
  ! account for partitioning into the aerosol phase.
  if (lpphc) then

    do k = jphcsta, jphcsto
      do i = il1, il2
        qSHNO3(i,k) = max(qSHNO3(i,k),0.0)
        qSHNO3(i,k) = min(qSHNO3(i,k),1.0)
        qSH2O (i,k) = max(qSH2O (i,k),0.0)
        qSH2O (i,k) = min(qSH2O (i,k),1.0)
      enddo
    enddo

    do k = jphcsta, jphcsto
      do i = il1, il2
        an2(27,i,k) = qH2O(i,k)  * (1.0-qSH2O(i,k))
        an2(28,i,k) = qHNO3(i,k) * (1.0-qSHNO3(i,k))
        an3(i,k,11) = qHNO3(i,k) * qSHNO3(i,k)
        an3(i,k,12) = qH2O(i,k)  * qSH2O(i,k)
      enddo
    enddo
  endif  ! end conditional on lpphc

  do k = 1, ilev
    do i = il1, il2
      xmin = qcpmin * airrow(i,k)

      do n=1,jpspcs
        an2(n,i,k) = max(an2(n,i,k), xmin)
      enddo
      an3(i,k,11) = max(an3(i,k,11), xmin)
      an3(i,k,12) = max(an3(i,k,12), xmin)
    enddo
  enddo

  ! HALF TIMESTEP OF EMISSIONS

  ! lightning emissions
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + emisln(i,k) * airrow(i,k) * 0.5*dtadv
    enddo
  enddo

  ! aircraft emissions
  xf1 = 0.95 * 0.5 * dtadv
  xf2 = 0.05 * 0.5 * dtadv
  do k = msg, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + xf1*acemphs(i,k)
      an2(9,i,k) = an2(9,i,k) + xf2*acemphs(i,k)
    enddo
  enddo

  ! emissions restricted to the surface and put into the bottom two levels
  xf1 = 0.5

  do k = ilev-1, ilev
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k) &
                  + xf1*0.95 * sfemrow(i,5) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2( 9,i,k) = an2( 9,i,k) &
                  + xf1*0.05 * sfemrow(i,5) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2(34,i,k) = an2(34,i,k) &
                  + xf1*sfemrow(i,2) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    enddo
  enddo

  ! emissions from open biomass burning put into the lowest ~ 1 km.
  ! For the standard 71-level model, this is the bottom 7 levels.
  k = ilev
  do i = il1, il2
    an2( 8,i,k) = an2( 8,i,k)   &
                + 0.095*sfemrow(i,4) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    an2( 9,i,k) = an2( 9,i,k)   &
                + 0.005*sfemrow(i,4) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    an2(34,i,k) = an2(34,i,k)   &
                + 0.100*sfemrow(i,1) * airrow(i,k)*0.5 * dtadv/delm(i,k)
  enddo

  xf1 = 1.0 / 6.0
  do k = ilev-6, ilev-1
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k)   &
                  + xf1*0.855*sfemrow(i,4) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2( 9,i,k) = an2( 9,i,k)   &
                  + xf1*0.045*sfemrow(i,4) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2(34,i,k) = an2(34,i,k)   &
                  + xf1*0.900*sfemrow(i,1) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    enddo
  enddo

  ! Special treatment for emissions from International Shipping:
  ! put them into the bottom two layers, but assume that 50% of NOx is already
  ! oxidized to HNO3 to account for non-linearities in plume.
  xf1 = 0.5
  do k = ilev-1, ilev
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k)   &
                  + xf1*0.475*sfemrow(i,6) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2( 9,i,k) = an2( 9,i,k)   &
                  + xf1*0.025*sfemrow(i,6) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2(28,i,k) = an2(28,i,k)   &
                  + xf1*0.500*sfemrow(i,6) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2(34,i,k) = an2(34,i,k)   &
                  + xf1*sfemrow(i,3) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    enddo
  enddo

  ! HALF TIMESTEP OF DRY DEPOSITION
  !
  ! See vdep1 subroutine for the composition of vddprow.

  ! Diagnostic calculation of dry deposition flux (units of moles/m^2/sec)
  xf1 = 1.0e4 * savetr / (6.0221415E23*dtadv)
  k = ilev

  do i = il1, il2
    wflxrow(i,3) = wflxrow(i,3) + xf1*delm(i,k)*an2(28,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vddprow(i,3)*delzi(i)))
    wflxrow(i,4) = wflxrow(i,4) + xf1*delm(i,k)*an2( 2,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vddprow(i,1)*delzi(i)))
    wflxrow(i,5) = wflxrow(i,5) + xf1*delm(i,k)*an2( 9,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vddprow(i,2)*delzi(i)))
    wflxrow(i,6) = wflxrow(i,6) + xf1*delm(i,k)*an2( 2,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vdsfrow(i)*delzi(i)))
  enddo

  ! Add dry deposition of HBr and HCl, assuming deposition velocity is equal
  ! to that of HNO3.
  k = ilev

  do i = il1, il2
    an2( 2,i,k) = an2( 2,i,k) * exp(-0.5*dtadv*vddprow(i,1)*delzi(i))
    an2( 9,i,k) = an2( 9,i,k) * exp(-0.5*dtadv*vddprow(i,2)*delzi(i))
    an2(16,i,k) = an2(16,i,k) * exp(-0.5*dtadv*vddprow(i,3)*delzi(i))
    an2(24,i,k) = an2(24,i,k) * exp(-0.5*dtadv*vddprow(i,3)*delzi(i))
    an2(28,i,k) = an2(28,i,k) * exp(-0.5*dtadv*vddprow(i,3)*delzi(i))
    an2(29,i,k) = an2(29,i,k) * exp(-0.5*dtadv*vddprow(i,4)*delzi(i))
    an2(32,i,k) = an2(32,i,k) * exp(-0.5*dtadv*vddprow(i,5)*delzi(i))
    an2(33,i,k) = an2(33,i,k) * exp(-0.5*dtadv*vddprow(i,6)*delzi(i))
  enddo

  ! Calculate gas-phase reaction rates
  call setrk05 (ark, qtemp, airrow, ssadphs, h2ocpy, il1, il2, lpphc)

  ! Separate subroutine to calculate N2O5 hydrolysis on tropospheric aerosols
  call n2o5hydro (ark, chdgrow, qtemp, airrow, h2ocpy, sfsaphs, &
                  savetr, il1, il2)

  ! Prevent halocarbons from decaying below the hole-filling minimum
  ! by turning off the decay as the concentration gets very small
  do k = 1, ilev
    do i = il1, il2
      hcmin = 1.0e-14 * airrow(i,k)

      if (an2(30,i,k) < hcmin) then
        ark(4,i,k)  = 1.0e-04*ark(4,i,k)
        ark(75,i,k) = 1.0e-04*ark(75,i,k)
        arj(28,i,k) = 1.0e-04*arj(28,i,k)
      endif
      if (an2(36,i,k) < hcmin) then
        ark(107,i,k) = 1.0e-04*ark(107,i,k)
        arj(31,i,k)  = 1.0e-04*arj(31,i,k)
      endif
      if (an2(37,i,k) < hcmin) then
        ark(108,i,k) = 1.0e-04*ark(108,i,k)
        arj(32,i,k)  = 1.0e-04*arj(32,i,k)
      endif
      if (an2(38,i,k) < hcmin) then
        ark(101,i,k) = 1.0e-04*ark(101,i,k)
        arj(38,i,k)  = 1.0e-04*arj(38,i,k)
      endif
      if (an2(39,i,k) < hcmin) then
        ark(102,i,k) = 1.0e-04*ark(102,i,k)
        arj(39,i,k)  = 1.0e-04*arj(39,i,k)
      endif
      if (an2(40,i,k) < hcmin) then
        ark(103,i,k) = 1.0e-04*ark(103,i,k)
        ark(104,i,k) = 1.0e-04*ark(104,i,k)
        arj(41,i,k)  = 1.0e-04*arj(41,i,k)
      endif
      if (an2(41,i,k) < hcmin) then
        ark(105,i,k) = 1.0e-04*ark(105,i,k)
        ark(106,i,k) = 1.0e-04*ark(106,i,k)
        arj(40,i,k)  = 1.0e-04*arj(40,i,k)
      endif
      if (an2(42,i,k) < hcmin) then
        ark(91,i,k) = 1.0e-04*ark(91,i,k)
        ark(92,i,k) = 1.0e-04*ark(92,i,k)
        arj(37,i,k) = 1.0e-04*arj(37,i,k)
      endif
      if (an2(43,i,k) < hcmin) then
        ark(111,i,k) = 1.0e-04*ark(111,i,k)
        ark(112,i,k) = 1.0e-04*ark(112,i,k)
        arj(42,i,k)  = 1.0e-04*arj(42,i,k)
      endif
      if (an2(44,i,k) < hcmin) then
        ark(113,i,k) = 1.0e-04*ark(113,i,k)
        ark(114,i,k) = 1.0e-04*ark(114,i,k)
        arj(43,i,k)  = 1.0e-04*arj(43,i,k)
      endif
    enddo
  enddo
  !
  ! --- Newton's method iterative solution for chemistry

  ! --- diagnostic counter
  nleft(:) = 0

  ! --- convergence flag
  iconv(:,:) = 0

  an0(:,:,:) = an2(:,:,:)

  do k = 1, ilev
    idamp = 0

    do i = il1, il2
      do it = 1, maxitr

        nleft(it) = nleft(it) + 1
        fb(:,:) = 0.0

        call coefsetv2 (fb, an2(1:jpspcs,i,k), an0(1:jpspcs,i,k),          &
                        arj(1:jpntjv,i,k), ark(1:jpnrks,i,k), airrow(i,k), &
                        dtadv, co2_ppm )

        ijacdim = jpnewt
        call sgetrf (ijacdim, ijacdim, fb, ijacdim, ipiv, info)

        if (info /= 0) then
          write(6,*) "mamchmf2 (sgetrF):"
          write(6,*) "SINGULAR MATRIX - stopping", k, i, it
          call flush(6)
          call xit ('MAMCHMF2',-16)
          stop
        endif

        inrhs = 1
        call sgetrs ('N', ijacdim, inrhs, fb, ijacdim, ipiv,  &
                     fb(:,jpnewt+1), ijacdim, info)
        if (info /= 0) then
          write(6,*) "mamchmf2 (sgetrS):"
          write(6,*) "SINGULAR MATRIX - stopping", k, i, it
          call flush(6)
          call xit ('MAMCHMF',-16)
          stop
        endif

        ! Update the species concentrations
        xmin=qcpmin*airrow(i,k)
        do j = 1, jpnewt
          an2(j,i,k) = max(an2(j,i,k)+fb(j,jpnewt+1), xmin)
        enddo

        iclcl=1
        do j=1,jpnewt
          if (abs(fb(j,jpnewt+1)/an2(j,i,k)) > xtol   &
             .and.  an2(j,i,k) > xmin) iclcl = 0
        enddo

        ! Damp any oscillating solutions
        if (it == 15 .and. iclcl == 0) then
          idamp = idamp + 1
          do j = 1, jpnewt
            an2(j,i,k) = max(an2(j,i,k) - 0.2*fb(j,jpnewt+1), xmin)
          enddo
        endif

        ! Update the species not included in the Jacobian
        call semimpv2 (an2(1:jpspcs,i,k), an0(1:jpspcs,i,k), &
                       arj(1:jpntjv,i,k), ark(1:jpnrks,i,k), &
                       airrow(i,k), dtadv, co2_ppm )

        if (iclcl == 1) then
          iconv(i,k) = 1
          exit
        endif

      enddo  ! end loop 250 over it=1,maxitr
    enddo    ! end loop 225 over i=il1,il2
  enddo      ! end loop 200 over k=1,ilev

  nrep = 0
  do k = 1, ilev
    do i = il1, il2
      if (iconv(i,k) == 0) nrep = nrep + 1
    enddo
  enddo

 ! if (nrep > 0) then
 !   write(6,'(a48,i6)') "mamchmf2: WARNING: Failed to converge all points",nrep
 ! endif

  !***********************************************************************
  ! Calculation of chemical diagnostics at the end of chemistry
  !
  call chdiag (chltrow, chlsrow, chdgrow, oxlfr, an2, an0, airrow, &
               delm, arj, ark, co2_ppm, savetr, dtadv, indxtrp,   &
               indxlst, il1, il2, ilg, ilev, ntrac, jpspcs)

  ! Supplementary diagnostic output of 3-D lightning NOx emission
  ! with units of molecules/cm^3/sec
  do k = msg+1, ilev
    do i = il1, il2
      chdgrow(i,k,13) = chdgrow(i,k,13) + savetr*emisln(i,k)*airrow(i,k)
    enddo
  enddo

  !***********************************************************************

  if (lpphc) then
    ! Pull out species required for heterogeneous chemistry

    do k = 1, ilev
      do i = il1, il2
        an3(i,k,1)  = an2(14,i,k)
        an3(i,k,2)  = an2(15,i,k)
        an3(i,k,3)  = an2(11,i,k)
        an3(i,k,4)  = an2(18,i,k)
        an3(i,k,5)  = an2(23,i,k)
        an3(i,k,6)  = an2(25,i,k)
        an3(i,k,7)  = an2(22,i,k)
        an3(i,k,8)  = an2(27,i,k)
        an3(i,k,9)  = an2(28,i,k)
        an3(i,k,10) = an2(16,i,k)
      enddo
    enddo

    call hetchem (an3(1,1,1), an3(1,1,2),  an3(1,1,3),  an3(1,1,4),  &
                  an3(1,1,5), an3(1,1,6),  an3(1,1,7),  an3(1,1,8),  &
                  an3(1,1,9), an3(1,1,10), an3(1,1,11), an3(1,1,12), &
                  qtemp, airrow, ssadphs, h2ocpy, csadwrk, dtadv,   &
                  il1, il2, ilg, ilev, jphcsta, jphcsto, jpsrxn, jppsc2)

    do k = 1, ilev
      do i = il1, il2
        an2(14,i,k) = an3(i,k,1)
        an2(15,i,k) = an3(i,k,2)
        an2(11,i,k) = an3(i,k,3)
        an2(18,i,k) = an3(i,k,4)
        an2(23,i,k) = an3(i,k,5)
        an2(25,i,k) = an3(i,k,6)
        an2(22,i,k) = an3(i,k,7)
        an2(27,i,k) = an3(i,k,8)
        an2(28,i,k) = an3(i,k,9)
        an2(16,i,k) = an3(i,k,10)
      enddo
    enddo

    ! Diagnostic output of stratospheric sulphate aerosol surface area
    ! density in units of cm^2/cm^3
    do k = 1, jphcsta-1
      do i = il1, il2
        csadrow(i,k,1) = 1.0e-08 * ssadphs(i,k)
      enddo
    enddo

    do k = jphcsta, jphcsto
      do i = il1, il2
        csadrow(i,k,1) = csadwrk(1,i,k)
      enddo
      do i = il1, il2
        csadrow(i,k,2) = csadwrk(2,i,k)
      enddo
    enddo
  !
  endif   ! end conditional on LPPHC
  !
  ! HALF TIMESTEP OF DRY DEPOSITION
  !
  ! Diagnostic calculation of dry deposition flux
  xf1 = 1.0e4 * savetr / (6.0221415e23*dtadv)
  k = ilev

  do i = il1, il2
    wflxrow(i,3) = wflxrow(i,3) + xf1*delm(i,k)*an2(28,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vddprow(i,3)*delzi(i)))
    wflxrow(i,4) = wflxrow(i,4) + xf1*delm(i,k)*an2( 2,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vddprow(i,1)*delzi(i)))
    wflxrow(i,5) = wflxrow(i,5) + xf1*delm(i,k)*an2( 9,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vddprow(i,2)*delzi(i)))
    wflxrow(i,6) = wflxrow(i,6) + xf1*delm(i,k)*an2( 2,i,k)/airrow(i,k) &
                 * (1.0 - exp(-0.5*dtadv*vdsfrow(i)*delzi(i)))
  enddo

  k = ilev
  do i = il1, il2
    an2( 2,i,k) = an2( 2,i,k) * exp(-0.5*dtadv*vddprow(i,1)*delzi(i))
    an2( 9,i,k) = an2( 9,i,k) * exp(-0.5*dtadv*vddprow(i,2)*delzi(i))
    an2(16,i,k) = an2(16,i,k) * exp(-0.5*dtadv*vddprow(i,3)*delzi(i))
    an2(24,i,k) = an2(24,i,k) * exp(-0.5*dtadv*vddprow(i,3)*delzi(i))
    an2(28,i,k) = an2(28,i,k) * exp(-0.5*dtadv*vddprow(i,3)*delzi(i))
    an2(29,i,k) = an2(29,i,k) * exp(-0.5*dtadv*vddprow(i,4)*delzi(i))
    an2(32,i,k) = an2(32,i,k) * exp(-0.5*dtadv*vddprow(i,5)*delzi(i))
    an2(33,i,k) = an2(33,i,k) * exp(-0.5*dtadv*vddprow(i,6)*delzi(i))
  enddo

  ! HALF TIMESTEP OF EMISSIONS
  !
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + emisln(i,k)*airrow(i,k)*0.5*dtadv
    enddo
  enddo

  xf1 = 0.95*0.5*dtadv
  xf2 = 0.05*0.5*dtadv
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + xf1*acemphs(i,k)
      an2(9,i,k) = an2(9,i,k) + xf2*acemphs(i,k)
    enddo
  enddo

  ! Collected surface emission sources
  xf1 = 0.5
  do k=ilev-1,ilev
    do i=il1,il2
      an2( 8,i,k) = an2( 8,i,k)   &
                  + xf1*0.95*sfemrow(i,5) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2( 9,i,k) = an2( 9,i,k)   &
                  + xf1*0.05*sfemrow(i,5) * airrow(i,k)*0.5 * dtadv/delm(i,k)
      an2(34,i,k) = an2(34,i,k)   &
                  + xf1*sfemrow(i,2) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    enddo
  enddo

  ! Open biomass burning
  k=ilev
  do i=il1,il2
    an2( 8,i,k) = an2( 8,i,k)   &
                + 0.095*sfemrow(i,4) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    an2( 9,i,k) = an2( 9,i,k)   &
                + 0.005*sfemrow(i,4) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    an2(34,i,k) = an2(34,i,k)   &
                + 0.100*sfemrow(i,1) * airrow(i,k)*0.5 * dtadv/delm(i,k)
  enddo

  xf1 = 1.0/6.0
  do k=ilev-6,ilev-1
    do i=il1,il2
      an2( 8,i,k) = an2( 8,i,k)   &
                  + xf1*0.855*sfemrow(i,4) * airrow(i,k)*0.5*dtadv/delm(i,k)
      an2( 9,i,k) = an2( 9,i,k)   &
                  + xf1*0.045*sfemrow(i,4) * airrow(i,k)*0.5*dtadv/delm(i,k)
      an2(34,i,k) = an2(34,i,k)   &
                  + xf1*0.900*sfemrow(i,1) * airrow(i,k)*0.5*dtadv/delm(i,k)
    enddo
  enddo

  ! International Shipping
  xf1 = 0.5
  do k = ilev-1, ilev
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k)   &
                  + xf1*0.475*sfemrow(i,6) * airrow(i,k)*0.5*dtadv/delm(i,k)
      an2( 9,i,k) = an2( 9,i,k)   &
                  + xf1*0.025*sfemrow(i,6) * airrow(i,k)*0.5*dtadv/delm(i,k)
      an2(28,i,k) = an2(28,i,k)   &
                  + xf1*0.500*sfemrow(i,6) * airrow(i,k)*0.5*dtadv/delm(i,k)
      an2(34,i,k) = an2(34,i,k)   &
                  + xf1*sfemrow(i,3) * airrow(i,k)*0.5 * dtadv/delm(i,k)
    enddo
  enddo

  do k = 1, ilev
    do i = il1, il2
      qO3P(i,k)     = an2(1,i,k)
      qO3(i,k)      = an2(2,i,k)
      qO1D(i,k)     = an2(3,i,k)
      qH(i,k)       = an2(4,i,k)
      qOH(i,k)      = an2(5,i,k)
      qHO2(i,k)     = an2(6,i,k)
      qHNO4(i,k)    = an2(7,i,k)
      qNO(i,k)      = an2(8,i,k)
      qNO2(i,k)     = an2(9,i,k)
      qNO3(i,k)     = an2(10,i,k)
      qN2O5(i,k)    = an2(11,i,k)
      qCL(i,k)      = an2(12,i,k)
      qCLO(i,k)     = an2(13,i,k)
      qClONO2(i,k)  = an2(14,i,k)
      qHOCl(i,k)    = an2(15,i,k)
      qHCl(i,k)     = an2(16,i,k)
      qCL2O2(i,k)   = an2(17,i,k)
      qCl2(i,k)     = an2(18,i,k)
      qOClO(i,k)    = an2(19,i,k)
      qBr(i,k)      = an2(20,i,k)
      qBrO(i,k)     = an2(21,i,k)
      qBrCl(i,k)    = an2(22,i,k)
      qBrONO2(i,k)  = an2(23,i,k)
      qHBr(i,k)     = an2(24,i,k)
      qHOBr(i,k)    = an2(25,i,k)
      qCH3O2(i,k)   = an2(26,i,k)
      qH2O(i,k)     = an2(27,i,k)
      qHNO3(i,k)    = an2(28,i,k)
      qH2O2(i,k)    = an2(29,i,k)
      qN2O(i,k)     = an2(30,i,k)
      qCH4(i,k)     = an2(31,i,k)
      qCH2O(i,k)    = an2(32,i,k)
      qCH3OOH(i,k)  = an2(33,i,k)
      qCO(i,k)      = an2(34,i,k)
      qH2(i,k)      = an2(35,i,k)
      qCFC11(i,k)   = an2(36,i,k)
      qCFC12(i,k)   = an2(37,i,k)
      qCCl4(i,k)    = an2(38,i,k)
      qCH3CCl3(i,k) = an2(39,i,k)
      qHCFC22(i,k)  = an2(40,i,k)
      qCH3Cl(i,k)   = an2(41,i,k)
      qCH3Br(i,k)   = an2(42,i,k)
      qCHBr3(i,k)   = an2(43,i,k)
      qCH2Br2(i,k)  = an2(44,i,k)

      qOx(i,k)   = qO3(i,k) + qO3P(i,k) + qO1D(i,k)
      qHOx(i,k)  = qH(i,k) + qOH(i,k) + qHO2(i,k) + 2.0*qH2O2(i,k)
      qNOx(i,k)  = qNO(i,k) + qNO2(i,k) + qNO3(i,k) + 2.0*qN2O5(i,k)
      qClOx(i,k) = qCl(i,k) + qClO(i,k) + 2.0*(qCl2O2(i,k) + qCl2(i,k)) &
                 + qHOCl(i,k) + qOClO(i,k)
      qBrOx(i,k) = qBr(i,k) + qBrO(i,k) + qBrONO2(i,k) + qBrCl(i,k) + qHOBr(i,k)

    enddo   ! end loop over i=il1,il2
  enddo  ! end loop over k=1,ilev

  ! For the range of model levels where heterogeneous chemistry is active,
  ! account for partitioning into the aerosol phase
  if (lpphc) then
    do k = jphcsta, jphcsto
      do i = il1, il2
        qHNO3(i,k)  = an2(28,i,k) + an3(i,k,11)
        qH2O(i,k)   = an2(27,i,k) + an3(i,k,12)
        qSHNO3(i,k) = an3(i,k,11)/qHNO3(i,k)
        qSH2O(i,k)  = an3(i,k,12)/qH2O(i,k)
      enddo
    enddo
  endif
  !
  return
  end subroutine mamchmf2

end module mamchmf2_mod
