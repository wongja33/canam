!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * jul 27/2019 - m.lazare. remove {com,csp,csn} parmsub variables.
!     * dec 23/2007 - m.lazare. new version for gcm15g:
!     *                         - "SL" and "SLSAVE" conditional
!     *                            code removed.
!     * jun 27/2006 - m.lazare. previous version com15pf for gcm15f.
!     *                         - no real*8 :: for trig functions.
!     * nov 28/03 - m.lazare/ previous version com13pc for gcm13c, with gcmparm
!     *            l.solheim. variables replaced by explicit parameter
!     *                       statement values.
!     * may 26/03 - m.lazare. new common deck (com13pb) containing declarations
!     *                       pertaining only to physics or s/l
!     *                       preparation parallel subroutines.
!     *                       longitude and level are explicitly split
!     *                       to facilitate conversion to/from pointers.
!     *                       note that all the common blocks have
!     *                       to be local (private) to each task.
!==================================================================
!     * grid slice work fields.
!
!     * FOR I/O PURPOSES, (U,V,T,ES,(TRAC))'TG MUST BE CONSECUTIVE.
!     * the following fields are mapped into the "FOUR1" work array
!     * in mhanlp4.
!
common /gr/ ttg   (ilg_tp,ilev)
common /gr/ estg  (ilg_tp,levs)
common /gr/ tractg(ilg_tp,ilev,ntraca)
common /gr/ vtg   (ilg_tp,ilev)
common /gr/ utg   (ilg_tp,ilev)
common /gr/ pstg  (ilg_tp)
!
common /gr/ pressg(ilg_tp)
common /gr/ pg    (ilg_tp,ilev),cg    (ilg_tp,ilev)
common /gr/ tg    (ilg_tp,ilev),esg   (ilg_tp,levs)
common /gr/ tracg (ilg_tp,ilev,ntraca)
common /gr/ psdpg (ilg_tp)
common /gr/ ug    (ilg_tp,ilev),vg    (ilg_tp,ilev)
common /gr/ psdlg (ilg_tp)
!$omp threadprivate (/gr/)
!
!     * note that /gr1/ contains fields used in both the physics
!     * and dynamics parallel regions.
!
common /gr1/ dsgj (idlm), dshj (idlm), dlnsgj(idlm)
common /gr1/ d1sgj(idlm), a1sgj(idlm), b1sgj (idlm)
common /gr1/ d2sgj(idlm), a2sgj(idlm), b2sgj (idlm)
!$omp threadprivate (/gr1/)
!==================================================================
!     * /gr2/ contains work fields only for physics.
!
common /gr2/ sgj (ilg,ilev),  shj  (ilg,ilev)
common /gr2/ shtj(ilg,ilevp1), sgbj (ilg,ilev)
!$omp threadprivate (/gr2/)
!==================================================================
!     * associated "ROW" vectors for multi-latitude formulation
!     * (both physics and s/l init).
!     * *** note that since these are used on both the transform
!     *     and general computation "ROWS", the largest, namely
!     *     the transform, is chosen.
!
real*8  :: cosj !<
real*8  :: wj !<
real*8  :: radj !<
real    :: dlon !<
integer :: jl !<
integer :: ilsl !<

common /gaussj/ cosj(ilg_tp), wj(ilg_tp)
common /gaussj/ radj(ilg_tp), dlon(ilg_tp)
common /gaussj/ jl(ilg_tp), ilsl(ilg_tp)
!$omp threadprivate (/gaussj/)
!======================================================================
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
