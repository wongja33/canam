!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_exp (data,data_zmax,alt,alt_exp,iorder, &
                      il1,il2,lev1,lev2,nlons,nlevs)
  !
  !  this routine exponentially damps a longitude by altitude array
  !  of data above a specified altitude.
  !
  !  aug. 13/95 - c. mclandress
  !
  !  output arguement:
  !  -----------------
  !
  !     * data = modified data array.
  !
  !  input arguements:
  !  -----------------
  !
  !     * data    = original data array.
  !     * alt     = altitudes.
  !     * alt_exp = altitude above which exponential decay applied.
  !     * iorder  = 1 means vertical levels are indexed from top down
  !     *           (i.e., highest level indexed 1 and lowest level nlevs)
  !     *           /= 1 highest level is index nlevs.
  !     * il1     = first longitudinal index to use (il1 >= 1).
  !     * il2     = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * lev1    = first altitude level to use (lev1 >=1).
  !     * lev2    = last altitude level to use (lev1 < lev2 <= nlevs).
  !     * nlons   = number of longitudes.
  !     * nlevs   = number of vertical
  !
  implicit none

  !  input work arrays:
  !  ------------------
  !
  !     * data_zmax = data values just above altitude alt_exp.
  !
  integer, intent(in) :: iorder !< Variable description\f$[units]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: lev1   !< Variable description\f$[units]\f$
  integer, intent(in) :: lev2   !< Variable description\f$[units]\f$
  integer, intent(in) :: nlons !< Variable description\f$[units]\f$
  integer, intent(in) :: nlevs !< Variable description\f$[units]\f$
  real, intent(in)    :: alt_exp !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: data !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons) :: data_zmax !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: alt !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: levbot
  integer  :: levtop
  integer  :: lincr
  integer  :: i
  integer  :: l
  real    :: hscale
  data  hscale / 5.e3 /
  !-----------------------------------------------------------------------
  !
  !  index of lowest altitude level (bottom of drag calculation).
  !
  levbot = lev2
  levtop = lev1
  lincr  = 1
  if (iorder /= 1) then
    levbot = lev1
    levtop = lev2
    lincr  = - 1
  end if
  !
  !  data values at first level above alt_exp.
  !
  do i = il1,il2
    do l = levtop,levbot,lincr
      if (alt(i,l) >= alt_exp) then
        data_zmax(i) = data(i,l)
      end if
    end do ! loop 10
  end do ! loop 20
  !
  !  exponentially damp field above alt_exp to model top at l=1.
  !
  do l = 1,lev2
    do i = il1,il2
      if (alt(i,l) >= alt_exp) then
        data(i,l) = data_zmax(i) * exp( (alt_exp - alt(i,l))/hscale)
      end if
    end do ! loop 30
  end do ! loop 40
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
