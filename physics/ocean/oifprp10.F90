!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine oifprp10(sno,evapf,qg,albs,dens, &   !output
                          snow,gt,gc,ps,    &   ! input
                          delt,ilg,iis,iif)
  !
  !     * feb 13/21 - m.lazare. pass in snow density and define to lim2
  !                       value when new snow falls.
  !     * may 06/14 - m.lazare. new version for gcm18+:
  !     *                       based on oifprp9 but only calculations
  !     *                       left are for evapf,qg, updating
  !     *                       sno with snow over seaice.
  !     *                       the updating of runoff with rain over
  !     *                       seaice has been moved out to sfcproc2
  !     *                       before the call to oifpst9.
  !     *                       all albedo calculations have
  !     *                       been moved to snosica for seaice and
  !     *                       to ocnalb for open water.
  !
  !     * performs preliminary calculations over open water and sea-ice.
  !
  use phys_consts, only : eps1, eps2, t1s, rw1, rw2, rw3, ri1, ri2, ri3
  implicit none
  !
  !     * output or i/o fields:
  !
  real, intent(inout), dimension(ilg) :: sno    !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: evapf  !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: qg     !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: albs   !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg)   :: dens   !<Variable description\f$[units]\f$
  !
  !     * input fields:
  !
  real, intent(in), dimension(ilg) :: snow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ps !< Variable description\f$[units]\f$
  !
  real, intent(in) :: delt !< Variable description\f$[units]\f$
  integer, intent(in) :: ilg !< Variable description\f$[units]\f$
  integer, intent(in) :: iis !< Variable description\f$[units]\f$
  integer, intent(in) :: iif !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  integer :: i !< Variable description\f$[units]\f$
  real :: fracw !< Variable description\f$[units]\f$
  !
  !     * in-line functions:
  !
  !     * computes the saturation vapour pressure over water or ice.
  !
  real :: ttt !<
  real :: uuu !<
  real :: esw !<
  real :: esi !<
  real :: esteff !<
  real :: q !<
  real :: eee !<
  real :: p !<
  real :: est !<
  esw(ttt)    = exp(rw1+rw2/ttt)*ttt**rw3
  esi(ttt)    = exp(ri1+ri2/ttt)*ttt**ri3
  esteff(ttt,uuu) = uuu*esw(ttt) + (1.-uuu)*esi(ttt)
  q(eee,p,eps1,eps2)=eps1*eee/(p-eps2*eee)
  !-----------------------------------------------------------------------
  do i=iis,iif
    !
    !       * ground evaporation efficiency.
    !       * =1. over open water and pack ice, gc=0.,1.
    !
    evapf(i)=1.
    !
    !       * saturation specific humidity at ground temperature and pressure.
    !       * compute the fractional probability of water phase existing
    !       * as a function of temperature (from rockel,
    !       * raschke and weyres, beitr. phys. atmosph., 1991.)
    !
    fracw = 1.
    if (gt(i)<t1s) then
      fracw = 0.0059+0.9941*exp(-0.003102*(t1s-gt(i))**2)
    end if
    est=esteff(gt(i),fracw)
    qg(i)=q(est, .01*ps(i), eps1, eps2)
    !
    !       * update snow amount and possibly snow albedo and snow density
    !       * snow accumulates over pack ice.
    !
    if (gc(i)==1.) then
      dens(i)=0.
      if (snow(i) > 0.) then
        sno(i)=sno(i)+snow(i)*delt
        albs(i)=0.84
      endif
      if(sno(i) > 0.) then
           dens(i)=330.
      end if
    end if
  end do ! loop 100
  !--------------------------------------------------------------------
  return
end subroutine oifprp10
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
