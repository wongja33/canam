  subroutine oiflux11 (qsens, tflux, qevap, evap, qflux, cdh, cdm, slim, &
                       st, su, sv, sq, srh, drag, gc, ugcm, vgcm, vmod,  &
                       th, ta, qa, pressg, gt, qg, zdslm, zdslh, zref,   &
                       ilg, il1, il2, jl, islfd, vdra )
!
! * JUN 06/14 - M.Lazare.   New version for gcm18:
! *                         - Add call to SLDIAG for ISLFD=1. This
! *                           requires passing in ISLFD switch along
! *                           with new input fields {ZDSLM,ZDSLH,ZREF}.
! *                         - ZREF now passed, so no need to pass SGL,SHL,
! *                           and is used in place of previous internal ZLEV.
! *                         - Modification of screen-level calculations
! *                           from Diana.
! *                         - Replace data constants for
! *                           {zero,one,two,ten} by actual values.
! *                         - Correct calculation for SQ, ie same
! *                           bugfix as in CLASST.
! *                         - SICN removed and ice-only calculations
! *                           for RATMOH (RATMOH=1.) with no SICN
! *                           weighting. This is the proper approach
! *                           for fractional water/ice.
! * oct 14/11 - m.lazare.   new version for gcm17:
! *                         - Calls Diana's DRCOEF to be consistent
! *                           with new class_v3.5, thus unused internal
! *                           work arrays zoms,zohs removed.
! * sep 16/11 - m.lazare.   new version for gcm15j:
! *                         - shortened class4 common block consistent
! *                           with new class version.
! * apr 23/10 - m.lazare.   previous version oiflux9 for gcm15i:
! *                         - adds calculation of screen rh through
! *                           call to new subroutine "SCREENRH".
! *                           "SRH" is the diagnostic output passed back out.
! * jan 17/08 - m.lazare.   previous version oiflux8 for gcm15g/h:
! *                         include gustiness effects for windspeed
! *                         that should have been incorporated into
! *                         this routine earlier on (contained in
! *                         "VMODL" passed from class driver).
! * jun 15/06 - m.lazare.   previous version oiflux7 for gcm15f:
! *                         - "ZERO","ONE","TWO","TEN","P9" data
! *                            constants added and used in calls
! *                            to intrinsics.
! *                          - uses internal work arrays instead
! *                            of passing in "WRK" workspace from physics.
! *                          - remove setting of transp to zero -
! *                            this is done through initialization
! *                            already at beginning of class driver.
! *                            since this is the only place where
! *                            transp is used, it is removed from
! *                            the routine alltogether.
! * dec 12/05 - k.vonsalzen. previous version oiflux6 for gcm15d/e:
! *                          - add gustiness field to wind speed
! *                            (passed in from physics).
! * dec 14/04 - m.lazare. previous version oiflux5 for gcm15c:
! *                       calls new drcoef5 version.
! * feb 18/03 - m.lazare. previous version oiflux4 for gcm15b.
! -----------------------------------------------------------------------------

  use phys_parm_defs, only : switch_gas_chem
!
  use hydcon, only : tfrez, rgas, rgasv, grav, sbc, vkc, &
                     sphair, clhmlt, clhvap, vmin, ct
  implicit none
!
! * Input/output arrays.
!
  real, intent(out), dimension(ilg) :: qsens !<
  real, intent(out), dimension(ilg) :: tflux !<
  real, intent(out), dimension(ilg) :: qevap !<
  real, intent(out), dimension(ilg) :: evap !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: qflux !<
  real, intent(in) , dimension(ilg) :: cdh !< Variable description\f$[units]\f$
  real, intent(in) , dimension(ilg) :: cdm !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: slim !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: st !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: su !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: sv !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: sq !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: srh !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: drag !< Variable description\f$[units]\f$
! * For gas-phase chemistry
  real, dimension(ilg,4), intent(out), optional :: vdra !<
!
! * Input arrays.
!
  real, intent(in), dimension(ilg) :: gc !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ugcm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: vgcm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: vmod !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: th !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ta !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: qa !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: pressg !< Variable description
  real, intent(in), dimension(ilg) :: gt !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: qg !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zdslm !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zdslh !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: zref !< Variable description\f$[units]\f$

  integer, intent(in) :: ilg   !< Variable description\f$[units]\f$
  integer, intent(in) :: il1   !< Variable description\f$[units]\f$
  integer, intent(in) :: il2   !< Variable description\f$[units]\f$
  integer, intent(in) :: jl    !< Variable description\f$[units]\f$
  integer, intent(in) :: islfd !< Variable description\f$[units]\f$
!
! * Internal work arrays / variables.
!
  real, dimension(ilg)  :: rib !<
  real, dimension(ilg)  :: gtv !<
  real, dimension(ilg)  :: tav !<
  real, dimension(ilg)  :: cdo !<
  real, dimension(ilg)  :: clhtop !<
  real, dimension(ilg)  :: ratmoh !<
  real, dimension(ilg)  :: va !<
  real, dimension(ilg)  :: zosclm !<
  real, dimension(ilg)  :: zosclh !<
  real, dimension(ilg)  :: crib !<
  real, dimension(ilg)  :: cflux !<
  real, dimension(ilg)  :: fct !<
  real, dimension(ilg)  :: zrsldm !<
  real, dimension(ilg)  :: zrsldh !<
  real, dimension(ilg)  :: frac !<
  real, dimension(ilg)  :: zom !<
  real, dimension(ilg)  :: zoh !<

  integer, dimension(ilg) :: iter !<
  integer :: i

  real :: fm2, quad, cteff, qlwout, ea, padry, rhoair, a, b, wa
  real :: const, tadp, factm, facth, ratiom, ratioh

!-------------------------------------------------------------------------------
! * Determine neutral drag parameters. Assume neutral drag coefficient for heat
! * over sea-ice is 2.5 times that of open water.
!
  do i = il1, il2
    va(i) = max(vmin, vmod(i))
    crib(i) = -grav * zref(i) / (th(i) * (1.+0.61*qa(i)) * (va(i)**2))

    if (gc(i) > -0.5 .and. gc(i) < 0.5) then
      fm2 = (1. + sqrt(ct)*log(zref(i)/10.) / vkc)**2
      cdo(i) = ct / fm2
!     QUAD=(MAX(VA(I)-10. , 0.))**2
      quad = 0.
      ratmoh(i) = 0.001*max(0.9,0.61+0.063*va(i)+0.05*quad) / (cdo(i)*fm2)
      clhtop(i) = clhvap

    elseif (gc(i) > 0.5) then
      cteff = 2.5 * ct
      fm2 = (1. + sqrt(cteff)*log(zref(i)/10.) / vkc)**2
      cdo(i) = cteff / fm2
      ratmoh(i) = 1.
      clhtop(i) = clhvap + clhmlt
    endif

    drag(i) = cdo(i)
    gtv(i) = gt(i) * (1.0+0.61*qg(i))
    tav(i) = ta(i) * (1.0+0.61*qa(i))
    zosclh(i) = exp(-vkc * sqrt(cdo(i)*ratmoh(i)) / cdo(i))
    zosclm(i) = exp(-vkc / sqrt(cdo(i)*ratmoh(i)))
    fct(i) = 1.
    iter(i) = 1
  enddo   ! loop 100

! * Calculate stability-dependent surface drag coefficients and other related
! * quantities. Note that in the above loop fct=1 (ie, the whole grid cell is
! * open water) and iter=1 (ie, always execute the loop once only).
!
  call drcoef (cdm, cdh, rib, cflux, qg, qa, zosclm, zosclh, crib, gtv, &
               tav, va, fct, iter, ilg, il1, il2)
!
! * Remaining calculations.
!
  do i = il1, il2
    qlwout = sbc * gt(i) * gt(i) * gt(i) * gt(i)
    ea = qa(i) * pressg(i) / (0.622+0.378*qa(i))
    padry = pressg(i) - ea
    rhoair = padry / (rgas*ta(i)) + ea/(rgasv*ta(i))
    slim(i) = rhoair * cflux(i)

    if (abs(slim(i)) > 1e10) then
      write(6,*)"oiflux11:  SLIM =", slim(i), "  CFLUX =",cflux(i), &
                         "  RHOAIR =", rhoair
    endif
    qsens(i) = rhoair * sphair * cflux(i) * (gt(i)-ta(i))

    if (ta(i) >= tfrez) then
      a = 17.269
      b = 35.86
    else
      a = 21.874
      b =  7.66
    endif

    wa = qa(i) / (1.0-qa(i))
    const = log(wa*padry/(0.622*611.0))
    tadp = (b*const - a*tfrez) / (const-a)
    evap(i) = rhoair * cflux(i) * (qg(i)-qa(i))
    if (evap(i) < 0.0 .and. gt(i) >= tadp) evap(i) = 0.0
    qevap(i) = clhtop(i) * evap(i)
    tflux(i) = -qsens(i) / (rhoair*sphair)
    qflux(i) = -evap(i) / rhoair
    frac(i)  = 1.
    zom(i)    = zosclm(i) * zref(i)
    zoh(i)    = zosclh(i) * zref(i)
    zrsldm(i) = zref(i) + zom(i)
    zrsldh(i) = zref(i) + zom(i)

    if (switch_gas_chem) vdra(i,1) = 1.0 / cflux(i)
  enddo ! loop 500
!
! * Screen level calculations.
!
  if (islfd == 0) then
    do i = il1, il2
      factm = zdslm(i) + zom(i)
      facth = zdslh(i) + zom(i)
      ratiom = sqrt(cdm(i))*log(factm/zom(i))/vkc
      ratiom = min(ratiom,1.)
      ratioh = sqrt(cdm(i))*log(facth/zoh(i))/vkc
      ratioh = min(ratioh,1.)

      if (rib(i) < 0.) then
        ratioh = ratioh*cdh(i)/cdm(i)
        ratioh = min(ratioh,(facth/zrsldh(i))**(1./3.))
      end if

      st(i) = gt(i) - (min(ratioh,1.))*(gt(i)-ta(i))
      sq(i) = qg(i) - (min(ratioh,1.))*(qg(i)-qa(i))
      su(i) = ratiom*ugcm(i)
      sv(i) = ratiom*vgcm(i)
    end do ! loop 550

  elseif (islfd == 1) then
    call sldiag (su, sv,st,sq, cdm, cdh, ugcm, vgcm, ta, qa, gt, qg, zom, &
                 zoh, frac, zrsldm, zdslm, zdslh, ilg, il1, il2, jl)
  else
    call xit('OIFLUX11',-1)
  end if
!
! * Calculate screen RH.
!
  call screenrh (srh, st, sq, pressg, frac, ilg, il1, il2)
!
  return
end subroutine oiflux11
!> \file
